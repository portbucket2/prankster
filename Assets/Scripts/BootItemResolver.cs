﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootItemResolver : MonoBehaviour
{
    public static bool bootSceneWasLoaded = false;
    public GameObject itemsToKick;
    void Awake()
    {
        if (!itemsToKick) bootSceneWasLoaded = true;
        else if (bootSceneWasLoaded) DestroyImmediate(itemsToKick);
        else
        {
            bootSceneWasLoaded = true;
            itemsToKick.transform.parent = null;
            DontDestroyOnLoad(itemsToKick);
        }
    }

}
