﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PrankLevelBehaviour : MonoBehaviour,IPrankBehaviour
{
    public static PrankLevelBehaviour instance;
    public int currentPhaseIndex;

    //public ScriptableObjective questionObject;
    //public LevelObjective objectives;

    bool startWasCalled;
    public Correctness lastOptionCorrectness = Correctness.WRONG;
    public bool isRightOnLastPhase
    {
        get
        {
            switch (lastOptionCorrectness)
            {
                case Correctness.RIGHT:
                case Correctness.RV:
                    return true;
                default:
                case Correctness.WRONG:
                    return false;
            }
        }
    }
    void IPrankBehaviour.OnLevelStart()
    {
        if (startWasCalled) return;
        startWasCalled = true;
        instance = this;
        AnalyticsAssistant.LogStepStart(LevelManager.SelectedLevelNumber,1);
        StartLevel();

    }
    protected abstract void  StartLevel();

    void IPrankBehaviour.RightOption()
    {
        lastOptionCorrectness = Correctness.RIGHT;
        RightAnswerSelected();
    }
    protected abstract void RightAnswerSelected();

    void IPrankBehaviour.WrongOption()
    {
        lastOptionCorrectness = Correctness.WRONG;
        WrongAnswerSelected();
    }

    protected abstract void WrongAnswerSelected();
    void IPrankBehaviour.RVOption()
    {
        lastOptionCorrectness = Correctness.RV;
        RVAnswerSelected();
    }


    protected virtual void RVAnswerSelected() 
    {
        throw new System.Exception("RV option override not implemented!");
    }


    void IPrankBehaviour.OnPhaseStart(int phaseIndex)
    {
    }
    
    void IPrankBehaviour.OnPhaseEnd(int phaseIndex)
    {
    }

   

    void IPrankBehaviour.ResetPhase(int resetStatusIndex)
    {
        if(resetStatusIndex ==0) AnalyticsAssistant.LogStepStart(LevelManager.SelectedLevelNumber, last_completed_phase );
        ResetPhase(resetStatusIndex);
    }

    protected abstract void ResetPhase(int resetStatusIndex);


    int last_completed_phase = -1;//analytics
    //public void ReportFail_Legacy(IPhaseReset t_PhaseReset, int phaseNumber)
    //{
    //    last_completed_phase = phaseNumber;
    //    AnalyticsAssistant.LogStepFailed(LevelManager.SelectedLevelNumber, phaseNumber);
    //    if (phaseNumber == 1)
    //        UIManager.Instance.ReportEarlyFailed();
    //    else
    //        UIManager.Instance.ReportFailed_1_2_3(t_PhaseReset);
    //}

    public void ForceSuccess()
    {
        lastOptionCorrectness = Correctness.RIGHT;
        ReportPhaseCompletion(1, true);
    }
    public void ReportPhaseCompletion(int phaseNumber,  bool isFinalPhase)
    {
        last_completed_phase = phaseNumber;
        if (isRightOnLastPhase)
        {
            AnalyticsAssistant.LogStepSuccess(LevelManager.SelectedLevelNumber, phaseNumber);
            if (isFinalPhase)
            {
                SuccessUIController.Instance.ReportComplete();
                LevelEndParticle.PlayIfAvailable();
                AnalyticsAssistant.LogLevelCompleted(LevelManager.SelectedLevelNumber);

                LevelManager.Instance. IncreaseGameLevel();
            }
            else AnalyticsAssistant.LogStepStart(LevelManager.SelectedLevelNumber, phaseNumber + 1);
        }
        else
        {
            if (phaseNumber == 1)
                UIManager.Instance.ReportEarlyFailed();
            else
                UIManager.Instance.ReportFailed();
            AnalyticsAssistant.LogStepFailed(LevelManager.SelectedLevelNumber, phaseNumber);
        }
    }

    int IPrankBehaviour.GetLastReportedCompletedPhase()
    {
        return last_completed_phase;
    }
}

public interface IPrankBehaviour
{
    void RightOption();
    void WrongOption();
    void RVOption();
    void OnLevelStart();
    void OnPhaseStart(int phaseIndex);
    void OnPhaseEnd(int phaseIndex);

    void ResetPhase(int resetStatusIndex);


    int GetLastReportedCompletedPhase();
}