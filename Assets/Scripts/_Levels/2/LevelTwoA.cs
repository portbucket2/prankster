﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;

public class LevelTwoA : PrankLevelBehaviour
{
    public int stateCounter;
    public Animator doorAnimator;

    public GameObject plasticObject;
    public Animator plasticAnimator;
    public Animator cylinderAnimator;
    public Vector3 plasticInitPos;
    public Vector3 plasticDestPos;

    public Transform playerDoorTrans;
    public Transform playerToiletTrans;
    public Transform mirrorTrans;
    
    public ParticleSystem peeParticle;
    //public ParticleSystem peeparTwo;
    public bool isStartGlue;
    public GlueVisual glueVisual;

    public Transform playerBrushHandTrans;
    public Vector3 brushInitPos;
    public Transform brushTrans;
    public Transform brushParentTrans;
    
    public Transform tempTrans;
    public ParticleSystem peeParTiclePlayer;
    public ParticleSystem faceColorParticle;
    
    public Transform colorBallParent;
    public List<GameObject> colorBallList;

    [Header("Wrong Objects")] 
    public Animator honkObject;
    public ParticleSystem honkConfetti;
    public ParticleSystem honkEffect;
    public ParticleSystem honkPeeEffect;
    public GlueVisual glueBrush;


    public GameObject mirrorFullObject;
    
    private void Awake()
    {
        plasticInitPos = plasticObject.transform.position;
        brushInitPos = brushTrans.transform.position;
        for (int i = 0; i < colorBallParent.childCount; i++)
        {
            colorBallList.Add(colorBallParent.GetChild(i).gameObject);
        }
    }


    private void Update()
    {
        if (isStartGlue)
        {
            if (Input.GetMouseButton(0))
            {
            }
        }
    }

    public IEnumerator PhaseStart()
    {
        mirrorFullObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        PlayerController.Instance.PlayDoorOpen();
        yield return new WaitForSeconds(1.75f);
        doorAnimator.enabled = true;
        yield return new WaitForSeconds(1f);
        PlayerController.Instance.PlayerMovement(playerDoorTrans.position);
        
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.toiletOriginTrans,CamPosObjectHolder.Instance.toiletTransList);
        yield return new WaitForSeconds(1.5f);
        UIManager.Instance.RequestQuestionPanel( stepIndex: 0);
    }

    public void Done()
    {
        stateCounter++;
        Gameplay.Instance.IncreaseObjectiveCounter();
        switch (stateCounter)
        {
            case 1:
                StartCoroutine(DoneStateOneRoutine());
                break;
            case 2:
                StartCoroutine(DoneStateTwoRoutine());
                break;
                
            default:
                break;
        }
    }

    IEnumerator ColorBallRoutine()
    {
        for (int i = 0; i < colorBallList.Count; i++)
        {
            colorBallList[i].SetActive(true);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator DoneStateTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.mirrorOriginTrans,CamPosObjectHolder.Instance.mirrorTransList);
        yield return new WaitForEndOfFrame();
        brushTrans.SetParent(playerBrushHandTrans);
        brushTrans.transform.localPosition = tempTrans.transform.position;
        brushTrans.transform.localRotation = tempTrans.rotation;
        yield return new WaitForEndOfFrame();
        
        PlayerController.Instance.PlayBrushAngry();
        
        yield return new WaitForSeconds(2f);
        StartCoroutine(ColorBallRoutine());
        //faceColorParticle.Play();
        
      //  AnalyticsAssistant.LogStepSuccess(LevelManager.Instance.GetCurrentLevel(),stateCounter);
       // AnalyticsAssistant.LogLevelCompleted(LevelManager.Instance.GetCurrentLevel());
        
        yield return new WaitForSeconds(3f);

        ReportPhaseCompletion(stateCounter,  true);

       // faceColorParticle.Stop();
    }

    private IEnumerator DoneStateOneRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.toiletPlayerOriginTrans,CamPosObjectHolder.Instance.toiletPlayerTransList);
        yield return new WaitForSeconds(0.25f);
        PlayerController.Instance.PlayerMovementToToilet(playerToiletTrans.position,true);
        yield return new WaitForSeconds(3f);
        peeParTiclePlayer.Play();
        yield return new WaitForSeconds(.75f);
        peeParticle.Play();
        yield return new WaitForSeconds(2f);
        peeParticle.Stop();
        peeParTiclePlayer.Stop();
        doorAnimator.SetTrigger("exit");
        
        ReportPhaseCompletion(stateCounter,false);
        //AnalyticsAssistant.LogStepSuccess(LevelManager.Instance.GetCurrentLevel(),stateCounter);
        
        yield return new WaitForSeconds(3f);
        
        mirrorFullObject.SetActive(true);
        
       // AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.mirrorOriginTrans,CamPosObjectHolder.Instance.mirrorTransList);
        yield return new WaitForSeconds(1);
        PlayerController.Instance.PlayerMoveToMirror(mirrorTrans.position);
        yield return new WaitForSeconds(2);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.brushOriginTrans,CamPosObjectHolder.Instance.brushTransList);
        yield return new WaitForSeconds(0.25f);
        UIManager.Instance.RequestQuestionPanel( stepIndex:1);
    }


    private void StartPhaseTwo()
    {
        isStartGlue = true;
        StartCoroutine(PhaseTwoRoutine());
    }

    private void StartPhaseOne()
    {
        StartCoroutine(PhaseOneRoutine());
    }

    IEnumerator PhaseTwoRoutine()
    {
        yield return new WaitForSeconds(1f);
        while (glueVisual.manualglue(Time.deltaTime * 3))
        {
            yield return new WaitForEndOfFrame();
        }
        //UIManager.Instance.DoneFootGlue(this);
        isStartGlue = false;
        yield return new WaitForSeconds(1f);
        Done();
    }

    private IEnumerator PhaseOneRoutine()
    {
        yield return StartCoroutine(plasticMoveRoutine(plasticDestPos));
    }

    
    IEnumerator plasticMoveRoutine(Vector3 t_Dest)
    {
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = plasticObject.transform.position;
        Vector3 t_DestValue = t_Dest;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            plasticObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                plasticObject.transform.position.y,plasticObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForSeconds(0.5f);
                plasticObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                plasticObject.transform.GetChild(1).GetComponent<Animator>().enabled = true;
                yield return new WaitForSeconds(1f);
                Done();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(plasticMoveRoutine(Vector3.zero));
    }
    


    private IEnumerator WrongStateTwo()
    {
        yield return new WaitForSeconds(.75f);
        while (glueBrush.manualglue(Time.deltaTime * 2))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.mirrorOriginTrans,CamPosObjectHolder.Instance.mirrorTransList);
        yield return new WaitForEndOfFrame();
        brushTrans.SetParent(playerBrushHandTrans);
        brushTrans.transform.localPosition = tempTrans.transform.position;
        brushTrans.transform.localRotation = tempTrans.rotation;
        yield return new WaitForEndOfFrame();
        
        PlayerController.Instance.PlayBrushFailed();
        
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(stateCounter + 1, false);
    }

    private IEnumerator WrongStateOne()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.toiletPlayerOriginTrans,CamPosObjectHolder.Instance.toiletPlayerTransList);
        yield return new WaitForSeconds(0.25f);
        PlayerController.Instance.PlayerMovementToToilet(playerToiletTrans.position,false);
        yield return new WaitForSeconds(3f);
        peeParTiclePlayer.Play();
        yield return new WaitForSeconds(0.1f);
        honkObject.SetTrigger("Appear");
        yield return new WaitForSeconds(0.5f);
        honkObject.SetTrigger("Honk");
        honkConfetti.Play();
       
        yield return new WaitForSeconds(2f);
        peeParTiclePlayer.Stop();
        doorAnimator.SetTrigger("exit"); 
        
        ReportPhaseCompletion(stateCounter + 1, false);
    }



    private IEnumerator ResetPhaseOneRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            PlayerController.Instance.PlayerResetPosition(playerDoorTrans);
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.toiletOriginTrans,
                CamPosObjectHolder.Instance.toiletTransList);
            honkObject.SetTrigger("Disappear");
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel( stepIndex: 0);
        }
    }

    private IEnumerator ResetPhaseTwoRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            glueBrush.ResetGlue();
            brushTrans.SetParent(brushParentTrans);
            brushTrans.position = brushInitPos;
            brushTrans.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosObjectHolder.Instance.brushOriginTrans,
                CamPosObjectHolder.Instance.brushTransList);
            yield return new WaitForSeconds(1f);
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel( stepIndex:1);
        }
    }

    protected override void StartLevel()
    {
        StartCoroutine(PhaseStart());
    }

    protected override void RightAnswerSelected()
    {
        switch (stateCounter)
        {
            case 0:
                StartPhaseOne();
                break;
            case 1:
                StartPhaseTwo();
                break;

            default:
                break;
        }
    }

    protected override void WrongAnswerSelected()
    {
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(WrongStateOne());
                break;
            case 1:
                StartCoroutine(WrongStateTwo());
                break;

            default:
                break;
        }
    }

    protected override void ResetPhase(int resetStatusIndex)
    {

        bool t_IsBefore = resetStatusIndex < 0;

        switch (stateCounter)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(t_IsBefore));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(t_IsBefore));
                break;
            default:
                break;
        }
    }
}
