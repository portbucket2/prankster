﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using SWS;
using UnityEngine;

public class LevelThreeA : PrankLevelBehaviour
{
    public int stateCounter;

    [Header("State One")] 
    public GameObject flourBottleObject;
    public Vector3 flourBottleInitPos;
    public Vector3 flouringBottlePos;
    public bool isStartFlouring;
    public ParticleSystem flourParticle;
    public ParticleSystem flourFumeParticle;
    public GameObject flourAreaDummy;
    public GameObject sauceAreaDummy;
    
    public ParticleSystem flourFromMouthParticle;
    public ParticleSystem flourFromMouthParticleLoop;

    public ParticleSystem sauceFromMouthParticle;
    public ParticleSystem sauceFromMouthParticleLoop;
    
    public Transform burgerHandPos;
    public GameObject burgerOpen;
    public GameObject burgerClosed;
    public Transform burgerClosedParent;

    public bool isStartSaucing;
    public GameObject sauceBottleObject;
    public ParticleSystem sauceParticle;
    
    
    
    [Header("State Two")] 
    public bool isStartPouring;
    public bool isStartSalting;
    
    public GameObject glassObject;
    public Vector3 bottleGlassPos;
    public Transform glassHolder;
    public GameObject pouringBottleObject;
    public GameObject pouringBottleSaltObject;
    
    public ParticleSystem peeperParticle;
    public ParticleSystem saltParticle;
    
    public GameObject greenWaterObject;
    public GameObject redWaterObject;
    public GameObject whiteWaterObject;
    
    public int upCounter;

    public Transform glassHandTransPos;
    public GameObject plateObject;

    public Transform glassHandTransParent;

    public ParticleSystem mouthFireParticle;
    public ParticleSystem earLeftParticle;
    public ParticleSystem earRightParticle;
    public ParticleSystem headFireParticle;

    public splineMove spMove;
    public MoveAnimator moveAnimator;
    public bool isPlayerRunning;
    
    private void Update()
    {
        if (isStartFlouring)
        {
            if (Input.GetMouseButtonDown(0))
            {
                flourParticle.Play();
                flourFumeParticle.Play();
            }
            if (Input.GetMouseButton(0))
            {
               
            }

            if (Input.GetMouseButtonUp(0))
            {
                flourParticle.Stop();
                flourFumeParticle.Stop();
            }
        }

        if (isStartPouring)
        {
            if (Input.GetMouseButtonDown(0))
            {
               peeperParticle.Play();
            }
            if (Input.GetMouseButton(0))
            {
            }

            if (Input.GetMouseButtonUp(0))
            {
                upCounter++;
                peeperParticle.Stop();
                Debug.Log(upCounter);
                if (upCounter >=1)
                {
                    redWaterObject.SetActive(true);
                    greenWaterObject.SetActive(false);
                }
            }
        }
        
        if (isStartSaucing)
        {
            if (Input.GetMouseButtonDown(0))
            {
                sauceParticle.Play();
               
            }
            if (Input.GetMouseButton(0))
            {
               
            }

            if (Input.GetMouseButtonUp(0))
            {
                sauceParticle.Stop();
            }
        }
        
        if (isStartSalting)
        {
            if (Input.GetMouseButtonDown(0))
            {
                saltParticle.Play();
            }
            if (Input.GetMouseButton(0))
            {
            }

            if (Input.GetMouseButtonUp(0))
            {
                upCounter++;
                saltParticle.Stop();
                
                if (upCounter >=1)
                {
                    whiteWaterObject.SetActive(true);
                    greenWaterObject.SetActive(false);
                }
            }
        }
        
    }

    public IEnumerator PhaseStart()
    {
        //AnalyticsAssistant.LogLevelStarted(LevelManager.Instance.GetCurrentLevel());
       // AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        yield return new WaitForEndOfFrame();
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerCamPosTop,CamPosLevelThree.Instance.burgerCamPosList);
        yield return new WaitForSeconds(1f);
        
        UIManager.Instance.RequestQuestionPanel( stepIndex: 0);
    }

    

    public void Done()
    {
        stateCounter++;
        Gameplay.Instance.IncreaseObjectiveCounter();
        switch (stateCounter)
        {
            case 1:
                StartCoroutine(DonePhaseOneRoutine());
                break;
            case 2:
                StartCoroutine(DonePhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    public void WrongDone()
    {
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(WrongDoneStateOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongDoneStateTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    

    #region State One
    private IEnumerator PhaseOneRightRoutine()
    {
        yield return StartCoroutine(FlourBottleMoveRoutine(flouringBottlePos));
        //isStartFlouring = true;
        flourAreaDummy.SetActive(true);
        
        flourParticle.Play();
        flourFumeParticle.Play();
        
        yield return new WaitForSeconds(4f);
        //UIManager.Instance.DoneFootGlue(this);
        Done();
    }
    IEnumerator FlourBottleMoveRoutine(Vector3 t_Dest)
    {
        flourBottleObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = flourBottleObject.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            flourBottleObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                flourBottleObject.transform.position.y,flourBottleObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForSeconds(0.5f);
                flourBottleObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(FlourBottleMoveRoutine(Vector3.zero));
    }

    private IEnumerator DonePhaseOneRoutine()
    {
        flourParticle.Stop();
        yield return StartCoroutine(FlourBottleMoveRoutine(flourBottleInitPos));
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerEatPlayerPos,CamPosLevelThree.Instance.burgerCamPosList);
        yield return new WaitForSeconds(.1f);
        flourAreaDummy.SetActive(false);
        burgerOpen.SetActive(false);
        burgerClosed.SetActive(true);
        yield return new WaitForSeconds(1f);
        burgerClosed.transform.SetParent(burgerHandPos);
        burgerClosed.transform.localPosition = new Vector3(-0.1f,0,0.14f);
        burgerClosed.transform.localRotation = Quaternion.Euler(new Vector3(0,0,-90));
        yield return new WaitForSeconds(.1f);
        PlayerControllerLevelThree.Instance.PlayCoughing();
       // plateObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        burgerClosed.SetActive(false);
        flourFumeParticle.Stop();
        flourFromMouthParticle.Play();
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelThree.Instance.PlayCoughingLoop();
        flourFromMouthParticleLoop.Play();
        
        ReportPhaseCompletion(stateCounter,false);
        //AnalyticsAssistant.LogStepSuccess(LevelManager.Instance.GetCurrentLevel(),stateCounter);
        
        yield return new WaitForSeconds(3f);
       // AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.glassTopCamPos,CamPosLevelThree.Instance.glassTopCapPosList);
        glassObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        UIManager.Instance.RequestQuestionPanel( stepIndex: 1);
    }
    
    
    private IEnumerator WrongStateOneRoutine()
    {
        yield return StartCoroutine(SauceBottleMoveRoutine(flouringBottlePos, sauceBottleObject.transform));
        //isStartSaucing = true;
        sauceAreaDummy.SetActive(true);
        sauceParticle.Play();
        
        yield return new WaitForSeconds(4f);
        //UIManager.Instance.HandColorWrongDone(this);
        WrongDone();
    }

    IEnumerator SauceBottleMoveRoutine(Vector3 t_Dest,Transform t_SelectedTrans)
    {
        t_SelectedTrans.gameObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_SelectedTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_SelectedTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_SelectedTrans.transform.position.y,t_SelectedTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForSeconds(0.5f);
                t_SelectedTrans.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
        StopCoroutine(SauceBottleMoveRoutine(Vector3.zero,null));
    }
    
    private IEnumerator WrongDoneStateOneRoutine()
    {
        sauceParticle.Stop();
        
        yield return StartCoroutine(SauceBottleMoveRoutine(flourBottleInitPos,sauceBottleObject.transform));
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerEatPlayerPos,CamPosLevelThree.Instance.burgerCamPosList);
        yield return new WaitForSeconds(.1f);
        sauceParticle.gameObject.SetActive(false);
        sauceAreaDummy.SetActive(false);
        burgerOpen.SetActive(false);
        burgerClosed.SetActive(true);
        yield return new WaitForSeconds(1f);
        burgerClosed.transform.SetParent(burgerHandPos);
        burgerClosed.transform.localPosition = new Vector3(-0.1f,0,0.14f);
        burgerClosed.transform.localRotation = Quaternion.Euler(new Vector3(0,0,-90));
        yield return new WaitForSeconds(.1f);
       // PlayerControllerLevelThree.Instance.PlayCoughing();
        
       // burgerClosed.SetActive(false);
        PlayerControllerLevelThree.Instance.PlaySauceDrop();
        yield return new WaitForSeconds(1f);
        sauceFromMouthParticle.Play();
        
        //sauceFromMouthParticleLoop.Play();
        //AnalyticsAssistant.LogStepFailed(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(stateCounter + 1, false);
        //UIManager.Instance.ShowLevelFailed(this);
    }
   
    #endregion


    #region State Two
    private IEnumerator PhaseTwoRightRoutine()
    {
        yield return StartCoroutine(PouringBottleMoveRoutine(bottleGlassPos));
       // isStartPouring = true;
        
        peeperParticle.Play();
        yield return new WaitForSeconds(2f);
        redWaterObject.SetActive(true);
        greenWaterObject.SetActive(false);
        yield return new WaitForSeconds(4f);
        //UIManager.Instance.DoneFootGlue(this);
        Done();
    }
    
    IEnumerator PouringBottleMoveRoutine(Vector3 t_Dest)
    {
        pouringBottleObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = pouringBottleObject.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            pouringBottleObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                pouringBottleObject.transform.position.y,pouringBottleObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForSeconds(0.5f);
                pouringBottleObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(FlourBottleMoveRoutine(Vector3.zero));
    }
    private IEnumerator DonePhaseTwoRoutine()
    {
        yield return StartCoroutine(PouringBottleMoveRoutine(flourBottleInitPos));
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerEatPlayerPos,CamPosLevelThree.Instance.burgerCamPosList);
        yield return new WaitForSeconds(.15f);
        glassObject.transform.position = glassHandTransPos.position;
        plateObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        flourFromMouthParticleLoop.Stop();
        PlayerControllerLevelThree.Instance.PlaySipWater();
        
        yield return new WaitForSeconds(0.5f);
        glassObject.transform.SetParent(glassHandTransParent);
        glassObject.transform.localPosition = new Vector3(0.208f,-0.0716f,0.0799f);
        glassObject.transform.localRotation = Quaternion.Euler(new Vector3(-80f,0,60f));
        
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelThree.Instance.PlayGetupChair();
        yield return new WaitForSeconds(0.1f);
        glassObject.SetActive(false);
        
        flourParticle.gameObject.SetActive(false);
        flourFromMouthParticle.gameObject.SetActive(false);
        flourFromMouthParticleLoop.gameObject.SetActive(false);
        
        mouthFireParticle.Play();
        earLeftParticle.Play();
        earRightParticle.Play();
        headFireParticle.Play();
        
        yield return new WaitForSeconds(0.5f);
        spMove.enabled = true;
        moveAnimator.enabled = true;
        PlayerControllerLevelThree.Instance.PlayRun();
        isPlayerRunning = true;
        
        //AnalyticsAssistant.LogStepSuccess(LevelManager.Instance.GetCurrentLevel(),stateCounter);
        //AnalyticsAssistant.LogLevelCompleted(LevelManager.Instance.GetCurrentLevel());
        
        while (isPlayerRunning)
        {
            yield return new WaitForSeconds(1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerEatPlayerPos, CamPosLevelThree.Instance.burgerCamPosList);
            isPlayerRunning = false;
            ReportPhaseCompletion(stateCounter,  true);
            //UIManager.Instance.ShowLevelComplete();
        }
    }
    
    private IEnumerator WrongStateTwoRoutine()
    {
        yield return StartCoroutine(SauceBottleMoveRoutine(bottleGlassPos,pouringBottleSaltObject.transform));
        //isStartSalting = true;
        
        saltParticle.Play();
        yield return new WaitForSeconds(2f);
        whiteWaterObject.SetActive(true);
        greenWaterObject.SetActive(false);
        yield return new WaitForSeconds(4f);
        //UIManager.Instance.HandColorWrongDone(this);
        WrongDone();
    }
    
    private IEnumerator WrongDoneStateTwoRoutine()
    {
        yield return StartCoroutine(SauceBottleMoveRoutine(flourBottleInitPos,pouringBottleSaltObject.transform));
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerEatPlayerPos,CamPosLevelThree.Instance.burgerCamPosList);
        yield return new WaitForSeconds(.15f);
        glassObject.transform.position = glassHandTransPos.position;
        plateObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        flourFromMouthParticleLoop.Stop();
        PlayerControllerLevelThree.Instance.PlaySipWater();
        
        yield return new WaitForSeconds(0.5f);
        glassObject.transform.SetParent(glassHandTransParent);
        glassObject.transform.localPosition = new Vector3(0.208f,-0.0716f,0.0799f);
        glassObject.transform.localRotation = Quaternion.Euler(new Vector3(-80f,0,60f));
        
        yield return new WaitForSeconds(1f);
        //PlayerControllerLevelThree.Instance.PlayGetupChair();
        yield return new WaitForSeconds(0.1f);
        glassObject.SetActive(true);
        //flourFromMouthParticle.Play();
        
        yield return new WaitForSeconds(1f);
        //AnalyticsAssistant.LogStepFailed(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);

        ReportPhaseCompletion(stateCounter + 1, false);
        //UIManager.Instance.ShowLevelFailed(this);
    }
    #endregion


    private IEnumerator ResetPhaseOneRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.burgerCamPosTop,
                CamPosLevelThree.Instance.burgerCamPosList);
            PlayerControllerLevelThree.Instance.PlayIdle();
            burgerOpen.SetActive(true);
            burgerClosed.transform.SetParent(burgerClosedParent);
            burgerClosed.transform.position = Vector3.zero;
            burgerClosed.transform.rotation = Quaternion.identity;
            yield return new WaitForSeconds(1f);
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    private IEnumerator ResetPhaseTwoRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelThree.Instance.glassTopCamPos,
                CamPosLevelThree.Instance.glassTopCapPosList);
            PlayerControllerLevelThree.Instance.PlayCoughingLoop();
            flourFromMouthParticleLoop.Play();
            glassObject.transform.SetParent(glassHolder);
            glassObject.transform.localPosition = Vector3.zero;
            glassObject.transform.rotation = Quaternion.identity;
            glassObject.SetActive(true);
            greenWaterObject.SetActive(true);
            whiteWaterObject.SetActive(false);
            yield return new WaitForSeconds(1f);
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel( stepIndex:1);
        }
    }

    protected override void StartLevel()
    {
        StartCoroutine(PhaseStart());
    }

    protected override void RightAnswerSelected()
    {
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(PhaseOneRightRoutine());
                break;
            case 1:
                StartCoroutine(PhaseTwoRightRoutine());
                break;

            default:
                break;
        }
    }

    protected override void WrongAnswerSelected()
    {
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(WrongStateOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongStateTwoRoutine());
                break;

            default:
                break;
        }
    }

    protected override void ResetPhase(int resetStatusIndex)
    {
        bool t_IsBefore = resetStatusIndex < 0;
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(t_IsBefore));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(t_IsBefore));
                break;
            default:
                break;
        }
    }
}
