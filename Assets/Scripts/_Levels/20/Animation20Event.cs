﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation20Event : MonoBehaviour
{
    public Level20Gameplay gameplay;
    
    public void FinishCommonEvent()
    {
        gameplay.isCommonFuuDone = true;
    }
}
