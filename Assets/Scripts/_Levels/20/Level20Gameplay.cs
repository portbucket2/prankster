﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering;

public class Level20Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    [Header("only for this level")] 

    public Animator propsAnimator;
    public bool isCommonFuuDone;

    public SkinMatController skinMatController;

    [Header("Special Option")] public Animator specialPropsAnimator;
    
    private void Start()
    {
    }

    public void PreProcess()
    {
        //playerMAT.SetTexture("_TexOverlay",new Texture2D(0,0));
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }

        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        //Debug.LogError(t_Key);
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
           // Debug.LogError(t_Key + " under");
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    public void PlayPropsAnimator(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            propsAnimator.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        PreProcess();
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
            }
        );
        yield return new WaitForSeconds(1.5f);
        PlayCharacterAnimation(animationKeyList[6]);
        PlayPropsAnimator(animationKeyList[6]);
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[8]);
        PlayPropsAnimator(animationKeyList[8]);
        WaitUntil t_WaitUntill = new WaitUntil(delegate
        {
            if (isCommonFuuDone)
                return true;
            
            return false;
        });
        yield return t_WaitUntill;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            }
        );
    }


    protected override void RightAnswerSelected()
    {
        //Debug.Log(currentPhaseIndex);
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForEndOfFrame();
//        Debug.LogError("REight ONE");
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, delegate
            {
            }));
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
               // PlayCharacterAnimation(animationKeyList[6]);
                //PlayPropsAnimator(animationKeyList[6]);
            }
        );
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[1]);
        PlayPropsAnimator(animationKeyList[1]);
        
        yield return new WaitForSeconds(.5f);
        prankSequenceList[0].propsList[1].propsParticle[2].Play();
        prankSequenceList[0].propsList[1].propsParticle[1].Play();
        prankSequenceList[0].propsList[1].propObject.SetActive(false);
        prankSequenceList[0].propsList[3].propObject.SetActive(false);
        yield return new WaitForSeconds(0.75f);
        skinMatController.SetMat(1);
        skinMatController.SetMatValue(1,"_MaskStrength",0.8f);
        
        
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        yield return new WaitForSeconds(3f);
        
        currentPhaseIndex++;
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList);
        
        yield return new WaitForEndOfFrame();
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(0.5f);
        PlayCharacterAnimation(animationKeyList[7]);
        float t_Value = 1;
        while (t_Value > 0)
        {
            t_Value -= Time.deltaTime;
            skinMatController.SetMatValue(1,"_MaskStrength",t_Value);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(3f);
        PlayCharacterAnimation(animationKeyList[5]);
        PlayPropsAnimator(animationKeyList[5]);
        prankSequenceList[1].propsList[1].propObject.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        
        skinMatController.SetMat(2);
        skinMatController.SetMatValue(2,"_MaskStrength",1);
       
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForEndOfFrame();
        //Debug.LogError("Wrong ONE");
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos,
            prankSequenceList[0].propsList[2].propObject, delegate
            {
                prankSequenceList[0].propsList[2].propsParticle[0].Play();
            }));
        
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                //PlayCharacterAnimation(animationKeyList[6]);
                //PlayPropsAnimator(animationKeyList[6]);
            }
        );
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[2]);
        PlayPropsAnimator(animationKeyList[2]);
        yield return new WaitForSeconds(.5f);
        prankSequenceList[0].propsList[2].propsParticle[1].Play();
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1);
        PlayCharacterAnimation(animationKeyList[7]);
        float t_Value = 1;
        while (t_Value > 0)
        {
            t_Value -= Time.deltaTime;
            skinMatController.SetMatValue(1,"_MaskStrength",t_Value);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[4]);
        PlayPropsAnimator(animationKeyList[4]);
        prankSequenceList[1].propsList[2].propObject.SetActive(false);
        prankSequenceList[1].propsList[1].propObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }


    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        //Debug.LogError(t_PhaseIndex + " Fomr UI MANAGER");
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].initialPos,
                prankSequenceList[0].propsList[2].propObject, delegate
                {
                }));
            
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].initialPos,
                prankSequenceList[0].propsList[0].propObject, delegate { }));
            
            yield return new WaitForSeconds(1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                delegate
                {
                }
            );
            PlayCharacterAnimation(animationKeyList[8]);
            PlayPropsAnimator(animationKeyList[8]);
            prankSequenceList[0].propsList[2].propsParticle[1].Stop();
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();

            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                    PlayCharacterAnimation(animationKeyList[0]);
                    PlayPropsAnimator(animationKeyList[0]);
                    skinMatController.SetMatValue(2,"_MaskStrength",0);
                    //playerMAT.SetFloat("_ColorMaskStrength",0.75f);
                    prankSequenceList[1].propsList[1].propObject.SetActive(true);
                    prankSequenceList[1].propsList[2].propObject.SetActive(true);
                }
            );
           
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex:1);
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,t_TargetTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }


    protected override void RVAnswerSelected()
    {
        StartCoroutine(RVSpecialRoutine());
    }

    private IEnumerator RVSpecialRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[2].camPosList[0].cameraOriginTrans,
            prankSequenceList[2].camPosList[0].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(0.5f);
        PlayCharacterAnimation(animationKeyList[7]);
        float t_Value = 1;
        while (t_Value > 0)
        {
            t_Value -= Time.deltaTime;
            skinMatController.SetMatValue(1,"_MaskStrength",t_Value);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(3f);
        PlayCharacterAnimation(animationKeyList[11]);
        propsAnimator.SetTrigger("open_box");
        specialPropsAnimator.SetTrigger(animationKeyList[11]);
        yield return new WaitForSeconds(3.7f);
        propsAnimator.SetTrigger("vanish_props");
        
        yield return new WaitForSeconds(5f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }
}
