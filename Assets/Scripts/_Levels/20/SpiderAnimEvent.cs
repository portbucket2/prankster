﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderAnimEvent : MonoBehaviour
{
    public Transform handTrans;
    public Transform spiTrans;
    public bool isHand;


    private void Update()
    {
        if (isHand)
        {
            spiTrans.localPosition = new Vector3(0,0,0);
            spiTrans.localRotation = Quaternion.Euler(new Vector3(0,0,0));
        }
    }

    public void AnimEvent()
    {
        spiTrans.SetParent(handTrans);
        isHand = true;
        GetComponent<Animator>().enabled = false;
    }
}
