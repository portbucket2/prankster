﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class Level13Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;
    
    [Header("level specific")]
    public Animator propsAnimator;

    public Transform bottleTrans;
    public GameObject propsOneObject;
    public GameObject propsTwoObject;
    public Animator propsTwoAnimator;
    
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    public void PlayPropsAnimator(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            propsAnimator.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }

    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                    delegate
                    {
                        UIManager.Instance.RequestQuestionPanel(stepIndex:0);
                    }
                );
    }
    
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[0].propObject.SetActive(true);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, delegate
            {
                prankSequenceList[0].propsList[1].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            }));
        yield return new WaitForSeconds(.5f);
        while ( prankSequenceList[0].propsList[0].propObject.GetComponent<GlueVisual>().manualglue(Time.deltaTime*2f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].initialPos,
            prankSequenceList[0].propsList[1].propObject, delegate
            {
            }));
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[1]);
                PlayPropsAnimator(animationKeyList[1]);
            });
        
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        yield return new WaitForSeconds(7f);
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex:1);
            });
        
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos,
            prankSequenceList[1].propsList[1].propObject, delegate
            {
            }));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos,
            prankSequenceList[1].propsList[0].propObject, delegate
            {
               // prankSequenceList[1].propsList[0].propObject.transform.SetParent(bottleTrans);
                //prankSequenceList[1].propsList[0].propObject.transform.localPosition = new Vector3(0,-0.1341f,0);
            }));
        yield return new WaitForSeconds(1f);
        propsOneObject.SetActive(false);
        propsTwoObject.SetActive(true);
        //propsTwoAnimator.SetTrigger(animationKeyList[5]);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[5]);
                propsTwoAnimator.SetTrigger(animationKeyList[5]);
            });
        yield return new WaitForSeconds(0.1f);
        prankSequenceList[1].propsList[3].propObject.transform.GetChild(1).gameObject.SetActive(false);
        prankSequenceList[1].propsList[3].propObject.transform.GetChild(0).gameObject.SetActive(true);
        prankSequenceList[1].propsList[0].propObject.SetActive(false);
        yield return new WaitForSeconds(4f);
        PlayCharacterAnimation(animationKeyList[3]);
        propsTwoAnimator.SetTrigger(animationKeyList[3]);
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelComplete();
    }
    
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForSeconds(1f);
        
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos,
            prankSequenceList[0].propsList[2].propObject, delegate
            {
                prankSequenceList[0].propsList[2].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                prankSequenceList[0].propsList[2].propsParticle[0].Play();
                prankSequenceList[0].propsList[2].propsParticle[1].Play();
            }));
        
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].initialPos,
            prankSequenceList[0].propsList[2].propObject, delegate
            {
                prankSequenceList[0].propsList[2].propsParticle[0].Stop();
                prankSequenceList[0].propsList[2].propsParticle[1].Play();
            }));
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[2]);
                PlayPropsAnimator(animationKeyList[2]);
            });
        
        yield return new WaitForSeconds(5f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].finalPos,
            prankSequenceList[1].propsList[2].propObject, delegate
            {
                prankSequenceList[1].propsList[2].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                prankSequenceList[1].propsList[2].propsParticle[0].Play();
            }));
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].initialPos,
            prankSequenceList[1].propsList[2].propObject, delegate
            {
            }));
        yield return new WaitForSeconds(1f);
        prankSequenceList[1].propsList[2].propsParticle[0].Stop();
        propsOneObject.SetActive(false);
        propsTwoObject.SetActive(true);
        //propsTwoAnimator.SetTrigger(animationKeyList[5]);
       
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[5]);
                propsTwoAnimator.SetTrigger(animationKeyList[5]);
            });
        yield return new WaitForSeconds(.1f);
        prankSequenceList[1].propsList[2].propsParticle[0].gameObject.SetActive(false);
        prankSequenceList[1].propsList[3].propObject.transform.GetChild(1).gameObject.SetActive(true);
        prankSequenceList[1].propsList[3].propObject.transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(4f);
        PlayCharacterAnimation(animationKeyList[4]);
        propsTwoAnimator.SetTrigger(animationKeyList[4]);
        yield return new WaitForSeconds(2.5f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }
    
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        yield return new WaitForEndOfFrame();
        if (t_PhaseIndex < 0)
        {
            PlayCharacterAnimation(animationKeyList[0]);
            PlayPropsAnimator(animationKeyList[0]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                delegate
                {
                }
            );
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].initialPos,
                prankSequenceList[0].propsList[2].propObject, delegate
                {
                }));
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel(stepIndex:0);
        }
        
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        yield return new WaitForEndOfFrame();
        if (t_PhaseIndex < 0)
        {
            PlayCharacterAnimation(animationKeyList[1]);
            
            prankSequenceList[1].propsList[3].propObject.transform.GetChild(1).gameObject.SetActive(false);
            prankSequenceList[1].propsList[3].propObject.transform.GetChild(0).gameObject.SetActive(false);
            propsOneObject.SetActive(true);
            PlayPropsAnimator(animationKeyList[1]);
            propsTwoObject.transform.position = new Vector3(-.561f,-.04f,-1.57f);
            propsTwoObject.SetActive(false);
            prankSequenceList[1].propsList[2].propsParticle[0].gameObject.SetActive(true);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                }
            );
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel(stepIndex:1);
        }
    }
    
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,t_TargetTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }
}
