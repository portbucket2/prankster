﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;

using UnityEngine;

public class LevelOneA : PrankLevelBehaviour
{
    
    public int stateCounter;

    public GameObject colorBottleObject;
    public GameObject pasteBottleObject;
    public bool isBottleMoveToColor;
    public ParticleSystem colorParticleSystem;
    public ParticleSystem pasteParticleSystem;
    
    
    public Animator clockAnimator;
    
    public Vector3 bottleInitPos;
    public Vector3 bottleColorPos;

    public GlueVisual glueVisual;
    public bool isStartGlue;
    public bool isStartBall;

    public ParticleSystem sleepParticle;
    public ParticleSystem ringingParticle;
    public ParticleSystem hitParticle;

    public ParticleSystem leftEarParticle;
    public ParticleSystem rightEarParticle;
    
    public Transform colorBallObjectHolder;
    public List<GameObject> redBallGameobject;
   
    public GameObject glueBottleObject;
    public GameObject waterBottleObjectHolder;
    

    [Header("Wrong Object")]
    public Transform pasteColorTrans;
    public List<GameObject> pasteColorGameobject;
    public GameObject footWaterObject;
    public GameObject waterDrippingObject;

    [Header("Extra Level")]
    public bool isExtraLevel;
    
    private void Awake()
    {
        for (int i = 0; i < pasteColorTrans.childCount; i++)
        {
            pasteColorGameobject.Add(pasteColorTrans.GetChild(i).gameObject);
        }
        for (int i = 0; i < colorBallObjectHolder.childCount; i++)
        {
            redBallGameobject.Add(colorBallObjectHolder.GetChild(i).gameObject);
        }
    }

    private void Update()
    {
        if (isBottleMoveToColor)
        {
            // if (Input.GetMouseButtonDown(0))
            // {
            //     colorParticleSystem.Play();
            //     
            //     if (!isStartBall)
            //     {
            //         StartCoroutine(ActivateColorBall());
            //         isStartBall = true;
            //     }
            // }
            //
            // if (Input.GetMouseButtonUp(0))
            // {
            //     colorParticleSystem.Stop();
            // }
        }

        if (isStartGlue)
        {
            // if (Input.GetMouseButton(0))
            // {
            //     if (glueVisual.manualglue(Time.deltaTime * 3))
            //     {
            //     }
            //     else
            //     {
            //         UIManager.Instance.DoneFootGlue(this);
            //         isStartGlue = false;
            //     }
            // }
        }
    }


    public IEnumerator PhaseStart()
    {
        //AnalyticsAssistant.LogLevelStarted(LevelManager.Instance.GetCurrentLevel());
       // AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.handOriginTrans,CamPosLevelOne.Instance.handShowTransList);
        yield return new WaitForSeconds(1.5f);
        UIManager.Instance.RequestQuestionPanel( stepIndex: 0);
    }
    
    public void StartPhaseOne()
    {
        StartCoroutine(PhaseOneRoutine());
    }

    public IEnumerator PhaseOneRoutine()
    {
        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(BottleMoveRoutine(bottleColorPos));
        yield return new WaitForSeconds(.6f);
        colorParticleSystem.Play();
        yield return new WaitForSeconds(.25f);
        if (!isStartBall)
        {
            StartCoroutine(ActivateColorBall(true));
            isStartBall = true;
        }
        
        yield return new WaitForSeconds(4f);
       
        //UIManager.Instance.HandColorDone(this);
        Done();
    }
    IEnumerator PasteBottleMoveRoutine(Vector3 t_Dest)
    {
        pasteBottleObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = pasteBottleObject.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            pasteBottleObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                pasteBottleObject.transform.position.y,pasteBottleObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                pasteBottleObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                //isBottleMoveToColor = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(PasteBottleMoveRoutine(Vector3.zero));
    }
    IEnumerator BottleMoveRoutine(Vector3 t_Dest)
    {
        colorBottleObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = colorBottleObject.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            colorBottleObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                colorBottleObject.transform.position.y,colorBottleObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                colorBottleObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                isBottleMoveToColor = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(BottleMoveRoutine(Vector3.zero));
    }
    IEnumerator GlueBottleMoveRoutine(Vector3 t_Dest)
    {
        glueBottleObject.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = glueBottleObject.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            glueBottleObject.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                glueBottleObject.transform.position.y,glueBottleObject.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                glueBottleObject.GetComponent<MouseDrag>().isGame = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(BottleMoveRoutine(Vector3.zero));
    }
    IEnumerator WaterBottleMoveRoutine(Vector3 t_Dest)
    {
        waterBottleObjectHolder.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = waterBottleObjectHolder.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            waterBottleObjectHolder.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                waterBottleObjectHolder.transform.position.y,waterBottleObjectHolder.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                waterBottleObjectHolder.GetComponent<MouseDrag>().isGame = true;
                //colorBottleObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                //isPasteMoveToColor = true;
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(WaterBottleMoveRoutine(Vector3.zero));
    }


    public void Done()
    {
        stateCounter++;
        Gameplay.Instance.IncreaseObjectiveCounter();
        switch (stateCounter)
        {
            case 1:
                StartCoroutine(DoneStateOneRoutine());
                break;
            case 2:
                StartCoroutine(DoneStateTwoRoutine());
                break;
                
            default:
                break;
        }
    }

    private IEnumerator DoneStateTwoRoutine()
    {
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.fallDownOriginTrans,CamPosLevelOne.Instance.fallDownTransList);
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlaySitToStand();
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.temp,CamPosLevelOne.Instance.tempList);
        yield return new WaitForSeconds(.75f);
        hitParticle.Play();
        yield return new WaitForSeconds(.75f);
        //leftEarParticle.Play();
        //rightEarParticle.Play();
        yield return new WaitForSeconds(2.5f);
        
        ReportPhaseCompletion(stateCounter,true);
    }

    IEnumerator DoneStateOneRoutine()
    {
        colorParticleSystem.Stop();
        yield return StartCoroutine(BottleMoveRoutine(bottleInitPos));

        yield return new WaitForSeconds(0.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.clockOriginTrans,
            CamPosLevelOne.Instance.clockTransList);
        clockAnimator.enabled = true;
        ringingParticle.Play();

        yield return new WaitForSeconds(1.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.wakeUpOriginTrans,
            CamPosLevelOne.Instance.wakeUpTransList);
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlayWakeUp();
        sleepParticle.Stop();
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlayFaceColor();
        yield return new WaitForSeconds(0.75f);
        //yield return StartCoroutine(ActivateFaceColorObject());
        PlayerControllerLevelOne.Instance.ChangePlayerFaceColor();
        StartCoroutine(ActivateColorBall(false));

        yield return new WaitForSeconds(.5f);
        PlayerControllerLevelOne.Instance.PlayColorAngry();
        yield return new WaitForSeconds(1f);
        leftEarParticle.Play();
        rightEarParticle.Play();

        ringingParticle.Stop();
        yield return new WaitForSeconds(2f);

        ReportPhaseCompletion(stateCounter,  false);

        if (isExtraLevel)
        {
            //Extra level works here
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.watchTrans,
                CamPosLevelOne.Instance.watchTransList, delegate
                {
                    UIManager.Instance.RequestQuestionPanel( stepIndex: 2);
                    leftEarParticle.Stop();
                    rightEarParticle.Stop();
                });
            
            
        }
        else
        {
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.footOriginTrans,
                CamPosLevelOne.Instance.footTransList);
            UIManager.Instance.RequestQuestionPanel( stepIndex: 1);
            leftEarParticle.Stop();
            rightEarParticle.Stop();
        }
    }


    public void StartPhaseTwo()
    {
        isStartGlue = true;
        isBottleMoveToColor = false;
        StartCoroutine(PhaseTwoRoutine());
    }

    IEnumerator PhaseTwoRoutine()
    {
        yield return new WaitForSeconds(1f);
        while (glueVisual.manualglue(Time.deltaTime * 3))
        {
            yield return new WaitForEndOfFrame();
        }
       
        isStartGlue = false;
        yield return new WaitForSeconds(1f);
        Done();
    }

    public IEnumerator ActivateColorBall(bool t_Status)
    {
        for (int i = 0; i < redBallGameobject.Count; i++)
        {
            redBallGameobject[i].SetActive(t_Status);
            yield return new WaitForSeconds(0.25f);
        }
    }


    IEnumerator WrongStateOne()
    {
        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(PasteBottleMoveRoutine(bottleColorPos));
        yield return new WaitForSeconds(.6f);
        pasteParticleSystem.Play();
        yield return new WaitForSeconds(.25f);
        
        StartCoroutine(ActivateFacePaste(true));
        yield return new WaitForSeconds(2.5f);
        pasteParticleSystem.Stop();
        yield return StartCoroutine(PasteBottleMoveRoutine(bottleInitPos));
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.wakeUpOriginTrans,CamPosLevelOne.Instance.wakeUpTransList);
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlayWakeUp();
        sleepParticle.Stop();
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlayFaceColor();
        yield return new WaitForSeconds(0.75f);
      
        PlayerControllerLevelOne.Instance.ChangePlayerFacePaste();
        StartCoroutine(ActivateFacePaste(false));
        
        yield return new WaitForSeconds(1.5f);

        // AnalyticsAssistant.LogStepFailed(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        ReportPhaseCompletion(stateCounter + 1, false);
    }
    
    public IEnumerator ActivateFacePaste(bool t_Status)
    {
        for (int i = 0; i < pasteColorGameobject.Count; i++)
        {
            pasteColorGameobject[i].SetActive(t_Status);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator WrongStateTwo()
    {
        yield return new WaitForEndOfFrame();
        footWaterObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.fallDownOriginTrans,CamPosLevelOne.Instance.fallDownTransList);
        yield return new WaitForSeconds(1f);
        PlayerControllerLevelOne.Instance.PlayWaterFailed();
        yield return new WaitForSeconds(1.5f);
        waterDrippingObject.SetActive(true);
        
        yield return new WaitForSeconds(2f);

        //AnalyticsAssistant.LogStepFailed(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        ReportPhaseCompletion(stateCounter + 1, false);
    }

    private IEnumerator ResetPhaseOneRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            PlayerControllerLevelOne.Instance.ChangePlayerOriginalMAT();
            PlayerControllerLevelOne.Instance.PlaySleepIdle();
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.handOriginTrans,
                CamPosLevelOne.Instance.handShowTransList);
        }
        else
        {
            //AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(), stateCounter + 1);
            yield return new WaitForSeconds(.5f);
            UIManager.Instance.RequestQuestionPanel( stepIndex: 0);
        }
    }

    private IEnumerator ResetPhaseTwoRoutine(bool t_IsBefore)
    {
        if (t_IsBefore)
        {
            PlayerControllerLevelOne.Instance.PlayWakeUp();
            footWaterObject.SetActive(false);
            waterDrippingObject.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            CameraMovementController.Instance.FocusCameraWithOrigin(CamPosLevelOne.Instance.footOriginTrans,
                CamPosLevelOne.Instance.footTransList);
        }
        else
        {
            //AnalyticsAssistant.LogStepStart(LevelManager.Instance.GetCurrentLevel(), stateCounter + 1);
            UIManager.Instance.RequestQuestionPanel(stepIndex:1);
        }
    }


    protected override void StartLevel()
    {
        StartCoroutine(PhaseStart());
    }

    protected override void RightAnswerSelected()
    {
        switch (stateCounter)
        {
            case 0:
                StartPhaseOne();
                break;
            case 1:
                StartPhaseTwo();
                break;

            default:
                break;
        }
    }

    protected override void WrongAnswerSelected()
    {
        // UIManager.Instance.ShowLevelFailed();
        //AnalyticsAssistant.LogStepFailed(LevelManager.Instance.GetCurrentLevel(),stateCounter+1);
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(WrongStateOne());
                break;
            case 1:
                StartCoroutine(WrongStateTwo());
                break;

            default:
                break;
        }
    }

    protected override void ResetPhase(int resetStatusIndex)
    {
        bool t_IsBefore = resetStatusIndex < 0;
        switch (stateCounter)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(t_IsBefore));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(t_IsBefore));
                break;
            default:
                break;
        }
    }
}

