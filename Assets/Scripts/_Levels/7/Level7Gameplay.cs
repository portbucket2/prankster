﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Level7Gameplay : PrankLevelBehaviour
{
        public Animator playerAnimatorReference;
        public List<string> animationKeyList;
        public Dictionary<string, int> animationHashedDictionary;
    
        public List<PrankSequence> prankSequenceList;

        [Header("Level Specific")] 
        public GlueVisual glueVisual;
        //public Material playerMAT;
        //public Texture peeTEX;
        //public Texture underwearTEX;
        public SkinMatController skinMatController;

        [Header("Special")] public Animator specialProps;

        private void Start()
        {
        }

        public void PreProcess()
        {
            animationHashedDictionary = new Dictionary<string, int>();
            for (int i = 0; i < animationKeyList.Count; i++)
            {
                animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
            }
            for (int i = 0; i < prankSequenceList.Count; i++)
            {
                for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
                {
                    prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                    prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
                }
            }
        }
    
        public void PlayCharacterAnimation(string t_Key)
        {
            if (animationHashedDictionary.ContainsKey(t_Key))
            {
                playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
            }
        }
        
        protected override void StartLevel()
        {
            PreProcess();
            StartCoroutine(StartLevelRoutine());
        }
        private IEnumerator StartLevelRoutine()
        {
            yield return new WaitForSeconds(0.75f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                delegate
                {
                    UIManager.Instance.RequestQuestionPanel(stepIndex:0);
                });
           
        }
        
    
        protected override void RightAnswerSelected()
        {
            switch (currentPhaseIndex)
            {
                case 0:
                    StartCoroutine(RightPhaseOneRoutine());
                    break;
                case 1:
                    StartCoroutine(RightPhaseTwoRoutine());
                    break;
                
                default:
                    break;
            }
        }
        private IEnumerator RightPhaseOneRoutine()
        {
            yield return new WaitForSeconds(1f);
            while (glueVisual.manualglue(Time.deltaTime * 2))
            {
                yield return new WaitForEndOfFrame();
            }
            
            yield return new WaitForSeconds(1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    PlayCharacterAnimation(animationKeyList[1]);
                });
            yield return new WaitForSeconds(3f);
            prankSequenceList[0].propsList[0].propsParticle[0].Play();
            //playerMAT.SetTexture("_MainTexture",underwearTEX);
            skinMatController.SetMat(1);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList);
            //_TexOverlay
            yield return new WaitForSeconds(4f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList);
            
            prankSequenceList[0].propsList[0].propObject.SetActive(false);
            PlayCharacterAnimation(animationKeyList[3]);
            ReportPhaseCompletion(currentPhaseIndex+1,false);
            
            yield return new WaitForSeconds(3f);

            currentPhaseIndex++;
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                    UIManager.Instance.RequestQuestionPanel(stepIndex:1);
                });
        }
    
        private IEnumerator RightPhaseTwoRoutine()
        {
            //To do, set all the things here
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos, prankSequenceList[1].propsList[0].propObject,
                delegate
                {
                    prankSequenceList[1].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
                    prankSequenceList[1].propsList[0].propsParticle[0].Play();
                }));
           // playerMAT.SetTexture("_TexOverlay",peeTEX);
            skinMatController.SetMat(2);
            float t_Val = 0;
            
            while (t_Val<1)
            {
                t_Val += Time.deltaTime;
                //playerMAT.SetFloat("_ColorMaskStrength",t_Val);
                skinMatController.SetMatValue(2,"_MaskStrength",t_Val);
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(1f);
            prankSequenceList[1].propsList[0].propsParticle[0].Stop();
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].initialPos, prankSequenceList[1].propsList[0].propObject,
                delegate
                {
                }));
            yield return new WaitForSeconds(.1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
                delegate
                {
                    PlayCharacterAnimation(animationKeyList[5]);
                });
           
           
            
            yield return new WaitForSeconds(4f);
            ReportPhaseCompletion(currentPhaseIndex+1,true);
            //UIManager.Instance.ShowLevelComplete();
        }
    
        protected override void WrongAnswerSelected()
        {
            switch (currentPhaseIndex)
            {
                case 0:
                    StartCoroutine(WrongPhaseOneRoutine());
                    break;
                case 1:
                    StartCoroutine(WrongPhaseTwoRoutine());
                    break;
                
                default:
                    break;
            }
        }
        IEnumerator WrongPhaseOneRoutine()
        {
            yield return new WaitForSeconds(1f);
            yield return StartCoroutine(ActivateThumstacks(true));
            yield return new WaitForSeconds(1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    PlayCharacterAnimation(animationKeyList[2]);
                });
            yield return new WaitForSeconds(1.75f);
            prankSequenceList[0].propsList[1].propsParticle[0].Play();
            yield return new WaitForSeconds(2f);
            ReportPhaseCompletion(currentPhaseIndex+1,false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }
    
        IEnumerator WrongPhaseTwoRoutine()
        {
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
                delegate
                {
                    
                });
            yield return new WaitForSeconds(1f);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos,
                prankSequenceList[1].propsList[1].propObject, delegate
                {
                    prankSequenceList[1].propsList[1].propsParticle[0].Play();
                    PlayCharacterAnimation(animationKeyList[6]);
                }));
            yield return new WaitForSeconds(1f);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].initialPos,prankSequenceList[1].propsList[1].propObject));
            
            yield return new WaitForSeconds(2f);
            ReportPhaseCompletion(currentPhaseIndex+1,true);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }
    
      
        IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
        {
            if (t_PhaseIndex < 0)
            {
                //reset things
                yield return StartCoroutine(ActivateThumstacks(false));
                yield return new WaitForEndOfFrame();
                CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                    delegate
                    {
                    }
                );
                PlayCharacterAnimation(animationKeyList[0]);
            }
            else
            {
                //after video
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            }
        }
    
        IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
        {
            if (t_PhaseIndex < 0)
            {
                //reset things
                yield return XAxisMoveRoutine(prankSequenceList[1].propsList[1].initialPos,
                    prankSequenceList[1].propsList[1].propObject, delegate { });
                yield return new WaitForEndOfFrame();
                CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                    delegate
                    {
                        
                    }
                );
                PlayCharacterAnimation(animationKeyList[4]);
            }
            else
            {
                //after video
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            }
        }
    
    
        protected override void ResetPhase(int phaseIndex)
        {
            switch (currentPhaseIndex)
            {
                case 0:
                    StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                    break;
                case 1:
                    StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                    break;
            
                default:
                    break;
            }
        }
        public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action = null)
        {
            t_TargetTrans.SetActive(true);
            float t_Progression = 0f;
            float t_Duration = .5f;
            float t_EndTIme = Time.time + t_Duration;
            Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
            Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
             
     
            WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
     
            //Platform Movement
            while (true)
            {
                t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
    
                t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                    t_TargetTrans.transform.position.y,t_TargetTrans.transform.position.z);
     
                if (t_Progression >= 1f)
                {
                    if (t_Action != null)
                    {
                        t_Action.Invoke();
                    }

                    break;
                }
     
                yield return t_CycleDelay;
            }
          
            StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
        }

        private IEnumerator ActivateThumstacks(bool t_IsActive)
        {
            for (int i = 0; i < prankSequenceList[0].propsList[1].propObject.transform.childCount; i++)
            {
                prankSequenceList[0].propsList[1].propObject.transform.GetChild(i).gameObject.SetActive(t_IsActive);
                yield return new WaitForEndOfFrame();
            }
        }


        protected override void RVAnswerSelected()
        {
            StartCoroutine(RVSpecialRoutine());

        }

        private IEnumerator RVSpecialRoutine()
        {
            yield return new WaitForSeconds(0.5f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[2].camPosList[0].cameraOriginTrans,prankSequenceList[2].camPosList[0].cameraTransList,
                delegate
                {
                });
            yield return new WaitForSeconds(0.5f);
            
            StartCoroutine(XAxisMoveRoutine(prankSequenceList[2].propsList[0].finalPos,
                prankSequenceList[2].propsList[0].propObject,
                delegate
                {
                }));
            
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[2].propsList[1].finalPos,
                prankSequenceList[2].propsList[1].propObject,
                delegate
                {
                    prankSequenceList[2].propsList[0].propObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetTrigger(animationKeyList[7]);
                    prankSequenceList[2].propsList[1].propObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetTrigger(animationKeyList[7]);
                    playerAnimatorReference.SetTrigger(animationKeyList[7]);
                    specialProps.SetTrigger(animationKeyList[7]);
                }));
            
            yield return new WaitForSeconds(7f);
            ReportPhaseCompletion(currentPhaseIndex+1,true);
            
        }
}
