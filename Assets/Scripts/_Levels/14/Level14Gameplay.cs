﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Level14Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;
    
    public SkinMatController skinMatController;
    
    

    private void Start()
    {
    }

    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[9]);
        prankSequenceList[0].propsList[0].propObject.GetComponent<Animator>().SetTrigger("temp");
        yield return new WaitForSeconds(.75f);
        PlayCharacterAnimation(animationKeyList[11]);
        prankSequenceList[0].propsList[0].propObject.GetComponent<Animator>().SetTrigger("2nd");
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }


    protected override void RightAnswerSelected()
    {
     
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForSeconds(.75f);
        prankSequenceList[0].propsList[0].propObject.SetActive(false);
        prankSequenceList[0].propsList[1].propObject.SetActive(true);
        PlayCharacterAnimation(animationKeyList[10]);
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        prankSequenceList[0].propsList[1].propObject.SetActive(false);
        yield return new WaitForSeconds(.1f);
        //playerMAT.SetTexture("_TexOverlay",bombMATMASK);
        skinMatController.SetMat(1);
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[2]);
        
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        yield return new WaitForSeconds(3f);
        prankSequenceList[0].propsList[2].propObject.SetActive(false);
        
        
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans,prankSequenceList[1].camPosList[2].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos, prankSequenceList[1].propsList[0].propObject,
            delegate
            {
                prankSequenceList[1].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            }));
        
        yield return new WaitForSeconds(2.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].initialPos, prankSequenceList[1].propsList[0].propObject,
            delegate
            {
                prankSequenceList[1].propsList[0].propObject.SetActive(false);
            }));
        PlayCharacterAnimation(animationKeyList[4]);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[6]);
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[7]);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        
        StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos, prankSequenceList[1].propsList[1].propObject,
            delegate
            {
            }));
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[3].cameraOriginTrans,prankSequenceList[1].camPosList[3].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(1f);
        
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForSeconds(.75f);
        prankSequenceList[0].propsList[0].propObject.SetActive(false);
        prankSequenceList[0].propsList[3].propObject.SetActive(true);
        PlayCharacterAnimation(animationKeyList[10]);
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[3].propsParticle[0].Play();
        prankSequenceList[0].propsList[3].propsParticle[1].Play();
        prankSequenceList[0].propsList[3].propObject.SetActive(false);
        PlayCharacterAnimation(animationKeyList[3]);
        skinMatController.SetMat(2);
        
        yield return new WaitForSeconds(1f);
        
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(currentPhaseIndex + 1, false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].finalPos, prankSequenceList[1].propsList[2].propObject,
            delegate
            {
                prankSequenceList[1].propsList[2].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            }));
        float t_Angle = 0;
        while (t_Angle>-180)
        {
            //Debug.Log("ROtation going ");
            t_Angle -= Time.deltaTime * 80;
            prankSequenceList[1].propsList[1].propObject.transform.localRotation = Quaternion.Euler(new Vector3(0,0,t_Angle));
            yield return new WaitForEndOfFrame();
        }
        
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].initialPos, prankSequenceList[1].propsList[2].propObject,
            delegate
            {
                prankSequenceList[1].propsList[0].propObject.SetActive(false);
            }));
        
        
        PlayCharacterAnimation(animationKeyList[4]);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[6]);
        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[8]);
        prankSequenceList[1].propsList[2].propsParticle[0].Play();
        
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[4].cameraOriginTrans,prankSequenceList[1].camPosList[4].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator ResetPhaseOneRoutine(int phaseIndex)
    {
        if (phaseIndex < 0)
        {
            PlayCharacterAnimation(animationKeyList[11]);
            prankSequenceList[0].propsList[0].propObject.SetActive(true);
            prankSequenceList[0].propsList[0].propObject.GetComponent<Animator>().SetTrigger("2nd");
        }
        else
        {
            yield return new WaitForSeconds(1f);
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int phaseIndex)
    {
        if (phaseIndex < 0)
        {
            prankSequenceList[1].propsList[1].propObject.transform.localRotation = Quaternion.Euler(new Vector3(0,0,0));
            PlayCharacterAnimation(animationKeyList[3]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans,prankSequenceList[1].camPosList[2].cameraTransList,
                delegate
                {
                });
        }
        else
        {
            yield return new WaitForSeconds(1f);
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        }
       
    }


    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,t_TargetTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }
    public IEnumerator YAxisMoveRoutine(float t_ExecutionTime,Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = t_ExecutionTime;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(t_TargetTrans.transform.position.x,
                Mathf.Lerp(t_CurrentFillValue.y,t_DestValue.y,t_Progression),
                t_TargetTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(YAxisMoveRoutine(0,Vector3.zero,null,null));
    }
    public IEnumerator ZAxisMoveRoutine(float t_ExecutionTime,Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = t_ExecutionTime;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(t_TargetTrans.transform.position.x,t_TargetTrans.transform.position.y,
                Mathf.Lerp(t_CurrentFillValue.z,t_DestValue.z,t_Progression)
                );
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(ZAxisMoveRoutine(0,Vector3.zero,null,null));
    }


    protected override void RVAnswerSelected()
    {
        StartCoroutine(RVRoutine());
    }

    private IEnumerator RVRoutine()
    {
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos, prankSequenceList[1].propsList[0].propObject,
            delegate
            {
                prankSequenceList[1].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = true;
            }));
        
        yield return new WaitForSeconds(2.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].initialPos, prankSequenceList[1].propsList[0].propObject,
            delegate
            {
                prankSequenceList[1].propsList[0].propObject.SetActive(false);
            }));
        PlayCharacterAnimation(animationKeyList[4]);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
            });
        
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[12]);
        yield return new WaitForSeconds(5f);
        StartCoroutine(YAxisMoveRoutine(0.15f,prankSequenceList[2].propsList[0].finalPos, prankSequenceList[2].propsList[0].propObject,
            delegate
            {
            }));
        prankSequenceList[2].propsList[0].propsParticle[0].Play();
        
        
        yield return new WaitForEndOfFrame();
        
        yield return new WaitForSeconds(4f);
        
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }
}
