﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif
//[CreateAssetMenu(fileName = "LevelGroup", menuName = "LevelManagement/LevelGroup", order = 1)]
//public class LevelGroup : ScriptableObject, ILevelDataContainer
//{
//    public List<LevelData> levels = new List<LevelData>();

//    void ILevelDataContainer.RemoveID(int id)
//    {
//        for (int i = levels.Count - 1; i >= 0; i--)
//        {
//            if (levels[i].id == id)
//            {
//                levels.RemoveAt(i);
//                return;
//            }
//        }
//    }
//    void ILevelDataContainer.AddElement(int id)
//    {
//        for (int i = levels.Count - 1; i >= 0; i--)
//        {
//            if (levels[i].id == id)
//            {
//                LevelData ld = new LevelData(true, id + 1);
//                levels.Insert(i + 1, ld);
//                return;
//            }
//        }
//    }
//    public void Sort(bool id)
//    {

//        List<LevelData> ldnew = new List<LevelData>();

//        while (levels.Count > 0)
//        {
//            int minIndex = -1;
//            for (int i = 0; i < levels.Count; i++)
//            {
//                if (!id)
//                {
//                    if (minIndex < 0 || levels[i].order < levels[minIndex].order)
//                    {
//                        minIndex = i;
//                    }
//                }
//                else
//                {
//                    if (minIndex < 0 || levels[i].id < levels[minIndex].id)
//                    {
//                        minIndex = i;
//                    }
//                }

//            }
//            ldnew.Add(levels[minIndex]);
//            levels.RemoveAt(minIndex);
//        }
//        levels.AddRange(ldnew);
//    }

//    public void SetAll(bool isChecked)
//    {
//        foreach (var item in levels)
//        {
//            item.enabled = isChecked;
//        }
//    }
//}

//#if UNITY_EDITOR

//[CustomEditor(typeof(LevelGroup))]
//public class LevelGroupEditor : Editor
//{
//    public void OnEnable()
//    {
//        LevelGroup lg = (LevelGroup)target;
//        LevelData.currentFocus = lg;
//    }

//    public override void OnInspectorGUI()
//    {
//        LevelGroup lg = (LevelGroup)target;
//        serializedObject.Update();
//        EditorGUILayout.PropertyField(serializedObject.FindProperty("sync"));
//        Show(serializedObject.FindProperty("levels"), lg);
//        serializedObject.ApplyModifiedProperties();
//    }

//    public static void Show(SerializedProperty list, LevelGroup lvlgrp)
//    {
//        EditorGUILayout.Space();
//        EditorGUI.indentLevel += 1;

//        EditorGUILayout.BeginHorizontal();
//        if (GUILayout.Button("Sort by Order"))
//        {
//            lvlgrp.Sort(false);
//        }
//        if (GUILayout.Button("Sort by ID"))
//        {
//            lvlgrp.Sort(true);
//        }
//        if (GUILayout.Button("Check All"))
//        {
//            lvlgrp.SetAll(true);
//        }
//        if (GUILayout.Button("Uncheck All"))
//        {
//            lvlgrp.SetAll(false);
//        }

//        EditorGUILayout.EndHorizontal();

//        if (GUILayout.Button("(+)Add"))
//        {
//            lvlgrp.levels.Insert(0, new LevelData(true, 1));
//        }

//        List<int> duplicateCheckList = new List<int>();
//        for (int i = 0; i < list.arraySize; i++)
//        {
//            LevelData ld = lvlgrp.levels[i];
//            GUILayout.BeginVertical("Box");
//            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
//            SceneAsset scAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(BuildSceneInfo.IDtoPath(ld.id));
//            ld.scene = scAsset;
//            EditorGUI.indentLevel -= 1;
//            if (!scAsset) EditorGUILayout.HelpBox(string.Format("Level Not Found: Assets/Scenes/Level{0}", ld.id), MessageType.Error);
//            else
//            {
//                if (duplicateCheckList.Contains(ld.id))
//                {
//                    EditorGUILayout.HelpBox(string.Format("Duplicate Entry Detected: {0}", ld.id), MessageType.Warning);
//                }
//                else
//                {
//                    duplicateCheckList.Add(ld.id);
//                }
//            }
//            EditorGUI.indentLevel += 1;
//            EditorGUILayout.EndVertical();

//        }

//        EditorGUI.indentLevel -= 1;
//        EditorGUILayout.Space();
//    }
//}
//#endif