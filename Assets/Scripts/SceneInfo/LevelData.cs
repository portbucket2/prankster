﻿using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class LevelData
{
    public static ILevelDataContainer currentFocus;
    public bool enabled;
    public bool isBonus;

    public string id;
    //public int idd;

    public string sceneName
    {
        get 
        {
            return string.Format("Level{0}",id);
        }
    }


    public HardData<bool> _bonusLockHD;
    public void UnlockBonusLevel()
    {
        if (_bonusLockHD == null)
        {
            _bonusLockHD = new HardData<bool>(string.Format("BONUS_LOCKED_{0}", id), false);
        }
        _bonusLockHD.value = false;
    }
    public bool isBonusLocked {
        get
        {
            if (!isBonus) return false;
            if (_bonusLockHD==null)
            {
                _bonusLockHD = new HardData<bool>(string.Format("BONUS_LOCKED_{0}",id), true);
            }
            return _bonusLockHD.value;
        }
    }


#if UNITY_EDITOR
    public SceneAsset scene;
#endif
    public LevelData() { }
    public LevelData(bool enabled, string id)
    {
        this.enabled = enabled;
        this.id = id;
    }
}

public interface ILevelDataContainer
{
    void RemoveID(string id);
    void AddElement(string id);

    int FindSerial(string id);
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(LevelData))]
public class LevelDataEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label.text = "";
        string id = property.FindPropertyRelative("id").stringValue;
        //int analyticsID = property.FindPropertyRelative("analyticsID").intValue;
        //int displayID = property.FindPropertyRelative("displayID").intValue;
        bool isBon = property.FindPropertyRelative("isBonus").boolValue;
        string title;

        float initialOffset = 0;
        if (isBon) initialOffset = 25;

        int serial = LevelData.currentFocus.FindSerial(id);
        if (serial >= 0)
        {
            if (!isBon)
                title = string.Format("Level {0}", serial);
            else
                title = string.Format("Bonus {0}", serial);
        }
        else
            title = string.Format("Serial NOT found");


        EditorGUIUtility.labelWidth = initialOffset;

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float btnW = 20;
        float spc = 2;
        float elementHeight = position.height;

        float offset = initialOffset;
        var enabledRect = new Rect(position.x + offset, position.y, 20, elementHeight);
        offset += (20 + spc);
        var titleRect = new Rect(position.x + offset, position.y, 120-initialOffset, elementHeight);
        offset += (120 -initialOffset+ spc);
        var isBonusRect = new Rect(position.x + offset, position.y, 20, elementHeight);
        offset += (20 + spc);
        var idRect = new Rect(position.x + offset, position.y, 40, elementHeight);
        offset += (40 + spc);

        var sceneRect = new Rect(position.x + offset, position.y,  position.width -offset- btnW*2-spc*2, elementHeight);

        var removeRect = new Rect(position.x + position.width-btnW*2 -spc, position.y, btnW, elementHeight);

        var addRect = new Rect(position.x + position.width - btnW, position.y, btnW, elementHeight);

        EditorGUI.PropertyField(enabledRect, property.FindPropertyRelative("enabled"), GUIContent.none);
        EditorGUI.LabelField(titleRect,title);
        EditorGUI.PropertyField(isBonusRect, property.FindPropertyRelative("isBonus"), GUIContent.none);
        EditorGUI.PropertyField(idRect, property.FindPropertyRelative("id"), GUIContent.none);
        EditorGUI.PropertyField(sceneRect, property.FindPropertyRelative("scene"), GUIContent.none);

        if (LevelData.currentFocus != null)
        {
            if (GUI.Button(removeRect, "-"))
            {
                LevelData.currentFocus.RemoveID(id);
            }
            if (GUI.Button(addRect, "+"))
            {
                LevelData.currentFocus.AddElement(id);
            }
        }
        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    //{
    //    return base.GetPropertyHeight(property, label) * 2 + 10;
    //}

}
#endif