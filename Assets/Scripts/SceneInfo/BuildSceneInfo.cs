﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FRIA;
using Portbliss;
using System.Linq;
using System.Collections;
using UnityEngine.XR;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class BuildSceneInfo : MonoBehaviour,ILevelDataContainer
{
    public const int NON_LEVEL_COUNT = 2;
    public static BuildSceneInfo Instance { get { return _instance; } }
    private static BuildSceneInfo _instance;
    public SequenceMode sequenceMode = SequenceMode.CSV;
    //public int forceCSVChoice = -1;
    public List<TextAsset> csvList;
    public bool showData = true;




    #region Data Management
    public void LoadFromCSV()
    {

        int choice;
        if (Application.isPlaying)
        {
            switch (sequenceMode)
            {
                default:
                case SequenceMode.CSV://if csv then choose which one
                    int abChoice = 1;// ABManager.GetValueInt(ABtype.bonus_characters); ;
                    switch (abChoice)
                    {
                        default:
                            choice = 0;
                            break;
                        case 0:
                        case 1:
                            choice = abChoice;
                            break;
                    }
                    break;
                case SequenceMode.LOCAL:// if local trim and return;
                    for (int i = levels.Count - 1; i >= 0; i--)
                    {
                        if (!levels[i].enabled) levels.RemoveAt(i);
                    }
                    Debug.Log("CSV choice not set, Trimming inactive scenes from list");
                    return;
            }
        }
        else
        {
            switch (sequenceMode)
            {
                default:
                case SequenceMode.CSV:
                    choice = csvList.Count - 1;
                    break;
                case SequenceMode.LOCAL:
                    return;
            }
        }


        //
        Debug.LogFormat("Loading CSV {0}",choice);
        levels = DataFromCSV(choice);
        if (Application.isPlaying)
        {
            for (int i = levels.Count - 1; i >= 0; i--)
            {
                if (!levels[i].enabled) levels.RemoveAt(i);
            }
        }
#if UNITY_EDITOR
        if (!Application.isPlaying) SaveToBuildSettings(true);
        
        IntegrityCheckForAllCSV();
#endif
    }
    List<LevelData> DataFromCSV(int choice)
    {
        TextAsset csvReference = csvList[choice];
        if (csvReference == null)
        {
            Debug.LogError("csv not assigned");
            return null;
        }
        List<LevelData> csvLevels = new List<LevelData>();
        
        CSVRow[] rows = CSVReader.ReadCSVAsset(csvReference, '|');

        int columbIndex_isActive = 0;
        int columbIndex_isBonus = 2;
#if UNITY_IOS
            columbIndex_isActive = 1;
#endif
        for (int i = 0; i < rows.Length; i++)
        {
            CSVRow row = rows[i];
            bool isSceneActive = !((string.Compare(row.fields[columbIndex_isActive], "x") == 0) || (string.Compare(row.fields[columbIndex_isActive], "X") == 0));
            LevelData ld = new LevelData(isSceneActive, row.fields[3]);
            ld.isBonus = ((string.Compare(row.fields[columbIndex_isBonus], "x") == 0) || (string.Compare(row.fields[columbIndex_isBonus], "X") == 0));
           
            csvLevels.Add(ld);
        }
        return csvLevels;
    }

#if UNITY_EDITOR
    //public TextAsset csvReference;
    public CSVDownloadBox csvBox;
    public void DownloadCSV()
    {
        int N = csvBox.dataList.Count;
        int completed = 0;
        csvList = new List<TextAsset>();
        for (int i = 0; i < N; i++)
        {
            csvList.Add(null);
            int index = i;
            csvBox.Download(index, (TextAsset textAsset)=> 
            {
                csvList[index] = textAsset;
                completed++;
                if (completed == N)
                {
                    sequenceMode = SequenceMode.CSV;
                    LoadFromCSV();
                }
            });
        }
        
    }
    
    public void SaveToBuildSettings(bool fromCSV)
    {
        EditorBuildSettingsScene[] original = EditorBuildSettings.scenes;
        var newSettings = new EditorBuildSettingsScene[levels.Count + 2];
        int activeCount = 0;
        for (int i = 0; i < newSettings.Length; i++)
        {
            if (i < NON_LEVEL_COUNT)
            {

                newSettings[i] = original[i];
            }
            else
            {
                LevelData level = levels[i - NON_LEVEL_COUNT];
                bool active = level.enabled || fromCSV;
                newSettings[i] = new EditorBuildSettingsScene(IDtoPath(level.id), active);
                if (!fromCSV && level.enabled)
                {
                    activeCount++;
                    //level.displayID = level.id;
                    //level.analyticsID = activeCount;
                }
            }
        }
        EditorBuildSettings.scenes = newSettings;
    }
    public void IntegrityCheckForAllCSV()
    {
        List<EditorBuildSettingsScene> sclist = new List<EditorBuildSettingsScene>(); 
        sclist.AddRange(EditorBuildSettings.scenes);
        for (int i = 0; i < csvList.Count; i++)
        {
            List<LevelData> ldlist = DataFromCSV(i); 
            //Debug.LogFormat("Checking CSV {0} ========", i );
            for (int ldi = 0; ldi < ldlist.Count; ldi++)
            {
                string path = IDtoPath(ldlist[ldi].id);
                EditorBuildSettingsScene scr = sclist.Find((EditorBuildSettingsScene sc) => { return sc.path == path; });
                if (scr == null)
                {

                    Debug.LogErrorFormat(string.Format("CSV {1}: Level {0} not found in build settings!", ldlist[ldi].id, i));
                }
                //else Debug.LogFormat("{0}. Found elements with matching path {1}",ldi+1, scr.path);
            }
        }
        Debug.LogFormat("Integrety Check Complete");
    }

    //public void Sort()//bool id)
    //{

    //    List<LevelData> ldnew = new List<LevelData>();

    //    while (levels.Count > 0)
    //    {
    //        int minIndex = -1;
    //        for (int i = 0; i < levels.Count; i++)
    //        {
    //            //if (!id)
    //            //{
    //            //    if (minIndex < 0 || levels[i].analyticsID < levels[minIndex].analyticsID)
    //            //    {
    //            //        minIndex = i;
    //            //    }
    //            //}
    //            //else
    //            {
    //                if (minIndex < 0 || levels[i].idd < levels[minIndex].idd)
    //                {
    //                    minIndex = i;
    //                }
    //            }

    //        }
    //        ldnew.Add(levels[minIndex]);
    //        levels.RemoveAt(minIndex);
    //    }
    //    levels.AddRange(ldnew);
    //}
    public void SetAll(bool isChecked)
    {
        foreach (var item in levels)
        {
            item.enabled = isChecked;
        }
    }
#endif
    void ILevelDataContainer.RemoveID(string id)
    {
        for (int i = levels.Count - 1; i >= 0; i--)
        {
            if (levels[i].id == id)
            {
                levels.RemoveAt(i);
                return;
            }
        }
    }
    void ILevelDataContainer.AddElement(string id)
    {
        for (int i = levels.Count - 1; i >= 0; i--)
        {
            if (levels[i].id == id)
            {
                LevelData ld = new LevelData(true, id);
                levels.Insert(i + 1, ld);
                return;
            }
        }
    }
    int ILevelDataContainer.FindSerial(string id)
    {
        int found = -1;
        for (int i = 0; i < levelCount; i++)
        {
            if (levels[i].id==id) return i + 1;
        }
        return found;
    }
    public static int PathToID(string path)
    {
        string numberPart = path.Split('.')[0].Split(new string[] { "Level" }, System.StringSplitOptions.None)[1];
        return int.Parse(numberPart);
    }
    public static string IDtoPath(string number)
    {
        return string.Format("Assets/Scenes/Level{0}.unity", number);
    }
#endregion




    public List<LevelData> levels = new List<LevelData>();
    public int levelCount { get { return levels.Count; } }

    public static string BRIDGE_SCENE_NAME;
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
            BRIDGE_SCENE_NAME = System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(1));

            if (sequenceMode != SequenceMode.CSV)
            {
                LoadFromCSV();

            }
            else
            {
#if UNITY_EDITOR
                IntegrityCheckForAllCSV();
#endif
                awaitingStart = true;
            }
        }
        else
        {
            DestroyImmediate(this);
        }
    }
    public static bool awaitingStart;
    IEnumerator Start()
    {
        yield return null;
        yield return null;
        yield return null;
        if (awaitingStart)
        {
            while ((!ABManager.isDataFetchComplete) || TestABController.isAB_trialValueSetupPending)
            {
                yield return null;
            }
            LoadFromCSV();
#if UNITY_EDITOR
            IntegrityCheckForAllCSV();
#endif
            awaitingStart = false;
        }
    }


    public static LevelData GetLevelData(int selectionNumber)
    {
        BuildSceneInfo bsi = Instance;
        if (!bsi) bsi = FindObjectOfType<BuildSceneInfo>();
        int modedNumber = ((selectionNumber - 1) % (bsi.levels.Count));
        //Debug.LogFormat("{0}%{1}={2}", selectionNumber, bsi.levels.Count, modedNumber);
        return bsi.levels[modedNumber];
    }
}
public enum SequenceMode
{
    CSV,
    LOCAL,
}