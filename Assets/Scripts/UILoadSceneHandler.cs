﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Portbliss.Ad;

public class UILoadSceneHandler : MonoBehaviour {
	
	[Header("Config	:	LoadScene (With Progress Bar")]
	public Image progressionBar;

	[Space(5f)]
	[Header("Config	:	LoadScene (Without Progress Bar")]
	[Range(0f,5f)]
	public float defaultDelay;
	public UnityEvent OnStartEvent;

	void Start()
	{
		//OnStartEvent.Invoke();
		//string t_SceneName = LevelManager.Instance.GetNameOfSceneToLoad();
		LoadSceneWithProgressBar();
	}

	#region Public Callback	:	LoadScene (Without Progress Bar)

	public void LoadThisScene()
	{
		string m_CurrentScene = SceneManager.GetActiveScene ().name;
		SceneManager.LoadScene (m_CurrentScene, LoadSceneMode.Single);
	}

	public void LoadScene(string m_SceneName){

		SceneManager.LoadScene (m_SceneName, LoadSceneMode.Single);
	}

	public void LoadSceneWithDelay(string m_SceneName){
		
		StartCoroutine(Delay(m_SceneName,defaultDelay));
	}

	public void LoadSceneWithDelay(string m_SceneName,float m_Delay){
		StartCoroutine(Delay(m_SceneName,m_Delay));
	}

	private IEnumerator Delay(string m_SceneName,float m_Delay){

		yield return new WaitForSeconds(m_Delay);
		LoadScene(m_SceneName);
	}

	#endregion

	//--------------------------------------------------
	#region Public Callback	:	LoadScene (With Progression Bar)
	
	public void LoadSceneWithProgressBar(){

		StartCoroutine(ControllerForLoadSceneWithProgressbar());
	}

	#endregion

	//--------------------------------------------------
	#region Configuretion	:	LoadScene (With Progression Bar)

	private const float LOAD_READY_PERCENTAGE = 0.9f;
	public static bool loadComplete;
	private IEnumerator ControllerForLoadSceneWithProgressbar()
	{

		yield return new WaitForSeconds(defaultDelay);

		float timePast = 0;

		while (!IAPManager.isIAPReady)
		{
			timePast += Time.deltaTime;
			if (timePast > 5) break;
			yield return null;
		}


		while (!AdController.instance//
			|| !AdController.gdpr_done//
			|| !ABManager.isDataFetchComplete //
			|| TestABController.isAB_trialValueSetupPending//
			|| BuildSceneInfo.awaitingStart)
		{
			yield return null;
		}


		LifetimeEvents.Report_GameStart();
		WaitForSeconds t_CycleDelay = new WaitForSeconds(0.1f);

		AsyncOperation t_AsyncOperationForLoadScene = LevelManager.FirstLoadAsync();
		t_AsyncOperationForLoadScene.allowSceneActivation = false;

		float t_LoadingProgression;

		while(!t_AsyncOperationForLoadScene.isDone)
		{

			t_LoadingProgression = t_AsyncOperationForLoadScene.progress;
			
			if(progressionBar != null)
			{
				progressionBar.fillAmount = t_LoadingProgression / LOAD_READY_PERCENTAGE;
			}

			if(t_LoadingProgression >= LOAD_READY_PERCENTAGE){
				break;
			}

			yield return t_CycleDelay;
		}

		//AnalyticsAssistant.LogLevelStarted(LevelManager.Instance.GetEffectiveLevelNumber());
		loadComplete = true;
        t_AsyncOperationForLoadScene.allowSceneActivation = true;
		//StopCoroutine(ControllerForLoadSceneWithProgressbar(""));
	}

	#endregion
}
