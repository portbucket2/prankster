﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using com.adjust.sdk;
public class LifetimeEvents 
{

    //public static LifetimeEvents manager;
    private static bool initialized = false;
    public static void Init()
    {
        if (!initialized)
        {
            counter_firstGameStart = new HardData<DateTime>("LTE_FIRST_START_DATETIME", DateTime.Now);
            //Debug.Log(counter_firstGameStart.value.Ticks + " a0 ==================================");
            //Debug.Log(DateTime.Now.Ticks + " a1 ==================================");
            counter_retDays = new HardData<int>("LTE_RETENTION_DAYS", 0);
            counter_interstitialShown = new HardData<int>("LTE_INTERSTITIAL_SHOWN_COUNT", 0);
            counter_rvClick = new HardData<int>("LTE_RV_CLICKED_COUNT", 0);
            counter_rvWatched = new HardData<int>("LTE_RV_WATCHED_COUNT", 0);
            counter_lvlCleared = new HardData<int>("LTE_LVL_REACHED_MAX", 0);
            initialized = true;
        }
    }



    static HardData<DateTime> counter_firstGameStart;
    static HardData<int> counter_retDays;
    static HardData<int> counter_interstitialShown;
    static HardData<int> counter_rvClick;
    static HardData<int> counter_rvWatched;
    static HardData<int> counter_lvlCleared;
    public static void Report_GameStart()//retention tracker
    {
        Init();
        DateTime diff = new DateTime(DateTime.Now.Ticks - counter_firstGameStart.value.Ticks);
        long currentDayCount = Application.isEditor ? diff.Minute : diff.Day;
        while (currentDayCount > counter_retDays.value)
        {
            counter_retDays.value++;
            long currentValue = counter_retDays.value;
            switch (currentValue)
            {
                default:
                    break;
                case 3:
                case 7:
                    AnalyticsAssistant.LogLifeTimeEvent(string.Format("d{0}_retained", currentValue));
                    break;
            }
        }

    }
    public static void Report_InterstialShown()//after each interstitial
    {
        Init();
        counter_interstitialShown.value++;
        int currentValue = counter_interstitialShown.value;
        switch (currentValue)
        {
            default:
                break;
            case 10:
            case 25:
                AnalyticsAssistant.LogLifeTimeEvent(string.Format("watch_inter_{0}", currentValue));
                break;
        }
    }
    public static void Report_RVClicked()//after each RV click
    {
        Init();
        counter_rvClick.value++;
        int currentValue = counter_rvClick.value;
        switch (currentValue)
        {
            default:
                break;
            case 5:
            case 15:
                AnalyticsAssistant.LogLifeTimeEvent(string.Format("click_reward_{0}", currentValue));
                break;
        }
    }
    public static void Report_RVWatched()//after each RV watch
    {
        Init();
        counter_rvWatched.value++;
        int currentValue = counter_rvWatched.value;
        switch (currentValue)
        {
            default:
                break;
            case 5:
            case 15:
                AnalyticsAssistant.LogLifeTimeEvent(string.Format("watch_rewarded_{0}", currentValue));
                break;
        }
    }
    public static void Report_NewLevelReached(int levelReached)//after each level Completion
    {
        int levelCleared = levelReached-1;
        while (levelCleared > counter_lvlCleared.value)
        {
            counter_lvlCleared.value++;
            int currentValue = counter_lvlCleared.value;
            switch (currentValue)
            {
                default:
                    if (currentValue == BuildSceneInfo.Instance.levelCount)
                    {
                        AnalyticsAssistant.LogLifeTimeEvent(string.Format("completed_all_levels"));
                    }
                    break;
                case 10:
                case 20:
                    AnalyticsAssistant.LogLifeTimeEvent(string.Format("level_achieved_{0}", currentValue));
                    break;
            }
        }
    }
}
