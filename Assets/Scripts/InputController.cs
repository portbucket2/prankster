﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class InputController : MonoBehaviour
{
    

    // Min length to detect the Swipe
    public float MinSwipeLength = 5f;

    private Vector2 _firstPressPos;
    private Vector2 _secondPressPos;
    private Vector2 _currentSwipe;

    private Vector2 _firstClickPos;
    private Vector2 _secondClickPos;

    public static Swipe SwipeDirection;
    public float ReturnForce = 10f;

    public Transform selectedTrans;
    public Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        DetectSwipe();
    }

    public void DetectSwipe()
    {
//        if (Input.touches.Length > 0)
//        {
//            Touch t = Input.GetTouch(0);
//
//            if (t.phase == TouchPhase.Began)
//            {
//                _firstPressPos = new Vector2(t.position.x, t.position.y);
//                
//                Ray t_Ray = mainCamera.ScreenPointToRay(_firstClickPos);
//
//                RaycastHit t_RaycastHit;
//                int t_LayerMask = ~LayerMask.GetMask("PuzzleEnd");
//
//                if (Physics.Raycast(t_Ray, out t_RaycastHit,100,t_LayerMask))
//                {
//                    if (t_RaycastHit.transform.CompareTag("Cube"))
//                    {
//                        selectedTrans = t_RaycastHit.transform;
//                    }
//                    else if (t_RaycastHit.transform.CompareTag("Spehre"))
//                    {
//                        selectedTrans = t_RaycastHit.transform;
//                    }
//                }
//            }
//
//            if (t.phase == TouchPhase.Ended)
//            {
//                _secondPressPos = new Vector2(t.position.x, t.position.y);
//                _currentSwipe = new Vector3(_secondPressPos.x - _firstPressPos.x, _secondPressPos.y - _firstPressPos.y);
//
//                // Make sure it was a legit swipe, not a tap
//                if (_currentSwipe.magnitude < MinSwipeLength)
//                {
//                    SwipeDirection = Swipe.None;
//                    return;
//                }
//
//                _currentSwipe.Normalize();
//
//                // Swipe up
//                if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
//                {
//                    SwipeDirection = Swipe.Up;
//                    
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe down
//                else if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
//                {
//                    SwipeDirection = Swipe.Down;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe left
//                else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
//                {
//                    SwipeDirection = Swipe.Left;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe right
//                else if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
//                {
//                    SwipeDirection = Swipe.Right;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe up left
//                else if (_currentSwipe.y > 0 && _currentSwipe.x < 0)
//                {
//                    SwipeDirection = Swipe.UpLeft;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe up right
//                else if (_currentSwipe.y > 0 && _currentSwipe.x > 0)
//                {
//                    SwipeDirection = Swipe.UpRight;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//                // Swipe down left
//                else if (_currentSwipe.y < 0 && _currentSwipe.x < 0)
//                {
//                    SwipeDirection = Swipe.DownLeft;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//
//                    // Swipe down right
//                }
//                else if (_currentSwipe.y < 0 && _currentSwipe.x > 0)
//                {
//                    SwipeDirection = Swipe.DownRight;
//                    if (selectedTrans != null)
//                    {
//                        PuzzlePointController.Instance.MoveDirection(SwipeDirection, selectedTrans);
//                    }
//
//                    selectedTrans = null;
//                }
//            }
//        }
        
            if (Input.GetMouseButtonDown(0))
            {
                _firstClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                Ray t_Ray = mainCamera.ScreenPointToRay(_firstClickPos);

                RaycastHit t_RaycastHit; 
                
     
                
                if (Physics.Raycast(t_Ray, out t_RaycastHit,1000))
                {
                    if (t_RaycastHit.transform.CompareTag("plastic"))
                    {
                        selectedTrans = t_RaycastHit.transform;
                    }
                }
            }
            else
            {
                SwipeDirection = Swipe.None;
                //Debug.Log ("None");
            }

            if (Input.GetMouseButtonUp(0))
            {
                _secondClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                _currentSwipe = new Vector3(_secondClickPos.x - _firstClickPos.x, _secondClickPos.y - _firstClickPos.y);

                // Make sure it was a legit swipe, not a tap
                if (_currentSwipe.magnitude < MinSwipeLength)
                {
                    SwipeDirection = Swipe.None;
                    return;
                }

                _currentSwipe.Normalize();

                //Swipe directional check
                // Swipe up
                if (_currentSwipe.y > 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    SwipeDirection = Swipe.Up;
                    Debug.Log("Up");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                }
                // Swipe down
                else if (_currentSwipe.y < 0 && _currentSwipe.x > -0.5f && _currentSwipe.x < 0.5f)
                {
                    SwipeDirection = Swipe.Down;
                    Debug.Log("Down");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                  
                }
                // Swipe left
                else if (_currentSwipe.x < 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    SwipeDirection = Swipe.Left;
                    Debug.Log("Left");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                }
                // Swipe right
                else if (_currentSwipe.x > 0 && _currentSwipe.y > -0.5f && _currentSwipe.y < 0.5f)
                {
                    SwipeDirection = Swipe.Right;
                    Debug.Log("right");
                    if (selectedTrans != null)
                    {
                        
                    }
                    //Gameplay.Instance.levelTwoB.PlasticSwiped();
                    selectedTrans = null;
                   
                } // Swipe up left
                else if (_currentSwipe.y > 0 && _currentSwipe.x < 0)
                {
                    SwipeDirection = Swipe.UpLeft;
                    Debug.Log("UpLeft");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                }
                // Swipe up right
                else if (_currentSwipe.y > 0 && _currentSwipe.x > 0)
                {
                    SwipeDirection = Swipe.UpRight;
                    Debug.Log("UpRight");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;

                }
                // Swipe down left
                else if (_currentSwipe.y < 0 && _currentSwipe.x < 0)
                {
                    SwipeDirection = Swipe.DownLeft;
                    Debug.Log("DownLeft");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                    // Swipe down right
                }
                else if (_currentSwipe.y < 0 && _currentSwipe.x > 0)
                {
                    SwipeDirection = Swipe.DownRight;
                    Debug.Log("DownRight");
                    if (selectedTrans != null)
                    {
                        
                    }

                    selectedTrans = null;
                }
            }
        }
    }

[System.Serializable]
public enum Swipe {
    None,
    Up,
    Down,
    Left,
    Right,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight
};
