﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleClass : MonoBehaviour
{
    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;

    public int counter;
    public bool isButtonCalled;
    
    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("gluearea"))
        {
            if (!other.GetComponent<MeshRenderer>().enabled)
            {
                other.GetComponent<MeshRenderer>().enabled = true;
                counter++;
                //Debug.Log(counter);
                if (counter > 60)
                {
                    if (!isButtonCalled)
                    {
                        //Gameplay.Instance.levelOneB.DoneFootGlue();
                        isButtonCalled = true;
                    }
                }
            }
        }
    }
}