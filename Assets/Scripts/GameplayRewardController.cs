﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.Ad;
using FRIA;
public class GameplayRewardController : MonoBehaviour
{
    public static GameplayRewardController instance;

    private const float PROGRESS_RATE_PERLEVEL = 0.5f;
    private const float FILL_TIME = 1.0f;
    private const float INITIAL_DELAY = 0.25f;
    private const float END_DELAY = 0.25f;
    public static float TotalTimeBeforePanelLoad
    {
        get 
        {
            return FILL_TIME + INITIAL_DELAY + END_DELAY;
        }
    }
    public static GameplayRewardController Instance { get; private set; }
    private static HardData<int> _unlockProgress;
    private static float unlockProgress
    {
        get
        {
            if (_unlockProgress == null) _unlockProgress = new HardData<int>("GAMEPLAY_REWARD_ITEM_UNLOCK_PROGRESS",0);
            return _unlockProgress / 100.0f;
        }
        set
        {
            if (_unlockProgress == null) _unlockProgress = new HardData<int>("GAMEPLAY_REWARD_ITEM_UNLOCK_PROGRESS", 0);
            _unlockProgress.value = Mathf.RoundToInt(value * 100);
        }
    }

    [Header("Fill")]
    public GameObject fillPanelRoot;
    public Image fillImage;
    public Text fillText;

    [Header("Reward")]
    public GameObject rewardPanelRoot;
    public Image rewardImage;
    public Button claimButtonGameReward;
    public Button claimButtonCollectible;
    public Button skipButton;
    public List<ShopGroupManager> shops;

    private IPurchasable chosenItem;
    private float startVal = 0;
    private float endVal = 1;

    [Header("Optionals")]
    public GameObject title1;
    public GameObject title2;
    public Image rayImage;
    public Color rayColor1 = Color.white;
    public Color rayColor2 = Color.white;
    public Image claimButtonImage;
    public Color claimColor1 = Color.white;
    public Color claimColor2 = Color.white;
    //public Text claimButtonText;

    private FRIA.ChancedList<IPurchasable> rollList = new FRIA.ChancedList<IPurchasable>();

    System.Action onPanelOpen;
    System.Action onPanelClose;

    private void Awake()
    {
        instance = this;
        fillPanelRoot.SetActive(false);
        rewardPanelRoot.SetActive(false);
    }
    public bool ShouldLoadReward(System.Action onPanelOpen, System.Action onPanelClose)
    {
        this.onPanelOpen = onPanelOpen;
        this.onPanelClose = onPanelClose;
        if (unlockProgress >= 1) unlockProgress = 0;
        startVal = unlockProgress;
        unlockProgress += PROGRESS_RATE_PERLEVEL;
        endVal = unlockProgress;
        //Debug.LogFormat("{0}=>{1}",startVal,endVal);

        rollList.Clear();
        foreach (var shop in shops)
        {
            foreach (var tab in shop.tablist)
            {
                foreach (ISlidable item in (tab as CategoryTab).slideItemList )
                {
                    IPurchasable purchasable = (BasicItemProfile)item;
                    if (purchasable.cost <= 0) continue;
                    if (purchasable.unlocked) continue;
                    rollList.Add(purchasable, purchasable.cost);
                }
            }
        }
        bool shouldEnableRewardSystem = rollList.items.Count>0;

        fillPanelRoot.SetActive(shouldEnableRewardSystem);
        if (shouldEnableRewardSystem)
        {
            chosenItem = rollList.Roll();
            StartCoroutine(FillItUp());
        }


        if (shouldEnableRewardSystem && endVal >= 1) return true;
        else if (LevelManager.pendingCollectible!=null) return true;
        else return false;
    }

    IEnumerator FillItUp()
    {

        float timeSpan = FILL_TIME / (endVal - startVal);
        float startTime = Time.time;
        fillImage.fillAmount = startVal;
        fillText.text = string.Format("{0}%", Mathf.RoundToInt(startVal * 100));
        yield return new WaitForSeconds(INITIAL_DELAY);
        float lastTextUpdateTime = -1000;
        float textUpdateInterval = 0.2f;
        while (Time.time <= startTime + timeSpan)
        {
            float p = Mathf.Clamp01((Time.time - startTime) / timeSpan);
            fillImage.fillAmount = Mathf.Lerp(startVal, endVal, p);
            if (Time.time > lastTextUpdateTime + textUpdateInterval)
            {
                fillText.text = string.Format("{0}%", Mathf.RoundToInt(fillImage.fillAmount * 100));
            }
            yield return null;
        }
        fillImage.fillAmount = endVal;
        fillText.text = string.Format("{0}%", Mathf.RoundToInt(endVal * 100));
        yield return new WaitForSeconds(END_DELAY);


        if (endVal >= 1)
        {
            onPanelOpen?.Invoke();
            onPanelOpen = null;
            LoadAs(RewardType.GameplayReward);
        }
        else if (LevelManager.pendingCollectible != null)
        {
            onPanelOpen?.Invoke();
            onPanelOpen = null;
            LoadAsCollectiblePanel();  
        }
    }

    private PrankCollectibleProfile collectible;
    public void LoadAsCollectiblePanel()
    {
        if (LevelManager.pendingCollectible == null) return;
        collectible = LevelManager.pendingCollectible;
        LoadAs(RewardType.PrankCollectible);
    }


    RewardType type;
    RVPlacement rv_placement;
    void LoadAs(RewardType rt)
    {
        type = rt;
        rewardPanelRoot.SetActive(true);
        title1.SetActive(rt == RewardType.GameplayReward);
        title2.SetActive(rt == RewardType.PrankCollectible);
        claimButtonGameReward.gameObject.SetActive(rt == RewardType.GameplayReward);
        claimButtonCollectible.gameObject.SetActive(rt == RewardType.PrankCollectible);

        claimButtonGameReward.interactable = true;
        claimButtonCollectible.interactable = true;
        skipButton.interactable = true;
        claimButtonGameReward.onClick.AddListener(OnClaim);
        claimButtonCollectible.onClick.AddListener(OnClaim);
        skipButton.onClick.AddListener(OnSkip);

        switch (type)
        {
            case RewardType.GameplayReward:
                rewardImage.sprite = chosenItem.GetDisplaySprite(false);
                rv_placement = RVPlacement.rewarded_item_unlock;
                rayImage.color = rayColor1;
                claimButtonImage.color = claimColor1;
                //claimButtonText.text = "Get It!";
                break;
            case RewardType.PrankCollectible:
                rewardImage.sprite = collectible.displaySprite;
                rv_placement = RVPlacement.collectible_item_unlock;
                rayImage.color = rayColor2;
                claimButtonImage.color = claimColor2;
                //claimButtonText.text = "Collect!";
                break;

            default:
                Debug.LogError("Unhandled type!");
                break;
        }


    }
    void OnClaim()
    {
        claimButtonGameReward.interactable = false;
        claimButtonCollectible.interactable = false;
        skipButton.interactable = false;
        AnalyticsAssistant.LogRv(RVState.rv_click, rv_placement);
        bool willShow = AdController.ShowRewardedVideoAd((bool success) => {
            if (success)
            {
                claimButtonGameReward.onClick.RemoveAllListeners();
                claimButtonCollectible.onClick.RemoveAllListeners();
                skipButton.onClick.RemoveAllListeners();
                switch (type)
                {
                    case RewardType.GameplayReward:
                        chosenItem.unlocked = true;
                        chosenItem.RequestToBeSelected();
                        break;
                    case RewardType.PrankCollectible:
                        collectible.unlocked.value = true;
                        break;

                    default:
                        Debug.LogError("Unhandled type!");
                        break;
                }

                rewardPanelRoot.SetActive(false);
                AnalyticsAssistant.LogRv(RVState.rv_complete, rv_placement);
                Debug.LogFormat("Claimed:=========================== {0}", LevelManager.pendingCollectible);
                if (type != RewardType.PrankCollectible && LevelManager.pendingCollectible != null)
                {
                    LoadAsCollectiblePanel();
                }
                else
                {
                    onPanelClose?.Invoke();
                    onPanelClose = null;
                }
            }
            else
            {
                claimButtonGameReward.interactable = true;
                claimButtonCollectible.interactable = true;
                skipButton.interactable = true;
            }
        });
        if (willShow) AnalyticsAssistant.LogRv(RVState.rv_start, rv_placement);
    }
    void OnSkip()
    {
        claimButtonGameReward.onClick.RemoveAllListeners();
        claimButtonCollectible.onClick.RemoveAllListeners();
        skipButton.onClick.RemoveAllListeners();
        rewardPanelRoot.SetActive(false);


        if (type != RewardType.PrankCollectible && LevelManager.pendingCollectible != null)
        {
            LoadAsCollectiblePanel();
        }
        else
        {
            onPanelClose?.Invoke();
            onPanelClose = null;
        }
    }
}
public enum RewardType
{
    GameplayReward = 1,
    PrankCollectible =2,
}