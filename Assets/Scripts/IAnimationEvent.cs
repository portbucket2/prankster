﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAnimationEvent : MonoBehaviour
{
    public ParticleSystem oilParticle;
    public Transform propsTwoObject;
    
    public void PlaySaltParticle()
    {
        //saltParticle.Play();
    }

    public void Reposistion()
    {
        propsTwoObject.transform.position = new Vector3(-0.724f,-0.04f,-1.608f);
        oilParticle.Play();
    }

    public void playoilParticle()
    {
        oilParticle.Play();
    }

    public void ReposFailed()
    {
        propsTwoObject.transform.position = new Vector3(-0.675f,-0.04f,-1.608f);
    }
}
