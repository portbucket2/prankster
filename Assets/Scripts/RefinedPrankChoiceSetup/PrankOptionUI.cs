﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Portbliss.Ad;
public class PrankOptionUI : MonoBehaviour
{
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private int RESET = Animator.StringToHash("reset");

    public static event Action onOptionFinalized;
    public static event Action onWentToAd;
    public static event Action onReturnedFromAd;

    public GameObject root;
    public Animator anim;
    public Image itemSprite;
    public Button button;


    System.Action<Correctness> onTap;
    PrankOption option;
    public void Load(PrankOption option, Action<Correctness> onTap)
    {
        root.SetActive(false);
        if (option.correctness == Correctness.DISABLED) return;
        if (option.correctness == Correctness.RV)
        {
            //if (ABManager.GetValueInt(ABtype.rv_choice_enable) == 0) return;
            switch (option.platformFilter)
            {
                case PlatformFilter.NONE:
                    return;
                case PlatformFilter.ALL_PLATFORM:
                    break;
                case PlatformFilter.ANDROID_ONLY:
#if UNITY_IOS
                    return;
#else
                    break;
#endif
                case PlatformFilter.IOS_ONLY:
#if UNITY_ANDROID
                    return;
#else
                    break;
#endif

                default:
                    throw new Exception("Unidentified case ");
            }
        }

        root.SetActive(true);
        this.option = option;
        this.onTap = onTap;
        itemSprite.sprite = option.itemSprite;

        anim.SetBool("RV", option.correctness == Correctness.RV);

        anim.SetTrigger(ENTRY);

        button.interactable = true;
        button.onClick.AddListener(OnClick);
        onOptionFinalized += OnOptionClickedElseWhere;
        onWentToAd += DisableInteractibility;
        onReturnedFromAd += EnableInteractibility;

    }


    void DisableInteractibility() { button.interactable = false; }
    void EnableInteractibility() { button.interactable = true; }
    void OnOptionClickedElseWhere()
    {
        onOptionFinalized -= OnOptionClickedElseWhere;
        onWentToAd -= DisableInteractibility;
        onReturnedFromAd -= EnableInteractibility;
        button.onClick.RemoveAllListeners();
        anim.SetTrigger(EXIT);
    }


    void OnClick()
    {
        switch (option.correctness)
        {
            case Correctness.WRONG:
            case Correctness.RIGHT:
                onTap?.Invoke(option.correctness);
                onOptionFinalized?.Invoke();
                break;
            case Correctness.RV:
                onWentToAd?.Invoke();
                AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.rv_option_selected);
                bool willShow = AdController.ShowRewardedVideoAd((bool success) =>
                {
                    onReturnedFromAd?.Invoke();
                    if (success)
                    {
                        onTap?.Invoke(option.correctness);
                        onOptionFinalized?.Invoke();
                        AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.rv_option_selected);
                    }
                });
                if (willShow) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.rv_option_selected);
                break;
        }

    }
}
