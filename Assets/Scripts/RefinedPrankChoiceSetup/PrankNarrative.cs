﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "narrative_", menuName = "PrankStarDataset/PrankNarrative", order = 1)]
public class PrankNarrative : ScriptableObject
{
    

    public List<PrankQuestion> questions = new List<PrankQuestion>();

    public static PrankQuestion GetNarrative(string levelID, int stepNumber)
    {
        string path = string.Format("QuestionNarratives/Narrative{0}", levelID);
        return Resources.Load<PrankNarrative>(path).questions[stepNumber];
    }


#if UNITY_EDITOR
    //public void Flush()
    //{
    //    questions = new List<PrankQuestion>();
    //    EditorFix.SetObjectDirty(this);
    //    AssetDatabase.SaveAssets();
    //}
    //public void Add(Objective oldObjective)
    //{
    //    PrankQuestion newQ = new PrankQuestion(false);
    //    newQ.question = oldObjective.objectiveQuestion;
    //    for (int i = 0; i < oldObjective.answerList.Count; i++)
    //    {
    //        Answer ans = oldObjective.answerList[i];
    //        switch (i)
    //        {
    //            case 0:
    //                newQ.answer1.LoadByLegacy(ans);
    //                break;
    //            case 1:
    //                newQ.answer2.LoadByLegacy(ans);
    //                break;
    //            default:
    //                Debug.LogError("Unexpected choiceCount in legacy data");
    //                break;
    //        }
    //    }
    //    questions.Add(newQ);
    //}


    //private static PrankNarrative GetAssetForLevel(string id)
    //{

    //    return pn;
    //}

    //static Dictionary<string, PrankNarrative> localDic = new Dictionary<string, PrankNarrative>();

    //public static void AddQuestionToLevel(string id, Objective oldObj)
    //{
    //    PrankNarrative nt;
    //    if (localDic.ContainsKey(id))
    //    {
    //        nt = localDic[id];
    //    }
    //    else
    //    {
    //        nt = (PrankNarrative)ScriptableObject.CreateInstance(typeof(PrankNarrative));
    //        localDic.Add(id, nt);
    //    }
    //    nt.Add(oldObj);

    //}
    //public static void PublishID(string id)
    //{
    //    string path = string.Format("Assets/Resources/QuestionNarratives/Narrative{0}.asset", id);
    //    PrankNarrative pn = localDic[id];
    //    AssetDatabase.CreateAsset(pn, path);
    //    AssetDatabase.SaveAssets();
    //}
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(PrankNarrative))]
public class PrankNarrativeEditor : Editor
{
    int duplicateID;
    public override void OnInspectorGUI()
    {
        PrankNarrative selected = (PrankNarrative)target;
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Duplicate"))
        {
            PrankNarrative createdNarrative =  (PrankNarrative)ScriptableObject.CreateInstance(typeof(PrankNarrative));
            foreach (PrankQuestion selectedQ in selected.questions)
            {
                PrankQuestion createdQ = new PrankQuestion();
                createdQ.answer1 = selectedQ.answer1;
                createdQ.answer2 = selectedQ.answer2;
                createdQ.answer3 = selectedQ.answer3;
                createdQ.question = selectedQ.question;
                createdNarrative.questions.Add(createdQ);
            }

            string path = string.Format("Assets/Resources/QuestionNarratives/Narrative{0}.asset", duplicateID);
            AssetDatabase.CreateAsset(createdNarrative, path);
            AssetDatabase.SaveAssets();
        }
        duplicateID = EditorGUILayout.IntField("Duplicate ID",duplicateID);
        EditorGUILayout.EndHorizontal();
        PrankNarrative pn = (PrankNarrative)target;
        serializedObject.Update();

        if (pn.questions == null) pn.questions = new List<PrankQuestion>();
        Show(serializedObject.FindProperty("questions"), pn);
        serializedObject.ApplyModifiedProperties();
    }

    public static void Show(SerializedProperty list, PrankNarrative pn)
    {
        EditorGUILayout.Space();
        EditorGUI.indentLevel += 1;
        if (GUILayout.Button("(+)Add"))
        {
            pn.questions.Insert(0, new PrankQuestion(false));
        }
       
        for (int i = 0; i < list.arraySize; i++)
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("",  GUILayout.Width(EditorGUIUtility.currentViewWidth -250));
            if (GUILayout.Button("(+)Add", GUILayout.Width(100)))
            {
                pn.questions.Insert(i+1, new PrankQuestion(false));
            }
            if (GUILayout.Button("(-)Remove", GUILayout.Width(100)))
            {
                pn.questions.RemoveAt(i);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space();
    }
}

#endif