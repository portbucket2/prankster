﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public struct PrankOption
{
    public Correctness correctness;
    public string itemName;
    public Sprite itemSprite;
    public PlatformFilter platformFilter;

    //public void LoadByLegacy(Answer ans)
    //{
    //    correctness = ans.isRight ? Correctness.RIGHT : Correctness.WRONG;
    //    itemSprite = ans.itemSprite;
    //    itemName = ans.answer;
    //}
}
public enum PlatformFilter
{
    ALL_PLATFORM = 0,
    ANDROID_ONLY = 1,
    IOS_ONLY = 2,
    NONE =3,
}

public enum Correctness
{
    DISABLED = -1,
    WRONG = 0,
    RIGHT = 1,
    RV = 2,
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(PrankOption))]
public class PrankOptionEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUIUtility.labelWidth = 0;
        label.text = string.Format("");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;


        Correctness c = (Correctness)property.FindPropertyRelative("correctness").intValue;
        bool disableDetails = c == Correctness.DISABLED;

        float elementHeight = (position.height-10)/3;
        float imageOffset = position.height;
        if (disableDetails)
        {
            elementHeight = position.height;
            imageOffset = position.height * 3 + 10;
        }
        

        var typeRect = new Rect(position.x , position.y, position.width- imageOffset, elementHeight);
        var answerRect = new Rect(position.x, position.y + elementHeight, position.width- imageOffset, elementHeight);
        var platformRect = new Rect(position.x, position.y + elementHeight*2, position.width - imageOffset, elementHeight);
        var imageRect = new Rect(position.x + position.width - imageOffset, position.y, imageOffset, position.height);

        //EditorGUI.LabelField(labelRect, "isRight?");
        EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("correctness"), GUIContent.none);
        if (!disableDetails) EditorGUI.PropertyField(answerRect, property.FindPropertyRelative("itemName"), GUIContent.none);
        //EditorGUI.PropertyField(imageRect, property.FindPropertyRelative("itemSprite"), GUIContent.none);
        if (!disableDetails) property.FindPropertyRelative("itemSprite").objectReferenceValue = EditorGUI.ObjectField(imageRect, property.FindPropertyRelative("itemSprite").objectReferenceValue, typeof(Sprite), false);
        else EditorGUI.LabelField(imageRect, "Collapsed");
        if(c==Correctness.RV) EditorGUI.PropertyField(platformRect, property.FindPropertyRelative("platformFilter"), GUIContent.none);

        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        Correctness c = (Correctness) property.FindPropertyRelative("correctness").intValue;
        if (c == Correctness.DISABLED) 
            return base.GetPropertyHeight(property, label);
        else
            return base.GetPropertyHeight(property, label) * 3 + 10;
    }
}
#endif