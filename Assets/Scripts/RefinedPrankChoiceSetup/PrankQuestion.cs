﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class PrankQuestion 
{
    public string question;
    [Space]
    public PrankOption answer1;
    public PrankOption answer2;
    public PrankOption answer3;

    public PrankQuestion() { }
    public PrankQuestion(bool rv)
    {
        answer1 = new PrankOption();
        answer1.correctness = Correctness.WRONG;
        answer2 = new PrankOption();
        answer2.correctness = Correctness.WRONG;
        answer3 = new PrankOption();
        answer3.correctness = rv?Correctness.RV:Correctness.DISABLED;
    }
}
