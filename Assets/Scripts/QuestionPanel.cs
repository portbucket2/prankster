﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using System;
public class QuestionPanel : MonoBehaviour
{
    [Header("Question Panel")] 
    public GameObject questionPanelObject;
    public Text objectiveText;

    float answerOffset_withAd;
    float answerOffset_noAd;
    public RectTransform answerSheetRect;
    public PrankOptionUI answer1;
    public PrankOptionUI answer2;
    public PrankOptionUI answer3;

    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private int RESET = Animator.StringToHash("reset");

    public List<Animator> innerAnims;
    public List<Animator> outerAnims;

    private void Awake()
    {
        answerOffset_withAd = answerSheetRect.anchoredPosition.y;
        answerOffset_noAd = answerOffset_withAd - 100;
        TrigInnerAnims(RESET); 
        TrigOuterAnims(RESET);
    }
    void TrigInnerAnims(int trig)
    {
        foreach (var item in innerAnims)
        {
            item.SetTrigger(trig);
        }
    }
    void TrigOuterAnims(int trig)
    {
        foreach (var item in outerAnims)
        {
            item.SetTrigger(trig);
        }
    }

    public void ShowQuestionPanel(PrankQuestion question, Action<Correctness> onChoice)
    {
        answerSheetRect.anchoredPosition = new Vector2(answerSheetRect.anchoredPosition.x, NoAdManager.isBlockingAds?answerOffset_noAd:answerOffset_withAd);
        QuestionAppear();
        objectiveText.text = question.question;
        System.Action<Correctness> addedAction = onChoice;
        addedAction += (Correctness co) => { QuestionDisappear(); };
        answer1.Load(question.answer1, addedAction);
        answer2.Load(question.answer2, addedAction);
        answer3.Load(question.answer3, addedAction);
    }
    public void QuestionAppear()
    {
        TrigOuterAnims(ENTRY);
        Centralizer.Add_DelayedAct(() => TrigInnerAnims(ENTRY), 0.5f);
    }


    public void QuestionDisappear()
    {
        //leftButton.onClick.RemoveAllListeners();
        //rightButton.onClick.RemoveAllListeners();
        TrigInnerAnims(EXIT);
        Centralizer.Add_DelayedAct(() => TrigOuterAnims(EXIT), 0.5f);
    }


    //public void RequestColorQuestionPanel(Objective objective)
    //{
    //    //Objective t_CurrentObjective = Gameplay.Instance.GetCurrentObjective();
    //    objectiveText.text = objective.objectiveQuestion;
    //    leftOptionImage.sprite = objective.answerList[0].itemSprite;
    //    rightOptionImage.sprite = objective.answerList[1].itemSprite;

    //    QuestionAppear();

    //    leftButton.onClick.RemoveAllListeners();
    //    rightButton.onClick.RemoveAllListeners();

    //    IPrankBehaviour ipb = PrankLevelBehaviour.instance;

    //    if (objective.answerList[0].isRight)
    //    {
    //        leftButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            ipb.RightOption();
    //        });
    //    }
    //    else
    //    {
    //        leftButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            ipb.WrongOption();
    //        });
    //    }

    //    if (objective.answerList[1].isRight)
    //    {
    //        rightButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            ipb.RightOption();
    //        });
    //    }
    //    else
    //    {
    //        rightButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            ipb.WrongOption();
    //        });
    //    }
    //}
    //public void ActivateColorQuestionPanel(IOption t_Option,Objective t_CurrentObjective)
    //{
    //    //Objective t_CurrentObjective = Gameplay.Instance.GetCurrentObjective();
    //    objectiveText.text = t_CurrentObjective.objectiveQuestion;
    //    leftOptionImage.sprite = t_CurrentObjective.answerList[0].itemSprite;
    //    rightOptionImage.sprite = t_CurrentObjective.answerList[1].itemSprite;
        
    //    QuestionAppear();
        
    //    leftButton.onClick.RemoveAllListeners();
    //    rightButton.onClick.RemoveAllListeners();
        

    //    if (t_CurrentObjective.answerList[0].isRight)
    //    {
    //        leftButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            t_Option.Right();
    //        });
    //    }
    //    else
    //    {
    //        leftButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            t_Option.Wrong();
    //        });
    //    }
        
    //    if (t_CurrentObjective.answerList[1].isRight)
    //    {
    //        rightButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            t_Option.Right();
    //        });
    //    }
    //    else
    //    {
    //        rightButton.onClick.AddListener(delegate
    //        {
    //            QuestionDisappear();
    //            t_Option.Wrong();
    //        });
    //    }
    //}



}
