﻿using FRIA;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject mainObject;
    [Header("Start Panel")]
    public GameObject startPanel;
    public Text levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;

    [Header("Upper panel")] public Animator upperPanelAnimator;
    public Text currentLevelText;




    [Header("Level Failed")]

    public Animator earlyFailAnimator;

    public GameObject levelFailedObject;
    public List<Animator> levelFailedAnimators;
    public Animator retryButtonAnimator;
    
    public Button tryAgainButton;
    public Button reviveWithAdButton;
    public Button reviveWithCoinButton;
    public Image timerDial;
    public Text timerNumber;

    [Header("Question Panel")] public QuestionPanel questionPanel;
    [Header("Success Panel")] public SuccessUIController successController;

    public Button doneButton;

    [Header("Instruction Panel")] public TextMeshProUGUI instructionText;
    public Animator instructionBarAnimator;

    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        if (SpecialBuildManager.enableUIToggle)
        {
            Canvas canv = this.transform.parent.GetComponent<Canvas>();
            canv.renderMode = RenderMode.ScreenSpaceCamera;
            canv.planeDistance = 0.3001f;
            canv.worldCamera = Camera.main;
        }
        //SpecialBuildManager.onUIToggle += OnUIToggle;
    }
    //void OnUIToggle()
    //{
    //    if(SpecialBuildManager.isLionMarketingBuild) mainObject.SetActive(!mainObject.activeSelf);
    //}
    //private void OnDestroy()
    //{
    //    SpecialBuildManager.onUIToggle -= OnUIToggle;
    //}

    private IEnumerator Start()
    {
        startPanel.SetActive(true);
        string levelTitle = LevelManager.Instance.GetDisplayTextForLevelNumber();
        currentLevelText.text = levelTitle;
        levelNumberText.text = levelTitle;
        tapButton.onClick.AddListener(OnTapToStart);
        yield return null;
        if (!LevelManager.showStartScreen) OnTapToStart();
    }
    #region onButton

    private void OnTapToStart()
    {
        tapButton.onClick.RemoveListener(OnTapToStart);
        startPanelAnimator.SetTrigger(EXIT);
        upperPanelAnimator.SetTrigger(ENTRY);

        Gameplay.Instance.StartGame();
    }


    


    private void OnTryAgainButton()
    {
        tryAgainButton.onClick.RemoveListener(OnTryAgainButton);
        LevelManager.LoadLevelDirect();
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Fail_LevelRestart);
    }
    private void OnSendPreResetAlerts()
    {
        //Debug.LogError("Success Alert Sent");
        reviveWithAdButton.onClick.RemoveListener(OnSendPreResetAlerts);
        IPrankBehaviour ipb = PrankLevelBehaviour.instance;
        ipb.ResetPhase(-1);
    }
    private void OnReviveWithAd()
    {
        //Debug.LogError("Ad Called");n
        waitingAdCallBack = true;
        reviveWithAdButton.interactable = false;
        reviveWithCoinButton.interactable = false;
        tryAgainButton.interactable = false;
        AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.revive);
        bool didShowRV = Portbliss.Ad.AdController.ShowRewardedVideoAd((success) =>
        {
            //Debug.LogError("AdResult" + success);
            //Debug.Log("rewarded ad has been played. Success? " + success);
            if (success)
            {
                reviveWithAdButton.onClick.RemoveListener(OnReviveWithAd);
                IPrankBehaviour ipb = PrankLevelBehaviour.instance;
                //reward the user. todo
                ipb.ResetPhase(0);
                if (countDownRoutine != null) StopCoroutine(countDownRoutine);
                StartCoroutine(HideLevelFailed());
                AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.revive);
            }
            else
            {
                waitingAdCallBack = false;
                reviveWithCoinButton.interactable = BasicCoinTracker.CoinBalance>=COST_TO_REVIVE;
                reviveWithAdButton.interactable = true;
                tryAgainButton.interactable = true;
            }
        });
        if(didShowRV) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.revive);
    }

    private void OnReviveWithCoin()
    {
        BasicCoinTracker.ChangeCoin(-COST_TO_REVIVE);
        waitingAdCallBack = true;
        reviveWithAdButton.interactable = false;
        reviveWithCoinButton.interactable = false;
        tryAgainButton.interactable = false;
        IPrankBehaviour ipb = PrankLevelBehaviour.instance;
        ipb.ResetPhase(-1);
        float ts = Time.timeScale;
        Time.timeScale = 50;
        Centralizer.Add_DelayedAct(()=> {
            ipb.ResetPhase(0);
            Time.timeScale = ts;
            if (countDownRoutine != null) StopCoroutine(countDownRoutine);
            StartCoroutine(HideLevelFailed());

        },1,true);
    }
    #endregion


    public void ShowInstruction(string t_Instruction)
    {
       // instructionText.text = t_Instruction;
       // instructionBarAnimator.SetTrigger(ENTRY);
    }

    public void HideInstruction()
    {
       // instructionBarAnimator.SetTrigger(EXIT);
    }






    bool waitingAdCallBack;
    Coroutine countDownRoutine;
    private const float EARLY_FAIL_RESTART_DELAY = 2.5f;

    //public void ReportFailed_1_2_3(IPhaseReset t_PhaseReset)
    //{
    //    TimedAd.AB_ControlledAd(null, IntAdPlacement.Fail_PreFailUI);
    //    bool showCoinRevive = false;// ABManager.GetValueInt(ABtype.coin_revive_enable) != 0;
    //    reviveWithCoinButton.gameObject.SetActive(showCoinRevive);
    //    tryAgainButton.interactable = true;
    //    reviveWithAdButton.interactable = true;
    //    reviveWithCoinButton.interactable = PrankCoinTracker.CoinBalance >= COST_TO_REVIVE;
    //    levelFailedObject.SetActive(true);
    //    foreach (var item in levelFailedAnimators) item.SetTrigger(ENTRY);
    //    Centralizer.Add_DelayedMonoAct(this, () => { retryButtonAnimator.SetTrigger(ENTRY); }, SKIP_ENABLE_DELAY); ;
    //    tryAgainButton.onClick.AddListener(OnTryAgainButton);

    //    countDownRoutine = StartCoroutine(LevelFailedDialRoutine(ANIMATION_TRANSITION_TIME));

    //    reviveWithAdButton.onClick.RemoveAllListeners();
    //    reviveWithAdButton.onClick.AddListener(delegate
    //    {
    //        reviveWithAdButton.interactable = false;
    //        reviveWithCoinButton.interactable = false;
    //        t_PhaseReset.ResetPhase(true);

    //        waitingAdCallBack = true;
    //        AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.revive);
    //        bool didShowRV = Portbliss.Ad.AdController.ShowRewardedVideoAd((success) =>
    //        {
    //            Debug.Log("rewarded ad has been played. Success? " + success);
    //            if (success)
    //            {
    //                //reward the user. todo
    //                t_PhaseReset.ResetPhase(false);
    //                if (countDownRoutine != null) StopCoroutine(countDownRoutine);
    //                StartCoroutine(HideLevelFailed());
    //                AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.revive);
    //            }
    //            else
    //            {
    //                waitingAdCallBack = false;
    //                reviveWithAdButton.interactable = true;
    //                reviveWithCoinButton.interactable = PrankCoinTracker.CoinBalance >= COST_TO_REVIVE;
    //            }
    //        });
    //        if (didShowRV) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.revive);

    //    });


    //    reviveWithCoinButton.onClick.RemoveAllListeners();
    //    reviveWithCoinButton.onClick.AddListener(()=>
    //    {
    //        PrankCoinTracker.ChangeCoin(-COST_TO_REVIVE);
    //        waitingAdCallBack = true;
    //        reviveWithAdButton.interactable = false;
    //        reviveWithCoinButton.interactable = false;
    //        t_PhaseReset.ResetPhase(true);

    //        float ts = Time.timeScale;
    //        Time.timeScale = 50;
    //        Centralizer.Add_DelayedAct(() => {
    //            t_PhaseReset.ResetPhase(false);
    //            Time.timeScale = ts;
    //            if (countDownRoutine != null) StopCoroutine(countDownRoutine);
    //            StartCoroutine(HideLevelFailed());

    //        }, 1,true);
    //    });
    //}
    public const int COST_TO_REVIVE = 300;
    public void ReportFailed()
    {
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Fail_PreFailUI);
        bool showCoinRevive = false;// ABManager.GetValueInt(ABtype.coin_revive_enable) != 0;
        reviveWithCoinButton.gameObject.SetActive(showCoinRevive);
        tryAgainButton.interactable = true;
        reviveWithAdButton.interactable = true;
        reviveWithCoinButton.interactable = BasicCoinTracker.CoinBalance>=COST_TO_REVIVE;
        levelFailedObject.SetActive(true);
        foreach (var item in levelFailedAnimators) item.SetTrigger(ENTRY);
        Centralizer.Add_DelayedMonoAct(this, () => { retryButtonAnimator.SetTrigger(ENTRY); }, SKIP_ENABLE_DELAY); 
        tryAgainButton.onClick.AddListener(OnTryAgainButton);
        IPrankBehaviour ipb = PrankLevelBehaviour.instance;
        reviveWithAdButton.onClick.RemoveAllListeners();
        reviveWithAdButton.onClick.AddListener(OnSendPreResetAlerts);
        reviveWithAdButton.onClick.AddListener(OnReviveWithAd);
        reviveWithCoinButton.onClick.RemoveAllListeners();
        reviveWithCoinButton.onClick.AddListener(OnReviveWithCoin);
        countDownRoutine = StartCoroutine(LevelFailedDialRoutine(ANIMATION_TRANSITION_TIME));
    }

    public void ReportEarlyFailed()
    {
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Fail_PreFailUI);
        earlyFailAnimator.SetTrigger("ENTRY");
        Centralizer.Add_DelayedAct(() => 
        {
            LevelManager.LoadLevelDirect();
            TimedAd.AB_ControlledAd(null, IntAdPlacement.Fail_LevelRestart);
        }, EARLY_FAIL_RESTART_DELAY, true);
    }


    public const float ANIMATION_TRANSITION_TIME = 0;
    public const float AUTO_RESTART_TIME = 5;
    public const float SKIP_ENABLE_DELAY = 1.85f;
    IEnumerator LevelFailedDialRoutine(float animationTime)
    {
        waitingAdCallBack = false;
        timerDial.fillAmount = 1;
        timerNumber.text = Mathf.RoundToInt(AUTO_RESTART_TIME).ToString();
        yield return new WaitForSeconds(animationTime);
        float stime = Time.time;
        while (Time.time < stime + AUTO_RESTART_TIME)
        {
            yield return null;
            if (waitingAdCallBack) stime += Time.deltaTime;
            float progLeft = 1 - Mathf.Clamp01((Time.time - stime) / AUTO_RESTART_TIME);
            timerDial.fillAmount = progLeft;
            timerNumber.text = Mathf.RoundToInt(AUTO_RESTART_TIME* progLeft).ToString();
        }
        timerDial.fillAmount = 0;
        timerNumber.text = 0.ToString();
        OnTryAgainButton();
    }

    public IEnumerator HideLevelFailed()
    {
        foreach (var item in levelFailedAnimators) item.SetTrigger(EXIT);
        retryButtonAnimator.SetTrigger(EXIT);
        yield return new WaitForSeconds(0.5f);
        levelFailedObject.SetActive(false);
    }

    //public void RequestQuestionPanel(IOption t_Option, int stepIndex)
    //{
    //    //Debug.LogError("SI: "+ stepIndex);
    //    PrankQuestion question = PrankNarrative.GetNarrative(LevelManager.Instance.GetID(), stepIndex);
    //    questionPanel.ShowQuestionPanel(question, (Correctness c) =>
    //    {
    //        switch (c)
    //        {
    //            case Correctness.WRONG:
    //                t_Option.Wrong();
    //                break;
    //            case Correctness.RIGHT:
    //                t_Option.Right();
    //                break;
    //            case Correctness.RV:
    //                t_Option.RV();
    //                break;
    //        }
    //    });
    //}
    public void RequestQuestionPanel(int stepIndex)
    {
        //Debug.LogError("SI: " + stepIndex);
        PrankQuestion question = PrankNarrative.GetNarrative(LevelManager.Instance.GetID(), stepIndex);
        questionPanel.ShowQuestionPanel(question, (Correctness c) =>
        {
            IPrankBehaviour ipb = PrankLevelBehaviour.instance;
            switch (c)
            {
                case Correctness.WRONG:
                    ipb.WrongOption();
                    break;
                case Correctness.RIGHT:
                    ipb.RightOption();
                    break;
                case Correctness.RV:
                    ipb.RVOption();
                    break;
            }
        });
    }
    


    //public void HandColorDone(IPhaseDone t_PhaseDone)
    //{
    //    doneButton.gameObject.SetActive(true);
    //    doneButton.GetComponent<Animator>().SetTrigger(ENTRY);
    //    doneButton.onClick.RemoveAllListeners();
    //    doneButton.onClick.AddListener(delegate
    //    {
    //        doneButton.GetComponent<Animator>().SetTrigger(EXIT);
    //        t_PhaseDone.Done();
    //    });
    //}
    //public void HandColorWrongDone(IPhaseWrongDone t_PhaseDone)
    //{
    //    doneButton.gameObject.SetActive(true);
    //    doneButton.GetComponent<Animator>().SetTrigger(ENTRY);
    //    doneButton.onClick.RemoveAllListeners();
    //    doneButton.onClick.AddListener(delegate
    //    {
    //        doneButton.GetComponent<Animator>().SetTrigger(EXIT);
    //        t_PhaseDone.WrongDone();
    //    });
    //}
    //public void DoneFootGlue(IPhaseDone t_PhaseDone)
    //{
    //    doneButton.gameObject.SetActive(true);
    //    doneButton.GetComponent<Animator>().SetTrigger(ENTRY);
    //    doneButton.onClick.RemoveAllListeners();
    //    doneButton.onClick.AddListener(delegate
    //    {
    //        doneButton.GetComponent<Animator>().SetTrigger(EXIT);
    //        t_PhaseDone.Done();
    //    });
    //}


    //public void DonePlastic(IPhaseDone t_PhaseDone)
    //{
    //    doneButton.gameObject.SetActive(true);
    //    doneButton.GetComponent<Animator>().SetTrigger(ENTRY);
    //    doneButton.onClick.RemoveAllListeners();
    //    doneButton.onClick.AddListener(delegate
    //    {
    //        doneButton.GetComponent<Animator>().SetTrigger(EXIT);
    //        t_PhaseDone.Done();
    //    });
    //}
}
