﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using Portbliss;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;
    
    
    public int objectiveCounter;
    
    
    public LevelOneA levelOneA;
    public LevelTwoA levelTwoA;
    public LevelThreeA levelThreeA;

    public PrankLevelBehaviour level7Gameplay;
    public PrankLevelBehaviour level20Gameplay;
    

   
    private void Awake()
    {
      if (Instance == null)
      {
        Instance = this;
      }

      levelOneA = GetComponent<LevelOneA>();
      levelTwoA = GetComponent<LevelTwoA>();
      levelThreeA = GetComponent<LevelThreeA>();
    }

    private void Start()
    {
        //if (!BootItemResolver.bootSceneWasLoaded) LoadLevel();

        string t_Scene = LevelManager.Instance.GetNameOfSceneToLoad();
        if (SceneManager.GetActiveScene().name != t_Scene)
        {
            Debug.LogError("Redirecting to Scene: " + t_Scene);
            LevelManager.LoadLevelDirect();
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            LevelManager.LoadHome();
        }
        else if (Input.GetKeyUp(KeyCode.End))
        {
            FindObjectOfType<PrankLevelBehaviour>().ForceSuccess();
        }

    }
#endif




    public void StartGame()
    {

        PrankLevelBehaviour plb = FindObjectOfType<PrankLevelBehaviour>();
        (plb as IPrankBehaviour).OnLevelStart();
       
    }

    

    public void IncreaseObjectiveCounter()
    {
      objectiveCounter++;
    }

    public void ResetObjectCounter()
    {
      objectiveCounter = 0;
    }

    //public Objective GetCurrentObjective()
    //{
    //  Objective t_Objective = new Objective();
      
    //  int t_CurrentLevel = LevelManager.Instance.GetCurrentLevelID();
    //  Debug.Log(t_CurrentLevel + " --- "+objectiveCounter);
    //  //t_Objective = scriptableObjective.levelObjectiveList[t_CurrentLevel-1].objectiveList[objectiveCounter];

    //  return t_Objective;
    //}

    //public static void LoadLevel()
    //{
    //  int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();
    //  string t_Scene = LevelManager.Instance.GetCurrentLevelSceneName() ;
    //  TimedAd.AdIteration(null, false);
    //  if (!Instance || Instance.isLevelCompleted)
    //  {
    //    SceneManager.LoadScene(t_Scene);
    //  }
    //  else
    //  {
    //    if (SceneManager.GetActiveScene().name != t_Scene)
    //    {
    //      SceneManager.LoadScene(t_Scene);
    //    }
    //  }


    //    AnalyticsAssistant.LogLevelStarted(LevelManager.Instance.GetCurrentLevel());
    //}
}
