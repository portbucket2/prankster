﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDragSaucesBottle : MonoBehaviour
{
    private Vector3 m_Offset;
    private float m_ZCoord;

    private bool m_IsDragging;

    public ParticleSystem pasteParticle;
    public ParticleSystem waterParticle;
    
    public bool isGlue;


    private void OnMouseDown()
    {
        m_ZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

        m_Offset = gameObject.transform.position - GetMouseWorldPos();

        m_IsDragging = true;
        if (isGlue)
        {
            pasteParticle.Play();
        }
        else
        {
            waterParticle.Play();
        }
    }



    private void OnMouseDrag()
    {
        if (m_IsDragging)
        {
            Vector3 t_ModifiedPos = GetMouseWorldPos() + m_Offset;
            transform.position = t_ModifiedPos;
            // if (transform.position.z < -0.1f)
            // {
            //     transform.position = new Vector3(transform.position.x,transform.position.y,-0.1f);
            // }
            //
            // if (transform.position.z > 0.7f)
            // {
            //     transform.position = new Vector3(transform.position.x,transform.position.y,0.7f);
            // }
            //
            // if (transform.position.x > .9f)
            // {
            //     transform.position = new Vector3(.9f,transform.position.y,transform.position.z);
            // }
            // if (transform.position.x < .5f)
            // {
            //     transform.position = new Vector3(.5f,transform.position.y,transform.position.z);
            // }
        }
    }

    private void OnMouseUp()
    {
        m_IsDragging = false;
        pasteParticle.Stop();
       
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 t_MousePoint = Input.mousePosition;

        t_MousePoint.z = m_ZCoord;
        return Camera.main.ScreenToWorldPoint(t_MousePoint);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(""))
        {
            Debug.LogError("Touched");
            m_IsDragging = false;
            transform.SetParent(other.transform);
            other.GetComponent<MeshRenderer>().enabled = false;
            transform.position = other.transform.position;
            transform.rotation = other.transform.rotation;
            enabled = false;
        }
    }
}
