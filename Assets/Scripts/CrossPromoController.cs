﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
public class CrossPromoController : MonoBehaviour
{
    static bool initialzed = false;
    static bool gameThinksEnabled = false;
    static bool sdkThinksEnabled = false;

    public bool log;

    private void Awake()
    {
        if (initialzed)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            initialzed = true;
        }
    }

    bool disabledExternally
    {
        get
        {
            if (!GDPRController.instance) return false;
            else if (GDPRController.isGDPRFlowOnRequestActive) return true;
            else return false;
        }
    }
    
    IEnumerator Start()
    {
        ///consider unity anchors 
        ///Y: 0.37-0.54
        ///X: 0.01-0.18
        ///which is a square image (for 1080x2160 1:2 resolution)
        float h_percentage = (0.54f-0.37f)*100; //difference
        float y_pos = (1-0.54f)*100;// from top

        float height_inPix = h_percentage * Screen.height/100.0f; 
        float width_inPix = height_inPix; //we want a square one
        float effective_percentage = width_inPix*100.0f / Screen.width; 
        // since width is the shorter dimention, both dimentions are percentage dimentions are percentages of width(positions are not)
        float x_pos = 1.0f;
        float rotation = 0;


        AppLovinCrossPromo.Init();
        AppLovinCrossPromo.Instance().HideMRec();
        sdkThinksEnabled = false;
        gameThinksEnabled = false;

        //AppLovinCrossPromo.Instance().ShowMRec(X_POS, Y_POS, WIDTH, HEIGHT, ROTATION);
        WaitForSeconds wfs1 = new WaitForSeconds(1);
        while (true)
        {
            if (sdkThinksEnabled != gameThinksEnabled && !disabledExternally)
            {
                sdkThinksEnabled = gameThinksEnabled;
                if (sdkThinksEnabled)
                {
                    if (log) Debug.Log("<color='magenta'>Cross promo enabled</color>");
                    AppLovinCrossPromo.Instance().ShowMRec(x_pos, y_pos, effective_percentage, effective_percentage, rotation);
                    yield return wfs1;//since desyncing is dangerous
                }
                else
                {
                    if (log) Debug.Log("<color='magenta'>Cross promo disabled</color>");
                    AppLovinCrossPromo.Instance().HideMRec();
                    yield return wfs1;//since desyncing is dangerous
                }
            }
            else
            {
                if (disabledExternally || !sdkThinksEnabled)
                {
                    if (log) Debug.Log("<color='magenta'>Cross promo disable enforced</color>");
                    AppLovinCrossPromo.Instance().HideMRec();
                    yield return wfs1;
                }
                else yield return null;
            }
        }
    }



    public static void Active(bool enable)
    {
        if (NoAdManager.isBlockingAds && enable)
        {
            return;
        }
        if (gameThinksEnabled != enable)
        {
            gameThinksEnabled = enable;

        }
    }
}
