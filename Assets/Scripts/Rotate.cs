﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [Range(0f, 100f)] public float rotSpeed;
    
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,rotSpeed*Time.deltaTime,0));
    }
}
