﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerLevelOne : MonoBehaviour
{
    public GameObject playerObject;
    public Animator playerAnimator;
    public SkinnedMeshRenderer playerMeshRenderer;

    public SkinMatController skinMatCon;
    //public Material playerFaceColorMAT;
    //public Material playerFacePaste;
    //public Material playerNormalMAT;
    
    private int WAKE_UP = Animator.StringToHash("wakeup");
    private int FACE_COLOR = Animator.StringToHash("facecolor");
    private int Color_ANGRY = Animator.StringToHash("colorangry");
    private int SIT_TO_STAND = Animator.StringToHash("sittostand");
    private int WATER_FAILED = Animator.StringToHash("waterfailed");
    private int SLEEP_IDEL = Animator.StringToHash("sleepidle");

    private int EXTRA_SUCCESS = Animator.StringToHash("extrasuccess");
    private int EXTRA_FAILED = Animator.StringToHash("extrafailed");
    
    public static PlayerControllerLevelOne Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayExtraSuccess()
    {
        playerAnimator.SetTrigger(EXTRA_SUCCESS);
    }

    public void PlayExtraFailed()
    {
        playerAnimator.SetTrigger(EXTRA_FAILED);
    }

    public void PlayTwoReset()
    {
        playerAnimator.SetTrigger("reset");
    }
    

    public void ChangePlayerFaceColor()
    {
        //playerMeshRenderer.sharedMaterial = playerFaceColorMAT;
        skinMatCon.SetMat(1);
    }
    
    
    public void PlayWakeUp()
    {
        playerAnimator.SetTrigger(WAKE_UP);
    }

    public void PlayFaceColor()
    {
        playerAnimator.SetTrigger(FACE_COLOR);
    }

    public void PlayColorAngry()
    {
        playerAnimator.SetTrigger(Color_ANGRY);
    }
    
    public void PlaySitToStand()
    {
        playerAnimator.SetTrigger(SIT_TO_STAND);
    }

    public void ChangePlayerFacePaste()
    {
        //playerMeshRenderer.sharedMaterial = playerFacePaste;
        skinMatCon.SetMat(2);
    }

    public void ChangePlayerOriginalMAT()
    {
        //playerMeshRenderer.sharedMaterial = playerNormalMAT;
        skinMatCon.SetMat(0);
    }

    public void PlayWaterFailed()
    {
       playerAnimator.SetTrigger(WATER_FAILED);
    }

    public void PlaySleepIdle()
    {
        playerAnimator.SetTrigger(SLEEP_IDEL);
    }

    public void PlayCarpet()
    {
        playerAnimator.SetTrigger("fly");
    }

    public void RotatePlayer()
    {
        playerAnimator.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
    }

    public void MovePosition()
    {
        playerObject.transform.position = new Vector3(0.5f,0.983f,-.35f);
    }
}
