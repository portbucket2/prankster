﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerLevelThree : MonoBehaviour
{
    public static PlayerControllerLevelThree Instance;

    public GameObject playerObject;
    public Animator playerAnimator;

    private int COUGHING = Animator.StringToHash("coughing");
    private int COUGHING_LOOP = Animator.StringToHash("coughingloop");
    private int SIP_WATER = Animator.StringToHash("sipwater");
    private int GET_UP_CHAIR = Animator.StringToHash("getupchair");
    private int RUN = Animator.StringToHash("run");
    private int SAUCE_DROP = Animator.StringToHash("saucedrop");
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayCoughing()
    {
        playerAnimator.SetTrigger(COUGHING);
    }

    public void PlayCoughingLoop()
    {
        playerAnimator.SetTrigger(COUGHING_LOOP);
    }
    public void PlaySauceDrop()
    {
        playerAnimator.SetTrigger(SAUCE_DROP);
    }

    public void PlaySipWater()
    {
        playerAnimator.SetTrigger(SIP_WATER);
    }

    public void PlayGetupChair()
    {
        playerAnimator.SetTrigger(GET_UP_CHAIR);
    }

    public void PlayRun()
    {
        playerAnimator.transform.rotation = Quaternion.identity;
        playerAnimator.SetTrigger(RUN);
    }

    public void PlayIdle()
    {
        playerAnimator.SetTrigger("idle");
    }
}
