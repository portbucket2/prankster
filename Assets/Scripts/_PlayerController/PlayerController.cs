﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject playerObject;
    public Animator playerAnimatorLevTwo;
    //public Animator playerBrushAnimator;
    //public Animator playerPissingAnimator;
    
    
    public static PlayerController Instance;

    private int WAKE_UP = Animator.StringToHash("wakeup");
    private int FACE_COLOR = Animator.StringToHash("facecolor");
    private int Color_ANGRY = Animator.StringToHash("colorangry");
    private int DOOR_OPEN = Animator.StringToHash("dooropen");
    private int SIT_TO_STAND = Animator.StringToHash("sittostand");
    private int WALK = Animator.StringToHash("walk");
    private int PISS = Animator.StringToHash("piss");
    private int BRUSH_ANGRY = Animator.StringToHash("brushangry");
    private int BRUSH_FAILED = Animator.StringToHash("brushfailed");
    private int BRUSH_IDLE = Animator.StringToHash("brushidle");
    private int PISSING = Animator.StringToHash("piss");
    private int PISSING_FAIELD = Animator.StringToHash("pissingfailed");
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayDoorOpen()
    {
        playerAnimatorLevTwo.SetTrigger(DOOR_OPEN);
    }

    

    public void PlayBrushAngry()
    {
        playerAnimatorLevTwo.SetTrigger(BRUSH_ANGRY);
    }
    public void PlayBrushIdle()
    {
        playerAnimatorLevTwo.SetTrigger(BRUSH_IDLE);
    }
    

    public void PlayWalk()
    {
        //playerAnimatorLevTwo.applyRootMotion = false;
        playerAnimatorLevTwo.SetTrigger(WALK);
    }

    public void PlayPissing()
    {
        //playerPissingAnimator.gameObject.SetActive(true);
        playerAnimatorLevTwo.SetTrigger(PISSING);
    }

    public void PlayPissingFailed()
    {
        //playerPissingAnimator.gameObject.SetActive(true);
        playerAnimatorLevTwo.SetTrigger(PISSING_FAIELD);
    }

    public void StopPissing()
    {
        playerAnimatorLevTwo.gameObject.SetActive(false);
    }

    public void PlayerMovement(Vector3 t_DestValue)
    {
        StartCoroutine(PlayerMoveRoutine(t_DestValue,1f));
    }
    public void PlayerMovementToToilet(Vector3 t_DestValue,bool t_IsRight)
    {
        PlayWalk();
        StartCoroutine(PlayerMoveToToiletRoutine(t_DestValue,1.5f,t_IsRight));
    }
    public void PlayerMoveToMirror(Vector3 t_MirrorTransPosition)
    {
        playerObject.transform.rotation = Quaternion.Euler(new Vector3(0,-90,0));
        StopPissing();
        playerAnimatorLevTwo.gameObject.SetActive(true);
        PlayWalk();
        StartCoroutine(PlayerMoveToMirrorRoutine(t_MirrorTransPosition,1.5f));
    }
    IEnumerator PlayerMoveToMirrorRoutine(Vector3 t_Dest,float t_DurationPara)
    {
        float t_Progression = 0f;
        float t_Duration = t_DurationPara;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = playerObject.transform.position;
        Vector3 t_DestValue = t_Dest;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            playerObject.transform.position = Vector3.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForEndOfFrame();
                playerObject.transform.GetChild(0).transform.localPosition = Vector3.zero;
                playerObject.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
                playerAnimatorLevTwo.gameObject.SetActive(true);
               // playerAnimatorLevTwo.gameObject.SetActive(false);
               // playerAnimatorLevTwo.applyRootMotion = true;
               PlayBrushIdle();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(PlayerMoveToMirrorRoutine(Vector3.zero,0));
    }
    IEnumerator PlayerMoveToToiletRoutine(Vector3 t_Dest,float t_DurationPara,bool t_IsRight)
    {
        float t_Progression = 0f;
        float t_Duration = t_DurationPara;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = playerObject.transform.position;
        Vector3 t_DestValue = t_Dest;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            playerObject.transform.position = Vector3.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForEndOfFrame();
                playerObject.transform.GetChild(0).transform.localPosition = Vector3.zero;
                playerObject.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
                //playerAnimatorLevTwo.gameObject.SetActive(false);
                if (t_IsRight)
                {
                    PlayPissing();
                }
                else
                {
                    PlayPissingFailed();
                }

                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(PlayerMoveToToiletRoutine(Vector3.zero,0,false));
    }
    
    IEnumerator PlayerMoveRoutine(Vector3 t_Dest,float t_DurationPara)
    {
        float t_Progression = 0f;
        float t_Duration = t_DurationPara;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = playerObject.transform.position;
        Vector3 t_DestValue = t_Dest;
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            playerObject.transform.position = Vector3.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);
 
            if (t_Progression >= 1f)
            {
                yield return new WaitForEndOfFrame();
                playerObject.transform.GetChild(0).transform.localPosition = Vector3.zero;
                playerObject.transform.rotation = Quaternion.Euler(new Vector3(0,60,0));
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(PlayerMoveRoutine(Vector3.zero,0));
    }


    public void ActivateBrushModel()
    {
      
    }

    public void PlayBrushFailed()
    {
        playerAnimatorLevTwo.SetTrigger(BRUSH_FAILED);
    }

    public void PlaySleepIdle()
    {
        
    }

    public void PlayerResetPosition(Transform playerDoorTrans)
    {
        playerAnimatorLevTwo.gameObject.SetActive(false);
      
        playerObject.transform.position = playerDoorTrans.position;
        playerObject.transform.rotation = Quaternion.Euler(new Vector3(0,60,0));
       playerAnimatorLevTwo.gameObject.SetActive(true);
    }
}

