﻿using System;
using UnityEngine;

public class MouseDrag : MonoBehaviour
{
    public ParticleSystem pasteParticle;
    //public ParticleSystem waterParticle;
    
    //public bool isGlue;
    public bool isGame;
    [Range(0f, 50f)] public float speed = 5f;
    private bool m_IsDraggingStart;
    private Vector3 m_InitialPos;
    private Vector3 m_CurrentPos;
    private Vector3 m_MoveDirection;
    private float m_RotationValue;
    public Camera mainCamera;
    private float mZCoord;
    private Vector3 mOffset;
    
    

    private void Start()
    {
        speed = 5.5f;
        //mainCamera = Camera.main;
    }
    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        
        Debug.Log(mousePoint + " Mouse point");
        // Convert it to world points
        return mainCamera.ScreenToWorldPoint(new Vector3(mousePoint.x,mousePoint.y,2.3f));
    }

    private void Update()
    {
        if (isGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mZCoord = mainCamera.WorldToScreenPoint(gameObject.transform.position).z;
                mOffset = transform.position - GetMouseAsWorldPoint();
                pasteParticle.Play();
                m_InitialPos =
                    mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            }

            if (Input.GetMouseButton(0))
            {
                m_IsDraggingStart = true;
                m_CurrentPos =
                    mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
                
                Debug.LogError(GetMouseAsWorldPoint()+ mOffset);
                transform.position = GetMouseAsWorldPoint() + mOffset;
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_IsDraggingStart = false;
                m_MoveDirection = Vector3.zero;
                m_InitialPos = Vector3.zero;
                m_CurrentPos = Vector3.zero;
                pasteParticle.Stop();
            }
        }
    }

    private void FixedUpdate()
    {
        if (m_IsDraggingStart)
        {
            // Vector3 t_Offset = m_CurrentPos - m_InitialPos;
            //
            // Vector3 t_Direction = Vector3.ClampMagnitude(t_Offset, 1);
            //
            // transform.position = new Vector3(t_Direction.x,transform.position.y,t_Direction.z);
           // transform.position = GetMouseAsWorldPoint() + mOffset;
        }
    }
}