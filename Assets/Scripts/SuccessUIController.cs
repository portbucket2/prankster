﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class SuccessUIController : MonoBehaviour
{
    private static int GetCoinPerLevel(bool isBonusLevel)
    {
        if (isBonusLevel) return 250;
        else return 50;
    }
    private static int GetBonusCoinOnAD(bool isBonusLevel)
    {
        if (isBonusLevel) return 750;
        else return 150;
    }

    [Header("Level Complete")] public GameObject root; 
    public Image rootImage;
    public List<Animator> levelCompleteAnimators = new List<Animator>();
    public List<Animator> levelCompleteDelayedAnimators = new List<Animator>();
    public Animator claimShakeAnim;
    public Text rewardText;
    public Text bonusRewardText;
    public Button nextLevelButton;
    public Button goHomeButton;
    public Button bonusRewardButton;
    public Button replayButton;


    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private int RESET = Animator.StringToHash("reset");
    private int SET = Animator.StringToHash("set");

    private bool delayedIconsExposedInCompletion = false;
    private int completedLevelNumber = -1;

    public static SuccessUIController Instance { get; private set; }
    private void Awake()
    {
        //Debug.Log("awaken");
        Instance = this;
        root.SetActive(false);

    }
    bool willOpenRewardPanel;

    LevelData wonLevelData;
    public void ReportComplete()
    {
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_PreCompletionUI);
        wonLevelData = LevelManager.SelectedLevel;
        completedLevelNumber = LevelManager.SelectedLevelNumber;
        root.SetActive(true);
        willOpenRewardPanel = GameplayRewardController.instance.ShouldLoadReward(AnimateToZero, AnimateToOne);
        foreach (var item in levelCompleteAnimators)
        {
            item.SetTrigger(ENTRY);
        }
        foreach (var item in levelCompleteDelayedAnimators)
        {
            item.SetTrigger(RESET);
        }
        if(!willOpenRewardPanel) Centralizer.Add_DelayedAct(CompletionDelayedIcons,  2);

        int coinWon = GetCoinPerLevel(wonLevelData.isBonus);
        BasicCoinTracker.ChangeCoin(coinWon);

        goHomeButton.interactable = true;
        nextLevelButton.interactable = true;
        replayButton.interactable = true;
        bonusRewardButton.interactable = true;
        rewardText.text = coinWon.ToString();
        bonusRewardText.text = (GetBonusCoinOnAD(wonLevelData.isBonus)+coinWon).ToString();

        bonusRewardButton.onClick.AddListener(OnBonusRewardButton);
        goHomeButton.onClick.AddListener(OnGoHomeButton);
        nextLevelButton.onClick.AddListener(OnNextLevelButton);
        replayButton.onClick.AddListener(OnReplayButton);

    }
    void AnimateToZero()
    {
        rootImage.enabled = false;
        foreach (var item in levelCompleteAnimators) item.SetTrigger(RESET);
        foreach (var item in levelCompleteDelayedAnimators) item.SetTrigger(RESET);
    }
    void AnimateToOne()
    {
        rootImage.enabled = true;
        foreach (var item in levelCompleteAnimators) item.SetTrigger(SET);
        foreach (var item in levelCompleteDelayedAnimators) item.SetTrigger(SET);
    }
    public void CompletionDelayedIcons()
    {
        if (delayedIconsExposedInCompletion) return;
        delayedIconsExposedInCompletion = true;
        foreach (var item in levelCompleteDelayedAnimators)
        {
            item.SetTrigger(ENTRY);
        }
    }


    private void OnGoHomeButton()
    {
        AnimateToZero();
        goHomeButton.onClick.RemoveListener(OnGoHomeButton);
        LevelManager.LoadHome();
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
    }
    private void OnNextLevelButton()
    {
        AnimateToZero();
        nextLevelButton.onClick.RemoveListener(OnNextLevelButton);
        LevelProgressionController.Proceed_IsBonus(fromHome: false);
        //LevelManager.LoadLevel_Conditional();
    }
    private void OnReplayButton()
    {
        AnimateToZero();
        LevelManager.Instance.DecreaseGameLevel();
        replayButton.onClick.RemoveListener(OnReplayButton);
        LevelManager.LoadLevelDirect();
        TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
    }
    private void OnBonusRewardButton()
    {
        goHomeButton.interactable = false;
        nextLevelButton.interactable = false;
        replayButton.interactable = false;
        bonusRewardButton.interactable = false;
        CompletionDelayedIcons();
        claimShakeAnim.SetTrigger("STOP");
        AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.currency_multiplier, completedLevelNumber);
        bool didShowRV = Portbliss.Ad.AdController.ShowRewardedVideoAd((success) =>
        {

            goHomeButton.interactable = true;
            nextLevelButton.interactable = true;
            replayButton.interactable = true;
            if (success)
            {
                int bonusCoin = GetBonusCoinOnAD(wonLevelData.isBonus);
                BasicCoinTracker.ChangeCoin(bonusCoin);
                StartCoroutine(CoinRewardAnim());
                AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.currency_multiplier, completedLevelNumber);
            }
            else
            {
                bonusRewardButton.interactable = true;
            }
        });

        if (didShowRV) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.currency_multiplier, completedLevelNumber);
    }

    IEnumerator CoinRewardAnim()
    {

        float startTime = Time.time;
        float timeToAnimate = 1;
        int stepCount = Mathf.RoundToInt(timeToAnimate / 0.05f);

        float stepTime = timeToAnimate / stepCount;
        int diff = GetBonusCoinOnAD(wonLevelData.isBonus);
        int initialValue = GetCoinPerLevel(wonLevelData.isBonus);
        int displayValue = initialValue;
        for (int i = 0; i < stepCount; i++)
        {
            yield return new WaitForSeconds(stepTime);

            float p = Mathf.Clamp01((Time.time - startTime) / timeToAnimate);
            displayValue = initialValue + Mathf.RoundToInt(diff * p);
            rewardText.text = displayValue.ToString();
        }
        displayValue = (initialValue + diff);
        rewardText.text = displayValue.ToString();

    }
}
