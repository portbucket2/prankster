﻿using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public struct LevelObjective
//{
//    public string levelNumberText;
//    public List<Objective> objectiveList;
//}

//[System.Serializable]
//public struct Objective
//{
//    public string objectiveQuestion;
//    public List<Answer> answerList;
//}

//[System.Serializable]
//public struct Answer
//{
//    public string answer;
//    public bool isRight;
//    public Sprite itemSprite;
//}

[System.Serializable]
public class PropsClass
{
    public string name;
    public Vector3 initialPos;
    public Vector3 finalPos;
    public Vector3 initialRotation;
    public Vector3 finalRotation;
    public GameObject propObject;
    public List<ParticleSystem> propsParticle;
}

[System.Serializable]
public struct CameraPosStruct
{
    public string posName;
    public Transform cameraOriginTrans;
    public List<Transform> cameraTransList;
}

[System.Serializable]
public class PrankSequence
{
    public string sequenceName;
    public List<CameraPosStruct> camPosList;
    public List<PropsClass> propsList;
}

