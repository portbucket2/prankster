﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor
{
   private LevelManager m_LevelManager;

   private void OnEnable()
   {
      m_LevelManager = (LevelManager) target;
   }

   public override void OnInspectorGUI()
    {
       //LevelManager lm = (LevelManager)target;
        serializedObject.Update();
        //GUILayout.BeginHorizontal();
       // EditorGUIUtility.labelWidth = 150;
        EditorGUILayout.PropertyField(serializedObject.FindProperty("initialLevelPreference"));
       // EditorGUIUtility.labelWidth = 50;
        EditorGUILayout.PropertyField(serializedObject.FindProperty("unlockLevelRoaming"));
       // GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
        //base.OnInspectorGUI();

      // EditorGUILayout.PropertyField(serializedObject.FindProperty("currentLevel"));
      GUILayout.BeginHorizontal();
      
      GUILayout.Label(m_LevelManager.GetEditorDisplayText(), GUILayout.MinWidth(100));

      if (GUILayout.Button("Increase", GUILayout.MinWidth(50)))
      {
         m_LevelManager.IncreaseGameLevel();
      }
      if (GUILayout.Button("Decrease", GUILayout.MinWidth(50)))
      {
         m_LevelManager.DecreaseGameLevel();
      }
      GUILayout.EndHorizontal();
   }
}
