﻿using UnityEngine;
using UnityEngine.UI;
using Portbliss.Ad;

public class LevelProgressionController : MonoBehaviour
{
    public const bool CHECHRA_MODE = true;
    public static LevelProgressionController Instance { get; private set; }

    public GameObject root;
    public Button claimButton;
    public Button skipButton;
    public Image levelPhoto;

    void Start()
    {
        Instance = this;
        root.SetActive(false);
    }

    void Load(LevelData levelData)
    {
        root.SetActive(true);

        claimButton.onClick.AddListener(OnClaim);

        skipButton.onClick.AddListener(OnSkip);
        Sprite sp = Resources.Load<Sprite>(string.Format("LevelPhotos/LevelPhoto_{0}",levelData.id));
        levelPhoto.gameObject.SetActive(sp);
        levelPhoto.sprite = sp;
    }
    void OnClaim()
    {
        claimButton.interactable = false;
        skipButton.interactable = false;
        AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.rewarded_level_unlock);
        bool willShow = AdController.ShowRewardedVideoAd((bool success) => {
            if (success)
            {
                claimButton.onClick.RemoveAllListeners();
                skipButton.onClick.RemoveAllListeners();
                LevelManager.SelectedLevel.UnlockBonusLevel();
                LevelManager.LoadLevelDirect(autoTapEnable: false);
                AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.rewarded_level_unlock);
            }
            else
            {
                claimButton.interactable = true;
                skipButton.interactable = true;
            }
        });
        if(willShow)AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.rewarded_level_unlock);
    }
    void OnSkip()
    {
        claimButton.onClick.RemoveAllListeners();
        skipButton.onClick.RemoveAllListeners();
        LevelManager.Instance.IncreaseGameLevel();
        LevelManager.LoadLevelDirect(autoTapEnable:fromHome);
        if (CHECHRA_MODE && !fromHome) TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
    }
    public static bool fromHome;
    public static bool Proceed_IsBonus(bool fromHome)
    {
        CrossPromoController.Active(false);
        LevelProgressionController.fromHome = fromHome;
        if (fromHome)
        {
            LevelData ld = LevelManager.SelectedLevel;
            if (ld.isBonusLocked)
            {
                Instance.Load(ld);
                return true;
            }
            else
            {
                LevelManager.LoadLevelDirect(autoTapEnable: true);
                return false;
            }
        }
        else
        {
            if (LevelManager.ShouldVisitHome)
            {
                LevelManager.LoadHome();
                TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
                return false;
            }
            else
            {
                LevelData ld = LevelManager.SelectedLevel;
                if (ld.isBonusLocked)
                {
                    if(CHECHRA_MODE) TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
                    Instance.Load(ld);
                    return true;

                }
                else
                {
                    LevelManager.LoadLevelDirect(autoTapEnable: false);
                    TimedAd.AB_ControlledAd(null, IntAdPlacement.Success_LevelDeparture);
                    return false;
                }
            }
        }               
    }
}
