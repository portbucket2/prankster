﻿using FRIA;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private static LevelManager instance;
    private static string GAME_LEVEL_PREFERENCE_KEY = "Game_Level_Pref";
    static bool LEVEL_ROAMING_WAS_UNLOCKED = false;
    [SerializeField] private int initialLevelPreference = 0;
    [SerializeField] private bool unlockLevelRoaming = false;

    #region Static Load Calls

    private static HardData<bool> isFirstTimeLoad;
    public static bool ShouldVisitHome
    {
        get
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return false;
            int N = 0;// ABManager.GetValueInt(ABtype.home_return_freq);
            switch (N)
            {
                default: return ((currentSelectionNumber - 1) % N == 0);
                case 0: return false;
                    //case 1: return true;
                    //case 2: return (GetLatestReachedLevelNumber() >= 4 && (instance.currentEffectiveLevelNumber+0) % 2 == 0);
                    //case 3: return (GetLatestReachedLevelNumber() >= 4 && (instance.currentEffectiveLevelNumber+2) % 3 == 0);
            }
        }
    }
    //public static bool autoTap { get; private set; }
    public static bool showStartScreen { get; private set; }
    public static PrankCollectibleProfile pendingCollectible;
    //public static bool blockTapButton { get { return pendingCollectible != null; } }
    public static void LoadHome()
    {
        SceneManager.LoadScene(BuildSceneInfo.BRIDGE_SCENE_NAME);
    }

    public static void LoadLevelDirect(bool autoTapEnable = false, int LevelNumber = -1)
    {

        if (LevelNumber < 0) LevelNumber = SelectedLevelNumber;
        else instance.ForceCurrentEffectiveNumberTo(LevelNumber);

        pendingCollectible = PrankCollectibleManager.GetProfile_ByLevel(SelectedLevel.id);
        if (SelectedLevelNumber == ReviewPromptManager.LEVEL_START_NUMBER_TO_SHOW)
        {
            Debug.Log("Auto tap disabled for review level");
            showStartScreen = true;
        }
        else showStartScreen = !autoTapEnable;
        


        string t_Scene = Instance.GetNameOfSceneToLoad();

        SceneManager.LoadScene(t_Scene);
        AnalyticsAssistant.LogLevelStarted(SelectedLevelNumber);
    }

    public static AsyncOperation FirstLoadAsync()
    {
        if (isFirstTimeLoad == null) isFirstTimeLoad = new HardData<bool>("IsFirstTimeLoadHD", true);
        string t_Scene = Instance.GetNameOfSceneToLoad();
        if (!isFirstTimeLoad)
        {
            t_Scene = BuildSceneInfo.BRIDGE_SCENE_NAME;
        }
        else
        {
            showStartScreen = true;
            isFirstTimeLoad.value = false;
        }
        AsyncOperation asop = SceneManager.LoadSceneAsync(t_Scene);
        asop.completed += (AsyncOperation asp) => {
            if (t_Scene != BuildSceneInfo.BRIDGE_SCENE_NAME)
                AnalyticsAssistant.LogLevelStarted(SelectedLevelNumber);
        };
        return asop;
    }

    #endregion

    #region Current Level Management
    private static HardData<int> _latestLevel;
    private static void InitializeCurrentNumbers()
    {
        if (_latestLevel == null)
        {
            int presetInitialNumber = instance? instance.initialLevelPreference:0;
            presetInitialNumber = presetInitialNumber >= 1 ? presetInitialNumber : 1;
            _latestLevel = new HardData<int>(GAME_LEVEL_PREFERENCE_KEY, presetInitialNumber);
            _currentEffectiveLevelNumberPointer = _latestLevel;
        }
    }

    private static int _currentEffectiveLevelNumberPointer = -1;
    private static int currentSelectionNumber
    {
        get
        {
            InitializeCurrentNumbers();
            return _currentEffectiveLevelNumberPointer;
        }
        set
        {
            InitializeCurrentNumbers();
            _currentEffectiveLevelNumberPointer = value;
            if (value > _latestLevel)
            {
                _latestLevel.value = value;
                LifetimeEvents.Report_NewLevelReached(value);
            }
        }
    }
    public void IncreaseGameLevel()
    {
        if (Application.isPlaying)
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return;
        }
        else
        {
            if (!unlockLevelRoaming) return;
        }
        currentSelectionNumber++;
        //Debug.LogError("CurrentLevelIndex Increased To" + currentEffectiveLevelNumber);
    }
    public void DecreaseGameLevel()
    {
        if (Application.isPlaying)
        {
            if (!LEVEL_ROAMING_WAS_UNLOCKED) return;
        }
        else
        {
            if (!unlockLevelRoaming) return;
        }
        currentSelectionNumber--;
        if (currentSelectionNumber <= 0) currentSelectionNumber = 1;
        //Debug.LogError(currentEffectiveLevelNumber + " CurrentLevelIndex"); ;
    }
    public void ForceCurrentEffectiveNumberTo(int value)
    {
        currentSelectionNumber = value;
    }
    public static int GetLatestReachedLevelNumber()
    {
        InitializeCurrentNumbers();
        return _latestLevel.value;
    }
    public static int SelectedLevelNumber
    {
        get
        {
            return currentSelectionNumber;
        }
    }
    public static LevelData SelectedLevel
    {
        get
        {
            return BuildSceneInfo.GetLevelData(currentSelectionNumber);
        }
    }
    #endregion

    public string GetID()
    {
        if (LEVEL_ROAMING_WAS_UNLOCKED)
        {
            return SelectedLevel.id;
        }
        else return SceneManager.GetActiveScene().name.Split('l')[1];// initialLevelPreference.ToString();
    }





    void Awake() 
    {
        instance = this;
        if (unlockLevelRoaming)
        {
            LEVEL_ROAMING_WAS_UNLOCKED = true;
        }
    }


    public static LevelManager Instance
    {
        get
        {
            return instance;
        }
    }



    public string GetEditorDisplayText()
    {
        if (!PlayerPrefs.HasKey(GAME_LEVEL_PREFERENCE_KEY))
        {
            return "Saved Index: NULL";
        }
        else
        {
            return string.Format("Saved Index: {0}", BuildSceneInfo.GetLevelData( currentSelectionNumber).id);
        }
    }


    public string GetDisplayTextForLevelNumber(int levelNumber = -1)
    {
        if (levelNumber <= 0) levelNumber = currentSelectionNumber;
        if (!LEVEL_ROAMING_WAS_UNLOCKED)
        {
            levelNumber = instance.initialLevelPreference;
        }
        else if (SelectedLevel!=null && SelectedLevel.isBonus)
        {
            return "Bonus Level";
        }
        return string.Format("Level {0}",levelNumber);
    }

    public string GetNameOfSceneToLoad(int i = -1)
    {
        if (i < 0) i = currentSelectionNumber;
        if (!LEVEL_ROAMING_WAS_UNLOCKED) return  UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        return BuildSceneInfo.GetLevelData(i).sceneName;
    }



}