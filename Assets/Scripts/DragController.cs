﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragController : MonoBehaviour
{
    public Camera mainCamera;
   // public Transform target;
    public DragDirection dragDirection;
    public bool isUseRotation;
    public float rotationBoundaryValue;
    
    
    private bool m_IsDraggingStart;
    private Vector3 m_InitialPos;
    private Vector3 m_CurrentPos;
    private Vector3 m_MoveDirection;
    private float m_RotationValue;

    public bool m_IsGameStart;
    
    private static DragController m_Instance;
    public delegate void OnButtonUpDelegate ();

    public OnButtonUpDelegate buttonUpDelegate;


    public MouseDrag mouse;
    

    public static DragController Instance
    {
        get { return m_Instance; }
    }

    public void StartGame()
    {
        m_IsGameStart = true;
    }

    public Vector3 MoveDirection
    {
        get { return m_MoveDirection; }
    }

    public float RotationValue
    {
        get { return m_RotationValue; }
    }

    public DragDirection DragDirection
    {
        set { dragDirection = value; }
    }
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    
    
    private void Update()
    {
        if (m_IsGameStart)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_InitialPos =
                    mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            }

            if (Input.GetMouseButton(0))
            {
                m_IsDraggingStart = true;
                m_CurrentPos =
                    mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_IsDraggingStart = false;
                m_MoveDirection = Vector3.zero;
                m_InitialPos = Vector3.zero;
                m_CurrentPos = Vector3.zero;
            }
        }
    }

    private void FixedUpdate()
    {
        if (m_IsDraggingStart)
        {
            Vector3 t_Offset = m_CurrentPos - m_InitialPos;
            
          
            Vector3 t_Direction = Vector3.ClampMagnitude(t_Offset, 1);
            
            m_MoveDirection = GetDirectionBasedonSetup(t_Direction);
            
           
            if (isUseRotation)
            {
                m_RotationValue = CalculateRotation(t_Direction);
            }
        }
    }

    private float CalculateRotation(Vector3 t_Direction)
    {
        float t_CalculatedRotation = 0f;
        
        
        if (dragDirection == DragDirection.NONE)
        {
            
        }
        else if (dragDirection == DragDirection.VERTICAL)
        {
            
        }
        else if (dragDirection == DragDirection.HORIZONTAL)
        {
            float t_TotalRotationValue = rotationBoundaryValue * 2;
            float t_TotalMovementValue = 1f * 2f;

            float t_PerUnitRotationValue = t_TotalRotationValue * 1f / t_TotalMovementValue;

            t_CalculatedRotation = t_PerUnitRotationValue * t_Direction.x;
        }
        
        return t_CalculatedRotation;
    }
    
    

    private Vector3 GetDirectionBasedonSetup(Vector3 t_Direction)
    {
        Vector3 t_ModifiedDirection = Vector3.zero;
        
        if (dragDirection == DragDirection.NONE)
        {
            t_ModifiedDirection = Vector3.zero;
        }
        else if (dragDirection == DragDirection.VERTICAL)
        {
            t_ModifiedDirection = new Vector3(0,t_Direction.y,0);
        }
        else if (dragDirection == DragDirection.HORIZONTAL)
        {
            t_ModifiedDirection = new Vector3(t_Direction.x,0,t_Direction.z);
        }

        return t_ModifiedDirection;
    }
}

public enum DragDirection
{
    NONE,
    HORIZONTAL,
    VERTICAL
}
