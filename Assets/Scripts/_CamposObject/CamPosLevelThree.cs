﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPosLevelThree : MonoBehaviour
{
   public static CamPosLevelThree Instance;

   [Header("Burger Pos")] public Transform burgerCamPosTop;
   public List<Transform> burgerCamPosList;

   [Header("Burger Eat Pos")] public Transform burgerEatPlayerPos;
   [Header("Glass Top")] public Transform glassTopCamPos;
   public List<Transform> glassTopCapPosList;
   private void Awake()
   {
      if (Instance == null)
      {
         Instance = this;
      }
   }
}
