﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPosObjectHolder : MonoBehaviour
{
    public static CamPosObjectHolder Instance;

    [Header("Cam Initial Pos")] 
    public Transform camInitialOrigin;
    public List<Transform> camInitialOriginList;
    
    [Header("Hand Pos")]
    public Transform handOriginTrans;
    public List<Transform> handShowTransList;


    [Header("Clock Pos")] public Transform clockOriginTrans;
    public List<Transform> clockTransList;
    
    [Header("Wakeup Pos")] public Transform wakeUpOriginTrans;
    public List<Transform> wakeUpTransList;
    [Header("Foot Pos")] public Transform footOriginTrans;
    public List<Transform> footTransList;
    
    [Header("FallDown Pos")] public Transform fallDownOriginTrans;
    public List<Transform> fallDownTransList;

    public Transform temp;
    public List<Transform> tempList;
    
    [Header("Toilet Pos")] public Transform toiletOriginTrans;
    public List<Transform> toiletTransList;
    [Header("Toilet Pos Player")] public Transform toiletPlayerOriginTrans;
    public List<Transform> toiletPlayerTransList;
    
    [Header("Mirror Pos Player")] public Transform mirrorOriginTrans;
    public List<Transform> mirrorTransList;
    [Header("Brush Pos Player")] public Transform brushOriginTrans;
    public List<Transform> brushTransList;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
}
