﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPosLevelOne : MonoBehaviour
{
    [Header("Hand Pos")]
    public Transform handOriginTrans;
    public List<Transform> handShowTransList;


    [Header("Clock Pos")] public Transform clockOriginTrans;
    public List<Transform> clockTransList;
    
    [Header("Wakeup Pos")] public Transform wakeUpOriginTrans;
    public List<Transform> wakeUpTransList;
    [Header("Foot Pos")] public Transform footOriginTrans;
    public List<Transform> footTransList;
    
    [Header("FallDown Pos")] public Transform fallDownOriginTrans;
    public List<Transform> fallDownTransList;
    
    public Transform temp;
    public List<Transform> tempList;

    [Header("Extra Level Segment")] 
    public Transform watchTrans;
    public List<Transform> watchTransList;

    public Transform extraShowTrans;
    public List<Transform> extraShowTransList;
    
    
    public static CamPosLevelOne Instance;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
}
