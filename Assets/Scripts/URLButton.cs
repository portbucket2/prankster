﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class URLButton : MonoBehaviour
{
    public string URLtoOpen;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => Application.OpenURL(URLtoOpen));
    }
}
