﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class LevelEndParticle : MonoBehaviour
{
    private static  List<ParticleSystem> ps = new List<ParticleSystem>();

    private ParticleSystem p;
    void Start()
    {
        p = GetComponent<ParticleSystem>();
        if (p) ps.Add(p);
    }
    private void OnDestroy()
    {
        if(p)ps.Remove(p);
    }

    // Update is called once per frame
    public static void PlayIfAvailable()
    {
        foreach (ParticleSystem p in ps)
        {
            if (p)
            {
                p.Play();
            }
        }
    }
}
