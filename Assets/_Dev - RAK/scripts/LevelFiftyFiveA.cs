﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelFiftyFiveA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    //public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform phase1Fail1;
    public Transform phase1Success1pos;
    public Transform phase1Success1focus;


    [Header("particles")]
    public ParticleSystem assOnFireParticle;
    public ParticleSystem assOnIceParticle;
    public ParticleSystem smokeOnSlideParticle;
    public ParticleSystem spikeHitParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator phase1CameraRoll;
    public Animator GreaseCanAnimator;
    public Animator sliderAnimator;
    public Animator blowTorchAnimator;
    public Animator iceBucketAnimator;
    public Animator icePoolAnimator;
    public Animator spikeAnimator;
    public MeshRenderer tubRenderer;
    public Material tubNormalMat;
    public Material tubIceMat;


    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Common1 = "common1";
    readonly string Common2 = "common2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Move = "move";
    readonly string Default = "default";
    readonly string Flower = "flower";
    readonly string Fart = "fart";
    readonly string COLOR = "_Color";

    bool isAnsRight = false;
    List<Transform> focuslist;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            blowTorchAnimator.SetTrigger(Move);
            sliderAnimator.SetTrigger(Move);
            smokeOnSlideParticle.Play();
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            spikeAnimator.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            GreaseCanAnimator.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            iceBucketAnimator.SetTrigger(Move);
            tubRenderer.material = tubIceMat;
            icePoolAnimator.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        //focuslist = new List<Transform>();
        //focuslist.Add(zoom1Focus);
        //CameraMovementController.Instance.FollowCamera(zoom1Pos, focuslist);
        /*phase one
        */

        rigAnimator.SetTrigger(IDLE1);
        //propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;

        yield return WAITTHREE;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 0;
        //propAnimator.speed = 0;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 1;
        //propAnimator.speed = 1;
        playerPosition.Follow();
        if (isAnsRight)
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            rigAnimator.SetTrigger(Common1);
            rigAnimator.SetTrigger(Success1);
            //propAnimator.SetTrigger(Success1);

            yield return WAITONE;
            phase1CameraRoll.SetTrigger(Move);
            focuslist = new List<Transform>();
            focuslist.Add(phase1Success1focus);
            CameraMovementController.Instance.FollowCamera(phase1Success1pos, focuslist);
            yield return WAITTWO;
            assOnFireParticle.Play();
            yield return WAITTWO;
            yield return WAITTHREE;

            isPhaseOne = false;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Common1);
            rigAnimator.SetTrigger(Fail1);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(phase1Fail1, focuslist);
            
            //propAnimator.SetTrigger(Fail1);
            yield return WAITFOUR;
            //CameraMovementController.Instance.UnfollowCamera();
            
            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        tubRenderer.material = tubNormalMat;
        assOnFireParticle.Stop();
        assOnIceParticle.Stop();
        CameraMovementController.Instance.UnfollowCamera();
        rigAnimator.SetTrigger(Common2);
        rigAnimator.speed = 0;
        //propAnimator.SetTrigger(IDLE2);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        playerPosition.UnFollow();
        if (isAnsRight)
        {
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            yield return WAITONE;
            rigAnimator.speed = 1;
            rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            //propAnimator.SetTrigger(Success2);

            yield return WAITONE;
            spikeHitParticle.Play();
            skinMatController.SetMat(1);
            yield return WAITTWO;
            playerPosition.Follow();
            yield return WAITTWO;

            ReportPhaseCompletion(2,  true);
            inputComplete = false;
        }
        else
        {
            assOnFireParticle.Stop();
            assOnIceParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            yield return WAITONE;
            rigAnimator.speed = 1;
            rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Fail2);
            //propAnimator.SetTrigger(Fail2);

            yield return WAITTHREE;
            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTWO;


            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }
        inputComplete = false;
        playerPosition.UnFollow();
    }

}

