﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelFiftySixA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;
    public Animator bonusAnimator;

    public bool isPhaseOne = true;
    public bool isBonus = false;
    public bool inputComplete = false;

    public GameObject nuclear;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndPos2;
    public Transform cameraEndFocus;
    public Transform preEndPos;
    public Transform preEndFocus;
    public Transform cameraEndPos3;


    [Header("particles")]
    public ParticleSystem eggSplashParticle;
    public ParticleSystem bowlingFallParticle;
    public ParticleSystem bombParticle;
    public ParticleSystem groundbombParticle;
    public ParticleSystem somkeParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject bowlingBall;
    public Animator ballAnimator;
    public Animator bombAnimator;
    public GameObject gloves;
    public Animator powerDial;
    public Animator ballMachine;


    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Transition = "transition";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Right = "right";
    readonly string Wrong = "wrong";
    readonly string Egg = "egg";
    readonly string Bowling = "bowling";
    readonly string Default = "default";
    readonly string Move = "move";
    readonly string Bonus = "bonus";

    bool isAnsRight = false;
    List<Transform> focuslist;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else if(isBonus)
        {

        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            ballAnimator.SetTrigger(Bowling);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            powerDial.SetTrigger(Right);
            ballMachine.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            ballAnimator.SetTrigger(Egg);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            powerDial.SetTrigger(Wrong);
            isAnsRight = false;
            inputComplete = true;
        }
    }

    protected override void RVAnswerSelected()
    {
        bonusAnimator.gameObject.SetActive(true);
        bombAnimator.SetTrigger(Bonus);
        
        StartCoroutine(RVSelectedPhaseRoutine());
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }

    IEnumerator RVSelectedPhaseRoutine()
    {
        yield return WAITHALF;
        ballMachine.SetTrigger(Move);
        yield return WAITONE;
        
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
        yield return WAITHALF;
        propAnimator.gameObject.SetActive(false);

        rigAnimator.SetTrigger(Bonus);
        bonusAnimator.SetTrigger(Bonus);
        yield return WAITFOUR;
        yield return WAITONE;
        bombParticle.Play();
        groundbombParticle.Play();
        somkeParticle.Play();
        nuclear.SetActive(false);
        skinMatController.SetMat(1);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(cameraEndPos3, focuslist);
        currentPhaseIndex++;
        yield return WAITTWO;
        ReportPhaseCompletion(2,  true);
        inputComplete = false;

        Debug.Log("RV Level Done");
    }


    IEnumerator StartPhaseOneRoutine()
    {
        //focuslist = new List<Transform>();
        //focuslist.Add(preEndFocus);
        //CameraMovementController.Instance.FollowCamera(preEndPos, focuslist);
        /*phase one
        */
        bowlingBall.SetActive(false);
        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;

        yield return WAITTHREE;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        yield return WAITHALF;
        rigAnimator.speed = 0.5f;
        propAnimator.speed = 0.5f;
        yield return WAITHALF;
        rigAnimator.speed = 0;
        propAnimator.speed = 0;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 1;
        propAnimator.speed = 1;
        playerPosition.UnFollow();
        if (isAnsRight)
        {
            bowlingBall.SetActive(true);
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);

            bowlingFallParticle.Play();

            yield return WAITONE;
            gloves.SetActive(false);
            //focuslist = new List<Transform>();
            //focuslist.Add(phase1Success1focus);
            //CameraMovementController.Instance.FollowCamera(phase1Success1pos, focuslist);
            yield return WAITFOUR;

            isPhaseOne = false;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            eggSplashParticle.Play();
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            yield return WAITTWO;
            //CameraMovementController.Instance.UnfollowCamera();

            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        powerDial.SetTrigger(Default);
        bowlingBall.SetActive(false);
        gloves.SetActive(false);
        playerPosition.UnFollow();
        CameraMovementController.Instance.UnfollowCamera();
        focuslist = new List<Transform>();
        focuslist.Add(cameraEndFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

        rigAnimator.SetTrigger(Transition);
        propAnimator.SetTrigger(Transition);

        yield return WAITFOUR;

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(preEndFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(preEndPos, focuslist);
        
        yield return WAITTWO;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        if (isAnsRight)
        {
            playerPosition.Follow();
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            yield return WAITONE;
            //rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);

            yield return WAITONE;
            rigAnimator.speed = 0.5f;
            propAnimator.speed = 0.5f;

            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);

            yield return WAITONE;
            rigAnimator.speed = 0.2f;
            propAnimator.speed = 0.2f;
            yield return WAITTHREE;
            yield return WAITHALF;
            rigAnimator.speed = 1f;
            propAnimator.speed = 1f;
            yield return WAITTWO;

            ReportPhaseCompletion(2,  true);
            inputComplete = false;
        }
        else
        {

            CameraMovementController.Instance.UnfollowCamera();
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraEndFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos2, focuslist);
            //yield return WAITONE;
            rigAnimator.speed = 1;
            propAnimator.speed = 1;
            //rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);

            yield return WAITHALF;

            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos2, focuslist);
            yield return WAITFOUR;


            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }
        inputComplete = false;
        playerPosition.UnFollow();
    }

}

