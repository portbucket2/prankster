﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelThirtyOneA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartPos2;
    public Transform cameraStartFocus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform zoomGaragePos;
    public Transform zoomGarageFocus;
    public Transform zoomMowerPos;
    public Transform zoomMowerFocus;
    public Transform zoomCatPos;
    public Transform zoomCatFocus;
    public Transform zoomMowerPos2;
    public Transform zoomMowerPos3;


    [Header("particles")]
    public ParticleSystem hitGroundParticle;
    public ParticleSystem bloodSplash;
    public ParticleSystem loudSound;
    public ParticleSystem grassCuttiingParticle;
    public ParticleSystem thoughtBubbleParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject ghostInsert;
    public GameObject glueInsert;
    public Animator catHolder;
    public Animator camera2Move;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Transition = "transition";
    readonly string Bal = "bal";
    readonly string Air = "air";
    readonly string Default = "default";
    readonly string Move = "move";

    bool isAnsRight = false;
    List<Transform> focuslist;
    //private PlayerFocusForCamera playerFocus;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            ghostInsert.SetActive(true);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            catHolder.SetTrigger(Bal);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            glueInsert.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            catHolder.SetTrigger(Air);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        */

        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(zoomGarageFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomGaragePos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(cameraStartPos, focuslist);
        playerPosition.Follow();
        yield return WAITTWO;
        ghostInsert.SetActive(false);
        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            yield return WAITFOUR;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraStartPos2, focuslist);
            camera2Move.SetTrigger(Move);
            yield return WAITFOUR;
            yield return WAITFOUR;

            isPhaseOne = false;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);

            yield return WAITFOUR;
            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraStartFocus, focuslist);
            yield return WAITFOUR;
            glueInsert.SetActive(false);

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        catHolder.SetTrigger(Default);
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(zoomMowerPos2, focuslist);
        yield return ENDOFFRAME;
        rigAnimator.SetTrigger(Transition);
        propAnimator.SetTrigger(Transition);
        yield return ENDOFFRAME;
        grassCuttiingParticle.Play();
        //grassSpotParticle.Play();
        
        yield return WAITTHREE;
        CameraMovementController.Instance.UnfollowCamera();

        focuslist = new List<Transform>();
        focuslist.Add(zoomCatFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomCatPos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;


        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);

            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(zoomMowerPos2, focuslist);
            yield return WAITTHREE;
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FollowCamera(zoomMowerPos3, focuslist);

            
            yield return WAITONE;
            bloodSplash.Play();
            catHolder.gameObject.SetActive(false);
            grassCuttiingParticle.Stop();
            thoughtBubbleParticle.Play();
            yield return WAITTHREE;

            ReportPhaseCompletion(2,  true);
            inputComplete = false;
        }
        else
        {
            
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);

            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(zoomMowerPos2, focuslist);

            yield return WAITTHREE;
            loudSound.Play();

            yield return WAITTHREE;
            grassCuttiingParticle.Pause();

            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }

}

