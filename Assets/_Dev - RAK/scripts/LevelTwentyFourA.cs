﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelTwentyFourA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform zoomPorchPos;
    public Transform zoomPorchFocus;
    public Transform zoomDoorPos;
    public Transform zoomDoorFocus;


    [Header("particles")]
    public ParticleSystem hitGroundParticle;
    public ParticleSystem fireParticle;
    public ParticleSystem heartParticle;
    public ParticleSystem greenGooParticle;
    public ParticleSystem stringHitParticle;
    public ParticleSystem plasticHitParticle;
    public ParticleSystem dingDongParticle;
    public ParticleSystem kittyPickUpParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject sting;
    public GameObject brokenSting;
    public GameObject cat;
    public GameObject fakeCat;
    public GameObject catBasket;
    public GameObject paperBag;
    public GameObject plasticWrap;
    public GameObject fakePlasticWrap;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Transition = "transition";
    readonly string Move = "move";
    readonly string Default = "default";

    bool isAnsRight = false;
    List<Transform> focuslist;
    //private PlayerFocusForCamera playerFocus;
    // prank level behavior start

    protected override void StartLevel()
    {
        //playerFocus = characterPos.GetComponent<PlayerFocusForCamera>();
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            paperBag.SetActive(true);
            fireParticle.Play();
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            fakePlasticWrap.SetActive(true);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            
            catBasket.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            sting.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        */
        cat.SetActive(false);
        catBasket.SetActive(false);
        paperBag.SetActive(false);
        sting.SetActive(false);
        brokenSting.SetActive(false);
        plasticWrap.SetActive(false);

        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(zoomPorchFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomPorchPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        dingDongParticle.Play();
        yield return WAITONE;
        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            

            
            yield return WAITTHREE;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            greenGooParticle.Play();
            yield return WAITONE;
            skinMatController.SetMat(1);
            fireParticle.Stop();
            paperBag.SetActive(false);
            greenGooParticle.Play();
            yield return WAITTWO;

            isPhaseOne = false;
            yield return WAITONE;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);

            yield return WAITTHREE;
            cat.SetActive(true);
            fakeCat.SetActive(false);
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            heartParticle.Play();
            yield return WAITONE;
            kittyPickUpParticle.Play();
            yield return WAITTWO;


            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        rigAnimator.SetTrigger(Transition);
        propAnimator.SetTrigger(Transition);
        brokenSting.SetActive(false);

        focuslist = new List<Transform>();
        focuslist.Add(zoomDoorFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomDoorPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;


        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);
            plasticWrap.SetActive(true);
            fakePlasticWrap.SetActive(false);

            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            yield return WAITTWO;
            plasticHitParticle.Play();
            hitGroundParticle.Play();
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            //phone.SetActive(false);
            yield return WAITTHREE;
            //yield return WAITONE;
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);

            yield return WAITTWO;
            stringHitParticle.Play();
            sting.SetActive(false);
            brokenSting.SetActive(true);
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTHREE;


            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }

}

