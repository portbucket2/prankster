﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelEighteenA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Transform playerHolder;
    public Animator rigAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform cameraStartFocus2;
    public Transform showerPos;
    public Transform showerfocus;

    [Header("particles")]
    public ParticleSystem showerParticle;
    public ParticleSystem hotWaterParticle;
    public ParticleSystem snowFallParticle;


    [Header("objects and other animators")]
    public SkinMatController skinmatController;
    public SkinnedMeshRenderer body;
    public MeshRenderer tubWater;
    public Material bathIceMat;
    public Animator iceCubeHolder;
    public Animator hotKettleHolder;
    public Animator colorNutellaHolder;
    //public Material normalMat;
    //public Material colorMat;
    //public Material nutellaMat;
    
    //public Transform sunLight;//

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";

    readonly string Transition1 = "transition";
    readonly string Move = "move";
    readonly string LerpValue = "LerpValue";
    readonly string MaskStrength = "_MaskStrength";
    readonly string Color = "color";
    readonly string Nutella = "nutella";

    bool isAnsRight = false;
    List<Transform> focuslist;
    Vector3 playerPosPhOne = new Vector3(-0.047f, 0.08f, -0.07f);
    Vector3 playerPosPhTwo = new Vector3(3.377f, 0.08f, -2.37f);

    // prank level behavior start

    protected override void StartLevel()
    {
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            //rigAnimator.SetTrigger(Common1);
            iceCubeHolder.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(stringCamFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
            colorNutellaHolder.SetTrigger(Color);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            hotKettleHolder.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoActionFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
            colorNutellaHolder.SetTrigger(Nutella);
            isAnsRight = false;
            inputComplete = true;
        }
    }



    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }
            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
         */
        hotWaterParticle.Stop();
        playerHolder.position = playerPosPhOne;
        rigAnimator.SetTrigger(IDLE2);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraStartFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITHALF;
        // do here the prank job detail

        yield return WAITTHREE;
        //focuslist = new List<Transform>();
        //focuslist.Add(monitorZoomFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(monitorZoom, focuslist);

        if (isAnsRight)
        {
            tubWater.material = bathIceMat;
            float temp = 5f;
            float Val = -5f;
            while (Val < temp)
            {
                Val += Time.deltaTime * 5;
                bathIceMat.SetFloat(LerpValue, Val);
                yield return ENDOFFRAME;
            }
            rigAnimator.SetTrigger(Success1);

            //yield return WAITONE;

            
            
            yield return WAITTHREE;
            isPhaseOne = false;
            inputComplete = false;
            yield return WAITONE;

            ReportPhaseCompletion(1, false);

            yield return WAITONE;
            StartLevel();
            // start second stage after delay
        }
        else
        {
            hotWaterParticle.Play();
            rigAnimator.SetTrigger(Fail1);
            yield return WAITONE;
            //phoneRingParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            inputComplete = false;
            yield return WAITTWO;
            
            ReportPhaseCompletion(1,  false);
            //IManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {

        /*phase two
        */
        playerHolder.position = playerPosPhOne;
        showerParticle.Play();
        focuslist = new List<Transform>();
        focuslist.Add(showerfocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(showerPos, focuslist);
        yield return WAITHALF;
        playerHolder.position = playerPosPhTwo;
        rigAnimator.SetTrigger(Transition1);

        yield return WAITTHREE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        showerParticle.Pause();
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        //CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos5, focuslist);
        //rigAnimator.SetTrigger(IDLE2);
        //playerLevelFive.transform.position = secondPos;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;
        showerParticle.Play();
        //

        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success2);
            yield return WAITONE;
            //body.material = colorMat;
            skinmatController.SetMat(1);
            float temp = 1f;
            float Val = 0f;
            while (Val < temp)
            {
                Val += Time.deltaTime * 2;
                //colorMat.SetFloat(MaskStrength, Val);
                skinmatController.SetMatValue(1, MaskStrength, Val);
                yield return ENDOFFRAME;
            }
            
            yield return WAITTWO;
            //fallFlatParticle.Play();
            //particles
            yield return WAITTWO;
            yield return WAITTHREE;
            yield return WAITONE;
            showerParticle.Pause();
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            
            inputComplete = false;
        }
        else
        {
            rigAnimator.SetTrigger(Fail2);
            yield return WAITONE;
            //body.material = nutellaMat;
            skinmatController.SetMat(2);
            float temp = 1f;
            float Val = 0f;
            while (Val < temp)
            {
                Val += Time.deltaTime * 2;
                //nutellaMat.SetFloat(MaskStrength, Val);
                skinmatController.SetMatValue(2, MaskStrength, Val);
                yield return ENDOFFRAME;
            }
            //waterSplashParticle.Play();
            yield return WAITONE;
            yield return WAITTWO;
            inputComplete = false;

            yield return WAITTHREE;
            yield return WAITONE;
            temp = 0f;
            Val = 1f;
            while (Val > temp)
            {
                Val -= Time.deltaTime * 2;
                //nutellaMat.SetFloat(MaskStrength, Val);
                skinmatController.SetMatValue(2, MaskStrength, Val);
                yield return ENDOFFRAME;
            }

            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            yield return WAITONE;
            showerParticle.Pause();
        }

        inputComplete = false;

    }



}
