﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;
using UnityEngine.Events;

public class LevelTwentyFourAExtra : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform zoomPorchPos;
    public Transform zoomPorchFocus;
    public Transform zoomDoorPos;
    public Transform zoomDoorFocus;


    [Header("particles")]
    public ParticleSystem hitGroundParticle;
    public ParticleSystem fireParticle;
    public ParticleSystem heartParticle;
    public ParticleSystem greenGooParticle;
    public ParticleSystem stringHitParticle;
    public ParticleSystem plasticHitParticle;
    public ParticleSystem dingDongParticle;
    public ParticleSystem kittyPickUpParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject sting;
    public GameObject brokenSting;
    public GameObject cat;
    public GameObject fakeCat;
    public GameObject catBasket;
    public GameObject paperBag;
    public GameObject plasticWrap;
    public GameObject fakePlasticWrap;

    [Header("GM Modification")] 
    public bool isExtraPhase;
    public List<PrankSequence> prankSequenceList;
    
    
    
    
    
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Transition = "transition";
    readonly string Move = "move";
    readonly string Default = "default";

    bool isAnsRight = false;
    List<Transform> focuslist;
    //private PlayerFocusForCamera playerFocus;
    // prank level behavior start
    
    
    
    

    private void Start()
    {
    }

    protected override void StartLevel()
    {
        //playerFocus = characterPos.GetComponent<PlayerFocusForCamera>();
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else if (isExtraPhase)
        {
            StopAllCoroutines();
            StartCoroutine(StartExtraPhaseRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    private IEnumerator StartExtraPhaseRoutine()
    {
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            });
        
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        
        yield return ENDOFFRAME;
        if (isAnsRight)
        {
            //Right
            yield return WAITONE;
            prankSequenceList[0].propsList[1].propsParticle[0].Play();
            yield return WAITTWO;
            prankSequenceList[0].propsList[1].propsParticle[0].Stop();
            yield return new WaitForSeconds(0.65f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                });
            yield return WAITONE;
            
            rigAnimator.SetTrigger("extrasuccess");

            yield return WAITFOUR;
            yield return WAITTWO;
            inputComplete = false;
            isExtraPhase = false;
            
            ReportPhaseCompletion(2,  false);
            
            StartLevel();
        }
        else
        {
            //Wrong
            yield return WAITONE;
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
                prankSequenceList[0].propsList[0].propObject, delegate
                {
                    prankSequenceList[0].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>()
                        .enabled = true;
                    prankSequenceList[0].propsList[0].propsParticle[0].Play();
                }));

            yield return WAITTHREE;
            prankSequenceList[0].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            prankSequenceList[0].propsList[0].propsParticle[0].Stop();
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].initialPos,
                prankSequenceList[0].propsList[0].propObject, delegate
                {
                }));
            
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                });
            yield return WAITONE;
            rigAnimator.SetTrigger("extrafailed");
            yield return WAITTHREE;
            yield return WAITTHREE;
            yield return WAITTWO;
            ReportPhaseCompletion(2,false);
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            paperBag.SetActive(true);
            fireParticle.Play();
            isAnsRight = true;
            inputComplete = true;
        }
        else if (isExtraPhase)
        {
            Debug.LogError("Extra RIGHT");
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            fakePlasticWrap.SetActive(true);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            catBasket.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
        else if (isExtraPhase)
        {
            Debug.LogError("Extra Wrong!");
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            sting.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }
            
            if (isExtraPhase)
            {
                inputComplete = false;
                rigAnimator.SetTrigger("extrareset");
                skinMatController.SetMat(1);
            }
            
            StartLevel();
        }

    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            // CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            //     delegate
            //     {
            //     }
            // );
        }
        else
        {
            //after video
        }
    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        */
        cat.SetActive(false);
        catBasket.SetActive(false);
        paperBag.SetActive(false);
        sting.SetActive(false);
        brokenSting.SetActive(false);
        plasticWrap.SetActive(false);

        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(zoomPorchFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomPorchPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        dingDongParticle.Play();
        yield return WAITONE;
        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            

            
            yield return WAITTHREE;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            greenGooParticle.Play();
            yield return WAITONE;
            skinMatController.SetMat(1);
            fireParticle.Stop();
            paperBag.SetActive(false);
            greenGooParticle.Play();
            yield return WAITTWO;

            isPhaseOne = false;
            yield return WAITONE;

            inputComplete = false;

            isExtraPhase = true;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);

            yield return WAITTHREE;
            cat.SetActive(true);
            fakeCat.SetActive(false);
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            heartParticle.Play();
            yield return WAITONE;
            kittyPickUpParticle.Play();
            yield return WAITTWO;


            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        rigAnimator.SetTrigger("extra2reset");
        propAnimator.SetTrigger(Transition);
        brokenSting.SetActive(false);

        focuslist = new List<Transform>();
        focuslist.Add(zoomDoorFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomDoorPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 2);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;


        if (isAnsRight)
        {
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    
                });
            yield return new WaitForSeconds(0.65f);
            rigAnimator.SetTrigger(Success2);
            yield return WAITTHREE;
            yield return WAITONE;
            yield return new WaitForSeconds(0.65f);
           
            propAnimator.SetTrigger(Success2);
            plasticWrap.SetActive(true);
            fakePlasticWrap.SetActive(false);

            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            
            yield return WAITTWO;
            plasticHitParticle.Play();
            hitGroundParticle.Play();
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            //phone.SetActive(false);
            yield return WAITTHREE;
            //yield return WAITONE;
            ReportPhaseCompletion(3,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                });
            
           
            rigAnimator.SetTrigger(Fail2);
            yield return WAITTHREE;
            yield return WAITONE;
            yield return new WaitForSeconds(0.65f);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            propAnimator.SetTrigger(Fail2);
            yield return WAITTWO;
            
            stringHitParticle.Play();
            sting.SetActive(false);
            brokenSting.SetActive(true);
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTHREE;


            ReportPhaseCompletion(3,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }
    
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
             
     
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
     
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
    
            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,Mathf.Lerp(t_CurrentFillValue.z,t_DestValue.z,t_Progression));
     
            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }
     
            yield return t_CycleDelay;
        }
          
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }


}

