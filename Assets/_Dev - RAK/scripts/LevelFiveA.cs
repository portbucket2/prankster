﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelFiveA : PrankLevelBehaviour
{
    public playerLevelFive playerLevelFive;
    public Animator rigAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoomToShoeLace;
    public Transform shoeLace;
    public Transform actionCameraPos1;
    public Transform actionCameraPos2;
    public Transform actionCameraPos3;
    public Transform actionCameraPos4;
    public Transform actionCameraPos5;
    public Transform actionCameraPos6;
    public Transform actionCameraFocus1;
    public Transform actionCameraFocus2;
    public Transform actionCameraFocus3;
    public Transform actionCameraFocus4;

    [Header("particles")]
    public ParticleSystem bindShoeParticle;
    public ParticleSystem fallFlatParticle;
    public ParticleSystem fallFlatParticle2;
    public ParticleSystem oilSplashParticle;

    [Header("objects and animators")]
    public GameObject shoeLaceJoint;    
    public Animator oilcanHolder;
    public Animator screwDriverHolder;
    public Animator doorFalling;
    public Transform playerHand;
    public Transform doorKnob;
    public Transform doorKnobInHand;


    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITFIVE = new WaitForSeconds(5f);

    readonly string WALK = "walk";
    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success5_1 = "5.1Success";
    readonly string Fail5_1 = "5.1Fail";
    readonly string Success5_2 = "5.2Success";
    readonly string Fail5_2 = "5.2Fail";

    readonly string OILSPILL = "spill";
    readonly string DOORKNOB = "knob";
    readonly string DOORHINGE = "hinge";
    readonly string DOORFALL = "fall";


    bool isAnsRight = false;
    List<Transform> focuslist;
    Vector3 firstPos = new Vector3(-2.99f, 0.08f, -0.477f);
    Vector3 secondPos = new Vector3(-3.2f, 0.08f, -4f);
    Vector3 doorKnobPos = new Vector3(-0.027f, 0.123f, 0.087f);
    Vector3 doorKnobRot = new Vector3(-15f, -125.911f, 22.398f);

    //start update
    protected override void StartLevel()
    {
        if (isPhaseOne)
        {
            
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }



    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            bindShoeParticle.Play();
            shoeLaceJoint.SetActive(true);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            screwDriverHolder.SetTrigger(DOORHINGE);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus1);
            focuslist.Add(actionCameraFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos6, focuslist);
            oilcanHolder.SetTrigger(OILSPILL);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            screwDriverHolder.SetTrigger(DOORKNOB);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }
    }

    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
         */
        rigAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(shoeLace);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomToShoeLace, focuslist);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail
        
        yield return WAITONE;
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(actionCameraFocus1);
        CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos1, focuslist);

        yield return ENDOFFRAME;

        if (isAnsRight)
        {
            yield return WAITHALF;
            rigAnimator.SetTrigger(Success5_1);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos1, focuslist);
            yield return WAITONE;           
            fallFlatParticle.Play();

            yield return WAITTWO;
            isPhaseOne = false;

            yield return WAITONE;
            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            yield return WAITHALF;
            rigAnimator.SetTrigger(Fail5_1);
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus1);
            focuslist.Add(actionCameraFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos1, focuslist);
            yield return WAITTWO;
            oilSplashParticle.Play();
            yield return WAITHALF;
            oilSplashParticle.Play();
            yield return WAITHALF;
            oilSplashParticle.Play();
            yield return WAITTWO;
            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        playerLevelFive.transform.position = firstPos;
        rigAnimator.SetTrigger(IDLE1);
        Rigidbody rb = doorKnobInHand.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        //yield return ENDOFFRAME;
        if (!doorKnobInHand.parent)
        {
            doorKnobInHand.parent = playerHand;
        }
        //yield return WAITHALF;
        doorKnobInHand.localPosition =  doorKnobPos;
        doorKnobInHand.localEulerAngles = doorKnobRot;
        doorKnob.gameObject.SetActive(true);
        doorKnobInHand.gameObject.SetActive(false);

        shoeLaceJoint.gameObject.SetActive(false);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(actionCameraFocus3);
        CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos4, focuslist);
        playerLevelFive.transform.position = firstPos;
        rigAnimator.SetTrigger(IDLE1);
        yield return WAITONE;
        yield return ENDOFFRAME;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        
        yield return WAITTWO;
        yield return WAITONE;
        
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;
        
        //

        if (isAnsRight)
        {
            yield return WAITHALF;
            rigAnimator.SetTrigger(Success5_2);
            playerLevelFive.transform.position = secondPos;
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus4);
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos3, focuslist);
            yield return WAITONE;
            yield return WAITHALF;
            doorFalling.SetTrigger(DOORFALL);
            yield return WAITONE;
            fallFlatParticle2.Play();
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus4);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos2, focuslist);
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {
            yield return WAITHALF;
            rigAnimator.SetTrigger(Fail5_2);
            playerLevelFive.transform.position = secondPos;
            yield return WAITONE;
            doorKnob.gameObject.SetActive(false);
            doorKnobInHand.gameObject.SetActive(true);
            focuslist = new List<Transform>();
            focuslist.Add(actionCameraFocus3);
            CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos2, focuslist);
            yield return WAITTWO;
            //yield return WAITONE;
            yield return WAITHALF;
            doorKnobInHand.parent = null;
            rb.isKinematic = false;
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;
    }

}
