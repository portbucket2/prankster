﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelFourtyThreeA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform zoom2Pos2;
    public Transform zoom2Pos3;


    [Header("particles")]
    public ParticleSystem hitChestParticle;
    public ParticleSystem waterTapParticle;
    public ParticleSystem noWaterTapParticle;
    public ParticleSystem vapourFromMouthParticle;
    public ParticleSystem vapourFromGlassParticle;
    public ParticleSystem waterFromGlassParticle;
    public ParticleSystem fallFlatParticle;
    //public ParticleSystem thoughtBubbleParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator mediballAnim;
    public Animator glueHolderAnim;
    public Animator waterCoolerAnim;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Common1 = "common1";
    readonly string Common2 = "common2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Move = "move";
    readonly string Default = "default";
    readonly string Cork = "cork";

    bool isAnsRight = false;
    List<Transform> focuslist;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            mediballAnim.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(zoom2Focus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos3, focuslist);
            waterCoolerAnim.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            glueHolderAnim.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(zoom2Focus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos3, focuslist);
            waterCoolerAnim.SetTrigger(Cork);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        */

        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;

        yield return WAITTWO;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 0;
        propAnimator.speed = 0;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 1;
        propAnimator.speed = 1;

        if (isAnsRight)
        {

            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phase1RightPos, focuslist);
            //yield return WAITONE;
            //rigAnimator.SetTrigger(Common1);
            //propAnimator.SetTrigger(Common1);
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            yield return WAITHALF;
            hitChestParticle.Play();
            fallFlatParticle.Play();
            yield return WAITTHREE;

            isPhaseOne = false;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            //rigAnimator.SetTrigger(Common1);
            //propAnimator.SetTrigger(Common1);
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            yield return WAITFOUR;
            yield return WAITFOUR;
            //CameraMovementController.Instance.UnfollowCamera();
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FollowCamera(characterPos, focuslist);
            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        waterCoolerAnim.SetTrigger(Default);
        rigAnimator.SetTrigger(IDLE2);
        propAnimator.SetTrigger(IDLE2);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;
        playerPosition.Follow();
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(zoom2Pos2, focuslist);

        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Common2);
            propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);

            yield return WAITFOUR;
            yield return WAITTWO;
            yield return WAITHALF;
            waterTapParticle.Play();
            vapourFromGlassParticle.Play();
            yield return WAITONE;
            waterFromGlassParticle.Play();
            //yield return WAITONE;
            //playerPosition.UnFollow();
            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(zoom2Focus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
            yield return WAITTWO;
            vapourFromMouthParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);

            yield return WAITFOUR;

            ReportPhaseCompletion(2,  true);
            inputComplete = false;
        }
        else
        {
            rigAnimator.SetTrigger(Common2);
            propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);

            yield return WAITFOUR;
            yield return WAITTWO;
            yield return WAITHALF;
            noWaterTapParticle.Play();
            //playerPosition.UnFollow();
            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(zoom2Focus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
            yield return WAITFOUR;
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraEndFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            yield return WAITTHREE;

            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }
        inputComplete = false;
    }

}

