﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelFiftyOneA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimatorIdle;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform phase1Success;
    public Transform phase1SuccessFocus;
    public Transform phase1Fail;
    public Transform phase1FailFocus;


    [Header("particles")]
    public ParticleSystem fallFlatParticle;
    public ParticleSystem fallFlatHugeParticle;
    public ParticleSystem ballKickParticle;
    public ParticleSystem explosionParticle;
    public ParticleSystem slipOnParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator propsEntry;


    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string Default = "default";
    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Transition = "transition";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Spring = "spring";
    readonly string Stone = "stone";
    readonly string BallEntry = "ballEntry";
    readonly string BallExit = "ballExit";
    readonly string Bomb = "bomb";
    readonly string Explode = "explode";

    bool isAnsRight = false;
    List<Transform> focuslist;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            propsEntry.SetTrigger(Stone);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            propsEntry.SetTrigger(Bomb);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            propsEntry.SetTrigger(Spring);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            propsEntry.SetTrigger(BallEntry);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        //focuslist = new List<Transform>();
        //focuslist.Add(preEndFocus);
        //CameraMovementController.Instance.FollowCamera(preEndPos, focuslist);
        /*phase one
        */

        rigAnimator.SetTrigger(IDLE1);
        propAnimatorIdle.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;

        yield return WAITTWO;
        yield return WAITHALF;
        propAnimatorIdle.gameObject.SetActive(false);
        propAnimator.gameObject.SetActive(true);
        rigAnimator.speed = 0;
        propAnimatorIdle.speed = 0;
        propAnimator.SetTrigger(Success1);
        propAnimator.speed = 0;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 1;
        propAnimator.speed = 1;
        playerPosition.UnFollow();
        if (isAnsRight)
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            slipOnParticle.Play();
            yield return WAITONE;
            fallFlatParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(phase1SuccessFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase1Success, focuslist);
            yield return WAITTWO;

            isPhaseOne = false;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(phase1FailFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase1Fail, focuslist);

            yield return WAITTWO;
            yield return WAITTHREE;
            //CameraMovementController.Instance.UnfollowCamera();

            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        //rigScale.localScale = new Vector3(1f,1f,1f);
        propsEntry.SetTrigger(Default);
        playerPosition.Follow();
        propAnimatorIdle.gameObject.SetActive(false);

        rigAnimator.SetTrigger(Transition);
        rigAnimator.speed = 0;


        CameraMovementController.Instance.UnfollowCamera();
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraEndFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

        yield return WAITTWO;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(phase1Success, focuslist);
        rigAnimator.SetTrigger(Transition);
        rigAnimator.speed = 1;
        yield return WAITTWO;
        playerPosition.UnFollow();

        if (isAnsRight)
        {
            playerPosition.UnFollow();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            //yield return WAITONE;
            //rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            //propAnimator.SetTrigger(Success2);

            yield return WAITTWO;
            ballKickParticle.Play();
            propsEntry.SetTrigger(Explode);
            yield return WAITONE;
            yield return WAITHALF;
            explosionParticle.Play();
            yield return WAITTWO;
            fallFlatHugeParticle.Play();
            yield return WAITTWO;
            

            ReportPhaseCompletion(2,  true);
            inputComplete = false;
        }
        else
        {

            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);
            yield return WAITTWO;
            propsEntry.SetTrigger(BallExit);
            ballKickParticle.Play();
            yield return WAITTHREE;


            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }
        inputComplete = false;
        playerPosition.UnFollow();
    }

}

