﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFocusForCamera : MonoBehaviour
{
    public Transform parent;
    public float yOffset = 1f;
    private bool isFollowing = false;
    void Update()
    {
        if (isFollowing)
        {
            transform.position = new Vector3(parent.position.x, yOffset, parent.position.z); 
        }
    }

    public void Follow() {
        isFollowing = true;
    }

    public void UnFollow()
    {
        isFollowing = false;
    }
}
