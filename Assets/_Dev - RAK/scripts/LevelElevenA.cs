﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelElevenA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform monitorZoom;
    public Transform monitorZoomFocus;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoomToTable;
    public Transform zoomToTableFocus;
    public Transform phaseTwoActionPos;
    public Transform phaseTwoActionPos2;
    public Transform phaseTwoActionFocus;
    public Transform phaseTwoActionFocus2;
    public Transform phaseTwoActionFocus3;
    public Transform phaseTwoActionPrankPos;
    public Transform stringCamFocus;
    public Transform rollCamStartPos;

    [Header("particles")]
    public ParticleSystem peeOnGroundParticle;
    public ParticleSystem fallFlatParticle;
    public ParticleSystem phoneRingParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject skateBoard;
    public GameObject phone;
    public Material standardMat;
    public Material peedPant;
    public Material redFace;
    //public SkinnedMeshRenderer renderer;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "Idle11_1";
    readonly string IDLE2 = "Idle11_2";
    readonly string Success1 = "Success11_1";
    readonly string Fail1 = "Fail11_1";
    readonly string Success2 = "Success11_2";
    readonly string Fail2 = "Fail11_2";
    readonly string IronEntry = "ironEntry";
    readonly string BottleEntry = "bottleEntry";
    readonly string PhoneExit = "phoneExit";
    readonly string SkateEntry = "skateEntry";
    readonly string StringEntry = "stringEntry";
    readonly string Move = "move";

    bool isAnsRight = false;
    List<Transform> focuslist;
    
    // prank level behavior start

    protected override void StartLevel()
    {
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            propAnimator.SetTrigger(IronEntry);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {

            focuslist = new List<Transform>();
            focuslist.Add(stringCamFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);

            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            propAnimator.SetTrigger(BottleEntry);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoActionFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }
        
    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
         */
        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITTWO;
        focuslist = new List<Transform>();
        focuslist.Add(zoomToTableFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomToTable, focuslist);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

        if (isAnsRight)
        {
            phoneRingParticle.Play();
            yield return WAITHALF;
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            yield return WAITTWO;
            
            phoneRingParticle.Stop();

            yield return WAITTWO;
            RedFace();
            isPhaseOne = false;
            yield return WAITONE;
            inputComplete = false;
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoActionFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);

            NormalMaterial();
            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            phoneRingParticle.Play();
            yield return WAITHALF;
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            yield return WAITTWO;
            phoneRingParticle.Stop();
            yield return WAITTWO;
            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        //focuslist = new List<Transform>();
        //focuslist.Add(monitorZoomFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(monitorZoom, focuslist);
        //yield return WAITTWO;
        skateBoard.SetActive(true);
        phone.SetActive(false);
        focuslist = new List<Transform>();
        focuslist.Add(phaseTwoActionFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
        yield return WAITONE;
        rigAnimator.SetTrigger(IDLE2);
        propAnimator.SetTrigger(IDLE1);
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(phaseTwoActionFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITONE;
        //CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos5, focuslist);
        //rigAnimator.SetTrigger(IDLE2);
        //playerLevelFive.transform.position = secondPos;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        //

        if (isAnsRight)
        {
            propAnimator.SetTrigger(StringEntry);
            yield return WAITHALF;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(rollCamStartPos, focuslist);
            rollCamStartPos.GetComponent<Animator>().SetTrigger(Move);
            yield return WAITONE;
            rigAnimator.SetTrigger(Success2);
            
            yield return WAITONE;
            fallFlatParticle.Play();
            yield return WAITONE;
            PeedPant();
            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoActionFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPos, focuslist);
            
            peeOnGroundParticle.Play();
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {

            yield return WAITONE;
            propAnimator.SetTrigger(SkateEntry);
            yield return WAITONE;

            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(rollCamStartPos, focuslist);
            rollCamStartPos.GetComponent<Animator>().SetTrigger(Move);
            yield return WAITONE;
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);
            yield return WAITTWO;
            CameraMovementController.Instance.UnfollowCamera();
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoActionFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPos, focuslist);

            //yield return WAITONE;
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoActionFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPos, focuslist);
            //yield return WAITONE;
            //yield return WAITHALF;
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoActionFocus3);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPos2, focuslist);
            yield return WAITTWO;
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }


    void PeedPant()
    {
        //renderer.material = peedPant;
        skinMatController.SetMat(1);
    }
    void RedFace()
    {
        //renderer.material = redFace;
        skinMatController.SetMat(2);
    }
    void NormalMaterial()
    {
        //renderer.material = standardMat;
        skinMatController.SetMat(0);
    }






}
