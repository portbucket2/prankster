﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffectEvent : MonoBehaviour
{
    public ParticleSystem[] particles;


    public void PlayParticleOnce(int index)
    {
        if (index < particles.Length)
        {
            particles[index].Play();
        }        
    }
}
