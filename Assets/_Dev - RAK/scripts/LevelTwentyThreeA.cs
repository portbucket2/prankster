﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LevelTwentyThreeA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform hangingBagPos;
    public Transform hangingBagFocus;
    public Transform punchingBagPos;
    public Transform punchingBagFocus;


    [Header("particles")]
    public ParticleSystem PunchPowParticle;
    public ParticleSystem punchWahmParticle;
    public ParticleSystem pourWaterParticle;
    public ParticleSystem secondBagBam;
    public ParticleSystem secondBagPow;
    public ParticleSystem hitWallParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator waterBucketHolder;
    public Animator cementHolder;
    public Animator springHolder;
    public Animator wheelHolder;
    public GameObject phone;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Transition = "transition";
    readonly string Move = "move";
    readonly string Default = "default";

    bool isAnsRight = false;
    List<Transform> focuslist;
    private PlayerFocusForCamera playerFocus;
    // prank level behavior start

    protected override void StartLevel()
    {
        playerFocus = characterPos.GetComponent<PlayerFocusForCamera>();
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            cementHolder.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            springHolder.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            waterBucketHolder.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            wheelHolder.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
         */
        rigAnimator.SetTrigger(IDLE1);
        //propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITTWO;
        focuslist = new List<Transform>();
        focuslist.Add(hangingBagFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(hangingBagPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        if (isAnsRight)
        {
            rigAnimator.SetTrigger(Success1);
            //propAnimator.SetTrigger(Success1);
            yield return WAITONE;
            punchWahmParticle.Play();
            yield return WAITTWO;
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            //yield return WAITONE;

            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            playerFocus.Follow();
            rigAnimator.SetTrigger(Transition);
            propAnimator.SetTrigger(Transition);
            yield return WAITTHREE;

            //rigAnimator.SetTrigger(IDLE2);
            //propAnimator.SetTrigger(IDLE1);
            CameraMovementController.Instance.UnfollowCamera();
            playerFocus.UnFollow();


            isPhaseOne = false;
            yield return WAITONE;
            phone.SetActive(true);
            yield return WAITONE;
            
            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            //propAnimator.SetTrigger(Fail1);
            yield return WAITONE;
            PunchPowParticle.Play();
            yield return WAITHALF;
            pourWaterParticle.Play();
            yield return WAITTHREE;
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            //yield return WAITTWO;
            yield return WAITONE;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        phone.SetActive(true);
        rigAnimator.SetTrigger(IDLE2);
        propAnimator.SetTrigger(IDLE2);
        wheelHolder.SetTrigger(Default);

        focuslist = new List<Transform>();
        focuslist.Add(cameraEndFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
        
        yield return WAITTWO;
        yield return WAITHALF;
        rigAnimator.speed = 0f;
        propAnimator.speed = 0f;
        yield return WAITHALF;
        focuslist = new List<Transform>();
        focuslist.Add(punchingBagFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(punchingBagPos, focuslist);
        rigAnimator.speed = 0f;
        propAnimator.speed = 0f;
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraStartPos);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartFocus, focuslist);

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        
        if (isAnsRight)
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            yield return WAITTWO;
            rigAnimator.speed = 1f;
            propAnimator.speed = 1f;
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            
            secondBagBam.Play();
            hitWallParticle.Play();
            //phone.SetActive(false);
            yield return WAITONE;
            rigAnimator.speed = 0.1f;
            yield return WAITTHREE;
            rigAnimator.speed = 1f;
            //yield return WAITONE;
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTWO;
            rigAnimator.speed = 1f;
            propAnimator.speed = 1f;
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);
            secondBagPow.Play();

            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITTHREE;


            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }

}

