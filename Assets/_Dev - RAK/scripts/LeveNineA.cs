﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class LeveNineA : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform monitorZoom;
    public Transform monitorZoomFocus;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoomToFanPos;
    public Transform zoomToFanFocus;
    public Transform zoomToChairPos;
    public Transform zoomToChairFocus;
    public Transform transitionViewPos;
    public Transform transitionViewFocus;
    public Transform transitionViewFocus2;
    public Transform zoomToChairTop;
    public Transform zoomToChairTopFocus;

    [Header("particles")]
    public ParticleSystem confettiParticle;
    public ParticleSystem fallFlatParticle;
    public ParticleSystem colorSplashParticle;
    public ParticleSystem waterSplashParticle;
    public ParticleSystem confettiBlastParticle;
    //public ParticleSystem fallFlatParticle;
    //public ParticleSystem phoneRingParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject bucketObject;
    public GameObject ConfettiObject;
    public GameObject bucketParent;

    //public Material standardMat;
    //public Material greenBody;
    public SkinnedMeshRenderer body;
    public Animator screwDriver;
    public Animator waterBottle;
    public Transform sunLight;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Common1 = "common1";
    readonly string Common2 = "common2";

    readonly string Transition1 = "transition1";
    readonly string Transition2 = "transition2";
    readonly string Move = "move";

    bool isAnsRight = false;
    List<Transform> focuslist;
    Vector3 sunPhaseOne = new Vector3(45f,212f,0f);
    Vector3 sunPhaseTwo = new Vector3(45f,25f,0f);

    // prank level behavior start

    protected override void StartLevel()
    {
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }

    protected override void RightAnswerSelected()
    { 
        if (isPhaseOne)
        {
            rigAnimator.SetTrigger(Common1);
            bucketObject.SetActive(true);
            ConfettiObject.SetActive(false);

            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(stringCamFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);

            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            rigAnimator.SetTrigger(Common1);
            bucketObject.SetActive(false);
            ConfettiObject.SetActive(true);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoActionFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoActionPrankPos, focuslist);
            isAnsRight = false;
            inputComplete = true;
        }
    }



    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }
            StartLevel();
        }

    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
         */
        bucketObject.SetActive(false);
        ConfettiObject.SetActive(false);
        sunLight.eulerAngles = sunPhaseOne;
        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        //yield return ENDOFFRAME;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraStartFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(zoomToFanFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomToFanPos, focuslist);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITHALF;
        propAnimator.SetTrigger(Success1);
        // do here the prank job detail
        yield return WAITHALF;
        focuslist = new List<Transform>();
        focuslist.Add(monitorZoomFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(monitorZoom, focuslist);

        if (isAnsRight)
        {
            yield return WAITHALF;
            //phoneRingParticle.Play();
            propAnimator.SetTrigger(Success1);
            yield return WAITHALF;
            rigAnimator.SetTrigger(Success1);
            
            yield return WAITONE;
            GreenBody();
            colorSplashParticle.Play();
            yield return WAITONE;
            //phoneRingParticle.Stop();
            //paint face here
            isPhaseOne = false;
            yield return WAITONE;
            inputComplete = false;
            yield return WAITTWO;
            //focuslist = new List<Transform>();
            //focuslist.Add(transitionViewFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(transitionViewPos, focuslist);
            rigAnimator.SetTrigger(Transition1);
            propAnimator.SetTrigger(Transition1);
            //NormalMaterial();

            yield return WAITFOUR;
            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            yield return WAITHALF;
            //phoneRingParticle.Play();
            propAnimator.SetTrigger(Success1);
            yield return WAITONE;
            yield return WAITHALF;

            confettiBlastParticle.Play();

            rigAnimator.SetTrigger(Fail1);
            confettiParticle.Play();

            yield return WAITONE;
            inputComplete = false;
            yield return WAITTWO;
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            //rigAnimator.SetTrigger(Transition1);
            //propAnimator.SetTrigger(Transition1);
            //NormalMaterial();
            confettiParticle.Stop();
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        propAnimator.SetTrigger(IDLE1);
        sunLight.eulerAngles = sunPhaseOne;
        bucketParent.SetActive(false);
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        rigAnimator.SetTrigger(Transition2);
        rigAnimator.speed = 0;
        NormalBody();
        focuslist = new List<Transform>();
        focuslist.Add(zoomToChairTopFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoomToChairTop, focuslist);


        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITONE;
        //CameraMovementController.Instance.FocusCameraWithOrigin(actionCameraPos5, focuslist);
        //rigAnimator.SetTrigger(IDLE2);
        //playerLevelFive.transform.position = secondPos;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        //

        if (isAnsRight)
        {
            focuslist = new List<Transform>();
            focuslist.Add(zoomToChairTopFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoomToChairTop, focuslist);
            // run screw driver
            screwDriver.SetTrigger(Move);
            yield return WAITTWO;
            sunLight.eulerAngles = sunPhaseTwo;
            focuslist = new List<Transform>();
            focuslist.Add(transitionViewFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(transitionViewPos, focuslist);

            rigAnimator.speed = 1;
            rigAnimator.SetTrigger(Transition2);
            propAnimator.SetTrigger(IDLE1);
            yield return WAITTWO;
            yield return WAITONE;
            rigAnimator.SetTrigger(Common2);
            yield return WAITONE;
            propAnimator.SetTrigger(Transition1);
            yield return WAITONE;
            rigAnimator.SetTrigger(IDLE1);
            yield return WAITONE;

            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(transitionViewFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(transitionViewPos, focuslist);
            fallFlatParticle.Play();
            //particles
            yield return WAITTWO;


            //peeOnGroundParticle.Play();
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(zoomToChairTopFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoomToChairTop, focuslist);
            // run screw driver
            waterBottle.SetTrigger(Move);
            yield return WAITTWO;
            sunLight.eulerAngles = sunPhaseTwo;
            focuslist = new List<Transform>();
            focuslist.Add(transitionViewFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(transitionViewPos, focuslist);

            rigAnimator.speed = 1;
            rigAnimator.SetTrigger(Transition2);
            propAnimator.SetTrigger(IDLE1);
            yield return WAITTWO;
            yield return WAITONE;
            rigAnimator.SetTrigger(Common2);
            yield return WAITONE;
            propAnimator.SetTrigger(Transition1);
            yield return WAITONE;
            rigAnimator.SetTrigger(IDLE1);
            waterSplashParticle.Play();
            yield return WAITONE;

            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);
            yield return WAITONE;

            //fallFlatParticle.Play();
            //particles
            yield return WAITTWO;
            //focuslist = new List<Transform>();
            //focuslist.Add(transitionViewFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(transitionViewPos, focuslist);
            ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }


    void PeedPant()
    {
        //renderer.material = peedPant;
    }
    void GreenBody()
    {
        //body.material = greenBody;
        skinMatController.SetMat(1);
    }
    void NormalBody()
    {
        //body.material = standardMat;
        skinMatController.SetMat(0);
    }

}
