﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;

public class Level54AnimTrigger : MonoBehaviour

{
    public CameraShake camShake;
   public List<ParticleSystem> particleSystemList;

   public void PlayFootParticle(int index)
   {
      particleSystemList[index].Play();
   }

   public void ResetCrying()
   {
     particleSystemList[2].Stop();
     particleSystemList[3].Stop();
   }
   public void PlayCrying()
   {
       particleSystemList[2].Play();
       particleSystemList[3].Play();
   }

   public void CameraShake()
   {
      camShake.ShowCameraShake();
   }
}
