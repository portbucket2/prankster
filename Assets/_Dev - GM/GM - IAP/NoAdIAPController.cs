﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.IAP;

public class NoAdIAPController : MonoBehaviour
{
    public Button NoAdbutton;
    public Button Nobutton;
    public Button Yesbutton;
    public GameObject NoAdPanel;
    // Start is called before the first frame update
    void Start()
    {
        NoAdbutton.onClick.AddListener(() => {

            GameUtil.LogMagenta("let us bring IAP window", true);
            NoAdPanel.SetActive(true);
        });
        Nobutton.onClick.AddListener(() => {

            GameUtil.LogMagenta("let us hide IAP window", true);
            NoAdPanel.SetActive(false);
        });
        Yesbutton.onClick.AddListener(() =>
        {
            GameUtil.LogMagenta("will try to buy IAP", true);
            Yesbutton.interactable = false;
            //InputSystemMouse.PauseGameInput();
            IAP_Controller.instance.Buy_NoAd((success) =>
            {
                if (success)
                {
                    NoAdPanel.SetActive(false);
                    NoAdbutton.gameObject.SetActive(false);
                }
                GameUtil.LogMagenta("success IAP? " + success, true);
                Yesbutton.interactable = true;
                //InputSystemMouse.ResumeGameInput();
                GameUtil.LogGreen("input has been resumed!", true);
            });
           
          // IAPManager.instance.BuyRemoveAds((success) =>
         //  {
          //     if (success)
           //    {
           //        NoAdPanel.SetActive(false);
           //        NoAdbutton.gameObject.SetActive(false);
           //    }
          // });
        });
    }
}
