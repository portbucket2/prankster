﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.adjust.sdk.purchase;
using Portbliss.IAP;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using UnityEngine.Serialization;
using FRIA;


public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager instance;

    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    
    
    //Step 1 create your products
    public string AndroidRemoveAdsID;
    
    
    
    public static bool isIAPReady;

    public static bool isADBlocked
    {
        get 
        {
            if (_isNoADavaialableHD == null) _isNoADavaialableHD = new HardData<bool>("HAS_NO_AD", false);
            return _isNoADavaialableHD.value;
        }
        set
        {
            if (_isNoADavaialableHD == null) _isNoADavaialableHD = new HardData<bool>("HAS_NO_AD", false);
            if (value==true) _isNoADavaialableHD.value = true;
        }
    }


    private static HardData<bool> _isNoADavaialableHD;


    private System.Action<bool> purchaseCallback;
    
    
    
    //private UnityAction OnPurchaseSuccessfull;
    //private UnityAction OnPurchaseFailedOwn;
   // private UnityAction<string> OnPassingLogEvent;

    //************************** **************************************
    public void InitializePurchasing()
    {
        if (IsInitialized()) { return; }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // if product is a consumable or non consumable
        builder.AddProduct(AndroidRemoveAdsID, ProductType.NonConsumable);

        UnityPurchasing.Initialize(this, builder);     
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //Create methods
    public void BuyRemoveAds(Action<bool> OnBuyProcessGoing)
    {
        Debug.Log(isADBlocked + " Is Ad blocked");
        if (isADBlocked)
        {
            OnBuyProcessGoing?.Invoke(true);
            return;
        }
        else if (!IsInitialized())
        {
            OnBuyProcessGoing?.Invoke(false);
            return;
        }
        BuyProductID(AndroidRemoveAdsID,OnBuyProcessGoing);
    }

    

    //Step 4 modify purchasing
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log(args.purchasedProduct.definition.id + " Current ID from STore");
        if(args.purchasedProduct.definition.id == AndroidRemoveAdsID)
        {
            StartCoroutine(ExitIAPWithSuccess(args.purchasedProduct));
        }

        return PurchaseProcessingResult.Complete;
    }
    

    private IEnumerator ExitIAPWithSuccess(Product purchasedProduct)
    {
         var lPrice = purchasedProduct.metadata.localizedPrice;
        var price = decimal.ToDouble(lPrice);
        var currencyCode = purchasedProduct.metadata.isoCurrencyCode;
        //var transactionID = args.purchasedProduct.transactionID;
        var productID = purchasedProduct.definition.id;
        var receiptDict = (Dictionary<string, object>)MiniJson.JsonDecode(purchasedProduct.receipt);
        
        string androidTransactionID = ""; 
        string androidPayload = ""; 
        string iOS_Payload = ""; 
        string androidPurchaseToken = "";


        GetIAP_SecurityData(purchasedProduct, out androidTransactionID, out androidPayload, out iOS_Payload, true);

       
#if UNITY_IOS
        //Ios call
#elif UNITY_ANDROID
       // var gpDetailsDict = (Dictionary<string, object>)MiniJson.JsonDecode(androidPayload);
        //var query = (Dictionary<string, object>)MiniJson.JsonDecode((string)gpDetailsDict["json"]);
        bool isLicenseValid = CheckGoogleValidityFromLicense(purchasedProduct,out androidPurchaseToken);
#endif


            GameUtil.LogGreen("now we will try to validate IAP with adjust sdk! platform: android", true);

                bool waiting = true;
                bool adjustValidated = false;


            AdjustPurchaseSDKManager.Instance.VerifyPurchaseForAndroidWithEventTracking(
            productID,
            androidPurchaseToken,
            androidPayload, 
            price, 
            currencyCode,
            (() =>
                {
                    Debug.Log("Purchased Successfull Under the return Code!!!");
                     waiting = false;
                     adjustValidated = true;
                }
            ),
            (() =>
                {
                    Debug.Log("Purchase Failed Under the return code!!");
                }
            ),
            (arg0 =>
            {
                Debug.Log(arg0);
            })
        );



                // AdjustPurchase.VerifyPurchaseAndroid(productID, androidPurchaseToken, androidPayload, (verificationInfo) =>
                // {

                //     GameUtil.LogGreen("so we varified this purchase on android and lets see the result from verification process - message: " + verificationInfo.Message
                //         + " and status code: " + verificationInfo.StatusCode + " and verification state: " + verificationInfo.VerificationState, true);
                //     waiting = false;
                    
                //     adjustValidated = (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed);

                // });

                while (waiting)
                {
                    if (!waiting) { break; }
                    yield return null;
                }

                if (adjustValidated)
                {
                    GameUtil.LogGreen("We have told the adjust sdk to report revenue after IAP verification. platform: android", true);
                }
                GameUtil.LogGreen("Validation process done for id: " + productID + " after validation, success? " + adjustValidated, true);
               purchaseCallback?.Invoke(adjustValidated);
              isADBlocked = true;
    }

    //private void PassingLogEvent(string arg0)
    //{
        
    //}

    //private void PurchaseSuccessfull()
    //{
    //    Debug.Log("Purchased Successfull!!!");
    //    isADBlocked = true;
    //    purchaseCallback?.Invoke(true);
    //}

    //private void PurchaseFailed()
    //{
    //    Debug.Log("Purchase Failed");
    //    purchaseCallback?.Invoke(false);
    //}


    //****************************  ***********************************
    private void Awake()
    {
        Singleton();
    }

    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    private void Singleton()
    {
        if (instance != null) { Destroy(gameObject); return; }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void BuyProductID(string productId,Action<bool> OnBuyProcessGoing)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
                purchaseCallback = OnBuyProcessGoing;
            }
            else
            {
                OnBuyProcessGoing?.Invoke(false);
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            OnBuyProcessGoing?.Invoke(false);
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestoreForAndroidRemoveAds(Action<bool> OnRestoreSuccess)
    {
        if (IsProductPurchased(AndroidRemoveAdsID))
        {
            Debug.LogWarning("Product purchased previously - Restored Again!!");
            OnRestoreSuccess?.Invoke(true);
        }
        else
        {
            OnRestoreSuccess?.Invoke(false);
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;

        RestoreForAndroidRemoveAds((bool success) =>
        {
            if (success)
            {
                isADBlocked = true;
            }

            isIAPReady = true;
        });
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        isIAPReady = true;
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

        //StartCoroutine(ExitIAPWithSuccess(product));
    }
    
    private bool IsProductPurchased(string productID)
    {
        Product p = m_StoreController.products.WithID(productID);
        return p.hasReceipt == true && string.IsNullOrEmpty(p.transactionID) == false;
    }

    public static bool CheckGoogleValidityFromLicense(Product product, out string googlePurchaseToken, bool isDebugMessageEnabled = true)
    {
            googlePurchaseToken = "";
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),AppleTangle.Data(), Application.identifier);
 
            GooglePlayReceipt gReceipt = null;
 
            if (Application.platform == RuntimePlatform.Android)
            {
                var g_result = validator.Validate(product.receipt);
                foreach (IPurchaseReceipt r in g_result)
                {
                    //if (r.productID != productID) { continue; }//??
                    gReceipt = r as GooglePlayReceipt;
                    GameUtil.LogRed("we have got the reciept data!", isDebugMessageEnabled);
                    break;
                }
            }
 
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                GameUtil.LogMagenta("iOS platform, unncessary license check.", isDebugMessageEnabled);
                return false;
            }
            else if (gReceipt == null)
            {
                GameUtil.LogRed("google reciept object is null!! ERROR!!!!!!!!!", isDebugMessageEnabled);
                return false;
            }
            else
            {
                googlePurchaseToken = gReceipt.purchaseToken;
                return true;
            }
        }

        public static void GetIAP_SecurityData(Product product, out string androidTransactionID, out string android_Payload, out string iOS_payload, bool isDebugEnabled = true)
        {
            androidTransactionID = android_Payload = iOS_payload = "";
#if UNITY_ANDROID
 
            IAP_RecieptObject_Android reciept = null;
            IAP_PayloadObject_Android payload = null;
            GetAndroidData(product, out reciept, out payload, isDebugEnabled);
 
            android_Payload = reciept.Payload;
            androidTransactionID = reciept.TransactionID;
#elif UNITY_IOS
            var receiptDict_v2 = (Dictionary<string, object>)MiniJson.JsonDecode(product.receipt);
            iOS_payload = (null == receiptDict_v2) ? "" : (string)receiptDict_v2["Payload"];
#endif
        }
 
        static void GetAndroidData(Product product, out IAP_RecieptObject_Android reciept, out IAP_PayloadObject_Android payload, bool isDebugEnabled = true)
        {
            reciept = null;
            payload = null;
 
            if (product != null && Application.platform == RuntimePlatform.Android)
            {
                try
                {
                    reciept = JsonUtility.FromJson<IAP_RecieptObject_Android>(product.receipt);
                }
                catch (Exception ex1)
                {
                    GameUtil.LogRed("'IAP_RecieptObject_Android' object json perse error: " + ex1.Message, isDebugEnabled);
                }
 
                if (reciept != null)
                {
                    try
                    {
                        payload = JsonUtility.FromJson<IAP_PayloadObject_Android>(reciept.Payload);
                    }
                    catch (Exception ex2)
                    {
                        GameUtil.LogRed("'IAP_PayloadObject_Android' object json perse error: " + ex2.Message, isDebugEnabled);
                    }
                }
            }
        }
}