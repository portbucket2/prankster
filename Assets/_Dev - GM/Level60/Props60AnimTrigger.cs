﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Props60AnimTrigger : MonoBehaviour
{
    public List<ParticleSystem> particleSystemList;

    public void PlayFootParticle(int index)
    {
        particleSystemList[index].Play();
    }
}
