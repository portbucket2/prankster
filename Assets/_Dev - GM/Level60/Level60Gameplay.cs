﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class Level60Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;
    
    [Header("Level Specific")] public Animator propsAnimator;
    public SkinMatController skinMatcontroller;

    public Level54AnimTrigger animTrigger;
    
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        PlayCharacterAnimation(animationKeyList[0]);
        propsAnimator.SetTrigger(animationKeyList[0]);
        yield return new WaitForSeconds(4f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
                PlayCharacterAnimation(animationKeyList[3]);
                propsAnimator.SetTrigger(animationKeyList[3]);
                prankSequenceList[0].propsList[5].propObject.SetActive(false);
            });
    }
    

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, true, delegate
            {
                prankSequenceList[0].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().enabled =
                    true;
            }));
        yield return new WaitForSeconds(1.6f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[1]);
                propsAnimator.SetTrigger(animationKeyList[1]);
            });
        yield return new WaitForSeconds(0.15f);
        prankSequenceList[0].propsList[0].propObject.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>()
            .enabled = false;
        prankSequenceList[0].propsList[4].propObject.SetActive(true);
        
        yield return new WaitForSeconds(7f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        yield return new WaitForSeconds(8f);
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            });
        
        prankSequenceList[1].propsList[2].propObject.SetActive(false);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        prankSequenceList[1].propsList[0].propObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[4]);
                propsAnimator.SetTrigger(animationKeyList[4]);
            });
        
        yield return new WaitForSeconds(9f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans,prankSequenceList[1].camPosList[2].cameraTransList,
            delegate
            {
                //prankSequenceList[1].propsList[0].propObject.SetActive(false);
            });
        yield return new WaitForSeconds(4f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, true, delegate
            {
                prankSequenceList[0].propsList[1].propObject.transform.GetChild(0).GetComponent<Animator>().enabled =
                    true;
            }));
        yield return new WaitForSeconds(1.6f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[2]);
                propsAnimator.SetTrigger(animationKeyList[2]);
            });
        yield return new WaitForSeconds(0.15f);
        prankSequenceList[0].propsList[1].propObject.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>()
            .enabled = false;
        prankSequenceList[0].propsList[2].propObject.SetActive(false);
        prankSequenceList[0].propsList[3].propObject.SetActive(true);
        
        yield return new WaitForSeconds(10f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        prankSequenceList[1].propsList[1].propObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[5]);
                propsAnimator.SetTrigger(animationKeyList[5]);
            });
        animTrigger.ResetCrying();
        
        
        yield return new WaitForSeconds(11f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }
    
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                });
            PlayCharacterAnimation(animationKeyList[6]);
            propsAnimator.SetTrigger(animationKeyList[6]);
            prankSequenceList[1].propsList[1].propObject.SetActive(false);
            animTrigger.PlayCrying();
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        }
    }
    

    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,t_TargetTrans.transform.position.z);
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }
    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,bool t_IsWorld = true,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = 1f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_IsWorld? t_TargetTrans.transform.position : t_TargetTrans.transform.localPosition;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            if (t_IsWorld)
            {
                t_TargetTrans.transform.position = new Vector3(
                    t_TargetTrans.transform.position.x,
                    t_TargetTrans.transform.position.y,Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));
            }
            else
            {
                t_TargetTrans.transform.localPosition = new Vector3(
                    t_TargetTrans.transform.localPosition.x,
                    t_TargetTrans.transform.localPosition.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));
            }

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(ZAxisMoveRoutine(Vector3.zero,null));
    }
}
