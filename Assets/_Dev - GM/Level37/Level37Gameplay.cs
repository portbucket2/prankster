﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class Level37Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    [Header("Level Speficc")] public Animator propsAnimator;
    
    
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        
        //PlayCharacterAnimation(animationKeyList[2]);
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            });
    }
    

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        bool t_Wait = false;
        
        WaitUntil t_WaitUntill = new WaitUntil(delegate
        {
            if (t_Wait)
            {
                return true;
            }

            return false;
        });
        
        yield return new WaitForSeconds(0.1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
                StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].camPosList[2].cameraTransList[0].position,
                    prankSequenceList[0].propsList[1].propObject,true, delegate
                    {
                        t_Wait = true;
                    }));
            });

        yield return t_WaitUntill;
        yield return new WaitForSeconds(0.5f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[3]);
                propsAnimator.SetTrigger(animationKeyList[3]);
            });
        
        yield return new WaitForSeconds(1.5f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        
        yield return new WaitForSeconds(5.25f);
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        
        yield return new WaitForSeconds(.75f);
        prankSequenceList[0].propsList[1].propsParticle[0].Stop();
        prankSequenceList[0].propsList[1].propsParticle[1].Play();
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        yield return new WaitForSeconds(2f);
        
        
        
        currentPhaseIndex++;
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            });
        //prankSequenceList[0].propsList[1].propsParticle[1].Stop();
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(0.1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos, prankSequenceList[1].propsList[0].propObject,false));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos, prankSequenceList[1].propsList[1].propObject,false));
        
        yield return new WaitForSeconds(0.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[7]);
                propsAnimator.SetTrigger(animationKeyList[7]);
            });
        
        yield return new WaitForSeconds(3.5f);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        prankSequenceList[0].propsList[0].propObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans,prankSequenceList[0].camPosList[3].cameraTransList,
            delegate
            {
                prankSequenceList[0].propsList[2].propObject.GetComponent<Animator>().SetTrigger("entry");
                prankSequenceList[0].propsList[2].propsParticle[0].Play();
            });
        
        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[4]);
                propsAnimator.SetTrigger(animationKeyList[4]);
            });
        
        yield return new WaitForSeconds(7f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[4].cameraOriginTrans,prankSequenceList[0].camPosList[4].cameraTransList,
            delegate
            {
                prankSequenceList[0].propsList[3].propObject.SetActive(true);
            });
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(0.1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[5].finalPos,
            prankSequenceList[1].propsList[5].propObject, true, delegate
            {
                prankSequenceList[1].propsList[5].propObject.transform.SetParent(prankSequenceList[1].propsList[0].propObject.transform);
            }));
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[8]);
                propsAnimator.SetTrigger(animationKeyList[8]);
            });
        yield return new WaitForSeconds(3.25f);
        prankSequenceList[1].propsList[0].propObject.transform.SetParent(prankSequenceList[1].propsList[2].propObject.transform);
        prankSequenceList[1].propsList[0].propObject.transform.localPosition = Vector3.zero;
        
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[5].propObject.transform.SetParent(prankSequenceList[1].propsList[4].propObject.transform);
        prankSequenceList[1].propsList[5].propObject.transform.localPosition = Vector3.zero;
        yield return new WaitForSeconds(.5f);
        prankSequenceList[1].propsList[2].propObject.GetComponent<Animator>().SetTrigger("entry");
        
        ReportPhaseCompletion(currentPhaseIndex+1,true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }
    
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            propsAnimator.SetTrigger(animationKeyList[0]);
            PlayCharacterAnimation(animationKeyList[0]);
            prankSequenceList[0].propsList[0].propObject.SetActive(true);
            prankSequenceList[0].propsList[3].propObject.SetActive(false);
            prankSequenceList[0].propsList[2].propObject.GetComponent<Animator>().SetTrigger("idle");
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
                delegate
                {
                });
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            propsAnimator.SetTrigger(animationKeyList[10]);
            PlayCharacterAnimation(animationKeyList[9]);
            
            prankSequenceList[1].propsList[0].propObject.transform.SetParent(prankSequenceList[1].propsList[6].propObject.transform);
            prankSequenceList[1].propsList[0].propObject.transform.localPosition = prankSequenceList[1].propsList[6].finalPos;
            prankSequenceList[1].propsList[0].propObject.transform.localRotation = Quaternion.identity;
            
            prankSequenceList[1].propsList[5].propObject.transform.SetParent(null);
            prankSequenceList[1].propsList[5].propObject.transform.position = new Vector3(-0.259f,1.3f,-2.46f);
           
            
            
            prankSequenceList[1].propsList[2].propObject.GetComponent<Animator>().SetTrigger("idle");
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                });
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        }
    }
    
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,bool t_IsWorld = true,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_IsWorld? t_TargetTrans.transform.position : t_TargetTrans.transform.localPosition;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            if (t_IsWorld)
            {
                t_TargetTrans.transform.position = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);
            }
            else
            {
                t_TargetTrans.transform.localPosition = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.localPosition.y, t_TargetTrans.transform.localPosition.z);
            }

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                    yield return new WaitForSeconds(0.5f);
                }

                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null));
    }
}
