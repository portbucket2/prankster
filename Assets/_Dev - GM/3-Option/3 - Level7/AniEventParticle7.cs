﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AniEventParticle7 : MonoBehaviour
{
   public List<ParticleSystem> particleList;

   public void PlayParticle()
   {
      particleList[0].Play();
      particleList[1].Play();
   }
}
