Level 24.1

Initial Common Part     : 0-94
Success Animation	: 95-180
Failure Animation	: 190-300

-------------------------------------
Level 24.2

Intro Idle Animation	: N/A	
Initial Common Part	: 310-374
Success Animation	: 375-440
Success Loop Animation	: 610-630		(Not Major Animation Seq)
Failure Animation	: 455-564
Failure Animation Loop	: 565-600

-------------------------------------
Level 24.3

Intro Idle Animation	: 610-630		(Same as 24.2 Success Loop Anim.)
Initial Common Part	: 631-745
Success Animation	: 746-825
Failure Animation	: 860-925


