﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class Level36Gameplay : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    [Header("Level Specific")] public Animator doorAnimator;
    public Transform glassTrans;
    
    public List<Rigidbody> glassRigidbodyList;

    public GameObject bungeeRope;
    public Transform beltTrans;
    public GameObject bungeeBeltObject;


    public Animator pullpushAnimator;
    public GameObject pullObject;
    public GameObject pushObject;
    
    private void Start()
    {
        for (int i = 0; i < glassTrans.childCount-1; i++)
        {
            glassRigidbodyList.Add(glassTrans.GetChild(i).GetComponent<Rigidbody>());
        }
    }

    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            });
    }
    

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForSeconds(0.25f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(2f);

        doorAnimator.enabled = true;
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject,true,
            delegate
            {
            }));
        yield return new WaitForSeconds(.2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans,prankSequenceList[0].camPosList[3].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[1]);
               
            });
        yield return new WaitForSeconds(4f);
        doorAnimator.SetTrigger(animationKeyList[1]);
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(.2f);
        for (int i = 0; i < glassRigidbodyList.Count; i++)
        {
            glassRigidbodyList[i].useGravity = true;
        }
        
        yield return new WaitForSeconds(.75f);
        prankSequenceList[0].propsList[0].propsParticle[1].Play();
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        
        yield return new WaitForSeconds(2.25f);
        currentPhaseIndex++;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
       
        yield return new WaitForSeconds(0.25f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1.5f);
        bungeeRope.SetActive(true);
        yield return StartCoroutine(BeltMoveRoutine(beltTrans.localPosition, bungeeBeltObject, delegate
        {
            bungeeBeltObject.transform.SetParent(beltTrans);
            bungeeBeltObject.transform.localPosition = new Vector3(-0.017f,0.075f,-.216f);
        }));
        yield return new WaitForSeconds(1f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[4]);
            });
        yield return new WaitForSeconds(6f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans,prankSequenceList[1].camPosList[2].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(.65f);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        
        yield return new WaitForSeconds(.4f);
        prankSequenceList[1].propsList[1].propsParticle[1].Play();
        
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForSeconds(0.25f);
        doorAnimator.SetTrigger("plate_rotate");
        
        pullpushAnimator.SetTrigger("rotate");
        pullObject.SetActive(false);
        pushObject.SetActive(true);
        yield return new WaitForSeconds(1f);
      
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                doorAnimator.enabled = true;
                //pullpushAnimator.enabled = false;
                PlayCharacterAnimation(animationKeyList[2]);
                doorAnimator.SetTrigger(animationKeyList[2]);
            });
        
        yield return new WaitForSeconds(10f);
       
        pullObject.SetActive(true);
        pushObject.SetActive(false);
        yield return new WaitForSeconds(4f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(0.25f);
        prankSequenceList[1].propsList[0].propObject.SetActive(true);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[3].cameraOriginTrans,prankSequenceList[1].camPosList[3].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[3]);
            });
        yield return new WaitForSeconds(1.7f);
        
        prankSequenceList[1].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().SetTrigger("run");
        yield return new WaitForSeconds(.3f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(5f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }

   
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            PlayCharacterAnimation(animationKeyList[5]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans,prankSequenceList[0].camPosList[3].cameraTransList,
                delegate
                {
                });
            prankSequenceList[1].propsList[0].propObject.transform.GetChild(0).GetComponent<Animator>().SetTrigger("idle");
            prankSequenceList[1].propsList[0].propObject.SetActive(false);
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        }
    }
    

    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,bool t_IsWorld = true,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_IsWorld? t_TargetTrans.transform.position : t_TargetTrans.transform.localPosition;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            if (t_IsWorld)
            {
                t_TargetTrans.transform.position = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);
            }
            else
            {
                t_TargetTrans.transform.localPosition = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.localPosition.y, t_TargetTrans.transform.localPosition.z);
            }

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null));
    }
    
    public IEnumerator BeltMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.localPosition;
        Vector3 t_DestValue = new Vector3(178f,1.589f,-34.8f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.localPosition= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.localPosition.y,Mathf.Lerp(t_CurrentFillValue.z,t_DestValue.z,t_Progression));
 
            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(BeltMoveRoutine(Vector3.zero,null,null));
    }
}
