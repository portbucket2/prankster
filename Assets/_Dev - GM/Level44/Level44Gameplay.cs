﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class Level44Gameplay : PrankLevelBehaviour
{
   public Animator playerAnimatorReference;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    [Header("level scepecif")] public Animator propsAnimator;
    
    
    public SkinMatController skinMatCon;
    
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i],Animator.StringToHash(animationKeyList[i]));
        }
        
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    
    protected override void StartLevel()
    {
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
                PlayCharacterAnimation(animationKeyList[6]);
                propsAnimator.SetTrigger(animationKeyList[6]);
            });
    }
    

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForSeconds(.5f);
        
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, false, delegate { }));
        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, false, delegate { }));
        yield return new WaitForSeconds(1f);
        
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[1]);
                propsAnimator.SetTrigger(animationKeyList[1]);
            });
        
        yield return new WaitForSeconds(3.5f);
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        
        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
            });
        yield return new WaitForSeconds(1.25f);
        prankSequenceList[0].propsList[0].propsParticle[0].Stop();
        skinMatCon.SetMat(1);

        float t = 0;
        while (t<0.5f)
        {
            t += Time.deltaTime;
            skinMatCon.SetMatValue(1,"_MaskStrength",t);
            yield return new WaitForEndOfFrame();
        }
        
        yield return new WaitForSeconds(3f);
        //----- start phase two-----//////
        ReportPhaseCompletion(currentPhaseIndex+1,false);
        
        currentPhaseIndex++;
       
        
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            });
        
        prankSequenceList[0].propsList[0].propObject.SetActive(false);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos,
            prankSequenceList[1].propsList[1].propObject, false, delegate { }));
        yield return new WaitForSeconds(1f);
        
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
               PlayCharacterAnimation(animationKeyList[3]);
               propsAnimator.SetTrigger(animationKeyList[3]);
            });
        bool t = true;
        yield return new WaitForSeconds(3.25f);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        while (t)
        {
            yield return new WaitForSeconds(1f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans,prankSequenceList[1].camPosList[2].cameraTransList,
                delegate
                {
                });
            yield return new WaitForSeconds(1.5f);
            ReportPhaseCompletion(currentPhaseIndex+1,true);
            t = false;
        }
        
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            
            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos,
            prankSequenceList[0].propsList[2].propObject, false, delegate
            {
                prankSequenceList[0].propsList[2].propObject.GetComponent<Animator>().enabled = true;
            }));
        yield return new WaitForSeconds(1f);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[2]);
                propsAnimator.SetTrigger(animationKeyList[2]);
            });
        
        yield return new WaitForSeconds(5.5f);
        prankSequenceList[0].propsList[1].propsParticle[0].Stop();
        prankSequenceList[0].propsList[2].propsParticle[0].Play();
        
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[2].propsParticle[0].Stop();
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(currentPhaseIndex+1,false);
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForSeconds(.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].finalPos,
            prankSequenceList[1].propsList[0].propObject, false, delegate { }));
        yield return new WaitForSeconds(1f);
        
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans,prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[4]);
                propsAnimator.SetTrigger(animationKeyList[4]);
            });
        yield return new WaitForSeconds(3.25f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        
        yield return new WaitForSeconds(4f);
        ReportPhaseCompletion(currentPhaseIndex+1,true);
    }
    
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            PlayCharacterAnimation(animationKeyList[5]);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[0].initialPos,
                prankSequenceList[1].propsList[0].propObject, false, delegate { }));
            
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
                delegate
                {
                });
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        }
    }
    

    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            
            default:
                break;
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,bool t_IsWorld = true,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_IsWorld? t_TargetTrans.transform.position : t_TargetTrans.transform.localPosition;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
         
 
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
 
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            if (t_IsWorld)
            {
                t_TargetTrans.transform.position = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);
            }
            else
            {
                t_TargetTrans.transform.localPosition = new Vector3(
                    Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                    t_TargetTrans.transform.localPosition.y, t_TargetTrans.transform.localPosition.z);
            }

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }
 
            yield return t_CycleDelay;
        }
      
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null));
    }
}
