﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventOneExtra : MonoBehaviour
{
    public SkinMatController skinMat;
    public List<ParticleSystem> particleSystemList;

    public void PlayParticle(int i)
    {
        particleSystemList[i].Play();
    }

    public void ChangePlayerMAT()
    {
        skinMat.SetMat(0);
    }

    public void StopParticle(int i)
    {
        particleSystemList[i].Stop();
    }

    public void SetupAnimation()
    {
        PlayerControllerLevelOne.Instance.RotatePlayer();
    }

    public void SetUpPosition()
    {
        PlayerControllerLevelOne.Instance.MovePosition();
    }
}
