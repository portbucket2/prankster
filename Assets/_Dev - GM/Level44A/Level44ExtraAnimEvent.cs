﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level44ExtraAnimEvent : MonoBehaviour
{
    public Animator candyAnimator;

    public List<ParticleSystem> particle;

    public void PlayParticle(int i)
    {
        particle[i].Play();
    }
    
    public void PlayCandyMouth()
    {
        candyAnimator.SetTrigger("eat");
    }
}
