﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level29ExtraAnimEvent : MonoBehaviour
{
   public Animator colorBottleAnim;
   
   
   public List<ParticleSystem> particleList;

   public void PlayParticle(int i)
   {
      particleList[i].Play();
   }

   public void PlayColorBottle()
   {
      colorBottleAnim.enabled = true;
   }
}
