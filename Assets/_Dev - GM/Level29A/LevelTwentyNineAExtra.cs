﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;
using UnityEngine.Events;

public class LevelTwentyNineAExtra : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;

    public bool isPhaseOne = true;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform shoeZoomPos;
    public Transform shoeZoomFocus;
    public Transform closeView1;
    public Transform closeView2;
    public Transform closeView3;
    public Transform closeView4;
    public Transform closeViewFocus1;
    public Transform closeViewFocus2;
    public Transform bagZoomPos;

    public Transform phaseTwoStart;
    public Transform phaseTwoMovePos;
    public Transform phaseTwoMoveFocus;
    public Transform phaseTwoSlamPos;
    public Transform phaseTwoSlamFocus;
    public Transform phaseTwoEndPos;
    public Transform phaseTwoEndFocus;

    [Header("particles")]
    public ParticleSystem punchFaceparticle;
    public ParticleSystem feetHurtParticle;
    public ParticleSystem waterFromShoesParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public GameObject iPad;
    public GameObject cockroch;
    public GameObject shoe;
    public GameObject bagOpened;
    public GameObject bagClosed;
    public Animator waterBottleHolder;
    public Animator superGlueHolder;
    public Animator bowlingBallHolder;
    public Animator cockRoachHolder;
    public GlueVisual glueVisual;
    
    
    [Header("GM Modification")] 
    public bool isExtraPhase;

    public Animator extraProps;
    public GlueVisual glueVisualExtra;
    
    public List<PrankSequence> prankSequenceList;
    
    
    
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Move = "move";

    bool isAnsRight = false;
    List<Transform> focuslist;

    // prank level behavior start

    protected override void StartLevel()
    {
        //UIManager.Instance
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else if (isExtraPhase)
        {
            StopAllCoroutines();
            StartCoroutine(StartExtraPhaseRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
    }
    

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            cockRoachHolder.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
        else if (isExtraPhase)
        {
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            bowlingBallHolder.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            waterBottleHolder.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else if (isExtraPhase)
        {
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }
            
            if (isExtraPhase)
            {
                inputComplete = false;
                rigAnimator.SetTrigger("1_2_reset");
                extraProps.SetTrigger("1_2_reset");
                glueVisualExtra.ResetGlue();
            }

            StartLevel();
        }

    }
    
    private IEnumerator StartExtraPhaseRoutine()
    {
        yield return WAITONE;
        Debug.LogError("EXTRA Phaes Started");
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans,prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 2);
            });
        
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        yield return WAITONE;
        Debug.LogError("do it here");
        
        bagClosed.SetActive(false);
        bagOpened.SetActive(true);
        extraProps.gameObject.SetActive(true);
        
        if (isAnsRight)
        {
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
                prankSequenceList[0].propsList[1].propObject));
            
            
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
                prankSequenceList[0].propsList[0].propObject));

            yield return WAITONE;
            
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    rigAnimator.SetTrigger("1_2_success");
                    extraProps.SetTrigger("1_2_success");
                });
            yield return WAITFOUR;
            yield return WAITFOUR;
            yield return WAITTHREE;
            
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
                delegate
                {
                });
            yield return WAITFOUR;
            yield return WAITTHREE;
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans,prankSequenceList[0].camPosList[3].cameraTransList,
                delegate
                {
                });
            
            yield return WAITFOUR;
            ReportPhaseCompletion(3,true);
        }
        else
        {
            while (glueVisualExtra.manualglue(Time.deltaTime * 2f))
            {
                yield return new WaitForEndOfFrame();
            }

            yield return WAITTWO;
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans,prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    rigAnimator.SetTrigger("1_2_failed");
                    extraProps.SetTrigger("1_2_failed");
                });
            yield return WAITFOUR;
            yield return WAITFOUR;
            yield return WAITTHREE;
            
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans,prankSequenceList[0].camPosList[2].cameraTransList,
                delegate
                {
                });
            
            yield return WAITFOUR;
            yield return WAITFOUR;
            
            ReportPhaseCompletion(3,true);
        }
    }


    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
         */
        bagClosed.SetActive(false);
        bagOpened.SetActive(true);
        cockroch.SetActive(false);
        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(shoeZoomFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(shoeZoomPos, focuslist);
        yield return WAITTWO;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail


        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(closeViewFocus1);
        CameraMovementController.Instance.FocusCameraWithOrigin(closeView1, focuslist);
        yield return WAITONE;
        if (isAnsRight)
        {
            cockroch.SetActive(true);
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(closeView2, focuslist);
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus1);
            CameraMovementController.Instance.FocusCameraWithOrigin(closeView4, focuslist);
            yield return WAITONE;
            yield return WAITHALF;
            punchFaceparticle.Play();
            yield return WAITONE;
            //focuslist = new List<Transform>();
            //focuslist.Add(closeViewFocus1);
            //CameraMovementController.Instance.FocusCameraWithOrigin(closeView4, focuslist);
            iPad.SetActive(false);
            yield return WAITTWO;
            isPhaseOne = false;
            yield return WAITONE;
            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus2);
            CameraMovementController.Instance.FocusCameraWithOrigin(closeView2, focuslist);
            yield return WAITTWO;
            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus1);
            CameraMovementController.Instance.FocusCameraWithOrigin(closeView4, focuslist);
            yield return WAITONE;
            waterFromShoesParticle.Play();
            yield return WAITONE;

            //focuslist = new List<Transform>();
            //focuslist.Add(closeViewFocus1);
            //CameraMovementController.Instance.FocusCameraWithOrigin(closeView4, focuslist);
            yield return WAITTWO;
            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        glueVisual.ResetGlue();
        bagClosed.SetActive(false);
        bagOpened.SetActive(true);
        shoe.SetActive(false);
        cockroch.SetActive(false);
        iPad.SetActive(false);
        rigAnimator.SetTrigger(IDLE2);
        propAnimator.SetTrigger(IDLE1);

        focuslist = new List<Transform>();
        focuslist.Add(closeViewFocus1);
        CameraMovementController.Instance.FocusCameraWithOrigin(bagZoomPos, focuslist);
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraStartPos);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartFocus, focuslist);

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        if (!isAnsRight)
        {
            superGlueHolder.SetTrigger(Move);
            while (glueVisual.manualglue(Time.deltaTime * 2f))
            {
                yield return new WaitForEndOfFrame();
            }
        }


        yield return WAITTWO;

        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        //

        if (isAnsRight)
        {
            bagClosed.SetActive(true);
            bagOpened.SetActive(false);
            rigAnimator.SetTrigger(Success2);
            propAnimator.SetTrigger(Success2);
            
            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus1);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoStart, focuslist);
            yield return WAITFOUR;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoMoveFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoMovePos, focuslist);
            yield return WAITTHREE;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoSlamFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoSlamPos, focuslist);
            yield return WAITONE;
            feetHurtParticle.Play();
            rigAnimator.speed = 0.2f;
            yield return WAITTHREE;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoEndPos, focuslist);
            rigAnimator.speed = 1;
            yield return WAITONE;
            ReportPhaseCompletion(2,  false);
            //UIManager.Instance.ShowLevelComplete();
            inputComplete = false;

            isExtraPhase = true;
            StartLevel();
        }
        else
        {
            bagClosed.SetActive(true);
            bagOpened.SetActive(false);
            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);

            focuslist = new List<Transform>();
            focuslist.Add(closeViewFocus1);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoStart, focuslist);
            yield return WAITFOUR;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoMoveFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoMovePos, focuslist);
            yield return WAITTHREE;
            //focuslist = new List<Transform>();
            //focuslist.Add(phaseTwoSlamFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoSlamPos, focuslist);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(phaseTwoMoveFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phaseTwoEndPos, focuslist);
            yield return WAITTWO;


            ReportPhaseCompletion(2,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }

        inputComplete = false;

    }


    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest,GameObject t_TargetTrans,UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);
             
     
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();
     
        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
    
            t_TargetTrans.transform.position= new Vector3(Mathf.Lerp(t_CurrentFillValue.x,t_DestValue.x,t_Progression),
                t_TargetTrans.transform.position.y,Mathf.Lerp(t_CurrentFillValue.z,t_DestValue.z,t_Progression));
     
            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }
     
            yield return t_CycleDelay;
        }
          
        StopCoroutine(XAxisMoveRoutine(Vector3.zero,null,null));
    }





}

