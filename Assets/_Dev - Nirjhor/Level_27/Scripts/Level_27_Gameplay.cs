﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Level_27_Gameplay : PrankLevelBehaviour
{
    //[Space(30.0f)]
    [Header("Level 27 Data:")]
    //public ScriptableObjective objectiveData;
    public PlayableDirector timelineDirector;
    private TimelineAsset timelineAsset;

    [Header("Props")]
    public Transform clothGhost;
    public Transform targetClothGhostPosition;
    public Transform coatHat;
    public Transform targetCoatHatPosition;
    public Transform coatHatInitialPosition;
    public float propPlacementTime;

    [Header("Material swap references")]
    public SkinMatController skinMatController;
    private int wetClothesMatIndex = 1;
    //public Transform player_Mesh_Parent;
    //public Material wetClothes_Mat;

    [System.Serializable]
    public struct LightDarkMaterials
    {
        public string materialName;
        public Material lightMaterial;
        public Material darkMaterial;
        public MeshRenderer[] meshRenderersReferencingThatMaterial;
    }
    public LightDarkMaterials[] Light_Dark_Mats;

    [Header("Revival timeline timestamps")]
    public float phaseOneReviveTime;
    public float phaseTwoReviveTime;

    private bool loopBackPointPassed = false;
    private bool shouldLoop = true;
    private bool loopBackOnce = false;

    private bool phaseTwoRevived = false;

    // Start is called before the first frame update
    private void Awake()
    {
        phaseTwoRevived = false;
        timelineAsset = (TimelineAsset)timelineDirector.playableAsset;
        PresetMuteUnmuteTracks();
        timelineDirector.Play();
    }

    public void LoopBackPointPassed()
    {
        loopBackPointPassed = true;
    }

    public void IntroLoopTimeline()
    {
        if (loopBackOnce)
        {
            loopBackOnce = false;
            SetTimelineTime(timelineDirector, 0f);
            return;
        }

        if (shouldLoop)
        {
            SetTimelineTime(timelineDirector, 0f);
            loopBackPointPassed = false;
        }
    }

    private void PresetMuteUnmuteTracks()
    {
        UnMuteTrack(1);
        UnMuteTrack(2);
        UnMuteTrack(3);
        UnMuteTrack(4);
        UnMuteTrack(5);
        UnMuteTrack(6);
        MuteTrack(7);
        MuteTrack(8);
        MuteTrack(9);
        MuteTrack(10);
        MuteTrack(11);
        MuteTrack(12);
        MuteTrack(13);
        MuteTrack(14);
    }

    IEnumerator PauseTimelineAfter(float t)
    {
        yield return new WaitForSeconds(t);
        PauseTimeline(timelineDirector);
    }

    IEnumerator PauseTimelineForTime(float t)
    {
        PauseTimeline(timelineDirector);
        yield return new WaitForSeconds(t);
        ResumeTimeline(timelineDirector);
    }

    void PauseTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
    }

    void ResumeTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    void SetTimelineTime(PlayableDirector playableDirector, float t)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetTime(t);
    }

    void MuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = true;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = false;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    protected override void StartLevel()
    {
        currentPhaseIndex = 0;
        Level_27_Start();
    }

    void Level_27_Start()
    {
        if (loopBackPointPassed)
            loopBackOnce = true;
        shouldLoop = false;
    }

    IEnumerator TimelineRoutine()
    {
        yield return new WaitForSeconds(3f);
        timelineDirector.Play();
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(1.0);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void PresentObjectiveOne()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }

    public void SwitchOnLight()
    {
        foreach (LightDarkMaterials lightDarkMat in Light_Dark_Mats)
        {
            foreach (MeshRenderer meshRenderer in lightDarkMat.meshRenderersReferencingThatMaterial)
            {
                meshRenderer.material = lightDarkMat.lightMaterial;
            }
        }
    }

    void RevertToDarkMaterials()
    {
        foreach (LightDarkMaterials lightDarkMat in Light_Dark_Mats)
        {
            foreach (MeshRenderer meshRenderer in lightDarkMat.meshRenderersReferencingThatMaterial)
            {
                meshRenderer.material = lightDarkMat.darkMaterial;
            }
        }
    }

    public void PresentObjectiveTwo()
    {
        if (!phaseTwoRevived)
        {
            PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false); 
        }
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    public void LevelFailed()
    {
        PauseTimeline(timelineDirector);
        if (currentPhaseIndex > 0)
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  true);
        else
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    public void LevelComplete()
    {
        PauseTimeline(timelineDirector);
        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                RightPhaseOne();
                break;
            case 1:
                RightPhaseTwo();
                break;
            default:
                break;
        }
    }

    void RightPhaseOne()
    {
        //Do right phase one
        StartCoroutine(PlaceProp(clothGhost, targetClothGhostPosition.position));
        currentPhaseIndex++;
    }

    IEnumerator PlaceProp(Transform prop, Vector3 targetPropPosition)
    {
        PauseTimeline(timelineDirector);
        Vector3 propStartPos = prop.position;
        float startTime = Time.realtimeSinceStartup;

        while(prop.position != targetPropPosition)
        {
            prop.position = Vector3.Lerp(propStartPos, targetPropPosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propPlacementTime));
            yield return null;
        }
        ResumeTimeline(timelineDirector);
    }

    void RightPhaseTwo()
    {
        ResumeTimeline(timelineDirector);
    }

    public void WetClothes()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = wetClothes_Mat;
        //}
        skinMatController.SetMat(wetClothesMatIndex);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                WrongPhaseOne();
                break;
            case 1:
                WrongPhaseTwo();
                break;
            default:
                break;
        }
    }

    void WrongPhaseOne()
    {
        MuteTrack(1);
        MuteTrack(2);
        MuteTrack(3);
        MuteTrack(4);
        MuteTrack(5);
        MuteTrack(6);
        UnMuteTrack(7);
        UnMuteTrack(8);
        StartCoroutine(PlaceProp(coatHat, targetCoatHatPosition.position));
    }

    void WrongPhaseTwo()
    {
        MuteTrack(1);
        MuteTrack(2);
        MuteTrack(3);
        MuteTrack(4);
        MuteTrack(5);
        MuteTrack(6);
        UnMuteTrack(9);
        UnMuteTrack(10);
        UnMuteTrack(11);
        UnMuteTrack(12);
        UnMuteTrack(13);
        UnMuteTrack(14);
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        if (currentPhaseIndex > 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0)
            ResetPhaseOne();
    }

    void ResetPhaseOne()
    {
        RevertToDarkMaterials();
        coatHat.position = coatHatInitialPosition.position;
        PauseTimeline(timelineDirector);
        SetTimelineTime(timelineDirector, phaseOneReviveTime);
        PresetMuteUnmuteTracks();
    }

    void ResetPhaseTwo()
    {
        phaseTwoRevived = true;
        PauseTimeline(timelineDirector);
        SetTimelineTime(timelineDirector, phaseTwoReviveTime);
        PresetMuteUnmuteTracks();
    }
}