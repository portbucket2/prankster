﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_57_PrankTools : MonoBehaviour
{
    public Level_57_Gameplay level_57_Gameplay;
    public Animator prankToolAnimator;
    public GlueVisual glueVisual;

    [Header("Particle Systems")]
    public ParticleSystem gunPowder;

    public void ResumeTimeline()
    {
        level_57_Gameplay.ResumeTimeline();
    }

    public void StartApplyingGlue()
    {
        StartCoroutine(ApplyGlue());
    }

    IEnumerator ApplyGlue()
    {
        while (glueVisual.manualglue(Time.deltaTime))
        {
            yield return null;
        }
    }

    public void SprinkleGunPowder()
    {
        gunPowder.Play();
        StartCoroutine(CloseLidAfter(1f));
    }

    IEnumerator CloseLidAfter(float t)
    {
        yield return new WaitForSeconds(t);
        prankToolAnimator.SetTrigger("LidClose");
    }
}
