﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering;
using UnityEngine.Timeline;

public class Level_58_Gameplay : PrankLevelBehaviour
{
    //[Space(30.0f)]
    [Header("Level 58 Data:")]
    //public ScriptableObjective objectiveData;
    public PlayableDirector timelineDirector;
    private TimelineAsset timelineAsset;

    [System.Serializable]
    public struct TrackController
    {
        public string sequenceName;
        public string tracksToMute;
        public string tracksToUnmute;
    }
    [Header("Mute Unmute Timeline Tracks:")]
    public TrackController[] trackControls;

    [Header("PrankTools")]
    private float propPlacementTime;
    public Animator prankToolAnimator;
    private string[] prankToolTriggers = { "PhaseOneSuccess", "PhaseOneFailure", "PhaseTwoSuccess", "PhaseTwoFailure" };
    public GlueVisual glueVisual;

    [Header("Material swap references")]
    public SkinMatController skinMatController;

    [Header("Revival timeline timestamps")]
    public float phaseOneReviveTime;
    public float phaseTwoReviveTime;

    private bool phaseTwoRevived = false;


    // Start is called before the first frame update
    void Awake()
    {
        phaseTwoRevived = false;
        timelineAsset = (TimelineAsset)timelineDirector.playableAsset;
        TrackControl(trackControls[0]);
        timelineDirector.Play();
        ResumeTimeline(timelineDirector);
    }

    void TrackControl(TrackController tc)
    {
        List<int> tracksToMute = new List<int>();
        List<int> tracksToUnmute = new List<int>();

        char[] splitters = { ',', ' ' };

        string stringToSplit1 = tc.tracksToMute;
        string[] listOfTracksToMute = stringToSplit1.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
        foreach (string trackNumString in listOfTracksToMute)
        {
            tracksToMute.Add(Convert.ToInt32(trackNumString));
        }

        string stringToSplit2 = tc.tracksToUnmute;
        string[] listOfTracksToUnmute = stringToSplit2.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
        foreach (string trackNumString in listOfTracksToUnmute)
        {
            tracksToUnmute.Add(Convert.ToInt32(trackNumString));
        }

        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);

        PauseTimeline(timelineDirector);
    }

    protected override void StartLevel()
    {
        currentPhaseIndex = 0;
        Level_50_Start();
    }

    void Level_50_Start()
    {
        TrackControl(trackControls[1]);
        ResumeTimeline(timelineDirector);
    }

    IEnumerator PauseTimelineAfter(float t)
    {
        yield return new WaitForSeconds(t);
        PauseTimeline(timelineDirector);
    }

    IEnumerator PauseTimelineForTime(float t)
    {
        PauseTimeline(timelineDirector);
        yield return new WaitForSeconds(t);
        ResumeTimeline(timelineDirector);
    }

    void PauseTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
    }

    void ResumeTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void ResumeTimeline()
    {
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    void SetTimelineTime(PlayableDirector playableDirector, float t)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetTime(t);
    }

    void MuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = true;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void MuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = true;
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = false;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = false;
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    IEnumerator TimelineRoutine()
    {
        yield return new WaitForSeconds(3f);
        timelineDirector.Play();
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(1.0);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void PresentObjectiveOne()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }

    public void PresentObjectiveTwo()
    {
        if (!phaseTwoRevived)
        {
            PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false);
        }
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    public void LevelFailed()
    {
        PauseTimeline(timelineDirector);
        if (currentPhaseIndex > 0)
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  true);
        else
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    public void LevelComplete()
    {
        PauseTimeline(timelineDirector);
        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                RightPhaseOne();
                break;
            case 1:
                RightPhaseTwo();
                break;
            default:
                break;
        }
    }

    void RightPhaseOne()
    {
        //Do right phase one
        prankToolAnimator.SetTrigger(prankToolTriggers[0]);
        currentPhaseIndex++;
    }

    IEnumerator PlaceProp(Transform prop, Vector3 targetPropPosition)
    {
        PauseTimeline(timelineDirector);
        prop.gameObject.SetActive(true);
        Vector3 propStartPos = prop.position;
        float startTime = Time.realtimeSinceStartup;

        while (prop.position != targetPropPosition)
        {
            prop.position = Vector3.Lerp(propStartPos, targetPropPosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propPlacementTime));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
        yield return new WaitForSeconds(2f);
        prop.gameObject.SetActive(false);
    }

    IEnumerator SwitchProp(Transform originalProp, Transform switchedProp)
    {
        //MuteTrack(6);
        PauseTimeline(timelineDirector);
        Vector3 originalPropStartPos = originalProp.position;
        float startTime1 = Time.realtimeSinceStartup;

        while (originalProp.position != switchedProp.position)
        {
            originalProp.position = Vector3.Lerp(originalPropStartPos, switchedProp.position,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime1) / (propPlacementTime / 2f)));
            yield return null;
        }

        Vector3 switchedPropStartPos = switchedProp.position;
        float startTime2 = Time.realtimeSinceStartup;
        while (switchedProp.position != originalPropStartPos)
        {
            switchedProp.position = Vector3.Lerp(switchedPropStartPos, originalPropStartPos,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime2) / (propPlacementTime / 2f)));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
    }

    void RightPhaseTwo()
    {
        ResumeTimeline();
    }

    public void Play_58_2_SuccessPrankTool()
    {
        PauseTimeline(timelineDirector);
        StartCoroutine(ApplyGlue());
    }

    IEnumerator ApplyGlue()
    {
        while (glueVisual.manualglue(Time.deltaTime * 2f))
        {
            yield return null;
        }
        prankToolAnimator.SetTrigger(prankToolTriggers[2]);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                WrongPhaseOne();
                break;
            case 1:
                WrongPhaseTwo();
                break;
            default:
                break;
        }
    }

    void WrongPhaseOne()
    {
        TrackControl(trackControls[2]);
        prankToolAnimator.SetTrigger(prankToolTriggers[1]);
    }

    void WrongPhaseTwo()
    {
        TrackControl(trackControls[3]);
        ResumeTimeline();
    }

    public void Play_58_2_FailurePrankTool()
    {
        PauseTimeline(timelineDirector);
        prankToolAnimator.SetTrigger(prankToolTriggers[3]);
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        if (currentPhaseIndex > 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0)
            ResetPhaseOne();
    }

    void ResetPhaseOne()
    {
        PauseTimeline(timelineDirector);
        SetTimelineTime(timelineDirector, phaseOneReviveTime);
    }

    void ResetPhaseTwo()
    {
        phaseTwoRevived = true;
        PauseTimeline(timelineDirector);

        SetTimelineTime(timelineDirector, phaseTwoReviveTime);
        TrackControl(trackControls[4]);
        ResumeTimeline(timelineDirector);
    }
}


