﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_58_PrankTools : MonoBehaviour
{
    public Level_58_Gameplay level_58_Gameplay;

    public GameObject screwDriver1Trails;
    public GameObject screwDriver2Trails;

    public GameObject swingChildRocket;

    public void ResumeTimeline()
    {
        level_58_Gameplay.ResumeTimeline();
    }

    public void ResumeTimelineActivateChildRocket()
    {
        swingChildRocket.SetActive(true);
        level_58_Gameplay.ResumeTimeline();
    }

    public void ActivateScrewDriver1Trails()
    {
        screwDriver1Trails.SetActive(true);
    }
    public void DeActivateScrewDriver1Trails()
    {
        screwDriver1Trails.SetActive(false);
    }

    public void ActivateScrewDriver2Trails()
    {
        screwDriver2Trails.SetActive(true);
    }
    public void DeActivateScrewDriver2Trails()
    {
        screwDriver2Trails.SetActive(false);
    }
}
