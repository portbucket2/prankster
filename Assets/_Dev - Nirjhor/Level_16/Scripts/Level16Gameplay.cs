﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Level16Gameplay : PrankLevelBehaviour
{
    //public ScriptableObjective objectiveData;
    
    public Animator playerAnimatorReference;
    public Animator propAnimatorReference;
    public Animator bonusPropAnimator;


    public ParticleSystem abductParticle;
    readonly string Bonus = "bonus";

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    public Camera mainCamera;
    public Transform player;
    public Transform playerHead;
    public Transform bucket;
    public Vector3 bucketLocalPos;
    public Vector3 bucketLocalPosInFlyoff;
    public GameObject bottle;
    public SkinMatController skinMatController;
    private int coloredMatIndex = 1;
    //public Transform playerMeshTransform;
    //public Material coloredBody;
    //public Material burntBody;

    public GameObject glueSurface;
    public Transform cameraStart;
    public Transform playerStart;

    [System.Serializable]
    public struct Sequence
    {
        public Transform cameraOrientation;
        public float cameraTransitionDuration;
        public bool useCurve;
        public AnimationCurve transitionCurve;
        public float transitionDelay;

        public Transform playerPosition;
        public float playerReachPositionTime;
        public string playerAnimationTrigger;

        public string propAnimTrigger;
    }
    public Sequence[] transitionSequences;
    public Sequence[] successSequences;
    public Sequence[] failureSequences;


    public enum SequenceCompleter
    {
        PlayerMove,
        CameraTransition
    }

    protected override void StartLevel()
    {
        //Debug.Log("LEVEL_START");
        currentPhaseIndex = 0;
        StartCoroutine(StartLevelRoutine());
    }

    public void PlaySequence(Sequence sequence, SequenceCompleter sequenceCompleter, Action OnSequenceEnd = null)
    {
        switch (sequenceCompleter)
        {
            case SequenceCompleter.PlayerMove:
                AnimateProp(sequence);
                MovePlayer(sequence, OnSequenceEnd);
                SwitchCameraTo(sequence);
                break;
            case SequenceCompleter.CameraTransition:
                AnimateProp(sequence);
                MovePlayer(sequence);
                SwitchCameraTo(sequence, OnSequenceEnd);
                break;
            default:
                break;
        }
    }

    void AnimateProp(Sequence sequence)
    {
        if (sequence.propAnimTrigger.Length > 0)
        {
            propAnimatorReference.enabled = true;
            propAnimatorReference.SetTrigger(sequence.propAnimTrigger); 
        }
    }

    void MovePlayer(Sequence sequence, Action OnMoveComplete = null)
    {
        void TransitionStartAction()
        {
            playerAnimatorReference.SetTrigger(sequence.playerAnimationTrigger);
        }
        StartCoroutine(Interpolate(player, sequence.playerPosition, sequence.playerReachPositionTime, 
            OnMoveComplete, sequence.transitionCurve, sequence.transitionDelay, 
            () => TransitionStartAction(), sequence.useCurve));
    }

    public void ChildBucket()
    {
        propAnimatorReference.enabled = false;
        bucket.transform.parent = playerHead;
        bucket.transform.localPosition = bucketLocalPos;
        bucket.GetComponent<Rigidbody>().isKinematic = true;
        //bucket.transform.localScale = new Vector3(1.2f, 1.1f, 1.2f);
    }

    void SwitchCameraTo(Sequence sequence, Action OnSwitchComplete = null)
    {
        StartCoroutine(Interpolate(mainCamera.transform, sequence.cameraOrientation, sequence.cameraTransitionDuration
            , OnSwitchComplete, sequence.transitionCurve, sequence.transitionDelay, null, sequence.useCurve));
    }    

    private IEnumerator Interpolate(Transform currentTransform, Transform targetTransform, float lerpTime, 
        Action OnComplete = null, AnimationCurve transitionCurve = null, float transitionDelay = 0f, Action OnStartTransition = null, bool useCurve = false)
    {
        yield return new WaitForSeconds(transitionDelay);
        OnStartTransition?.Invoke();

        float startTime = Time.realtimeSinceStartup;
        Vector3 currentPosition = currentTransform.position;
        Quaternion currentRotatoin = currentTransform.rotation;
        bool complete = false;

        while (!complete)
        {
            //yield return null;
            float progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / lerpTime);
            if (useCurve)
            {
                currentTransform.position = Vector3.Lerp(currentPosition, targetTransform.position, transitionCurve.Evaluate(progress));
                currentTransform.rotation = Quaternion.Slerp(currentRotatoin, targetTransform.rotation, transitionCurve.Evaluate(progress));
            }
            else
            {
                currentTransform.position = Vector3.Lerp(currentPosition, targetTransform.position, progress);
                currentTransform.rotation = Quaternion.Slerp(currentRotatoin, targetTransform.rotation, progress);
            }
                
            if (currentTransform.position == targetTransform.position)
            {
                complete = true;
                break;
            }
            yield return null;
        }

        OnComplete?.Invoke();
    }

    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();
        PlaySequence(transitionSequences[0], SequenceCompleter.CameraTransition, () => {
            //Debug.Log("Phase01 Transition Complete");
            UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
        });
    }

    protected override void RVAnswerSelected()
    {
        bucket.gameObject.SetActive(false);
        PlaySequence(successSequences[6], SequenceCompleter.PlayerMove,
                        () =>
        StartCoroutine(RVSelectedPhaseRoutine()));
    }
    
    IEnumerator RVSelectedPhaseRoutine()
    {
        yield return ENDOFFRAME;
        //yield return WAITHALF;
        abductParticle.Play();
        playerAnimatorReference.SetTrigger(Bonus);
        bonusPropAnimator.SetTrigger(Bonus);
        yield return WAITFOUR;
        yield return WAITFOUR;
        abductParticle.Stop();
        yield return WAITONE;
        currentPhaseIndex++;
        //yield return WAITTWO;
        ReportPhaseCompletion(2, true);
        


    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return new WaitForEndOfFrame();
        //Debug.Log("Right One Done");
        void SubsequentSuccessSequences()
        {
            StartCoroutine(ChangeMaterial());
            PlaySequence(successSequences[1], SequenceCompleter.CameraTransition,
            () => PlaySequence(transitionSequences[2], SequenceCompleter.CameraTransition,
            () =>
            {
                Debug.Log("Success01 Complete");
                currentPhaseIndex++;
                PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false);
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            }
            ));
        }

        void FirstSuccessSequence()
        {
            PlaySequence(successSequences[0], SequenceCompleter.PlayerMove, () => SubsequentSuccessSequences());
        }

        bucket.gameObject.SetActive(true);
        PlaySequence(transitionSequences[1], SequenceCompleter.CameraTransition, () => FirstSuccessSequence());
    }

    IEnumerator ChangeMaterial()
    {
        yield return new WaitForSeconds(0.5f);
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in playerMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = coloredBody;
        //}
        skinMatController.SetMat(coloredMatIndex);
    }

    public void ChangeToBurntMaterial()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in playerMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = burntBody;
        //}
        skinMatController.SetMat(coloredMatIndex);
    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        //Debug.Log("Right One Done");
        void PlaySuccessSequence3()
        {
            void PlaySuccessSequence4()
            {
                void PlaySuccessSequence5()
                {
                    PlaySequence(successSequences[5], SequenceCompleter.CameraTransition,
                        () => StartCoroutine(LevelComplete(2f)));
                }

                PlaySequence(successSequences[4], SequenceCompleter.CameraTransition,
                    () => PlaySuccessSequence5());
            }

            PlaySequence(successSequences[3], SequenceCompleter.CameraTransition,
                () => PlaySuccessSequence4());
        }

        PlaySequence(successSequences[2], SequenceCompleter.CameraTransition,
            () => PlaySuccessSequence3());
    }

    IEnumerator LevelComplete(float delay)     //Called from animation
    {
        yield return new WaitForSeconds(delay);

        PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true);

        //UIManager.Instance.ShowLevelComplete();
    }

    IEnumerator LevelFailed(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (currentPhaseIndex == 0)
            PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false);
        else
            PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true);

        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    public void SetBucketProperly()
    {
        bucket.localPosition = bucketLocalPosInFlyoff;
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        yield return new WaitForEndOfFrame();
        //Debug.Log("Wrong phase ONE");
        void PlayFailureSequence()
        {
            PlaySequence(failureSequences[0], SequenceCompleter.CameraTransition,
            () => PlaySequence(failureSequences[1], SequenceCompleter.CameraTransition,
            () => StartCoroutine(LevelFailed(3.5f))));
        }

        bottle.SetActive(true);
        PlaySequence(transitionSequences[1], SequenceCompleter.CameraTransition, () => PlayFailureSequence());
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        //Debug.Log("Wrong phase TWO");
        void PlayFailureSequence3()
        {
            void PlayFailureSequence4()
            {
                void PlayFailureSequence5()
                {
                    PlaySequence(failureSequences[5], SequenceCompleter.CameraTransition,
                        () => StartCoroutine(LevelFailed(1f)));
                }

                PlaySequence(failureSequences[4], SequenceCompleter.CameraTransition,
                    () => PlayFailureSequence5());
            }

            PlaySequence(failureSequences[3], SequenceCompleter.CameraTransition,
                () => PlayFailureSequence4());
        }
        PlaySequence(failureSequences[2], SequenceCompleter.CameraTransition,
            () => PlayFailureSequence3());
    }

    void ResetPhaseOne()
    {
        player.position = playerStart.position;
        player.rotation = playerStart.rotation;
        mainCamera.transform.position = cameraStart.position;
        mainCamera.transform.rotation = cameraStart.rotation;

        StartCoroutine(StartLevelRoutine());
    }

    void ResetPhaseTwo()
    {
        mainCamera.transform.position = successSequences[1].cameraOrientation.position;
        mainCamera.transform.rotation = successSequences[1].cameraOrientation.rotation;
        player.position = successSequences[0].playerPosition.position;
        player.rotation = successSequences[0].playerPosition.rotation;
        ChildBucket();
        glueSurface.SetActive(false);

        PlaySequence(transitionSequences[2], SequenceCompleter.CameraTransition,
                    () =>
                    {
                        //Debug.Log("Success01 Complete");
                        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
                    }
                    );
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        //Debug.Log("RESET_THIS_PHASE");
        if (currentPhaseIndex > 0 && phaseIndex >= 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0 && phaseIndex >= 0)
        {
            playerAnimatorReference.SetTrigger("FIRST_PHASE_REVIVED");
            propAnimatorReference.SetTrigger("FIRST_PHASE_REVIVED");
            ResetPhaseOne();
        }
            
    }


}
