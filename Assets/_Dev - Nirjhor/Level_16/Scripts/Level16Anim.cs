﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level16Anim : MonoBehaviour
{
    [SerializeField] private Level16Gameplay level16Gameplay;
    [SerializeField] private Bucket bucket;
    [SerializeField] private GameObject bottle;

    //Prop Anim Controlled
    [SerializeField] private GameObject impactParticle;
    [SerializeField] private Transform impactParticleOrientationForBottle;
    [SerializeField] private Transform impactParticleOrientationForHammer;
    [SerializeField] private GameObject sparkParticle;
    [SerializeField] private Transform sparkParticleOrientation;
    [SerializeField] private GameObject electrifyParticle;
    [SerializeField] private Transform electrifyParticleOrientation;
    [SerializeField] private GameObject colorParticle;

    //Player Anim Controlled
    [SerializeField] private GameObject shockedEffectParticle;
    [SerializeField] private Transform shockedEffectParticleOrientation;
    [SerializeField] private GameObject sparkExplosionParticle;
    [SerializeField] private Transform sparkExplosionParticleOrientation;

    //On Player Body (Child)
    [SerializeField] private GameObject smokeExplostionParticle;
    [SerializeField] private Transform smokeExplostionParticleOrientation;
    [SerializeField] private GameObject smokeParticle;
    [SerializeField] private Transform smokeParticleOrientation;

    private void OnAnimatorMove()
    {

    }
    public void PlayOnImpactByBottleParticle()
    {
        Instantiate(impactParticle, impactParticleOrientationForBottle.position, impactParticleOrientationForBottle.rotation);
        impactParticle.transform.localScale = impactParticleOrientationForBottle.localScale;
    }

    public void PlayOnImpactByHammerParticle()
    {
        Instantiate(impactParticle, impactParticleOrientationForHammer.position, impactParticleOrientationForHammer.rotation);
        impactParticle.transform.localScale = impactParticleOrientationForHammer.localScale;
    }

    public void PlayOnSwitchBrokenParticles()
    {
        Instantiate(sparkParticle, sparkParticleOrientation.position, sparkParticleOrientation.rotation);
        sparkParticle.transform.localScale = sparkParticleOrientation.localScale;
        Instantiate(electrifyParticle, electrifyParticleOrientation.position, electrifyParticleOrientation.rotation);
        electrifyParticle.transform.localScale = electrifyParticleOrientation.localScale;
    }

    public void PlayShockParticles()
    {
        Instantiate(shockedEffectParticle, shockedEffectParticleOrientation.position, shockedEffectParticleOrientation.rotation);
        shockedEffectParticle.transform.localScale = shockedEffectParticleOrientation.localScale;
        Instantiate(sparkExplosionParticle, sparkExplosionParticleOrientation.position, sparkExplosionParticleOrientation.rotation);
        sparkExplosionParticle.transform.localScale = sparkExplosionParticleOrientation.localScale;
    }

    public void PlaySmokeExplosion()
    {
        Instantiate(smokeExplostionParticle, level16Gameplay.player);
        smokeExplostionParticle.transform.localPosition = smokeExplostionParticleOrientation.localPosition;
        smokeExplostionParticle.transform.localRotation = smokeExplostionParticleOrientation.localRotation;
        smokeExplostionParticle.transform.localScale = smokeExplostionParticleOrientation.localScale;
        level16Gameplay.ChangeToBurntMaterial();
    }

    public void PlaySmoke()
    {
        Instantiate(smokeParticle, level16Gameplay.player);
        smokeParticle.transform.localPosition = smokeParticleOrientation.localPosition;
        smokeParticle.transform.localRotation = smokeParticleOrientation.localRotation;
        smokeParticle.transform.localScale = smokeParticleOrientation.localScale;
    }

    public void DisablePropAnimator()
    {
        level16Gameplay.propAnimatorReference.enabled = false;
        level16Gameplay.SetBucketProperly();
    }

    public void BucketFlyOffInShock()
    {
        bucket.FlyOff(Vector3.up + Vector3.right);
    }

    public void BucketFlyOffOnPull()
    {
        bucket.FlyOff(Vector3.up + Vector3.forward);
    }

    public void ChildBucketToHead()
    {
        level16Gameplay.ChildBucket();
    }

    public void DeactivateBottle()
    {
        bottle.SetActive(false);
    }

    public void ColorSpill()
    {
        Instantiate(colorParticle, level16Gameplay.bucket);
        colorParticle.transform.localEulerAngles = new Vector3(-90f, 0f, 0f);
        colorParticle.transform.localPosition = Vector3.zero;
    }
}
