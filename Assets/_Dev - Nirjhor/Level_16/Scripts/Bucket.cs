﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bucket : MonoBehaviour
{
    [SerializeField] private float flyOffForce = 200f;
    [SerializeField] private Rigidbody bucketBody;

    public void FlyOff(Vector3 direction)
    {
        bucketBody.isKinematic = false;
        bucketBody.AddForce(direction * flyOffForce);
    }
}
