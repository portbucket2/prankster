Level 15.1 (Prev 15.2)
Theme: Outdoor
Environment: Sidewalk
Action: Eating a chocolate apple. looking at phone
Success: Replace the apple with an onion, making the player cry when they bite into it
Success Icon: Onion
Failure: Replace the apple with a balloon. The balloon pops when the player bits. Player is slightly annoyed
Failure Icon: Water Balloon
Instruction: Emotional meal
Art Notes:

Level 15.2 (Prev. 15.1)
Theme: Outdoor
Environment: Sidewalk
Action: Walking down the street while looking at the phone
Success: Place a roadblock in the player’s path. Player hits it, stumbles, and drops the phone, which breaks into pieces.
Success Icon: Roadblock
Failure: Place a stop sign in front of the player. The player bumps into it, rubs head, then moves on.
Failure Icon: Stop Sign
Instruction: Watch where you go
Art Notes: