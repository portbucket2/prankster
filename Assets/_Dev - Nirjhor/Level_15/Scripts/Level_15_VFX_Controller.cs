﻿using System.Collections;
using UnityEngine;

public class Level_15_VFX_Controller : MonoBehaviour
{
    [SerializeField] Level_15_Gameplay level_15_Gameplay;

    [SerializeField] Material hitByStopSign;
    [SerializeField] Transform meshParent;

    [SerializeField] GameObject hitStopSignParticle;
    [SerializeField] Transform hitStopSignParticlePosition;
    [SerializeField] GameObject hitRoadParticle;
    [SerializeField] Transform hitRoadParticlePosition;

    [SerializeField] GameObject waterDrippingParticle;
    [SerializeField] Transform waterDrippingParticlePosition;
    [SerializeField] GameObject waterSplashParticle;
    [SerializeField] Transform waterSplashParticlePosition;
    [SerializeField] GameObject popEffectParticle;

    [SerializeField] GameObject tearFallPrefab;
    public GameObject tearFallLeft { get; private set; }
    public GameObject tearFallRight { get; private set; }
    [SerializeField] Transform leftEyeTear;
    [SerializeField] Transform rightEyeTear;

    [SerializeField] GameObject phoneHitParticle;

    public void OnHitRoadVFX()
    {
        if (!level_15_Gameplay.levelFailed)
        {
            Instantiate(hitRoadParticle, hitRoadParticlePosition.position, hitRoadParticle.transform.rotation); 
        }
    }

    public void OnHitStopSignVFX()
    {
        if (level_15_Gameplay.levelFailed)
        {
            Instantiate(hitStopSignParticle, hitStopSignParticlePosition.position, hitStopSignParticle.transform.rotation);
            //ChangeFaceMaterial(); 
        }
    }

    void ChangeFaceMaterial()
    {
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in meshParent.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            skinnedMeshRenderer.sharedMaterial = hitByStopSign;
        }
    }

    public void WaterBalloonPopVFX()
    {
        if (level_15_Gameplay.levelFailed)
        {
            Instantiate(waterDrippingParticle, level_15_Gameplay.originalStick.transform);
            StartCoroutine(SetWaterDripLocalPos());
            Instantiate(waterSplashParticle, waterSplashParticlePosition.position, waterSplashParticle.transform.rotation);
            Instantiate(popEffectParticle, waterSplashParticlePosition.position, popEffectParticle.transform.rotation);
            level_15_Gameplay.PopWaterBalloon();
        }
    }

    IEnumerator SetWaterDripLocalPos()
    {
        yield return null;
        waterDrippingParticle.transform.localPosition = new Vector3(-0.031f, 0.149f, 0.014f);
        waterDrippingParticle.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    public void TearyEyeEffect()
    {
        if (!level_15_Gameplay.levelFailed)
        {
            tearFallLeft = Instantiate(tearFallPrefab, leftEyeTear.position, tearFallPrefab.transform.rotation);
            tearFallLeft.transform.parent = leftEyeTear.parent;
            tearFallRight = Instantiate(tearFallPrefab, rightEyeTear.position, tearFallPrefab.transform.rotation);
            tearFallRight.transform.parent = rightEyeTear.parent;
        }
    }

    public void WipeAwayTears()
    {
        //Destroy(tearFallLeft);
        //Destroy(tearFallRight);
        foreach (ParticleSystem particleSystem in leftEyeTear.parent.GetComponentsInChildren<ParticleSystem>())
        {
            Destroy(particleSystem.gameObject);
        }
        foreach (ParticleSystem particleSystem in rightEyeTear.parent.GetComponentsInChildren<ParticleSystem>())
        {
            Destroy(particleSystem.gameObject);
        }
    }

    public void PhoneHitVFX(Vector3 atLocation)
    {
        Instantiate(phoneHitParticle, atLocation, phoneHitParticle.transform.rotation);
    }
}
