﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneHitGround : MonoBehaviour
{
    bool collided = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (!collided)
        {
            ContactPoint contactPoint = collision.GetContact(0);

            FindObjectOfType<Level_15_VFX_Controller>().PhoneHitVFX(contactPoint.point);

            collided = true; 
        }
    }
}
