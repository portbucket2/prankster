﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Level_15_Gameplay : PrankLevelBehaviour
{
    public PlayableDirector timelineDirector;
    private TimelineAsset timelineAsset;
    public bool levelFailed { get; private set; } = false;

    public float stickThrowForce;
    public Transform playerLeftHand;
    public GameObject originalStick;
    public GameObject stickPrefab;
    private GameObject stick;
    public GameObject originalApple;
    public GameObject applePrefab;
    private GameObject apple;
    public GameObject onionPrefab;
    private GameObject onion;
    public GameObject waterBalloonPrefab;
    private GameObject waterBalloon;

    public float propReplacementTime;

    public Transform playerRightHand;
    public GameObject originalPhone;
    public GameObject phonePrefab;
    public float phoneThrowForce;

    List<GameObject> instantiatedGameObjects;   //destory all of these on revival

    [Space(20.0f)]
    public double phaseOneReviveTime;
    public double phaseTwoReviveTime;

    [Space(10.0f)]
    public Transform stopSign;
    private Vector3 stopSignStartPos;
    public Transform roadBlock;
    private Vector3 roadBlockStartPos;

    [Space(5.0f)]
    [SerializeField] Level_15_VFX_Controller vfx_Controller;

    // Start is called before the first frame update

    private void Awake()
    {
        //Debug.LogError("awake");
        stopSignStartPos = stopSign.position;
        roadBlockStartPos = roadBlock.position;
        instantiatedGameObjects = new List<GameObject>();
        originalApple.SetActive(true);
        originalStick.SetActive(true);
        timelineAsset = (TimelineAsset)timelineDirector.playableAsset;
        PresetMuteUnmuteTracks();
        //Play Idle Animation
        timelineDirector.Play();
    }
    bool initializationPending = true;

    void Start()
    {
        //Debug.LogError("start");
        if (initializationPending)
        {
            initializationPending = false;
            PauseTimeline(timelineDirector);
        }
    }

    protected override void StartLevel()
    {
        //Debug.LogError("tapstart");
        currentPhaseIndex = 0;
        if (!initializationPending)
        {
            ResumeTimeline(timelineDirector);
        }
        else
        {
            initializationPending = false;
        }
        //Debug.Log("LEVEL_START");
    }

    private void PresetMuteUnmuteTracks()
    {
        UnMuteTrack(1);
        UnMuteTrack(2);
        UnMuteTrack(3);
        UnMuteTrack(4);
        MuteTrack(5);
        MuteTrack(6);
        MuteTrack(7);
        MuteTrack(8);
        MuteTrack(9);
    }

    IEnumerator PauseTimelineAfter(float t)
    {
        yield return new WaitForSeconds(t);
        PauseTimeline(timelineDirector);
    }

    void PauseTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
    }

    void ResumeTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    void MuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = true;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }
    
    void UnMuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = false;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }




    IEnumerator TimelineRoutine()
    {
        yield return new WaitForSeconds(3f);
        timelineDirector.Play();
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(1.0);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void PresentObjectiveOne()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }

    public void PresentObjectiveTwo()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    public void LevelFailed()
    {
        if (levelFailed)
        {
            PauseTimeline(timelineDirector);
            if (currentPhaseIndex > 0)
                PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  true);
            else
                PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated(); 
        }
    }

    public void LevelComplete()
    {
        if (!levelFailed)
        {
            PauseTimeline(timelineDirector);
            PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
            //UIManager.Instance.ShowLevelComplete(); 
        }
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                RightPhaseOne();
                break;
            case 1:
                RightPhaseTwo();
                break;
            default:
                break;
        }
    }

    void RightPhaseOne()
    {
        StartCoroutine(DisplaceAppleWithOnion());
        currentPhaseIndex++;
    }

    IEnumerator DisplaceAppleWithOnion()
    {
        stick = Instantiate(stickPrefab, playerLeftHand);
        instantiatedGameObjects.Add(stick);
        yield return null;
        stick.transform.localPosition = new Vector3(0.05f, 0.105f, 0.037f);
        stick.transform.localEulerAngles = new Vector3(0f, 0f, -88.717f);
        originalStick.SetActive(false);

        originalApple.SetActive(false);
        apple = Instantiate(applePrefab, stick.transform);
        instantiatedGameObjects.Add(apple);
        yield return null;
        apple.transform.localPosition = new Vector3(0f, 0.108f, 0.011f);
        apple.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        apple.transform.parent = null;
        yield return null;
        Vector3 targetApplePosition = apple.transform.position - Vector3.forward * 3f;
        Vector3 appleStartPos = apple.transform.position;
        float appleSwitchStartTime = Time.realtimeSinceStartup;
        bool appleRemovalComplete = false;
        while (!appleRemovalComplete)
        {
            apple.transform.position = Vector3.Lerp(appleStartPos, targetApplePosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - appleSwitchStartTime) / propReplacementTime));

            yield return null;

            if (apple.transform.position == targetApplePosition)
            {
                appleRemovalComplete = true;
            }
        }
        Destroy(apple);

        onion = Instantiate(onionPrefab, stick.transform);
        instantiatedGameObjects.Add(onion);
        onion.SetActive(false);
        yield return null;
        Vector3 onionTargetLocalPos = new Vector3(0f, 0.148f, 0.071f);
        onion.transform.localPosition = onionTargetLocalPos;
        onion.transform.localEulerAngles = new Vector3(-90f, 0f, 0f);
        yield return null;
        onion.transform.position = onion.transform.position - Vector3.forward * 3f;
        yield return null;
        Vector3 startPos = onion.transform.localPosition;
        float startTime = Time.realtimeSinceStartup;
        onion.SetActive(true);

        bool onionSetupComplete = false;
        while (!onionSetupComplete)
        {
            onion.transform.localPosition = Vector3.Lerp(startPos, onionTargetLocalPos,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propReplacementTime));

            yield return null;

            if (onion.transform.localPosition == onionTargetLocalPos)
            {
                onionSetupComplete = true;
                onion.transform.parent = stick.transform;
            }
        }

        ResumeTimeline(timelineDirector);
    }

    public void ShowTearsLonger()
    {
        PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false);
        StartCoroutine(PauseTimelineForTime(2f));
    }

    IEnumerator PauseTimelineForTime(float t)
    {
        PauseTimeline(timelineDirector);
        yield return new WaitForSeconds(t);
        ResumeTimeline(timelineDirector);
    }

    public void ThrowOnion()
    {
        stick.transform.parent = null;
        Rigidbody stickBody = stick.GetComponent<Rigidbody>();
        stickBody.isKinematic = false;
        stickBody.AddForce((Vector3.forward - Vector3.up).normalized * stickThrowForce);
    }

    void RightPhaseTwo()
    {
        UnMuteTrack(8);
        ResumeTimeline(timelineDirector);
    }

    public void ThrowPhone()
    {
        if (!levelFailed)
        {
            originalPhone.SetActive(false);
            GameObject phone = Instantiate(phonePrefab, playerRightHand);
            instantiatedGameObjects.Add(phone);
            phone.transform.localPosition = originalPhone.transform.localPosition;
            phone.transform.localRotation = originalPhone.transform.localRotation;

            Rigidbody phoneBody = phone.GetComponent<Rigidbody>();
            phoneBody.isKinematic = false;
            phoneBody.useGravity = true;
            phone.transform.parent = null;
            phoneBody.AddForce((Vector3.up + Vector3.right).normalized * phoneThrowForce); 
        }
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                WrongPhaseOne();
                break;
            case 1:
                WrongPhaseTwo();
                break;
            default:
                break;
        }
    }

    void WrongPhaseOne()
    {
        levelFailed = true;

        StartCoroutine(DisplaceAppleWithWaterBalloon());
    }

    IEnumerator DisplaceAppleWithWaterBalloon()
    {
        originalApple.SetActive(false);
        apple = Instantiate(applePrefab, originalStick.transform);
        instantiatedGameObjects.Add(apple);
        yield return null;
        apple.transform.localPosition = new Vector3(0f, 0.108f, 0.011f);
        apple.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        apple.transform.parent = null;
        yield return null;
        Vector3 targetApplePosition = apple.transform.position - Vector3.forward * 3f;
        Vector3 appleStartPos = apple.transform.position;
        float appleSwitchStartTime = Time.realtimeSinceStartup;
        bool appleRemovalComplete = false;
        while (!appleRemovalComplete)
        {
            apple.transform.position = Vector3.Lerp(appleStartPos, targetApplePosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - appleSwitchStartTime) / propReplacementTime));

            yield return null;

            if (apple.transform.position == targetApplePosition)
            {
                appleRemovalComplete = true;
            }
        }
        Destroy(apple);

        waterBalloon = Instantiate(waterBalloonPrefab, originalStick.transform);
        instantiatedGameObjects.Add(waterBalloon);
        waterBalloon.SetActive(false);
        yield return null;
        Vector3 waterBalloonTargetLocalPos = new Vector3(0f, 0.31f, 0f);
        waterBalloon.transform.localPosition = waterBalloonTargetLocalPos;
        waterBalloon.transform.localEulerAngles = new Vector3(180f, 0f, 0f);
        yield return null;
        waterBalloon.transform.position = waterBalloon.transform.position - Vector3.forward * 3f;
        yield return null;
        Vector3 startPos = waterBalloon.transform.localPosition;
        float startTime = Time.realtimeSinceStartup;
        waterBalloon.SetActive(true);

        bool waterBalloonSetupComplete = false;
        while (!waterBalloonSetupComplete)
        {
            waterBalloon.transform.localPosition = Vector3.Lerp(startPos, waterBalloonTargetLocalPos,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propReplacementTime));

            yield return null;

            if (waterBalloon.transform.localPosition == waterBalloonTargetLocalPos)
            {
                waterBalloonSetupComplete = true;
                waterBalloon.transform.parent = originalStick.transform;
            }
        }

        MuteTrack(2);
        UnMuteTrack(5);
        ResumeTimeline(timelineDirector);
    }

    public void PopWaterBalloon()
    {
        Destroy(waterBalloon);
    }

    void WrongPhaseTwo()
    {
        levelFailed = true;
        MuteTrack(2);
        MuteTrack(3);
        MuteTrack(5);
        UnMuteTrack(6);
        UnMuteTrack(7);
        UnMuteTrack(9);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        if (currentPhaseIndex > 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0)
            ResetPhaseOne();
    }

    void ResetPhaseOne()
    {
        levelFailed = false;
        PresetMuteUnmuteTracks();
        foreach (GameObject gObj in instantiatedGameObjects)
        {
            if (gObj != null)
                Destroy(gObj);
        }
        instantiatedGameObjects.Clear();
        originalApple.SetActive(true);
        originalStick.SetActive(true);

        PauseTimeline(timelineDirector);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(phaseOneReviveTime);
        ResumeTimeline(timelineDirector);
    }

    void ResetPhaseTwo()
    {
        levelFailed = false;
        
        roadBlock.position = roadBlockStartPos;
        stopSign.position = stopSignStartPos;
        vfx_Controller.TearyEyeEffect();

        PresetMuteUnmuteTracks();
        foreach (GameObject gObj in instantiatedGameObjects)
        {
            if (gObj != null)
                Destroy(gObj);
        }
        instantiatedGameObjects.Clear();

        StartCoroutine(PhaseTwoResetSetup());

        PauseTimeline(timelineDirector);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(phaseTwoReviveTime);
        ResumeTimeline(timelineDirector);
    }

    IEnumerator PhaseTwoResetSetup()
    {
        stick = Instantiate(stickPrefab, playerLeftHand);
        instantiatedGameObjects.Add(stick);
        yield return null;
        stick.transform.localPosition = new Vector3(0.05f, 0.105f, 0.037f);
        stick.transform.localEulerAngles = new Vector3(0f, 0f, -88.717f);
        originalStick.SetActive(false);

        onion = Instantiate(onionPrefab, stick.transform);
        instantiatedGameObjects.Add(onion);
        onion.SetActive(false);
        yield return null;
        Vector3 onionTargetLocalPos = new Vector3(0f, 0.148f, 0.071f);
        onion.transform.localPosition = onionTargetLocalPos;
        onion.transform.localEulerAngles = new Vector3(-90f, 0f, 0f);
        onion.SetActive(true);
    }
}
