Level 50.1
Theme: Outdoors
Environment: Driveway
Action: Player is skating down the driveway on a skateboard, doing a kickflip.
Success: Pour a puddle of superglue where the player will land. When the player lands, the skateboard sticks to the ground, but the player is thrown forward to the ground.
Success Icon: Superglue tube
Failure: Place a tiny speedbump on the path. Player uses the bump to do a successful kickflip
Failure Icon: speed bump sign
Instruction: Stick the landing!
Art Notes:

Level 50.2
Theme: Outdoors
Environment: Driveway
Action: After the fall, player pulls off the skateboard again, and this time is doing a grind on a roadside guard railing
Success: Place a tiny electric saw on the rail. The board splits apart, and the player falls down on the rail crotch first, hurting his balls. The player clutches his balls, freezes like a statue, and slowly falls sideways off the rails
Success Icon: Electric Saw
Failure: Pour grease all over the rail. The player smoothly grinds over it and sticks the landing perfectly!
Failure Icon: Grease Can
Instruction: Wanna play a game?
Art Notes: