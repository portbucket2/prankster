﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_50_PranksTools : MonoBehaviour
{
    public Level_50_Gameplay level_50_Gameplay;
    public Transform blade;
    public float bladeMaxSpeed;
    float bladeSpeed = 0f;
    public GameObject greasePuddle;

    public void ResumeTimeline()
    {
        level_50_Gameplay.ResumeTimeline();
    }

    public void SpawnGreasePuddle()
    {
        greasePuddle.SetActive(true);
    }

    public void SpinBladeAndResumeTimeline()
    {
        StartCoroutine(BladeSpeedRampUp());
        level_50_Gameplay.ResumeTimeline();
    }

    IEnumerator BladeSpeedRampUp()
    {
        yield return null;
        float startTime = Time.realtimeSinceStartup;
        float speedRampUpTime = 0.3f;
        float rampUpProgress = 0f;
        while (rampUpProgress < 1f)
        {
            rampUpProgress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / speedRampUpTime);
            bladeSpeed = Mathf.Lerp(0f, bladeMaxSpeed, rampUpProgress);
            blade.Rotate(blade.right, bladeSpeed * Time.deltaTime, Space.World);
            yield return null;
        }
        while (true)
        {
            blade.Rotate(blade.right, bladeSpeed * Time.deltaTime, Space.World);
            yield return null; 
        }
    }
}
