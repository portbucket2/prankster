Level 38.1
Theme: Home
Environment: Kitchen
Action: Character is boiling pasta in a pan with a long handle, reading from a cookbook placed beside the stove
Success: Use a screwdriver to loosen the handle. When the character picks up the pan, the pan comes loose, and falls down, spilling hot water and pasta all over the player
Success Icon: Screwdriver
Failure: Use a lighter to heat up the handle. When the character touches it, they immediately let go and jump away in pain, but otherwise they are ok
Failure Icon: Lighter
Instruction: Pasta Bath
Art Notes: Idle loop: Player reading from a cookbook placed beside the stove

Level 38.2
Theme: Home
Environment: Kitchen
Action: Character is trying to wipe off the pasta from themselves with a cloth
Success: Apply superglue to the cloth. When the character uses it, it sticks to their shirt, and after some struggle it comes off, ripping a hole in the shirt. Character is furious
Success Icon: Super glue tube
Failure: Put some color on the cloth so when the character uses it, it spreads the color all over them. The character is annoyed and throws away the cloth
Failure Icon: Color tube
Instruction: Sticky mess
Art Notes: