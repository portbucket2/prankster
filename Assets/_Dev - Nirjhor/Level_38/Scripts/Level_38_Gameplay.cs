﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Level_38_Gameplay : PrankLevelBehaviour
{
    //[Space(30.0f)]
    [Header("Level 38 Data:")]
    //public ScriptableObjective objectiveData;
    public PlayableDirector timelineDirector;
    private TimelineAsset timelineAsset;

    [System.Serializable]
    public struct TrackController
    {
        public string sequenceName;
        public string tracksToMute;
        public string tracksToUnmute;
    }
    [Header("Mute Unmute Timeline Tracks:")]
    public TrackController[] trackControls;

    [Header("Props")]
    public float propPlacementTime;

    [Header("Material swap references")]
    public SkinMatController skinMatController;
    private int oilSpilledBodyMatIndex = 3;
    private int coloredBodyMatIndex = 2;
    private int shirtTornBodyMatIndex = 1;
    public MeshRenderer tornPiece;
    public SkinnedMeshRenderer body;
    private Material currentSkinMat;
    //public Transform player_Mesh_Parent;
    //public Material shirtTornMat;
    //public Material playerColoredBodyMat;
    //public Material playerOilSpilledBodyMat;

    public MeshRenderer animatedCloth;
    public MeshRenderer propCloth;

    public MeshRenderer panHandle;

    [Header("Revival timeline timestamps")]
    public float phaseOneReviveTime;
    public float phaseTwoReviveTime;

    private bool phaseTwoRevived = false;


    // Start is called before the first frame update
    void Awake()
    {
        phaseTwoRevived = false;
        timelineAsset = (TimelineAsset)timelineDirector.playableAsset;
        TrackControl(trackControls[0]);
        timelineDirector.Play();
    }

    void TrackControl(TrackController tc)
    {
        List<int> tracksToMute = new List<int>();
        List<int> tracksToUnmute = new List<int>();

        char[] splitters = { ',', ' ' };

        string stringToSplit1 = tc.tracksToMute;
        string[] listOfTracksToMute = stringToSplit1.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
        foreach (string trackNumString in listOfTracksToMute)
        {
            tracksToMute.Add(Convert.ToInt32(trackNumString));
        }

        string stringToSplit2 = tc.tracksToUnmute;
        string[] listOfTracksToUnmute = stringToSplit2.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
        foreach (string trackNumString in listOfTracksToUnmute)
        {
            tracksToUnmute.Add(Convert.ToInt32(trackNumString));
        }

        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);
    }

    protected override void StartLevel()
    {
        currentSkinMat = body.sharedMaterial;
        tornPiece.material = currentSkinMat;
        currentPhaseIndex = 0;
        Level_38_Start();
    }

    void Level_38_Start()
    {
        TrackControl(trackControls[1]);
    }

    IEnumerator PauseTimelineAfter(float t)
    {
        yield return new WaitForSeconds(t);
        PauseTimeline(timelineDirector);
    }

    IEnumerator PauseTimelineForTime(float t)
    {
        PauseTimeline(timelineDirector);
        yield return new WaitForSeconds(t);
        ResumeTimeline(timelineDirector);
    }

    void PauseTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
    }

    void ResumeTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    void SetTimelineTime(PlayableDirector playableDirector, float t)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetTime(t);
    }

    void MuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = true;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void MuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = true;
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = false;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = false;
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    IEnumerator TimelineRoutine()
    {
        yield return new WaitForSeconds(3f);
        timelineDirector.Play();
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(1.0);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void PresentObjectiveOne()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }

    public void PresentObjectiveTwo()
    {
        if (!phaseTwoRevived)
        {
            PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false);
        }
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    public void LevelFailed()
    {
        PauseTimeline(timelineDirector);
        if (currentPhaseIndex > 0)
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  true);
        else
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    public void LevelComplete()
    {
        StartCoroutine(PauseTimelineAfter(15f));
        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                RightPhaseOne();
                break;
            case 1:
                RightPhaseTwo();
                break;
            default:
                break;
        }
    }

    void RightPhaseOne()
    {
        //Do right phase one
        ResumeTimeline(timelineDirector);
        currentPhaseIndex++;
    }

    IEnumerator PlaceProp(Transform prop, Vector3 targetPropPosition)
    {
        PauseTimeline(timelineDirector);
        prop.gameObject.SetActive(true);
        Vector3 propStartPos = prop.position;
        float startTime = Time.realtimeSinceStartup;

        while (prop.position != targetPropPosition)
        {
            prop.position = Vector3.Lerp(propStartPos, targetPropPosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propPlacementTime));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
        yield return new WaitForSeconds(2f);
        prop.gameObject.SetActive(false);
    }

    IEnumerator SwitchProp(Transform originalProp, Transform switchedProp)
    {
        //MuteTrack(6);
        PauseTimeline(timelineDirector);
        Vector3 originalPropStartPos = originalProp.position;
        float startTime1 = Time.realtimeSinceStartup;

        while (originalProp.position != switchedProp.position)
        {
            originalProp.position = Vector3.Lerp(originalPropStartPos, switchedProp.position,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime1) / (propPlacementTime / 2f)));
            yield return null;
        }

        Vector3 switchedPropStartPos = switchedProp.position;
        float startTime2 = Time.realtimeSinceStartup;
        while (switchedProp.position != originalPropStartPos)
        {
            switchedProp.position = Vector3.Lerp(switchedPropStartPos, originalPropStartPos,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime2) / (propPlacementTime / 2f)));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
    }

    void RightPhaseTwo()
    {
        ResumeTimeline(timelineDirector);
    }

    public void TearShirt()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = shirtTornMat;
        //}
        skinMatController.SetMat(shirtTornBodyMatIndex);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                WrongPhaseOne();
                break;
            case 1:
                WrongPhaseTwo();
                break;
            default:
                break;
        }
    }

    void WrongPhaseOne()
    {
        TrackControl(trackControls[2]);
    }

    public void ReddeningOfPanHandle()
    {
        StartCoroutine(GraduallyReddenPanHandle());
    }

    IEnumerator GraduallyReddenPanHandle()
    {
        panHandle.material.SetFloat("_MaskStrength", 0f);
        float progress = 0f;
        float startTime = Time.realtimeSinceStartup;
        float coloringDuration = 3f;
        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / coloringDuration);
            float maskStrength = Mathf.Lerp(0f, 0.5f, progress);
            panHandle.material.SetFloat("_MaskStrength", maskStrength);
            yield return null;
        }
    }

    void WrongPhaseTwo()
    {
        TrackControl(trackControls[3]);
    }

    public void ColorPropCloth()
    {
        StartCoroutine(GraduallyColorPropCloth());
    }

    IEnumerator GraduallyColorPropCloth()
    {
        propCloth.material.SetFloat("_MaskStrength", 0f);
        float progress = 0f;
        float startTime = Time.realtimeSinceStartup;
        float coloringDuration = 1f;
        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / coloringDuration);
            float maskStrength = Mathf.Lerp(0f, 1f, progress);
            propCloth.material.SetFloat("_MaskStrength", maskStrength);
            yield return null;
        }
        animatedCloth.material.SetFloat("_MaskStrength", 1f);
    }

    public void ColorPlayerBody()
    {
        StartCoroutine(GraduallyColorPlayerBody());
    }

    IEnumerator GraduallyColorPlayerBody()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = playerColoredBodyMat;
        //}
        skinMatController.SetMat(coloredBodyMatIndex);

        float progress = 0f;
        float startTime = Time.realtimeSinceStartup;
        float coloringDuration = 1.5f;
        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / coloringDuration);
            float maskStrength = Mathf.Lerp(0f, 0.8f, progress);
            //playerColoredBodyMat.SetFloat("_MaskStrength", maskStrength);
            skinMatController.SetMatValue(coloredBodyMatIndex, "_MaskStrength", maskStrength);
            yield return null;
        }
    }

    public void OilSpilledOnPlayerBody()
    {
        StartCoroutine(GraduallyOilPlayerBody());
    }

    IEnumerator GraduallyOilPlayerBody()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = playerOilSpilledBodyMat;
        //}
        skinMatController.SetMat(oilSpilledBodyMatIndex);

        float progress = 0f;
        float startTime = Time.realtimeSinceStartup;
        float oilingDuration = 2f;
        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / oilingDuration);
            float maskStrength = Mathf.Lerp(0f, 0.4f, progress);
            //playerOilSpilledBodyMat.SetFloat("_MaskStrength", maskStrength);
            skinMatController.SetMatValue(oilSpilledBodyMatIndex, "_MaskStrength", maskStrength);
            yield return null;
        }
    }

    public void RubOilOffPlayerBody()
    {
        StartCoroutine(GraduallyRubPlayerBody());
    }

    IEnumerator GraduallyRubPlayerBody()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = playerOilSpilledBodyMat;
        //}
        skinMatController.SetMat(oilSpilledBodyMatIndex);

        //float currentMaskStrength = playerOilSpilledBodyMat.GetFloat("_MaskStrength");
        float currentMaskStrength = skinMatController.GetMatCurrentValue(oilSpilledBodyMatIndex, "_MaskStrength");
        float progress = 0f;
        float startTime = Time.realtimeSinceStartup;
        float rubbingDuration = 2f;
        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / rubbingDuration);
            float maskStrength = Mathf.Lerp(currentMaskStrength, 0f, progress);
            //playerOilSpilledBodyMat.SetFloat("_MaskStrength", maskStrength);
            skinMatController.SetMatValue(oilSpilledBodyMatIndex, "_MaskStrength", maskStrength);
            yield return null;
        }
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        if (currentPhaseIndex > 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0)
            ResetPhaseOne();
    }

    void ResetPhaseOne()
    {
        PauseTimeline(timelineDirector);
        SetTimelineTime(timelineDirector, phaseOneReviveTime);
        //PresetMuteUnmuteTracks();
        List<int> tracksToMute = new List<int> { 1, 14, 15, 16, 17, 18, 19, 20, 21 };
        List<int> tracksToUnmute = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);
    }

    void ResetPhaseTwo()
    {
        phaseTwoRevived = true;
        PauseTimeline(timelineDirector);

        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = playerOilSpilledBodyMat;
        //}
        //playerOilSpilledBodyMat.SetFloat("_MaskStrength", 0.4f);
        skinMatController.SetMat(oilSpilledBodyMatIndex);
        skinMatController.SetMatValue(oilSpilledBodyMatIndex, "_MaskStrength", 0.4f);

        propCloth.material.SetFloat("_MaskStrength", 0f);
        animatedCloth.material.SetFloat("_MaskStrength", 0f);

        SetTimelineTime(timelineDirector, phaseTwoReviveTime);
        TrackControl(trackControls[4]);
    }
}

