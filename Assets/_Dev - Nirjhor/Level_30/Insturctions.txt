Level 30.1
Theme: Outdoors
Environment: Sidewalk
Action: Player is walking down the sidewalk with backpack, still looking up at the sky
Success: Place a beehive in front of them. When they step on it, the bees get angry and sting the player multiple times. The player screams in pain
Success Icon: Beehive
Failure: Put Dog Poop in front of the player. When they step on it, they are disgusted, but they quickly shake it off from their shoes
Failure Icon: Poop Emoji
Instruction: Watch your step!
Art Notes: Idle loop: Player stands still and stares upwards, with their hand on their forehead like a salute, trying to spot something up in the sky.

Level 30.2
Theme: Outdoors
Environment: Sidewalk
Action: Screaming from the stings, player shakes down their backpack upside down and looks around for some balm/medicine for the stings
Success: Replace the medicine tube/can with a taser. Player is still screaming and blinded from pain, so they rub the taser on their arms and get tased, and get knocked out cold
Success Icon: Taser
Failure: Replace medicine bottle with water bottle. Player pours water over themselves, and looks slightly relieved
Failure Icon: Water Bottle
Instruction: What a shocker!
Art Notes: After getting stung, the player shakes the backpack upside down, and a medicine tube falls down. Zoom in on the tube then present the choices