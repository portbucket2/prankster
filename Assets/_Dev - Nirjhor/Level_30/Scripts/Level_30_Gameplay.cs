﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class Level_30_Gameplay : PrankLevelBehaviour
{
    //[Space(30.0f)]
    [Header("Level 30 Data:")]
    //public ScriptableObjective objectiveData;
    public PlayableDirector timelineDirector;
    private TimelineAsset timelineAsset;

    [Header("Props")]
    public float propPlacementTime;
    public Transform beeHive;
    public Transform initialBeeHiveTransform;
    public Transform targetBeeHivePosition;
    public Transform dogPoop;
    public Transform initialDogPoopTransform;
    public Transform targetDogPoopPosition;

    public Transform bag;
    public Transform rightHand;
    public Transform backBone;

    public Transform balm;
    public Transform taser;
    public Transform taserTransformInHand;
    public Transform bottle;
    public Transform bottleTransformInHand;
    public Transform initialBottleTransform;

    [Header("Material swap references")]
    public SkinMatController skinMatController;
    private int beeStungMatIndex = 1;
    //public Transform player_Mesh_Parent;
    //public Material beeStungMat;

    [Header("Revival timeline timestamps")]
    public float phaseOneReviveTime;
    public float phaseTwoReviveTime;

    private bool phaseTwoRevived = false;

    // Start is called before the first frame update
    private void Awake()
    {
        phaseTwoRevived = false;
        timelineAsset = (TimelineAsset)timelineDirector.playableAsset;
        PresetMuteUnmuteTracks();
        timelineDirector.Play();
    }


    protected override void StartLevel()
    {
        currentPhaseIndex = 0;
        Level_30_Start();
    }

    void Level_30_Start()
    {
        List<int> tracksToUnmute = new List<int> { 2, 3, 4 , 5, 6, 7, 8, 9, 10, 11, 12, 13};
        UnMuteTracks(tracksToUnmute);
    }

    private void PresetMuteUnmuteTracks()
    {
        UnMuteTrack(1);
        List<int> tracksToMute = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
        20, 21, 22, 23, 24, 25, 26, 27 };
        MuteTracks(tracksToMute);
    }

    IEnumerator PauseTimelineAfter(float t)
    {
        yield return new WaitForSeconds(t);
        PauseTimeline(timelineDirector);
    }

    IEnumerator PauseTimelineForTime(float t)
    {
        PauseTimeline(timelineDirector);
        yield return new WaitForSeconds(t);
        ResumeTimeline(timelineDirector);
    }

    void PauseTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
    }

    void ResumeTimeline(PlayableDirector playableDirector)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    void SetTimelineTime(PlayableDirector playableDirector, float t)
    {
        playableDirector.playableGraph.GetRootPlayable(0).SetTime(t);
    }

    void MuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = true;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void MuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = true; 
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTrack(int trackNumber)
    {
        // Get track from TimelineAsset
        TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(trackNumber);

        // Change TimelineAsset's muted property value
        timelineTrackAsset.muted = false;

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    void UnMuteTracks(List<int> trackNumbers)
    {
        foreach (int track in trackNumbers)
        {
            // Get track from TimelineAsset
            TrackAsset timelineTrackAsset = timelineAsset.GetOutputTrack(track);

            // Change TimelineAsset's muted property value
            timelineTrackAsset.muted = false;
        }

        double t = timelineDirector.time; // Store elapsed time
        timelineDirector.RebuildGraph(); // Rebuild graph
        timelineDirector.time = t; // Restore elapsed time
    }

    IEnumerator TimelineRoutine()
    {
        yield return new WaitForSeconds(3f);
        timelineDirector.Play();
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(0f);
        yield return new WaitForSeconds(5f);
        timelineDirector.playableGraph.GetRootPlayable(0).SetTime(1.0);
        timelineDirector.playableGraph.GetRootPlayable(0).SetSpeed(1f);
    }

    public void PresentObjectiveOne()
    {
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
    }

    public void PresentObjectiveTwo()
    {
        if (!phaseTwoRevived)
        {
            PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false); 
        }
        PauseTimeline(timelineDirector);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
    }

    public void LevelFailed()
    {
        PauseTimeline(timelineDirector);
        if (currentPhaseIndex > 0)
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  true);
        else
            PrankLevelBehaviour.instance.ReportPhaseCompletion(currentPhaseIndex + 1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    public void LevelComplete()
    {
        PauseTimeline(timelineDirector);
        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                RightPhaseOne();
                break;
            case 1:
                RightPhaseTwo();
                break;
            default:
                break;
        }
    }

    void RightPhaseOne()
    {
        //Do right phase one
        StartCoroutine(PlaceProp(beeHive, targetBeeHivePosition.position));
        currentPhaseIndex++;
    }

    IEnumerator PlaceProp(Transform prop, Vector3 targetPropPosition)
    {
        PauseTimeline(timelineDirector);
        prop.gameObject.SetActive(true);
        Vector3 propStartPos = prop.position;
        float startTime = Time.realtimeSinceStartup;

        while (prop.position != targetPropPosition)
        {
            prop.position = Vector3.Lerp(propStartPos, targetPropPosition,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / propPlacementTime));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
        yield return new WaitForSeconds(2f);
        prop.gameObject.SetActive(false);
    }

    IEnumerator SwitchProp(Transform originalProp, Transform switchedProp)
    {
        MuteTrack(6);
        PauseTimeline(timelineDirector);
        Vector3 originalPropStartPos = originalProp.position;
        float startTime1 = Time.realtimeSinceStartup;

        while (originalProp.position != switchedProp.position)
        {
            originalProp.position = Vector3.Lerp(originalPropStartPos, switchedProp.position,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime1) / (propPlacementTime / 2f)));
            yield return null;
        }

        Vector3 switchedPropStartPos = switchedProp.position;
        float startTime2 = Time.realtimeSinceStartup;
        while (switchedProp.position != originalPropStartPos)
        {
            switchedProp.position = Vector3.Lerp(switchedPropStartPos, originalPropStartPos,
                Mathf.Clamp01((Time.realtimeSinceStartup - startTime2) / (propPlacementTime / 2f)));
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        ResumeTimeline(timelineDirector);
    }

    public void ChildBagToRighHand()
    {
        bag.parent = rightHand;
    }

    public void ChildBagToBack()
    {
        bag.parent = backBone;
    }

    public void ChildTaserToRighHand()
    {
        taser.parent = rightHand;
        StartCoroutine(ChildTaser());
    }

    IEnumerator ChildTaser()
    {
        yield return null;
        taser.localPosition = taserTransformInHand.localPosition;
        taser.localRotation = taserTransformInHand.localRotation;
    }

    void RightPhaseTwo()
    {
        StartCoroutine(SwitchProp(balm, taser));
    }

    public void ApplyBeeStungEffect()
    {
        //foreach (SkinnedMeshRenderer skinnedMeshRenderer in player_Mesh_Parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        //{
        //    skinnedMeshRenderer.sharedMaterial = beeStungMat;
        //}
        skinMatController.SetMat(beeStungMatIndex);
        StartCoroutine(SkinReddening());
    }

    IEnumerator SkinReddening()
    {
        float targetMaskStrength = 0.75f;
        float reddeningTime = 2f;
        float startTime = Time.realtimeSinceStartup;
        float progress = 0f;

        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / reddeningTime);
            float maskStrength = Mathf.Lerp(0f, targetMaskStrength, progress);
            //beeStungMat.SetFloat("_MaskStrength", maskStrength);
            skinMatController.SetMatValue(beeStungMatIndex, "_MaskStrength", maskStrength);
            yield return null;
        }
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                WrongPhaseOne();
                break;
            case 1:
                WrongPhaseTwo();
                break;
            default:
                break;
        }
    }

    void WrongPhaseOne()
    {
        List<int> tracksToMute = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
        List<int> tracksToUnmute = new List<int> { 14, 15, 16, 17, 18, 19, 20, 21 };
        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);

        StartCoroutine(PlaceProp(dogPoop, targetDogPoopPosition.position));
    }

    public void ChildBottleToRighHand()
    {
        bottle.parent = rightHand;
        StartCoroutine(ChildBottle());
    }

    IEnumerator ChildBottle()
    {
        yield return null;
        bottle.localPosition = bottleTransformInHand.localPosition;
        bottle.localRotation = bottleTransformInHand.localRotation;
    }

    void WrongPhaseTwo()
    {
        List<int> tracksToMute = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 11, 13 };
        List<int> tracksToUnmute = new List<int> { 22, 23, 24, 25, 26, 27 };
        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);

        StartCoroutine(SwitchProp(balm, bottle));
    }

    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex < 0)
            return;
        if (currentPhaseIndex > 0)
            ResetPhaseTwo();
        else if (currentPhaseIndex == 0)
            ResetPhaseOne();
    }

    void ResetPhaseOne()
    {
        dogPoop.position = initialDogPoopTransform.position;
        dogPoop.rotation = initialDogPoopTransform.rotation;
        dogPoop.localScale = initialDogPoopTransform.localScale;

        PauseTimeline(timelineDirector);
        SetTimelineTime(timelineDirector, phaseOneReviveTime);
        //PresetMuteUnmuteTracks();
        List<int> tracksToMute = new List<int> { 1, 14, 15, 16, 17, 18, 19, 20, 21 };
        List<int> tracksToUnmute = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);
    }

    void ResetPhaseTwo()
    {
        phaseTwoRevived = true;
        PauseTimeline(timelineDirector);

        bottle.parent = null;
        bottle.position = initialBottleTransform.position;
        bottle.rotation = initialBottleTransform.rotation;
        bottle.localScale = initialBottleTransform.localScale;

        SetTimelineTime(timelineDirector, phaseTwoReviveTime);
        List<int> tracksToUnmute = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 11, 13 };
        List<int> tracksToMute = new List<int> { 22, 23, 24, 25, 26, 27 };
        MuteTracks(tracksToMute);
        UnMuteTracks(tracksToUnmute);
    }
}
