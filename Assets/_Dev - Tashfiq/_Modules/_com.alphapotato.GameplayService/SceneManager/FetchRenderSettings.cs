﻿namespace com.alphapotato.GameplayService {
    using UnityEngine;
    using UnityEngine.Rendering;

    public class FetchRenderSettings : MonoBehaviour {

        #region Public Variables

        public Color ambientEquatorColor;
        public Color ambientGroundColor;
        public float ambientIntensity;
        public Color ambientLight;
        public AmbientMode ambientMode;
        public SphericalHarmonicsL2 ambientProbe;
        public Color ambientSkyColor;
        public Cubemap customReflection;
        public DefaultReflectionMode defaultReflectionMode;
        public int defaultReflectionResolution;
        public float flareFadeSpeed;
        public float flareStrength;
        public bool fog;
        public Color fogColor;
        public float fogDensity;
        public float fogEndDistance;
        public FogMode fogMode;
        public float fogStartDistance;
        public float haloStrength;
        public int reflectionBounces;
        public float reflectionIntensity;
        public Material skybox;
        public Color subtractiveShadowColor;
        public Light sun;
        
        #endregion

        #region Public Callback

        public void SyncRenderSettings(){

            ambientEquatorColor = RenderSettings.ambientEquatorColor;
            ambientGroundColor = RenderSettings.ambientGroundColor;
            ambientIntensity = RenderSettings.ambientIntensity;
            ambientLight = RenderSettings.ambientLight;
            ambientMode = RenderSettings.ambientMode;
            ambientSkyColor = RenderSettings.ambientSkyColor;
            customReflection = RenderSettings.customReflection;
            defaultReflectionMode = RenderSettings.defaultReflectionMode;
            defaultReflectionResolution = RenderSettings.defaultReflectionResolution;
            flareFadeSpeed = RenderSettings.flareFadeSpeed;
            flareStrength = RenderSettings.flareStrength;
            fog = RenderSettings.fog;
            fogColor = RenderSettings.fogColor;
            fogDensity = RenderSettings.fogDensity;
            fogEndDistance = RenderSettings.fogEndDistance;
            fogMode = RenderSettings.fogMode;
            fogStartDistance = RenderSettings.fogStartDistance;
            haloStrength = RenderSettings.haloStrength;
            reflectionBounces = RenderSettings.reflectionBounces;
            reflectionIntensity = RenderSettings.reflectionIntensity;
            skybox = RenderSettings.skybox;
            subtractiveShadowColor = RenderSettings.subtractiveShadowColor;
            sun = RenderSettings.sun;
        }

        #endregion
    }
}