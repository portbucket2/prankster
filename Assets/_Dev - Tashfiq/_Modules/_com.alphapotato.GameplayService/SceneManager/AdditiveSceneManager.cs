﻿namespace com.alphapotato.GameplayService {

    using System.Collections.Generic;
    using System.Collections;
    using UnityEditor;
    using UnityEngine.Events;
    using UnityEngine.SceneManagement;
    using UnityEngine;

    public class AdditiveSceneManager : MonoBehaviour {

        #region OnEditorMethod

#if UNITY_EDITOR

        public void CreateListOfAddtiveSceneInfo () {

            if (listOfAdditiveScene == null)
                listOfAdditiveScene = new List<AdditiveSceneInfo> ();
        }

        public void CreateAnEmptyAdditiveSceneInfo () {

            listOfAdditiveScene.Add (new AdditiveSceneInfo ());

            int t_NumberOfActiveScene = listOfAdditiveScene.Count;

            for (int i = 0; i < t_NumberOfActiveScene; i++) {

                if (i == (t_NumberOfActiveScene - 1)) {

                    listOfAdditiveScene[i].isSceneEnabledInBuild = true;
                    listOfAdditiveScene[i].sceneMapForEnable = new List<bool> ();

                    for (int j = 0; j < t_NumberOfActiveScene; j++) {
                        if (i == j) {
                            listOfAdditiveScene[i].sceneMapForEnable.Add (true);
                        } else {
                            listOfAdditiveScene[i].sceneMapForEnable.Add (false);
                        }
                    }
                } else {
                    listOfAdditiveScene[i].sceneMapForEnable.Add (false);
                }
            }
        }

        private bool IsValidScene (int t_SceneIndex) {

            return SceneManager.GetSceneByPath (listOfAdditiveScene[t_SceneIndex].scenePath).IsValid ();
        }

        public string GetSceneName (int t_SceneIndex) {

            return listOfAdditiveScene[t_SceneIndex].sceneName;

        }

#endif

        #endregion

        #region Custom Variables

        public enum SceneTypeForLoading {
            Default,
            RemoveAllOtherScene,
            Custom
        }

        [System.Serializable]
        public class AdditiveSceneInfo {

            #region Public Variables

#if UNITY_EDITOR

            public bool showSceneAsset;
            public bool showSceneSettingsForScene;
            public bool showBuildSettingsForScene;
            public bool isSceneEnabledInBuild;

#endif

            [SerializeField]
            public string scenePath;
            public string sceneName;
            public SceneTypeForLoading sceneTypeForLoading = SceneTypeForLoading.Default;
            public List<bool> sceneMapForEnable;
            public UnityAction OnActivateSceneOnLoadComplete;

            #endregion

            #region Public Callback

            float t_LoadingProgression;

            public IEnumerator ControllerForLoadingScene (
                bool t_ActivateSceneOnLoadComplete = true,
                UnityAction OnLoadComplete = null,
                UnityAction OnActivateScene = null
            ) {

                WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime (0.0167f);

                float t_LoadingProgression = 0;
                AsyncOperation t_AsyncOperationForLoadScene = SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);
                t_AsyncOperationForLoadScene.allowSceneActivation = false;

                while (!t_AsyncOperationForLoadScene.isDone) {

                    t_LoadingProgression = t_AsyncOperationForLoadScene.progress;

                    if (t_LoadingProgression >= 0.9f) {
                        OnLoadComplete?.Invoke ();
                        break;
                    }

                    yield return t_CycleDelay;
                }

                GameObject[] t_RootObjects = SceneManager.GetSceneByName (sceneName).GetRootGameObjects ();
                if (t_RootObjects != null && t_RootObjects.Length >= 1) {

                    foreach (GameObject t_RootObject in t_RootObjects) {

                        FetchRenderSettings t_Fetcher = t_RootObject.GetComponentInChildren<FetchRenderSettings> ();
                        if (t_Fetcher != null) {
                            Debug.Log (RenderSettings.subtractiveShadowColor + " : " + t_Fetcher.subtractiveShadowColor);
                            RenderSettings.subtractiveShadowColor = t_Fetcher.subtractiveShadowColor;
                            break;
                        } else {
                            Debug.Log ("Couldn't find the agent for fetching data of render settings for scene : " + sceneName);
                        }
                    }

                } else {

                    Debug.Log ("No Root Object Found");
                }

                // GameObject[] t_RootOfTheLoadedScene = SceneManager.GetSceneByName(sceneName).GetRootGameObjects();
                // int t_NumberOfRoot                  = t_RootOfTheLoadedScene.Length;
                // for(int rootIndex = 0 ; rootIndex < t_NumberOfRoot;  rootIndex++){
                //     Debug.Log(t_RootOfTheLoadedScene[rootIndex].name);
                //     RenderSettings t_FetchedRenderSettings = (t_RootOfTheLoadedScene[rootIndex] as GameObject);
                //     if(t_FetchedRenderSettings != null){
                //         OnPassingRenderSettings.Invoke(t_FetchedRenderSettings);
                //         break;
                //     }
                // }

                if (t_ActivateSceneOnLoadComplete) {

                    OnActivateSceneOnLoadComplete = null;

                    t_AsyncOperationForLoadScene.allowSceneActivation = true;
                    OnActivateScene?.Invoke ();
                } else {

                    OnActivateSceneOnLoadComplete = new UnityAction (delegate {

                        t_AsyncOperationForLoadScene.allowSceneActivation = true;
                        OnActivateScene?.Invoke ();
                    });
                }
            }

            #endregion

        }

        #endregion

        #region Public Variables

#if UNITY_EDITOR


        public bool showDefaultEditor;

#endif

        public List<AdditiveSceneInfo> listOfAdditiveScene;

        #endregion

        #region Private Variables

        #endregion

        #region Public Configuretion

        public UnityAction ProcessOnLoadScene (
            int t_SceneIndex,
            bool t_ActivateSceneOnLoadComplete = true,
            UnityAction OnSceneLoaded = null,
            UnityAction OnActivateScene = null) {

#if UNITY_EDITOR

            if (EditorApplication.isPlaying) {
                StartCoroutine (listOfAdditiveScene[t_SceneIndex].ControllerForLoadingScene ());
            } else {
                UnityEditor.SceneManagement.EditorSceneManager.OpenScene (listOfAdditiveScene[t_SceneIndex].scenePath, UnityEditor.SceneManagement.OpenSceneMode.Additive);
            }

#else

            StartCoroutine (listOfAdditiveScene[t_SceneIndex].ControllerForLoadingScene ());

#endif

            return listOfAdditiveScene[t_SceneIndex].OnActivateSceneOnLoadComplete;
        }

        public void ProcessOnUnloadScene (int t_SceneIndex) {

#if UNITY_EDITOR

            if (EditorApplication.isPlaying) {
                SceneManager.UnloadSceneAsync (listOfAdditiveScene[t_SceneIndex].sceneName);
            } else {
                UnityEditor.SceneManagement.EditorSceneManager.CloseScene (SceneManager.GetSceneByPath (listOfAdditiveScene[t_SceneIndex].scenePath), false);
            }

#else

            SceneManager.UnloadSceneAsync (listOfAdditiveScene[t_SceneIndex].sceneName);

#endif
        }

        #endregion

        #region Public Callback

        public bool IsSceneLoaded (int t_SceneIndex) {

            return SceneManager.GetSceneByName (listOfAdditiveScene[t_SceneIndex].sceneName).isLoaded;
        }

        /// <summary>
        /// You can use the return type (UnityAction) when 't_ActivateSceneOnLoadComplete' is set to 'true' for manually activating the scene, otherwise it will return a null function (As the scene will already be activated when load is complete)
        /// </summary>
        /// <param name="t_SceneName">Pass the "name" of the scene</param>
        /// <param name="t_ActivateSceneOnLoadComplete">It will instantly activate the scene when the loading is complete</param>
        /// <param name="OnLoadComplete">InvokeAnEvent when scene load is complete</param>
        /// <param name="OnActivateScene">InvokeAnEvent when scene is activated</param>
        /// <returns>You can use the return type (UnityAction) when 't_ActivateSceneOnLoadComplete' is set to 'true' for manually activating the scene, otherwise it will return a null function (As the scene will already be activated when load is complete)</returns>
        public UnityAction LoadScene (
            string t_SceneName,
            bool t_ActivateSceneOnLoadComplete = true,
            UnityAction OnLoadComplete = null,
            UnityAction OnActivateScene = null) {

            int t_NumberOfAvailableAdditiveScene = listOfAdditiveScene.Count;

            for (int i = 0; i < t_NumberOfAvailableAdditiveScene; i++) {

                if (listOfAdditiveScene[i].sceneName == t_SceneName) {

                    return LoadScene (i, t_ActivateSceneOnLoadComplete, OnLoadComplete, OnActivateScene);
                }
            }

            return null;
        }

        /// <summary>
        /// You can use the return type (UnityAction) when 't_ActivateSceneOnLoadComplete' is set to 'true' for manually activating the scene, otherwise it will return a null function (As the scene will already be activated when load is complete)
        /// </summary>
        /// <param name="t_SceneName">Pass the "index" of the scene</param>
        /// <param name="t_ActivateSceneOnLoadComplete">It will instantly activate the scene when the loading is complete</param>
        /// <param name="OnLoadComplete">InvokeAnEvent when scene load is complete</param>
        /// <param name="OnActivateScene">InvokeAnEvent when scene is activated</param>
        /// <returns>You can use the return type (UnityAction) when 't_ActivateSceneOnLoadComplete' is set to 'true' for manually activating the scene, otherwise it will return a null function (As the scene will already be activated when load is complete)</returns>
        public UnityAction LoadScene (
            int t_SceneIndex,
            bool t_ActivateSceneOnLoadComplete = true,
            UnityAction OnLoadComplete = null,
            UnityAction OnActivateScene = null) {

            UnityAction OnResultAction = null;

            List<bool> t_SceneToBeLoaded = listOfAdditiveScene[t_SceneIndex].sceneMapForEnable;
            int t_NumberOfSceneToBeLoaded = t_SceneToBeLoaded.Count;
            for (int i = 0; i < t_NumberOfSceneToBeLoaded; i++) {

                switch (listOfAdditiveScene[t_SceneIndex].sceneTypeForLoading) {

                    case SceneTypeForLoading.Default:

                        if (i == t_SceneIndex && !IsSceneLoaded (i)) {

                            OnResultAction = ProcessOnLoadScene (i, t_ActivateSceneOnLoadComplete, OnLoadComplete, OnActivateScene);
                        }

                        break;

                    case SceneTypeForLoading.RemoveAllOtherScene:

                        if (i == t_SceneIndex && !IsSceneLoaded (i)) {

                            OnResultAction = ProcessOnLoadScene (i, t_ActivateSceneOnLoadComplete, OnLoadComplete, OnActivateScene);

                        } else if (i != t_SceneIndex && IsSceneLoaded (i)) {

                            ProcessOnUnloadScene (i);
                        }

                        break;

                    case SceneTypeForLoading.Custom:

                        if (t_SceneToBeLoaded[i] && !IsSceneLoaded (i)) {

                            OnResultAction = ProcessOnLoadScene (i, t_ActivateSceneOnLoadComplete, OnLoadComplete, OnActivateScene);
                        } else if (!t_SceneToBeLoaded[i] && IsSceneLoaded (i)) {

                            ProcessOnUnloadScene (i);
                        }

                        break;
                }

            }

            return OnResultAction;
        }

        public void LightingSettingsManupulation () {

        }

        #endregion
    }
}