﻿namespace com.faithstudio.Mesh
{
    using UnityEngine;

    public class MeshDeformationController : MonoBehaviour
    {
        #region Public Varaibles

        [Space(5.0f)]
        [Header("Reference : External")]
        public Camera cameraReference;
        public BasicMeshGenerator basicMeshGeneratorReference;

        [Space(5.0f)]

        [Range(0, 5f)]
        public float impactArea;
        public Vector3 meshDeformDirection;
        public MeshFilter meshFiltererForDeformation;

        #endregion

        #region Private Variables

        private Mesh        m_MeshReference;
        private Transform   m_MeshTransformReference;

        private bool m_IsPressed;

        private int m_NumberOfVertices;

        private float m_AbsoluteImpactArea;
        private float m_ErrorRateOfVertexDisplacement = 0;

        private Transform m_CameraTransformReference;

        private Vector3 m_TouchDownPosition;
        private Vector3 m_TouchPosition;
        private Vector3 m_TouchUpPosition;
        private Vector3 m_TouchPointOfRayCast;

        private Ray m_Ray;
        private RaycastHit m_RayCastHit;

        private DeformedVertexPoint[] m_DeformedVertexPointReference;

        #endregion

        #region Mono Behaviour

        private void Awake()
        {
            m_CameraTransformReference = cameraReference.transform;
        }

        private void Start()
        {
            CalculateVertices();
        }

        private void Update()
        {
            TouchController();

            if (m_IsPressed)
            {
                m_Ray = cameraReference.ScreenPointToRay(m_TouchPointOfRayCast);

                if (Physics.Raycast(m_Ray, out m_RayCastHit))
                {
                    if (m_RayCastHit.collider != null)
                    {
                        ApplyMeshDeformation(m_RayCastHit.point);
                    }

                }
            }
        }

        //private void OnCollisionEnter(Collision collision)
        //{
        //    ContactPoint[] collisionPoints = collision.contacts;

        //    int t_NumberOfContactPoints = collisionPoints.Length;
        //    for (int i = 0; i < t_NumberOfContactPoints; i++)
        //    {

        //        ApplyMeshDeformation(collisionPoints[i].point);
        //    }
        //}

        #endregion

        #region Configuretion   :   TouchController

        private void TouchController()
        {


#if UNITY_EDITOR

            if (Input.GetMouseButtonDown(0))
            {

                OnTouchDown(Input.mousePosition);
            }

            if (Input.GetMouseButton(0))
            {

                OnTouch(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {

                OnTouchUp(Input.mousePosition);
            }

#elif UNITY_ANDROID || UNITY_IOS

        switch (Input.GetTouch(0).phase){

            case TouchPhase.Began:

                OnTouchDown(Input.GetTouch(0).position);
                break;

            case TouchPhase.Stationary:

                OnTouch(Input.GetTouch(0).position);
                break;

            case TouchPhase.Moved:

                OnTouch(Input.GetTouch(0).position);
                break;

            case TouchPhase.Ended:

                OnTouchUp(Input.GetTouch(0).position);
                break;

            case TouchPhase.Canceled:

                OnTouchUp(Input.GetTouch(0).position);
                break;
        }
#endif
        }

        private void OnTouchDown(Vector2 t_TouchPosition)
        {

            m_TouchDownPosition = t_TouchPosition;
            m_TouchPointOfRayCast = t_TouchPosition;

            m_IsPressed = true;
        }

        private void OnTouch(Vector2 t_TouchPosition)
        {

            m_TouchPosition = t_TouchPosition;
            m_TouchPointOfRayCast = t_TouchPosition;

        }

        private void OnTouchUp(Vector2 t_TouchPosition)
        {
            m_TouchUpPosition = t_TouchPosition;

            m_IsPressed = false;
        }

        #endregion

        #region Configuretion

        private void CalculateVertices()
        {
            MeshFilter t_MeshFilterReference;
            if (meshFiltererForDeformation == null)
            {
                t_MeshFilterReference = basicMeshGeneratorReference.GetMeshFilter();
                m_DeformedVertexPointReference = basicMeshGeneratorReference.GetArraysOfDeformedVertexPoint();
                m_ErrorRateOfVertexDisplacement = basicMeshGeneratorReference.GetErrorRateOfVertexDisplacement();
                m_NumberOfVertices = t_MeshFilterReference.mesh.vertices.Length;
            }
            else
            {
                t_MeshFilterReference = meshFiltererForDeformation;
                m_NumberOfVertices = m_MeshReference.vertices.Length;
                m_DeformedVertexPointReference = new DeformedVertexPoint[m_NumberOfVertices];
                for (int i = 0; i < m_NumberOfVertices; i++)
                {
                    m_DeformedVertexPointReference[i] = new DeformedVertexPoint(
                            i,
                            Vector2Int.zero, // Need To Fix Later
                            m_MeshReference.vertices[i]
                        );
                }

            }
            m_MeshReference = t_MeshFilterReference.mesh;
            m_MeshTransformReference = t_MeshFilterReference.transform;

            m_AbsoluteImpactArea = (m_MeshTransformReference.localScale.x + m_MeshTransformReference.localScale.y + m_MeshTransformReference.localScale.z) * impactArea;

        }

        private Vector3 GetWorldPositionOfVertex(Vector3 t_VertexPosition) {

            return m_MeshTransformReference.position + new Vector3(
                    m_MeshTransformReference.localScale.x * t_VertexPosition.x,
                    m_MeshTransformReference.localScale.y * t_VertexPosition.y,
                    m_MeshTransformReference.localScale.z * t_VertexPosition.z
                );
        }

        private bool IsValidRowAndColumn(int t_Resolution, int t_Row, int t_Column) {

            if ((t_Row >= 0 && t_Row < t_Resolution) && (t_Column >= 0 && t_Column < t_Resolution))
                return true;

            return false;
        }

        private void ApplyMeshDeformation(Vector3 t_InputPoint)
        {
            Vector3[] t_NewVertices = m_MeshReference.vertices;
            Vector3 t_ModifiedDeformation;
            int   t_Resolution = basicMeshGeneratorReference.resolution;
            int   t_NearestVertex = 0;
            float t_CurrentDistance = 0;
            float t_LowestDistance  = Mathf.Infinity;

            for (int i = 0; i < m_NumberOfVertices; i++) {

                t_CurrentDistance = Vector3.Distance(t_InputPoint, GetWorldPositionOfVertex(m_DeformedVertexPointReference[i].GetCurrentVertexPosition()));
                if (t_LowestDistance > t_CurrentDistance) {

                    t_LowestDistance = t_CurrentDistance;
                    t_NearestVertex  = i;
                }
            }

            int t_Row   = Mathf.FloorToInt( (t_NearestVertex / ((float)(t_Resolution * t_Resolution))) * t_Resolution);
            int t_Column= t_NearestVertex - (t_Row * t_Resolution);

            t_NewVertices[t_NearestVertex] = m_DeformedVertexPointReference[t_NearestVertex].GetDeformedPositionFromInitialPosition(meshDeformDirection, true);

            int t_DistortionPoint1, t_DistortionPoint2, t_DistortionPoint3, t_DistortionPoint4;
            int t_DistortionPoint5, t_DistortionPoint6, t_DistortionPoint7, t_DistortionPoint8;

            ////DownLeft
            //if (IsValidRowAndColumn(t_Resolution, t_Row - 1, t_Column - 1))
            //{

            //    t_DistortionPoint1 = ((t_Row - 1) * t_Resolution) + (t_Column - 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            -m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            -m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint1] = m_DeformedVertexPointReference[t_DistortionPoint1].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////DownRight
            //if (IsValidRowAndColumn(t_Resolution, t_Row - 1, t_Column + 1))
            //{
            //    t_DistortionPoint2 = ((t_Row - 1) * t_Resolution) + (t_Column + 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            -m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint2] = m_DeformedVertexPointReference[t_DistortionPoint2].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////UpperLeft
            //if (IsValidRowAndColumn(t_Resolution, t_Row + 1, t_Column - 1))
            //{
            //    t_DistortionPoint3 = ((t_Row + 1) * t_Resolution) + (t_Column - 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            -m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint3] = m_DeformedVertexPointReference[t_DistortionPoint3].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////UpperRight
            //if (IsValidRowAndColumn(t_Resolution, t_Row + 1, t_Column + 1))
            //{
            //    t_DistortionPoint4 = ((t_Row + 1) * t_Resolution) + (t_Column + 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint4] = m_DeformedVertexPointReference[t_DistortionPoint4].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////Left
            //if (IsValidRowAndColumn(t_Resolution, t_Row - 0, t_Column - 1))
            //{
            //    t_DistortionPoint5 = ((t_Row - 0) * t_Resolution) + (t_Column - 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            -m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            0f
            //        );
            //    t_NewVertices[t_DistortionPoint5] = m_DeformedVertexPointReference[t_DistortionPoint5].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////Right
            //if (IsValidRowAndColumn(t_Resolution, t_Row - 0, t_Column + 1))
            //{
            //    t_DistortionPoint6 = ((t_Row - 0) * t_Resolution) + (t_Column + 1);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            m_ErrorRateOfVertexDisplacement,
            //            0f,
            //            0f
            //        );
            //    t_NewVertices[t_DistortionPoint6] = m_DeformedVertexPointReference[t_DistortionPoint6].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////Down
            //if (IsValidRowAndColumn(t_Resolution, t_Row - 1, t_Column - 0))
            //{
            //    t_DistortionPoint7 = ((t_Row - 1) * t_Resolution) + (t_Column - 0);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            0f,
            //            0f,
            //            m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint7] = m_DeformedVertexPointReference[t_DistortionPoint7].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            ////Up
            //if (IsValidRowAndColumn(t_Resolution, t_Row + 1, t_Column - 0))
            //{
            //    t_DistortionPoint8 = ((t_Row + 1) * t_Resolution) + (t_Column - 0);
            //    t_ModifiedDeformation = meshDeformDirection + new Vector3(
            //            0f,
            //            0f,
            //            -m_ErrorRateOfVertexDisplacement
            //        );
            //    t_NewVertices[t_DistortionPoint8] = m_DeformedVertexPointReference[t_DistortionPoint8].GetDeformedPositionFromInitialPosition(t_ModifiedDeformation, true);
            //}

            for (int i = 0; i < m_NumberOfVertices; i++)
            {
                if (Vector3.Distance(t_InputPoint, GetWorldPositionOfVertex(m_DeformedVertexPointReference[i].GetCurrentVertexPosition())) <= m_AbsoluteImpactArea)
                {
                    t_NewVertices[i] = m_DeformedVertexPointReference[i].GetDeformedPositionFromInitialPosition(meshDeformDirection, true);
                }
            }

            m_MeshReference.vertices = t_NewVertices;

            m_MeshReference.RecalculateBounds();
            m_MeshReference.RecalculateNormals();
            m_MeshReference.RecalculateTangents();
        }

        #endregion
    }
}


