﻿namespace com.faithstudio.Mesh
{
    using UnityEngine;

    public class MeshFace
    {
        #region Private Variables

        private Shape m_Shape;
        private Mesh m_Mesh;
        private int m_Resolution;
        private Vector3 m_LocalUp;
        private Vector3 m_VertexDisplacement;
        private Vector3 m_AxisA;
        private Vector3 m_AxisB;

        private DeformedVertexPoint[] m_DeformedVertexPoints;

        #endregion

        #region Public Callback

        public MeshFace(Shape t_Shape, Mesh t_Mesh, int t_Resolution, Vector3 t_LocalUp, Vector3 t_VertexDisplacement)
        {
            m_Shape = t_Shape;
            m_Mesh = t_Mesh;
            m_Resolution = t_Resolution;
            m_LocalUp = t_LocalUp;
            m_VertexDisplacement = t_VertexDisplacement;

            m_AxisA = new Vector3(m_LocalUp.y, m_LocalUp.z, m_LocalUp.x);
            m_AxisB = Vector3.Cross(m_LocalUp, m_AxisA);

            m_DeformedVertexPoints = new DeformedVertexPoint[t_Resolution * t_Resolution];
        }

        public void ConstructMesh()
        {

            Vector3[] t_Vertices = new Vector3[m_Resolution * m_Resolution];
            int[] t_Triangles = new int[(m_Resolution - 1) * (m_Resolution - 1) * 2 * 3];
            int t_TriangleIndex = 0;

            for (int y = 0; y < m_Resolution; y++)
            {

                for (int x = 0; x < m_Resolution; x++)
                {

                    int t_AbsoluteIndex = x + (y * m_Resolution);
                    Vector2 t_Percent = new Vector2(x, y) / (m_Resolution - 1);
                    Vector3 t_PointOnUnitCube = m_VertexDisplacement + ((t_Percent.x - 0.5f) * 1 * m_AxisA) + ((t_Percent.y - 0.5f) * 1 * m_AxisB);

                    switch (m_Shape)
                    {
                        case Shape.Plane:
                            t_Vertices[t_AbsoluteIndex] = t_PointOnUnitCube;
                            break;
                        case Shape.Cube:
                            t_Vertices[t_AbsoluteIndex] = t_PointOnUnitCube;
                            break;

                        case Shape.Sphere:
                            Vector3 t_PointOnUnitSphere = t_PointOnUnitCube.normalized;
                            t_Vertices[t_AbsoluteIndex] = t_PointOnUnitSphere;
                            break;
                    }

                    m_DeformedVertexPoints[t_AbsoluteIndex] = new DeformedVertexPoint(
                            t_AbsoluteIndex,
                            new Vector2Int(y, x),
                            t_Vertices[t_AbsoluteIndex]
                        );

                    if ((x != (m_Resolution - 1)) && (y != (m_Resolution - 1)))
                    {

                        t_Triangles[t_TriangleIndex] = t_AbsoluteIndex;
                        t_Triangles[t_TriangleIndex + 1] = t_AbsoluteIndex + m_Resolution + 1;
                        t_Triangles[t_TriangleIndex + 2] = t_AbsoluteIndex + m_Resolution;

                        t_Triangles[t_TriangleIndex + 3] = t_AbsoluteIndex;
                        t_Triangles[t_TriangleIndex + 4] = t_AbsoluteIndex + 1;
                        t_Triangles[t_TriangleIndex + 5] = t_AbsoluteIndex + m_Resolution + 1;
                        t_TriangleIndex += 6;
                    }
                }
            }


            m_Mesh.Clear();
            m_Mesh.vertices = t_Vertices;
            m_Mesh.triangles = t_Triangles;
            m_Mesh.RecalculateNormals();
        }

        public DeformedVertexPoint[] GetDeformedVertexPoint() {

            return m_DeformedVertexPoints;
        }

        #endregion
    }
}


