﻿namespace com.faithstudio.Camera {
    using UnityEditor;
    using UnityEngine;

    [CustomEditor (typeof (CameraShake))]
    public class CameraShakeEditor : Editor {

        private CameraShake Reference;

        private void OnEnable () {

            Reference = (CameraShake) target;
        }

        public override void OnInspectorGUI () {

            serializedObject.Update ();

            if (EditorApplication.isPlaying) {

                if (GUILayout.Button ("CameraShake")) {

                    Reference.ShowCameraShake ();
                }
            }

            EditorGUILayout.Space ();
            DrawDefaultInspector ();

            serializedObject.ApplyModifiedProperties ();
        }

    }
}