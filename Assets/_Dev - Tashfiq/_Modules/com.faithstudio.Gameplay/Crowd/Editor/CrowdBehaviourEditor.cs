﻿namespace com.alphapotato.Gameplay
{
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(CrowdBehaviour))]
public class CrowdBehaviourEditor : Editor
{
    private CrowdBehaviour CrowdBehaviourReference;

    private void OnEnable()
    {
        CrowdBehaviourReference = (CrowdBehaviour)target;
    }

    public override void OnInspectorGUI()
    {

        // if(GUILayout.Button("PreProcess")){
        //     CrowdBehaviourReference.PreProcess();
        // }

        // EditorGUILayout.BeginHorizontal();
        // {   
        //     if(GUILayout.Button("Enable Ragdoll")){
        //         CrowdBehaviourReference.EnableRagdoll();
        //     }
        //     if(GUILayout.Button("Disable Ragdoll")){
        //         CrowdBehaviourReference.DisableRagdoll();
        //     }
        // }
        // EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        {
            if (EditorApplication.isPlaying && GUILayout.Button("RandomForce")) {

                CrowdBehaviourReference.EnableRagdoll();
                CrowdBehaviourReference.ApplyForceOnImpact(Random.onUnitSphere, CrowdBehaviourReference.reactionForceOnImpact);
            }

            if (GUILayout.Button("Config Ragdoll"))
            {

                CrowdBehaviourReference.ConfigRagdoll();

            }
        }
        EditorGUILayout.EndHorizontal();


        

        base.OnInspectorGUI();
    }
}

}
