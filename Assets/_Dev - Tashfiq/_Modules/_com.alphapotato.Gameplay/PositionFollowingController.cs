﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using com.alphapotato.Gameplay;

public class PositionFollowingController : MonoBehaviour
{
    #region Public Variables

    public Transform        defaultFollowingObject;
    public List<Transform>  defaultPositionFollowingObject;

    [Space(5.0f)]
    public bool isUseTimeManagerForTimeScale;
    public bool isPositionFollowingControllerStartOnEnable;
    public bool isRotateOverThePoint;

    [Space(5.0f)]
    [Range(0f,25f)]
    public float defaultForwardVelocity = 1f;
    [Range(0f, 1f)]
    public float defaultAngulerVelocity = 0.1f;
    [Range(0f,5f)]
    public float defaultDelayOnCompletingLoop;

    [Space(5.0f)]
    public Vector3 defaultPositionOffset;

    [Space(5.0f)]
    public bool snapPositionOnX;
    public bool snapPositionOnY;
    public bool snapPositionOnZ;

    #endregion

    #region Private Variables

    private bool            m_IsPositionFollowingControllerRunning;
    private bool            m_IsConvertPositionFromTransformReferences;

    private int             m_CurrentIndexOfPosition;
    private int             m_NumberOfFollowingPosition;

    private float           m_AbsoluteForwardVelocity;
    private float           m_AbsoluteAngulerVelocity;
    private float           m_AbsoluteDelayAtEndOfLoop;
    private float           m_AbsoluteDeltaTime;

    private Vector3         m_TargetPosition;

    private List<Transform> m_ListOfransformReferencesOfCurrentFollowingPosition;
    private Transform       m_CurrentFollowingObject;

    private List<Vector3>   m_ListOfCurrentFollowingPosition;
    private Vector3         m_CurrentPositionOffset;

    private UnityAction     OnLoopStart;
    private UnityAction     OnLoopEnd;

    private TimeController     timeManagerReference;
    

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (isPositionFollowingControllerStartOnEnable)
            EnableAndDisableMonoBehaviour(true);
        else
            EnableAndDisableMonoBehaviour(false);
    }


    private void OnEnable()
    {
        if (isPositionFollowingControllerStartOnEnable) {

            StartPositionFollowingController(
                    defaultPositionFollowingObject
                );
        }
    }

    private void OnDisable()
    {
        if (isPositionFollowingControllerStartOnEnable)
        {
            StopPositionFollowingController();
        }
    }

    private void Update()
    {

        if (isUseTimeManagerForTimeScale)
        {
            m_AbsoluteDeltaTime = timeManagerReference.GetAbsoluteDeltaTime();
        }
        else
            m_AbsoluteDeltaTime = Time.deltaTime;

        if (m_IsConvertPositionFromTransformReferences)
        {
            m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] = m_ListOfransformReferencesOfCurrentFollowingPosition[m_CurrentIndexOfPosition].position;
        }

        m_TargetPosition = m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] + m_CurrentPositionOffset;

        Vector3 t_ModifiedPosition = Vector3.MoveTowards(
                    m_CurrentFollowingObject.position,
                    m_TargetPosition,
                    m_AbsoluteForwardVelocity * m_AbsoluteDeltaTime
                );
        t_ModifiedPosition = new Vector3(
                snapPositionOnX ? m_TargetPosition.x : t_ModifiedPosition.x,
                snapPositionOnY ? m_TargetPosition.y : t_ModifiedPosition.y,
                snapPositionOnZ ? m_TargetPosition.z : t_ModifiedPosition.z
            );
        
        m_CurrentFollowingObject.position = t_ModifiedPosition;

        if (isRotateOverThePoint)
        {

            Quaternion t_ModifiedRotation = Quaternion.Slerp(
                    m_CurrentFollowingObject.rotation,
                    Quaternion.LookRotation(m_TargetPosition - m_CurrentFollowingObject.position),
                    m_AbsoluteAngulerVelocity
                );
            m_CurrentFollowingObject.rotation = t_ModifiedRotation;
        }

        if (Vector3.Distance(m_CurrentFollowingObject.position, m_TargetPosition) <= 0.1f)
        {

            m_CurrentIndexOfPosition++;
            if (m_CurrentIndexOfPosition >= m_NumberOfFollowingPosition)
            {
                m_CurrentIndexOfPosition = 0;

                OnLoopEnd?.Invoke();

                if (m_AbsoluteDelayAtEndOfLoop > 0) {

                }
                //Need A Delay
                OnLoopStart?.Invoke();
            }
        }
    }

    #endregion

    #region Configuretion

    private void EnableAndDisableMonoBehaviour(bool t_IsEnable = true)
    {
        enabled = t_IsEnable;
    }

    #endregion

    #region Public Callback

    public void StartPositionFollowingController(
        Transform t_FollowingObject = null,
        Vector3 t_PositionOffset = new Vector3(),
        int t_StartingIndex = 0,
        UnityAction OnLoopStart = null,
        UnityAction OnLoopEnd = null)
    {
        m_ListOfransformReferencesOfCurrentFollowingPosition = defaultPositionFollowingObject;

        int t_NumberOfFollowingPosition = defaultPositionFollowingObject.Count;
        List<Vector3> t_ListOfCurrentFollowingPosition = new List<Vector3>();
        for (int i = 0; i < t_NumberOfFollowingPosition; i++)
        {

            t_ListOfCurrentFollowingPosition.Add(m_ListOfransformReferencesOfCurrentFollowingPosition[i].position);
        }

        m_IsConvertPositionFromTransformReferences = true;
        StartPositionFollowingController(
            t_ListOfCurrentFollowingPosition,
            t_FollowingObject,
            t_PositionOffset,
            t_StartingIndex,
            OnLoopStart,
            OnLoopEnd);
    }

    public void StartPositionFollowingController(
        List<Transform> t_Position, 
        Transform t_FollowingObject = null, 
        Vector3 t_PositionOffset = new Vector3(),
        int t_StartingIndex = 0,
        UnityAction OnLoopStart = null,
        UnityAction OnLoopEnd = null) {

        m_ListOfransformReferencesOfCurrentFollowingPosition    = new List<Transform>(t_Position);
        
        int             t_NumberOfFollowingPosition     = t_Position.Count;
        List<Vector3> t_ListOfCurrentFollowingPosition  = new List<Vector3>();
        for (int i = 0; i < t_NumberOfFollowingPosition; i++) {

            t_ListOfCurrentFollowingPosition.Add(m_ListOfransformReferencesOfCurrentFollowingPosition[i].position);
        }

        m_IsConvertPositionFromTransformReferences = true;
        StartPositionFollowingController(
            t_ListOfCurrentFollowingPosition, 
            t_FollowingObject, 
            t_PositionOffset, 
            t_StartingIndex,
            OnLoopStart,
            OnLoopEnd);
    }

    public void StartPositionFollowingController(
        List<Vector3> t_Position, 
        Transform t_FollowingObject = null, 
        Vector3 t_PositionOffset = new Vector3(),
        int t_StartingIndex = 0,
        UnityAction OnLoopStart = null,
        UnityAction OnLoopEnd = null) {

        m_ListOfCurrentFollowingPosition    = new List<Vector3>(t_Position);
        m_NumberOfFollowingPosition         = t_Position.Count;

        if (t_FollowingObject == null)
            m_CurrentFollowingObject = defaultFollowingObject;
        else
            m_CurrentFollowingObject = t_FollowingObject;

        if (t_PositionOffset == Vector3.zero)
            m_CurrentPositionOffset = defaultPositionOffset;
        else
            m_CurrentPositionOffset = t_PositionOffset;

        //Future Development : Accessed Through Script
        m_AbsoluteForwardVelocity = defaultForwardVelocity;
        m_AbsoluteAngulerVelocity = defaultAngulerVelocity;
        m_AbsoluteDelayAtEndOfLoop= defaultDelayOnCompletingLoop;

        m_CurrentIndexOfPosition            = t_StartingIndex;
        m_CurrentFollowingObject.position   = m_ListOfCurrentFollowingPosition[m_CurrentIndexOfPosition] + m_CurrentPositionOffset;

        this.OnLoopStart    = OnLoopStart;
        this.OnLoopEnd      = OnLoopEnd;

        timeManagerReference= TimeController.Instance;

        EnableAndDisableMonoBehaviour(true);

        m_IsPositionFollowingControllerRunning = true;
    }

    public void StopPositionFollowingController() {

        m_IsConvertPositionFromTransformReferences = false;
        m_IsPositionFollowingControllerRunning = false;
        EnableAndDisableMonoBehaviour(false);
    }

    #endregion
}
