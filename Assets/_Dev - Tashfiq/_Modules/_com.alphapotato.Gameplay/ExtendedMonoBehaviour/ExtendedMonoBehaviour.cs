﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections;

public class ExtendedMonoBehaviour : MonoBehaviour
{
    #region CustomVariables

    public delegate void    OnUpdateEventHandler();

    private delegate void   OnCouroutineExecutionEvent();
    private delegate bool   OnCouroutineExecutionEventWithReturningBool();
    private delegate float  OnCouroutineExecutionEventWithReturningFloat();

    private class CouroutineEvent{

        public float    delayOnStartingCouroutineEvent = 0f;
        public          UnityAction OnCouroutineEventStart;

        public float    delayOnEndingCouroutineEvent = 0f;
        public          UnityAction OnCouroutineEventEnd;
        
        public          UnityAction OnCouroutineEvent;

        public CouroutineEvent(UnityAction OnCouroutineEvent){
            this.OnCouroutineEvent = OnCouroutineEvent;
        }

        public CouroutineEvent(
            UnityAction OnCouroutineEvent,
            float delayOnStartingCouroutineEvent, 
            UnityAction OnCouroutineEventStart){
            
            this.OnCouroutineEvent              = OnCouroutineEvent;
        
            this.delayOnStartingCouroutineEvent = delayOnStartingCouroutineEvent;
            this.OnCouroutineEventStart         = OnCouroutineEventStart;
        }

        public CouroutineEvent(
            UnityAction OnCouroutineEvent,
            float       delayOnStartingCouroutineEvent, 
            UnityAction OnCouroutineEventStart,
            float       delayOnEndingCouroutineEvent,
            UnityAction OnCouroutineEventEnd){
            
            this.OnCouroutineEvent              = OnCouroutineEvent;
        
            this.delayOnStartingCouroutineEvent = delayOnStartingCouroutineEvent;
            this.OnCouroutineEventStart         = OnCouroutineEventStart;

            this.delayOnEndingCouroutineEvent   = delayOnEndingCouroutineEvent;
            this.OnCouroutineEventEnd           = OnCouroutineEventEnd;
        }
    }
    private class UpdateCycleUsingCoroutine
    {
        public bool isForceQuit = false;

        public IEnumerator CouroutineForThisUpdateCycle(CouroutineEvent[] couroutineEvents, bool isUnscaledDelay = false){

            int t_NumberOfCoroutineEvents = couroutineEvents.Length;
            for(int i = 0; i < t_NumberOfCoroutineEvents; i++){

                yield return null;
            }
        }
    }

    

    #endregion

    #region Public Variables

    public OnUpdateEventHandler OnUpdateEvent;

    #endregion

    #region  Private Variables

    #endregion

    #region Mono Behaviour

    private void Awake() {
        
        DisableMonoBeheaviour();
    }

    private void Update() {
        
        OnUpdateEvent?.Invoke();
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void  EnableMonoBehaviour(){

        enabled = true;
    }

    public void DisableMonoBeheaviour(){

        enabled = false;
    }

    #endregion
}
