﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class IAnimationEventForLevel46Prank : IAnimationBaseClass
{


    #region Public Variables

    [Header("Common")]
    public ParticleSystem particleForPuke;


    [Header("Prank1 :   WormMeal")]
    public Vector3 cameraFocusOffsetAfterWormMeal;
    public Transform cameraOriginAfterWormMeal;
    public List<Transform> cameraFocusAfterWormMeal;

    [Space(5.0f)]
    public GameObject microphone;

    [Header("Prank2 :   Avalanche!")]
    public WormSwarmController wormAtJarTop;
    
    [Space(5.0f)]
    public Animator animatorForIceCylinder;

    [Space(5.0f)]
    public Vector3 cameraFocusOffsetAfterPrank2Setup;
    public Transform cameraOriginAfterPrank2Setup;
    public List<Transform> cameraFocusAfterPrank2Setup;

    [Space(5.0f)]
    public CameraShake cameraShakeReference;
    public ParticleSystem particleOnFaceBoom;
    public ParticleSystem particleOnPlayerFall;

    #endregion

    #region Private Variables

    private Transform m_InitialParentOfWormAtJarTop;
    private Vector3 m_InitialPositionOfWormAtJarTop;
    private Vector3 m_InitialPositionOfMicrophone;

    #endregion

    #region Mono Behaviour

    private void Awake() {
        m_InitialParentOfWormAtJarTop = wormAtJarTop.transform.parent;
        m_InitialPositionOfWormAtJarTop = wormAtJarTop.transform.localPosition;
        m_InitialPositionOfMicrophone = microphone.transform.localPosition;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAppearingIceCylinder(){

        yield return new WaitForSeconds(3f);
        animatorForIceCylinder.SetTrigger("APPEAR");
        StopCoroutine(ControllerForAppearingIceCylinder());
    }

    #endregion

    #region Public Callback

    public void StartPuking(){

        particleForPuke.Play();
    }

    public void StopPuking(){

        particleForPuke.Stop();
    }

    #endregion

    #region Public Callback :   Prank1

    public void DisappearMicrophone(){

        microphone.SetActive(false);
    }

    public void AppearMicrophone(){

        microphone.transform.localPosition = m_InitialPositionOfMicrophone;
        microphone.SetActive(true);
    }

    public void FocusCameraAfterWormMeal(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint(
            cameraOriginAfterWormMeal,
            cameraFocusAfterWormMeal,
            new Constraint(){x = false, y = true, z = false},
            false,
            cameraFocusOffsetAfterPrank2Setup
        );
    }


    #endregion

    #region Public Callback :   Prank2

    public void FollowCameraAfterPrank2Setup(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint(
            cameraOriginAfterPrank2Setup,
            cameraFocusAfterPrank2Setup,
            new Constraint(){x = false, y = true, z = false},
            false,
            cameraFocusOffsetAfterPrank2Setup
        );
    }

    public void FallWromFromJarTop(){

        wormAtJarTop.transform.parent = null;
        wormAtJarTop.SpawnWormSwarm();
    }

    public void ClearWormFromFallenJar(){

        wormAtJarTop.ClearWormList();
        wormAtJarTop.transform.parent = m_InitialParentOfWormAtJarTop;
        wormAtJarTop.transform.localPosition = m_InitialPositionOfWormAtJarTop;
    }

    public void PlayBoomParticleOnFace(){

        particleOnFaceBoom.Play();
        cameraShakeReference.ShowCameraShake();
    }

    public void PlayOnPlayerFallParticle(){

        particleOnPlayerFall.Play();
    }

    public void AppearIceCylinder(){

        StartCoroutine(ControllerForAppearingIceCylinder());
    }

    public void DisappearIceCylinder(){

        animatorForIceCylinder.SetTrigger("DEFAULT");
    }

    #endregion
}
