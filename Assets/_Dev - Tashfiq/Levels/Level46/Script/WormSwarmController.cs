﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormSwarmController : MonoBehaviour
{
    #region Public Variables

    [Range(0f,5f)]
    public float initialDelay = 0;

    [Space(5.0f)]
    public GameObject wormPrefab;
    [Range(1,30)]
    public int wormSwarmSize;
    [Range(0f,1f)]
    public float delayBetweenSpawn;

    #endregion

    #region Private Variables

    private bool m_IsWormSpawnControllerRunning;
    private List<GameObject> m_ListOfSpawnedWorm;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private IEnumerator ControllerForSpawningWormSwarm(){

        yield return new WaitForSeconds(initialDelay);

        int t_NumberOfWormToBeSpawned = Random.Range(wormSwarmSize / 2, wormSwarmSize);
        while(t_NumberOfWormToBeSpawned > 0){

            GameObject t_NewWorm = Instantiate(wormPrefab, Vector3.zero, Quaternion.identity);
            t_NewWorm.transform.SetParent(transform);
            t_NewWorm.transform.localPosition = Vector3.zero;

            m_ListOfSpawnedWorm.Add(t_NewWorm);

            yield return new WaitForSeconds(Random.Range(0.0167f,delayBetweenSpawn));
            t_NumberOfWormToBeSpawned--;
        }

        StopCoroutine(ControllerForSpawningWormSwarm());
        m_IsWormSpawnControllerRunning = false;
    }

    #endregion

    #region Public Callback

    public void SpawnWormSwarm(){

        if(!m_IsWormSpawnControllerRunning){

            

            m_IsWormSpawnControllerRunning = true;

            if(m_ListOfSpawnedWorm == null)
                m_ListOfSpawnedWorm = new List<GameObject>();
            else
                ClearWormList();

            StartCoroutine(ControllerForSpawningWormSwarm());
        }
    }

    public void ClearWormList(){

        foreach(GameObject t_Worm in m_ListOfSpawnedWorm){

            Destroy(t_Worm);
        }

        m_ListOfSpawnedWorm.Clear();
    }

    #endregion
}
