﻿using UnityEngine;
using com.faithstudio.Camera;
using System.Collections.Generic;
public class IAnimationEventForLevel4Prank : IAnimationBaseClass
{
    #region Public Variables

    [Header("Reference  :   External")]
    public CameraShake cameraShakeReference;

    [Header("Reference  :   PrankWithSofa")]
    public ParticleSystem oilParticle;
    public ParticleSystem honkParticle;
    [Space(5.0f)]
    public Animator honkAnimator;
    [Space(5.0f)]
    public GameObject mobileReference;
    [Space(5.0f)]
    public Transform        cameraOriginOnFailedInSofa;
    public List<Transform>  cameraFocusOnFailedInSofa;

    [Header("Reference  :   PrankWithCoffee")]

    [Space(5.0f)]
    public Transform cameraOrigin;
    public List<Transform> cameraFocus;

    [Space(5.0f)]
    public GameObject dryIceCubeReference;
    public GameObject saltCubeReference;

    [Space(5.0f)]
    public ParticleSystem dryIceSmoke;

    #endregion

    #region Public Callback :   PrankWithSofa

    public void DoOilSeeping() {
        oilParticle.Play();
    }

    public void DoHonk() {
        honkAnimator.SetTrigger("Honk");
        honkParticle.Play();
        
    }

    public void ShowMobile() {
        mobileReference.SetActive(true);
    }

    public void HideMobile()
    {
        mobileReference.SetActive(false);
    }

    public void FollowCameraToPlayerOnFailedInSofa(){

        CameraMovementController.Instance.FollowCamera(
            cameraOriginOnFailedInSofa,
            cameraFocusOnFailedInSofa
        );
    }

    public void DoCameraShake(){

        cameraShakeReference.ShowCameraShake();
    }

    #endregion

    #region Public Callback :   PrankWithCoffee

    public void HideDryIceCube() {

        dryIceCubeReference.SetActive(false);
    }

    public void HideSaltCube() {

        saltCubeReference.SetActive(false);
    }

    public void ShowDryIceSmoke(){

        dryIceSmoke.Play();
    }

    public void FollowCameraOnPlayer(){

        CameraMovementController.Instance.FollowCamera(
            cameraOrigin,
            cameraFocus
        );
    }

    #endregion

}
