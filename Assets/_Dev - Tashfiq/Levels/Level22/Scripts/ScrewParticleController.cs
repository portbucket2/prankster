﻿using UnityEngine;
using System.Collections.Generic;

public class ScrewParticleController : MonoBehaviour
{
    #region Public Variables

    public ParticleSystem particleForScrewing;

    #endregion

    public void PlayScrewingParticle(){

        particleForScrewing.Play();
    }

    public void StopScrewingParticle(){

        particleForScrewing.Stop();
    }
}
