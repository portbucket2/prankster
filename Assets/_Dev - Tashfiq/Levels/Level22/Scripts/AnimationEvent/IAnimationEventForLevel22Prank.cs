﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;
using com.faithstudio.Math;

public class IAnimationEventForLevel22Prank : IAnimationBaseClass
{

    #region Public Variables

    [Header("Prank  :   Headlong Collision")]
    public GlueVisual glueVisualReference;

    [Space(5.0f)]
    public ParticleSystem particleOnHeadbangFall;

    [Space(5.0f)]
    public Vector3 cameraFocusOffsetOnHeadbang;
    public Transform cameraOriginOnHeadbang;
    public List<Transform> cameraFocusOnHeadBang;

    [Header("Prank  :   Interval")]
    [Range(1f,5f)]
    public float durationOfCameraTransation;
    public Transform playerRigReference;
    public Transform playerTargetReference;
    public Vector3 cameraFocusOffsetOnInterval;
    public Transform cameraOriginOnInterval;
    public List<Transform> cameraFocusOnInterval;

    [Header("Prank  :   NeedForSpeed")]

    public ParticleSystem speedlineParticleOnTrademil;

    [Space(5.0f)]
    public Transform cameraOriginWhenPlayerFallFromTrademil;
    public List<Transform> cameraFocusWhenPlayerFallFromTrademil;


    [Space(5.0f)]
    public Vector3 cameraFocusoffsetWhenPrankFailedInTrademil;
    public Transform cameraOriginWhenPrankFailedAtTrademil;
    public List<Transform> cameraFocusWhenPrankFailedAtTrademil;

    #endregion

    #region Configuretion

    private IEnumerator ControllerForGluefied(){

        yield return new WaitForSeconds(1f);

        while(glueVisualReference.manualglue(Time.deltaTime * 2)){

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ControllerForLerpingCameraPosition(){

        float t_CycleLength = 0.0167f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);

        float t_RemainingTimeForDuration = durationOfCameraTransation;

        while(t_RemainingTimeForDuration > 0){

            float t_Progress = 1f - (t_RemainingTimeForDuration / durationOfCameraTransation);
            Vector3 t_ModifiedPosition = Vector3.Lerp(
                playerTargetReference.position,
                playerRigReference.position,
                0.5f
            );
            t_ModifiedPosition = new Vector3(
                t_ModifiedPosition.x,
                0f,
                t_ModifiedPosition.z
            );
            playerTargetReference.position = t_ModifiedPosition;
            t_RemainingTimeForDuration -= t_CycleLength;
            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForLerpingCameraPosition());
    }

    #endregion

    #region Public Callback

    public void GlueIt(){

        StartCoroutine(ControllerForGluefied());
    }

    public void UnfollowCamera(){

        CameraMovementController.Instance.UnfollowCamera();
    }

    public void FocusOnHeadbang(){

        CameraMovementController.Instance.FollowCamera(
            cameraOriginOnHeadbang,
            cameraFocusOnHeadBang,
            true,
            cameraFocusOffsetOnHeadbang
        );
    }

    public void PlayParticleOnHeadbangFall(){

        particleOnHeadbangFall.Play();
    }

    public void FocusCameraForInterval(){

        StartCoroutine(ControllerForLerpingCameraPosition());
        CameraMovementController.Instance.FocusCameraWithOrigin(
            cameraOriginOnInterval,
            cameraFocusOnInterval,
            true,
            cameraFocusOffsetOnInterval
        );
    }

    public void FocusCameraOnPlayerWhenFallFromTrademil(){

        speedlineParticleOnTrademil.Play();
        CameraMovementController.Instance.FollowCamera(
            cameraOriginWhenPlayerFallFromTrademil,
            cameraFocusWhenPlayerFallFromTrademil
        );
    }

    public void FocusCameraOnPlayerWhenTrademilPrankFailed(){

        CameraMovementController.Instance.FocusCameraWithOrigin(
            cameraOriginWhenPrankFailedAtTrademil,
            cameraFocusWhenPrankFailedAtTrademil,
            true,
            cameraFocusoffsetWhenPrankFailedInTrademil
        );
    }

    #endregion

}
