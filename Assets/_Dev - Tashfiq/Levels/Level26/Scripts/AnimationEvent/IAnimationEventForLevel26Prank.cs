﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
public class IAnimationEventForLevel26Prank : IAnimationBaseClass {

    [Header ("PrankSetup :   Popping Dreams")]
    public Animator animatorOfPillow;
    public Animator animatorOfBalloon;
    public Animator animatorOfBrick;

    public ParticleSystem balloonPopParticle;
    public ParticleSystem brickBangParticle;

    public void SwitchPillowToBalloon () {

        animatorOfPillow.SetTrigger ("DISAPPEAR");
        animatorOfBalloon.SetTrigger ("APPEAR");
    }

    public void SwitchPillowToBrick () {

        animatorOfPillow.SetTrigger ("DISAPPEAR");
        animatorOfBrick.SetTrigger ("APPEAR");
    }

    public void PlayParticleForHeadbangingBalloon () {

        animatorOfBalloon.SetTrigger ("DISAPPEAR");
        balloonPopParticle.Play ();
    }

    public void PlayParticleForHeadbangingBrick () {

        brickBangParticle.Play ();
    }

    [Header ("PrankSetup :   Light Out")]
    public Material lightBulbMaterial;
    public List<Material> listOfLightingMaterial;

    [Space (5.0f)]
    [Range (0.1f, 5f)]
    public float durationForFuse;
    [Range (1, 5)]
    public float maxLightIntensity = 3;
    public AnimationCurve fuseIntensityThroughTime;
    public Color defaultDarkColorOfRoom;
    public Color lightBulbColorOnPrankFailed;
    public Color lightBulbColorOnPrankSuccess;

    [Space (5.0f)]
    public ParticleSystem lightBulbSwitchParticle;
    public ParticleSystem lightBulbFuseSetupParticle;
    public ParticleSystem lightBulbFuseParticle;

    [Space (5.0f)]
    public Transform cameraOriginForLightOut;
    public List<Transform> cameraFocusForLightOut;

    [Space (5.0f)]
    public Transform cameraOriginWhenLookingAtLightBulb;
    public List<Transform> cameraFocusOnLookingAtLightBulb;

    #region Private Variables


    #endregion

    #region Mono Behaviour

    private void Awake(){

    }

    #endregion

    public void PlayLightBulbSwitchParticle () {

        lightBulbSwitchParticle.Play ();
    }

    public void PlayLightBulbFuseParticle () {

        lightBulbFuseParticle.Play ();
    }

    public void ChangeLightBulbColorToRed () {

        lightBulbMaterial.SetColor ("_LitColor", Color.red);
    }

    public void ChangeLightBulbToYellow () {

        lightBulbFuseSetupParticle.Play ();
        lightBulbMaterial.SetColor ("_LitColor", Color.yellow);
    }

    public void FollowCameraTowardsPlayer () {

        CameraMovementController.Instance.FollowCamera (
            cameraOriginForLightOut,
            cameraFocusForLightOut
        );
    }

    public void LookAtLightBulb () {

        CameraMovementController.Instance.FocusCameraWithOrigin (
            cameraOriginWhenLookingAtLightBulb,
            cameraFocusOnLookingAtLightBulb
        );
    }

    public void EnlightRoomWithRedLight () {

        foreach (Material t_Material in listOfLightingMaterial) {

            t_Material.SetColor ("_LitColor", lightBulbColorOnPrankFailed);
        }
    }

    public void FuseBulb () {

        foreach (Material t_Material in listOfLightingMaterial) {

            t_Material.SetColor ("_LightColor", lightBulbColorOnPrankSuccess);
        }
        StartCoroutine (ControllerForFuseAnimation ());
    }

    public void ResetRoomLighting () {

        lightBulbMaterial.SetColor ("_LitColor", Color.white);
        
        int t_NumberOfLightingMaterial = listOfLightingMaterial.Count;
        for(int i = 0 ; i < t_NumberOfLightingMaterial; i++){

            listOfLightingMaterial[i].SetColor("_LitColor", defaultDarkColorOfRoom);
            listOfLightingMaterial[i].SetFloat ("_LightIntensity", 0);
        }
    }

    private IEnumerator ControllerForFuseAnimation () {

        float t_CycleLength = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds (t_CycleLength);

        float t_RemainingTimeForFuse = durationForFuse;
        while (t_RemainingTimeForFuse > 0) {

            float t_Progression = 1f - (t_RemainingTimeForFuse / durationForFuse);

            foreach (Material t_Material in listOfLightingMaterial) {

                t_Material.SetFloat ("_LightIntensity", fuseIntensityThroughTime.Evaluate (t_Progression) * maxLightIntensity);
            }

            yield return t_CycleDelay;
            t_RemainingTimeForFuse -= t_CycleLength;
        }

        foreach (Material t_Material in listOfLightingMaterial) {

            t_Material.SetFloat ("_LightIntensity", fuseIntensityThroughTime.Evaluate (1) * maxLightIntensity);
        }

        StopCoroutine (ControllerForFuseAnimation ());
    }
}