﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IAnimationEventForLevel26Prank))]
public class IAnimationEventForLevel26PrankEditor : Editor
{
    private IAnimationEventForLevel26Prank Reference;

    private void OnEnable(){

        Reference = (IAnimationEventForLevel26Prank) target;
    }
    
    public override void OnInspectorGUI(){

        serializedObject.Update();

        if(GUILayout.Button("Show Spark")){

            Reference.FuseBulb();
        }

        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }
}
