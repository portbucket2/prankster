﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.ScriptedParticle;
using com.alphapotato.Gameplay;

public class IAnimationEventForLevel33Prank : IAnimationBaseClass
{
    #region Public Variables

    public ToonCharacterOverlayController playerToonOverlay;
    public Texture  textureForColorMask;
    public Color    colorOnColorMask;

    [Header("Prank1 :   Hells's Kitchen")]
    public ParticleSystem particleOnStove;

    [Space(5.0f)]
    [Range(0f,5f)]
    public float durationForIncreaseParticleEffect = 0.5f;

    [Space(5.0f)]
    public ParticleSystem oilPouringParticle;
    public ParticleSystem waterPouringParticle;

    [Space(5.0f)]
    public ParticleController particleOfSmokeWhenStoveOnFire;
    public ParticleController particleOfFireWhenStoveOnFire;

    [Space(5.0f)]
    public ObjectFollowerWithConstraint cameraTransationForPrank1Success;
    [Header("Prank2 :   Burn Baby Burn!")]

    public ParticleSystem particleOfFireExtinguisher;
    public ParticleSystem bustParticleOnFrypan;
    public ParticleSystem particleOfAerosol;

    [Space(5.0f)]
    [Range(0f,5f)]
    public float durationForSlowmotion = 1f;
    [Range(0f,1f)]
    public float scaleOfSlowmotion = 0.2f;
    public AnimationCurve slowmowScale;

    [Space(5.0f)]
    public ObjectFollowerWithConstraint cameraTransationForPrank2Success;

    #endregion

    #region Mono Behaviour

    private IEnumerator Start(){

        yield return new WaitForSeconds(0.25f);
        ResetColorMask();
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForFirePower(bool t_IsIncrease, UnityAction OnFireAnimationEnd = null){

        particleOfSmokeWhenStoveOnFire.PlayParticle();
        particleOfFireWhenStoveOnFire.PlayParticle();

        float t_CycleLength = 0.0167f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);

        float t_RemainingTimeForIncreasingFire = durationForIncreaseParticleEffect;
        while(t_RemainingTimeForIncreasingFire > 0){

            

            if(t_IsIncrease){

                float t_Progress = 1f - (t_RemainingTimeForIncreasingFire / durationForIncreaseParticleEffect);
                particleOfSmokeWhenStoveOnFire.LerpOnPreset(t_Progress);
                particleOfFireWhenStoveOnFire.LerpOnPreset(t_Progress);
            }else{

                float t_Progress = t_RemainingTimeForIncreasingFire / durationForIncreaseParticleEffect;
                particleOfSmokeWhenStoveOnFire.LerpOnDefault(t_Progress);
                particleOfFireWhenStoveOnFire.LerpOnDefault(t_Progress);
            }

            yield return t_CycleDelay;
            t_RemainingTimeForIncreasingFire -= t_CycleLength;
        }

        OnFireAnimationEnd?.Invoke();

        StopCoroutine(ControllerForFirePower(false));
    }

    #endregion

    #region Public Callback

    public void PourOil(){

        oilPouringParticle.Play();
    }

    public void PourWater(){

        waterPouringParticle.Play();
    }

    public void BlowFireExtenguisher(){

        particleOfFireExtinguisher.Play();
        DecreaseFireOnStove();
    }

    public void BlowFireAerosol(){
        
        bustParticleOnFrypan.Play();
        particleOfAerosol.Play();
        TimeController.Instance.DoSlowMotion(
            true,
            durationForSlowmotion,
            scaleOfSlowmotion,
            slowmowScale,
            delegate{
                TimeController.Instance.RestoreToInitialTimeScale();
            }
        );
    }

    public void StopFireAerosol(){

        particleOfAerosol.Stop();
    }

    public void ResetFireOnStove(){

        particleOnStove.Play();

        particleOfSmokeWhenStoveOnFire.StopParticle(true);
        particleOfFireWhenStoveOnFire.StopParticle(true);
    }

    public void SetFireOnStove(){

        particleOnStove.Stop();

        particleOfSmokeWhenStoveOnFire.LerpOnDefault(1);
        particleOfFireWhenStoveOnFire.LerpOnDefault(1);

        particleOfSmokeWhenStoveOnFire.PlayParticle();
        particleOfFireWhenStoveOnFire.PlayParticle();
    }

    public void IncreaseFireOnStove(){

        StartCoroutine(ControllerForFirePower(true));
    }

    public void DecreaseFireOnStove(){

        StartCoroutine(ControllerForFirePower(false, delegate{
            ResetFireOnStove();
        }));
    }

    public void ExecuteCameraTransitionForPrank1Success(){

        SetFireOnStove();
        cameraTransationForPrank1Success.Follow();
    }

    public void ExecuteCameraTransitionForPrank2Success(){

        cameraTransationForPrank2Success.Follow();
    }

    public void ImplementColorMask(){

        playerToonOverlay.ChangeOverlayMaskTexture(textureForColorMask);
        playerToonOverlay.ChangeMaskColor(colorOnColorMask);
        playerToonOverlay.ChangeMaskStrength(colorOnColorMask.a);

    }

    public void ResetColorMask(){

        playerToonOverlay.ChangeOverlayMaskTexture(null);
        playerToonOverlay.ChangeMaskColor(Color.white);
        playerToonOverlay.ChangeMaskStrength(0f);

    }

    #endregion

}
