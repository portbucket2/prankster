﻿using UnityEngine;
using System.Collections;

public class IAnimationEventForLevel34Prank : IAnimationBaseClass
{
    #region Public Variables

    [Header("Prank1 :   Bass Boost!")]

    public GameObject iceCreamOnHand;

    [Space(5.0f)]
    public ParticleSystem particleForTurtleBite;

    [Space(5.0f)]
    public Animator animatorForTurtleOnNouse; 
    public Animator animatorForTutrleJump;
    public Animator animatorForMicrophoneSwapper;

    [Space(5.0f)]
    public ToonCharacterOverlayController playerToonOverlay;
    public Texture  textureForColorMask;
    public Color    colorOnColorMask;

    [Space(5.0f)]
    [Range(0.1f,10f)]
    public float durationForAppearing;
    public AnimationCurve colorMaskStrenghOnAppearingThroughTransation;

    [Space(5.0f)]
    [Range(0.1f,10f)]
    public float durationForDisappearing;
    public AnimationCurve colorMaskStrenghOnDisappearingThroughTransation;

    [Header("Prank2 :   Trap Music!")]
    public ParticleSystem particleForGetInTheMouseTrap;
    public GameObject mouseTrapInHand;
    public Animator mouseTrapAnimation;
    public GlueVisual glueVisualReference;
    public ObjectFollowerWithConstraint cameraFollowerTransation;

    #endregion

    #region Mono Behaviour

    private IEnumerator Start(){

        yield return new WaitForSeconds(0.25f);

        ResetColorMask();
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForGluefied(){

        yield return new WaitForSeconds(1f);
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        while(glueVisualReference.manualglue(Time.deltaTime * 2)){

            yield return t_CycleDelay;
        }
    }

    #endregion

    #region Public Callback

    public void ShowIceCreamOnHand(){

        iceCreamOnHand.SetActive(true);
        animatorForMicrophoneSwapper.SetTrigger("RESET");
    }

    public void HideIceCreamOnHand(){

        iceCreamOnHand.SetActive(false);
    }

    public void ImplementColorMaskWithTransation(){

        playerToonOverlay.ShowColorMaskTransation(
            textureForColorMask,
            colorOnColorMask,
            colorMaskStrenghOnAppearingThroughTransation,
            durationForAppearing
        );
    }

    public void ResetColorMaskWithTransation(){

        playerToonOverlay.ShowColorMaskTransation(
            textureForColorMask,
            colorOnColorMask,
            colorMaskStrenghOnDisappearingThroughTransation,
            durationForDisappearing
        );
    }

    public void ResetColorMask(){

        playerToonOverlay.ChangeOverlayMaskTexture(null);
        playerToonOverlay.ChangeMaskColor(Color.white);
        playerToonOverlay.ChangeMaskStrength(0f);
        
    }

    public void ShowParticleForTurtleBite(){

        particleForTurtleBite.Play();
        ImplementColorMaskWithTransation();
    }

    public void ShowParticleForGetInTheMouseTrap(){

        particleForGetInTheMouseTrap.Play();
    }

    public void MakeTurtleJump(){

        animatorForTutrleJump.SetTrigger("JUMP");
    }

    public void VisibleTurtleOnNouse(){

        animatorForTurtleOnNouse.SetTrigger("SHOW");
    }

    public void ThrowTurtleFromNouse(){

        animatorForTurtleOnNouse.SetTrigger("THROW");
    }

    public void ResetPrank1Environment(){
        mouseTrapAnimation.SetTrigger("DEFAULT");
        glueVisualReference.ResetGlue();
    }

    public void SetMouseTrapOnKeyboard(){

        mouseTrapAnimation.SetTrigger("APPEAR");
    }

    public void SetMouseTrapOnHand(){

        ShowParticleForGetInTheMouseTrap();
        mouseTrapAnimation.SetTrigger("DEFAULT");
        mouseTrapInHand.SetActive(true);
    }

    public void SetGlueVisualOnKeyboard(){

        StartCoroutine(ControllerForGluefied());
    }

    public void ExecuteCameraTransation(){

        cameraFollowerTransation.Unfollow();
        cameraFollowerTransation.Follow(true);
    }

    #endregion
}
