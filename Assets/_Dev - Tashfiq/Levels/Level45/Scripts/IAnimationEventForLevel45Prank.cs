﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class IAnimationEventForLevel45Prank : IAnimationBaseClass
{
    #region Public Variables

    [Header("External Reference")]
    public CameraShake cameraShakeReference;
    public EnvironmentLightController livingRoomLightController;

    [Header("Prank1 :   Remote Trap")]

    public ParticleSystem particleWhenPutHandOnMouseTrap;
    public ParticleSystem particleOnClickingPhone;
    public ParticleSystem particleOnFall;

    [Space(5.0f)]
    public Vector3 cameraFocusOffsetAfterPrank1Execution;
    public Transform cameraOriginAfterPrank1Execution;
    public List<Transform> cameraFocusAfterPrank1Execution;

    [Header("Prank2 :   TooCrunchy")]
    public ParticleSystem particleForCrumpedPaper;
    public ParticleSystem paritcleForStone;
    public ParticleSystem particleForToothBreak;

    [Space(5.0f)]
    
    public Transform cameraOriginOnPrank2Failed;
    public Vector3 cameraFocusOffsetOnPrank2Failed;
    public List<Transform> cameraFocusOnPrank2Failed;

    #endregion

    #region Mono Behaviour

    private void Awake() {
        
        livingRoomLightController.ShowTransationForLightIntensity();
    }

    #endregion

    #region Public Callback :   Prank1

    public void PlayMouseTrapParticle(){

        particleWhenPutHandOnMouseTrap.Play();
    }

    public void PlayPhoneClickParticle(){

        particleOnClickingPhone.Play();
    }

    public void PlayParticleOnFall(){
        
        particleOnFall.Play();
    }

    public void FollowCharacterAfterPrank1Execution(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint(
            cameraOriginAfterPrank1Execution,
            cameraFocusAfterPrank1Execution,
            new Constraint(){x = false, y = true, z = false},
            false,
            cameraFocusOffsetAfterPrank1Execution
        );
    }

    #endregion

    #region Public Callback :   Prank2

    public void PlayCrumpedPaperFallParticle(){

        particleForCrumpedPaper.Play();
    }

    public void PlayStoneFallParticle(){

        paritcleForStone.Play();
    }

    public void ShowParticleForToothBreak(){

        particleForToothBreak.Play();
        cameraShakeReference.ShowCameraShake(1,5,false);
    }

    public void CameraTransationOnPrankFailed(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint(
            cameraOriginOnPrank2Failed,
            cameraFocusOnPrank2Failed,
            new Constraint(){x = false, y = true, z = false},
            false,
            cameraFocusOffsetOnPrank2Failed
        );
    }

    #endregion
}
