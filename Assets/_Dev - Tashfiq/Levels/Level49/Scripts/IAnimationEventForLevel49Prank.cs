﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;

public class IAnimationEventForLevel49Prank : IAnimationBaseClass {
    
    #region Public Variables

    

    [Space(5.0f)]
    public Constraint cameraFocusConstraintWhenFollowingPlayer;
    public Vector3 cameraFocusOffsetWhenFollowingForPlayer;
    public Transform cameraOriginWhenFollowingPlayer;
    public List<Transform> cameraFocusWhenFollowingPlayer;

    [Header ("Prank1 :   Colorful Wardrobe")]

    [Space (5.0f)]
    public GameObject redSock;
    public GameObject soapBar;

    [Space(5.0f)]
    public ParticleSystem particleForWashingPowder;
    public ParticleSystem particleForSlap;

    [Space(5.0f)]
    public Color initialColorOfTShirt;
    public Color colorOfTShirt;
    public Renderer coloredTShirtRenderer;

    [Space (5.0f)]
    public Constraint cameraFocusConstraintWhenFocusingPlayerAndWashingMachine;
    public Vector3 cameraFocusOffsetWhenFocusingPlayerAndWashingMachine;
    public Transform cameraOriginWhenFocusingPlayerAndWashingMachine;
    public List<Transform> cameraFocusWhenFocusingPlayerAndWashingMachine;

    [Header ("Prank2 :   Wardrobe Malfunction")]
    public GameObject brick;

    [Space(5.0f)]
    public Animator glueTubeAnimator;
    public Animator throwingClothAnimator;

    [Space(5.0f)]
    public ParticleSystem glueTubeParticle;
    public ParticleSystem particleForKick;
    public ParticleSystem particleForWashingMachineExploision;

    [Space(5.0f)]
    public Renderer faceTShirtRenderer;

    [Header("Camera :   OnPrank2Execution")]
    public Constraint cameraFocusConstraintAfterPrank2Execution;
    public Vector3 cameraFocusOffsetAfterPrank2Execution;
    public Transform cameraOriginAfterPrank2Execution;
    public List<Transform> cameraFocusAfterPrank2Execution;

    [Header("Camera :   ThrowingCloth")]
    public Constraint cameraFocusConstraintOnThrowingCloth;
    public Vector3 cameraFocusOffsetOnThrowingCloth;
    public Transform cameraOriginOnThrowingCloth;
    public List<Transform> cameraFocusOnThrowingCloth;

    [Header("Camera :   AtPrank2Execution")]
    public Constraint cameraFocusConstraintAtEndOfPrank2Execution;
    public Vector3 cameraFocusOffsetAtEndOfPrank2Execution;
    public Transform cameraOriginAtEndOfPrank2Execution;
    public List<Transform> cameraFocusAtEndOfPrank2Execution;

    #endregion

    #region Private Variables

    private Vector3 m_InitialPositionOfRedSock;
    private Vector3 m_InitialPositionOfSoap;
    private Vector3 m_InitialPositionOfBrick;

    #endregion

    #region Mono Behaviour

    private void Awake () {
        m_InitialPositionOfRedSock = redSock.transform.localPosition;
        m_InitialPositionOfSoap = soapBar.transform.localPosition;
        m_InitialPositionOfBrick = brick.transform.localPosition;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForPuttingItemInWashingMachine (GameObject t_Item, Vector3 t_InitialPosition = new Vector3 (), float t_DelayForExecution = 1f, bool t_ShowOpenAndCloseWashingCabinet = true) {


        yield return new WaitForSeconds (1f);

        if (t_InitialPosition != Vector3.zero)
            t_Item.transform.localPosition = t_InitialPosition;

        if (t_Item.GetComponent<ParticleSystem> ()) {
            if (!t_Item.activeInHierarchy)
                t_Item.SetActive (true);

            t_Item.GetComponent<ParticleSystem> ().Play ();
        } else {

            t_Item.SetActive (true);
        }

        yield return new WaitForSeconds (t_DelayForExecution);


        yield return new WaitForSeconds (1f);

        if (t_Item.GetComponent<ParticleSystem> ()) {
            t_Item.GetComponent<ParticleSystem> ().Stop ();
        } else {
            t_Item.SetActive (false);
        }

        StopCoroutine (ControllerForPuttingItemInWashingMachine (null, Vector3.zero));
    }

    #endregion

    #region Public Callback

    public void FollowPlayer () {

        CameraMovementController.Instance.FollowCameraWithFocusConstraint (
            cameraOriginWhenFollowingPlayer,
            cameraFocusWhenFollowingPlayer,
            cameraFocusConstraintWhenFollowingPlayer,
            true,
            cameraFocusOffsetWhenFollowingForPlayer
        );
    }

    

    #endregion

    #region Public Callback :   Prank1

    public void PlayParticleOfWashingPowder(){

        particleForWashingPowder.Play();
    }

    public void PlayParticleOnSlapingWashingMachine(){

        particleForSlap.Play();
    }

    public void PutRedSockInWashingMachine () {

        StartCoroutine (ControllerForPuttingItemInWashingMachine (redSock, m_InitialPositionOfRedSock,2));
    }

    public void PutSoapInWashingMachine () {

        StartCoroutine (ControllerForPuttingItemInWashingMachine (soapBar, m_InitialPositionOfSoap,2));
    }

    public void ChangeColorOfTShirtToRed(){
        coloredTShirtRenderer.material.SetFloat("_LightIntensity", 0f);
        coloredTShirtRenderer.material.SetColor("_LitColor", colorOfTShirt);
    }

    public void ChangeColorOfTShirtToWashed(){
        coloredTShirtRenderer.material.SetFloat("_LightIntensity", 1f);
        coloredTShirtRenderer.material.SetColor("_LitColor", Color.white);
    }

    public void ResetColorForTShirt(){
        coloredTShirtRenderer.material.SetFloat("_LightIntensity", 1f);
        coloredTShirtRenderer.material.SetColor("_LitColor", initialColorOfTShirt);
    }

    public void FollowPlayerAndWashingMachine(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint (
            cameraOriginWhenFocusingPlayerAndWashingMachine,
            cameraFocusWhenFocusingPlayerAndWashingMachine,
            cameraFocusConstraintWhenFocusingPlayerAndWashingMachine,
            true,
            cameraFocusOffsetWhenFocusingPlayerAndWashingMachine
        );
    }

    #endregion

    #region Public Callback :   Prank2

    public void PlayParticleOnKickingWashingMachine(){

        particleForKick.Play();
    }

    public void PlayParticleOnExploidingWashingMachine(){

        particleForWashingMachineExploision.Play();
    }

    public void PutBrickInWashingMachine () {

        StartCoroutine (ControllerForPuttingItemInWashingMachine (brick, m_InitialPositionOfBrick, 1f, false));
    }

    public void PutGlueInWashingMachine () {

        glueTubeParticle.Play();
        glueTubeAnimator.SetTrigger("GLUIFIED");
        //StartCoroutine (ControllerForGluing());
    }

    public void ChangeColorOfFaceTShirtToRed(){
        
        faceTShirtRenderer.material.SetFloat("_LightIntensity", 0f);
        faceTShirtRenderer.material.SetColor("_LitColor", colorOfTShirt);
    }

    public void ResetColorOfFaceTShirtToRed(){
        
        faceTShirtRenderer.material.SetFloat("_LightIntensity", 1f);
        faceTShirtRenderer.material.SetColor("_LitColor", initialColorOfTShirt);
    }

    public void ThrowClothFromWashingMachine(){

        throwingClothAnimator.SetTrigger("THROW");
    }

    public void ResetThrowingClothOfWashingMachine(){

        throwingClothAnimator.SetTrigger("DEFAULT");
    }

    public void CameraFocusOnPlayerAndWashingMachine () {

        CameraMovementController.Instance.FocusCameraWithOriginAndFocusConstraint (
            cameraOriginAfterPrank2Execution,
            cameraFocusAfterPrank2Execution,
            cameraFocusConstraintAfterPrank2Execution,
            true,
            cameraFocusOffsetAfterPrank2Execution,
            Vector3.zero,
            -0.5f
        );
    }

    public void CameraFocusOnThrowingCloth(){

        CameraMovementController.Instance.FollowCameraWithFocusConstraint (
            cameraOriginOnThrowingCloth,
            cameraFocusOnThrowingCloth,
            cameraFocusConstraintOnThrowingCloth,
            true,
            cameraFocusOffsetOnThrowingCloth,
            Vector3.zero,
            -0.35f
        );
    }

    public void CameraFocusOnPlayerAtEndOfPrank2Execution () {

        CameraMovementController.Instance.FollowCameraWithFocusConstraint (
            cameraOriginAtEndOfPrank2Execution,
            cameraFocusAtEndOfPrank2Execution,
            cameraFocusConstraintAtEndOfPrank2Execution,
            true,
            cameraFocusOffsetAtEndOfPrank2Execution
        );
    }

    #endregion
}