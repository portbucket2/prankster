﻿using UnityEngine;
using com.faithstudio.Camera;
public class IAnimationEventForLevel10Prank : IAnimationBaseClass
{
    #region Public Variables

    [Header("Prank  :   Mouse")]
    public GameObject mouseReference;
    public GameObject mouseTrapReference;
    [Space(5.0f)]
    public ParticleSystem clickParticle;

    [Header("Prank  : Screen")]
    public CameraShake cameraShakeReference;

    [Space(5.0f)]
    public GameObject horrorImage;
    public GameObject screenCrackImage;
    public GameObject screenBuggedImage;

    [Space(5.0f)]
    public ParticleSystem ouchParticle;
    
    #endregion

    #region Public Callback :   Prank - Mouse

    public void ReplaceMouseWithMouseTrap(){

        mouseReference.SetActive(false);
        mouseTrapReference.SetActive(true);
    }

    public void ResetMouse(){

        mouseReference.SetActive(true);
        mouseTrapReference.SetActive(false);
    }

    public void HideMouseTrap(){

        mouseTrapReference.SetActive(false);
    }

    public void ShowOuchParticle(){
        ouchParticle.Play();
    }

    #endregion

    #region Public Callback :   Prank - Screen

    public void ShowClickParticle(){
        clickParticle.Play();
    }

    public void ShowHorrorImage(){

        horrorImage.SetActive(true);
    }

    public void ShowScreenCrack(){

        screenCrackImage.SetActive(true);
    }

    public void ShowScreenBuggedImage(){
        screenBuggedImage.SetActive(true);
    }

    public void DoCameraShake(){

        cameraShakeReference.ShowCameraShake();
    }

    public void ResetScreenPrank(){

        horrorImage.SetActive(false);
        screenCrackImage.SetActive(false);
        screenBuggedImage.SetActive(false);
    }

    

    #endregion
}
