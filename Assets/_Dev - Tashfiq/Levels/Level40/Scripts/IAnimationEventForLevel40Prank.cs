﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.ScriptedParticle;
using com.faithstudio.Camera;

public class IAnimationEventForLevel40Prank : IAnimationBaseClass
{
    #region Public Variables

    [Header("Extra")]
    public ParticleController[] carExaustPipe;

    [Header("Prank1 :   All Backed Up!")]
    
    public GameObject playerMeshContainer;

    [Space(5.0f)]
    public ParticleSystem particleForCarSmoke;
    public ParticleSystem[] particleForFirework;
    
    [Space(5.0f)]
    public Animator[] animatorForPotato;

    [Space(5.0f)]
    public Animator[] animatorForFireCracker;

    [Space(5.0f)]
    public Animator         animatorForCar;

    [Space(5.0f)]
    public Vector3 cameraFocusOffsetOnFollowingCar;
    public Transform cameraOriginWhenFollowingCar;
    public List<Transform> cameraFocusWhenFollowingCar;

    [Header("Prank2 :   Drive Them Crazy")]
    
    public ParticleSystem particleForJam;

    [Space(5.0f)]
    public Animator animatorForScrewdriver;


    [Space(5.0f)]
    public Vector3 cameraFocusOffsetOnPrank2;
    public Transform cameraOriginOnPrank2;
    public List<Transform> cameraFocusOnPrank2;

    [Space(5.0f)]
    public List<Transform> cameraFocusPointOnJam;

    #endregion

    #region Configuretion

    private IEnumerator ControllerForFireworkBurst(){

        CameraMovementController.Instance.FollowCamera(
            cameraOriginWhenFollowingCar,
            cameraFocusWhenFollowingCar,
            true,
            cameraFocusOffsetOnFollowingCar
        );

        yield return new WaitForSeconds(1f);

        playerMeshContainer.SetActive(false);

        foreach(ParticleController t_ParticleController in carExaustPipe){

            t_ParticleController.PlayParticle();
            t_ParticleController.LerpOnPreset(1f);
        }  

        foreach(ParticleSystem t_Particle in particleForFirework){
            t_Particle.Play();
        }

        animatorForCar.SetTrigger("RUN");

        StopCoroutine(ControllerForFireworkBurst());
    }

    #endregion

    #region Public Callback :   Prank1

    public void SetPotatoInExustpipe(){

        foreach(Animator animator in animatorForPotato){

            animator.SetTrigger("APPEAR");
        }   

        // foreach(ParticleController t_ParticleController in carExaustPipe){

        //     t_ParticleController.PlayParticle();
        //     t_ParticleController.LerpOnDefault(1f);
        // }
    }

    public void SetFireCrackerInExustpipe(){

        foreach(Animator animator in animatorForFireCracker){

            animator.SetTrigger("APPEAR");
        }
    }

    public void SetCarOnSmoke(){

        particleForCarSmoke.Play();
    }

    public void StopCarSmoke(){

        particleForCarSmoke.Stop();
    }

    public void SetCarOnBurst(){

        StartCoroutine(ControllerForFireworkBurst());
    }

    #endregion

    #region Public Callback :   All Backed Up!

    public void Screwing(){

        animatorForScrewdriver.SetTrigger("SCREW_CYCLE");
    }

    public void FocusOnJam(){

        CameraMovementController.Instance.FocusCameraWithOrigin(
            cameraOriginOnPrank2,
            cameraFocusPointOnJam,
            true,
            cameraFocusOffsetOnPrank2
        );
    }

    public void FocusOnPrank2(){

        CameraMovementController.Instance.FocusCameraWithOrigin(
            cameraOriginOnPrank2,
            cameraFocusOnPrank2,
            true,
            cameraFocusOffsetOnPrank2
        );
    }

    public void SplitJam(){

        particleForJam.Play();
    }

    #endregion
}
