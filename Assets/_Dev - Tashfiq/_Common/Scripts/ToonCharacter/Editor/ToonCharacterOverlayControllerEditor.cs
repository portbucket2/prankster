﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (ToonCharacterOverlayController))]
public class ToonCharacterOverlayControllerEditor : Editor {
    private ToonCharacterOverlayController Reference;

    private SerializedProperty SP_SkinMatController;

    private void OnEnable () {

        Reference = (ToonCharacterOverlayController) target;

        if (Reference != null) {

            SP_SkinMatController = serializedObject.FindProperty ("skinMatControllerReference");

            FetchRendererReference ();

            if (!EditorApplication.isPlaying) {

                if (Reference.skinMatControllerReference == null) {

                    Reference.ChangeMainTexture (Reference.mainTexture);
                }

                Reference.ChangeOverlayMaskTexture (Reference.overlayMaskTexture);
                Reference.ChangeMaskColor (Reference.colorOfMaskOverlay);
                Reference.ChangeMaskStrength (Reference.maskStrength);
            }

        }
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        Reference.showDefaultEditor = EditorGUILayout.Toggle (
            "Show DefaultInspector",
            Reference.showDefaultEditor
        );

        if (Reference.showDefaultEditor) {

            DrawDefaultInspector ();
        } else {

            if (Reference.arrayOfRenderer == null || Reference.arrayOfRenderer.Length == 0) {

                EditorGUILayout.HelpBox ("No 'Renderer' component found in childs, Please attach the script with object containing 'Renderer' within the object or in the childs", MessageType.Error);
            } else {

                SkinMatGUI ();

                MaskGUI ();

                EditorGUILayout.Space ();
                MaskTransationGUI ();
            }

        }

        serializedObject.ApplyModifiedProperties ();
    }

    #region CustomGUI

    private void SkinMatGUI () {

        EditorGUILayout.Space ();

        EditorGUILayout.BeginHorizontal (); {
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("skinMatControllerReference"));
            if (Reference.skinMatControllerReference == null) {

                if (GUILayout.Button ("Fetch")) {

                    SP_SkinMatController.objectReferenceValue = Reference.GetComponent<SkinMatController> ();
                }
            } else {

                if (GUILayout.Button ("Remove")) {
                    SP_SkinMatController.objectReferenceValue = null;
                }
            }
        }
        EditorGUILayout.EndHorizontal ();

        if (Reference.skinMatControllerReference != null) {

            EditorGUILayout.BeginHorizontal (); {
                int t_MatIndex = Reference.GetSelectedMatIndexFromSkinMatController ();
                EditorGUILayout.LabelField ("Mat (" + t_MatIndex + ") : " + Reference.skinMatControllerReference.mats[t_MatIndex].name);
                if (GUILayout.Button ("<-")) {

                    if (t_MatIndex <= 0) {
                        Reference.SetSelectedMatIndexFromSkinMatController (
                            Reference.skinMatControllerReference.mats.Count - 1,
                            Reference.colorOfMaskOverlay);
                    } else {

                        Reference.SetSelectedMatIndexFromSkinMatController (
                            --t_MatIndex,
                            Reference.colorOfMaskOverlay);
                    }
                }

                if (GUILayout.Button ("->")) {

                    if (t_MatIndex >= (Reference.skinMatControllerReference.mats.Count - 1)) {
                        Reference.SetSelectedMatIndexFromSkinMatController (
                            0,
                            Reference.colorOfMaskOverlay);
                    } else {

                        Reference.SetSelectedMatIndexFromSkinMatController (
                            ++t_MatIndex,
                            Reference.colorOfMaskOverlay);
                    }
                }
            }
            EditorGUILayout.EndHorizontal ();
            EditorGUILayout.HelpBox ("It will now change the mask strength through Renderer->Material(Instance)", MessageType.Info);
        } else {

            EditorGUILayout.HelpBox ("It will now change the 'MaskStrength' through 'SkinMatController.SetValue'", MessageType.Info);
        }
    }

    private void MaskGUI () {

        EditorGUILayout.Space ();
        EditorGUILayout.LabelField ("EditorOnly  :   Experimental Values", EditorStyles.boldLabel);

        if (Reference.skinMatControllerReference == null) {

            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("mainTexture"));
            if (EditorGUI.EndChangeCheck ()) {

                Texture2D t_NewTexture = (Texture2D) serializedObject.FindProperty ("mainTexture").objectReferenceValue;
                Reference.ChangeMainTexture (t_NewTexture);
            }
        }

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("overlayMaskTexture"));
        if (EditorGUI.EndChangeCheck ()) {

            Texture2D t_NewTexture = (Texture2D) serializedObject.FindProperty ("overlayMaskTexture").objectReferenceValue;
            Reference.ChangeOverlayMaskTexture (t_NewTexture);
        }

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("colorOfMaskOverlay"));
        if (EditorGUI.EndChangeCheck ()) {

            Reference.ChangeMaskColor (Reference.colorOfMaskOverlay);
            Reference.ChangeMaskStrength (Reference.maskStrength);

        }

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("maskStrength"));
        if (EditorGUI.EndChangeCheck ()) {

            Reference.ChangeMaskStrength (Reference.maskStrength);
            Reference.ChangeMaskColor (Reference.colorOfMaskOverlay);
        }

        EditorGUILayout.Space ();
        EditorGUILayout.HelpBox ("AlphaChannel of 'ColorOfMaskOverlay' works as the 'Mask Strength'", MessageType.Info);
    }

    private void MaskTransationGUI () {

        EditorGUILayout.BeginVertical (); {
            EditorGUILayout.LabelField ("EditorOnly  :   Transation", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("transationDuration"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("transationCurve"));
            if (EditorApplication.isPlaying &&
                !Reference.IsColorMaskTransationRunning () &&
                GUILayout.Button ("PlayTransation")) {

                Reference.ShowColorMaskTransation (
                    Reference.overlayMaskTexture,
                    Reference.colorOfMaskOverlay,
                    Reference.transationCurve,
                    Reference.transationDuration
                );
            } else {
                if (!EditorApplication.isPlaying) {

                    EditorGUILayout.HelpBox ("To test the transation, please go to 'PlayMode' from editor", MessageType.Info);
                }
            }
        }
        EditorGUILayout.EndVertical ();

    }

    #endregion

    #region Configuretion

    private void FetchRendererReference () {

        Reference.arrayOfRenderer = Reference.transform.GetComponentsInChildren<Renderer> ();
    }

    #endregion
}