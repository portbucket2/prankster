﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof (SkinMatController))]
public class ToonCharacterOverlayController : MonoBehaviour {
    #region Public Variables

#if UNITY_EDITOR

    [HideInInspector]
    public bool showDefaultEditor;

    [Space (5.0f)]
    public Texture mainTexture;
    public Texture overlayMaskTexture;
    public Color colorOfMaskOverlay;
    [Range (0f, 1f)]
    public float maskStrength = 0.35f;

    [Space (5.0f)]
    [Range (1f, 10f)]
    public float transationDuration = 1;
    public AnimationCurve transationCurve;

#endif

    public SkinMatController skinMatControllerReference;

    public Renderer[] arrayOfRenderer;

    #endregion

    #region Private Variables

    private bool m_IsColorMaskTransationRunning;

    private string SELECTED_MAT_INDEX = "TCOC_SELECTED_MAT_INDEX";

    #endregion

    #region Configuretion

    private IEnumerator ControllerForColorMaskTransation (
        Texture t_TextureForColorMask,
        Color t_ColorOfColorMask,
        AnimationCurve t_TransitionCurve,
        float t_Duration = 1f) {

        ChangeOverlayMaskTexture (t_TextureForColorMask);
        ChangeMaskColor (t_ColorOfColorMask);
        ChangeMaskStrength (t_ColorOfColorMask.a);

        float t_CycleLength = 0.0167f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime (t_CycleLength);

        float t_RemainingTime = t_Duration;
        float t_TransationValue;
        while (t_RemainingTime > 0) {

            t_TransationValue = t_TransitionCurve.Evaluate (1f - (t_RemainingTime / t_Duration));
            ChangeMaskStrength (t_ColorOfColorMask.a * t_TransationValue);

            yield return t_CycleDelay;
            t_RemainingTime -= t_CycleLength;
        }

        ChangeMaskStrength (t_ColorOfColorMask.a * t_TransitionCurve.Evaluate (1));
        StopCoroutine (ControllerForColorMaskTransation (null, Color.white, null, 0));
        m_IsColorMaskTransationRunning = false;
    }

    #endregion

    #region Public Callback

    public int GetSelectedMatIndexFromSkinMatController () {

        return PlayerPrefs.GetInt (SELECTED_MAT_INDEX, 0);
    }

    public void SetSelectedMatIndexFromSkinMatController (int t_NewMatIndex, Color t_ColorMask = new Color()) {

//        if (t_NewMatIndex >= 0 && t_NewMatIndex < skinMatControllerReference.mats.Count) {

//            PlayerPrefs.SetInt (SELECTED_MAT_INDEX, t_NewMatIndex);

//            foreach (Renderer renderer in arrayOfRenderer) {

//#if UNITY_EDITOR 

//                if (Application.isPlaying) {

//                    renderer.material =  skinMatControllerReference.mats[t_NewMatIndex];

//                } else {

//                    renderer.sharedMaterial = skinMatControllerReference.mats[t_NewMatIndex];
//                }

//#else

//                renderer.material = skinMatControllerReference.mats[t_NewMatIndex];

//#endif
//            }

//            if(t_ColorMask != new Color()){

//                ChangeMaskColor(t_ColorMask);
//                ChangeMaskStrength(t_ColorMask.a);
//            }
//        }else{

//            Debug.LogError ("Invalid MatIndex : " + t_NewMatIndex + ", There are only '" + skinMatControllerReference.mats.Count + "' available to switch");
//        }

        
    }

    public bool IsColorMaskTransationRunning () {

        return m_IsColorMaskTransationRunning;
    }

    public void ChangeMainTexture (Texture t_MainTexture) {

        foreach (Renderer renderer in arrayOfRenderer) {

#if UNITY_EDITOR 

            if (Application.isPlaying) {

                renderer.material.SetTexture ("_MainTexture", t_MainTexture);

            } else {

                renderer.sharedMaterial.SetTexture ("_MainTexture", t_MainTexture);
            }

#else

            renderer.material.SetTexture ("_MainTexture", t_MainTexture);

#endif

        }
    }

    public void ChangeOverlayMaskTexture (Texture t_TexOverlay) {

        foreach (Renderer renderer in arrayOfRenderer) {

#if UNITY_EDITOR 

            if (Application.isPlaying) {

                renderer.material.SetTexture ("_TexOverlay", t_TexOverlay);
            } else {

                renderer.sharedMaterial.SetTexture ("_TexOverlay", t_TexOverlay);
            }

#else

            renderer.material.SetTexture ("_TexOverlay", t_TexOverlay);

#endif

        }
    }

    public void ChangeMaskColor (Color t_MaskColor) {

        foreach (Renderer renderer in arrayOfRenderer) {

#if UNITY_EDITOR 

            if (Application.isPlaying) {

                renderer.material.SetColor ("_OverlayMaskColor", t_MaskColor);

            } else {

                renderer.sharedMaterial.SetColor ("_OverlayMaskColor", t_MaskColor);
            }

#else

            renderer.material.SetColor ("_OverlayMaskColor", t_MaskColor);

#endif

        }

        #if UNITY_EDITOR

        maskStrength = t_MaskColor.a;

        #endif

        
    }

    public void ChangeMaskStrength (float t_MaskStrength) {

        foreach (Renderer renderer in arrayOfRenderer) {

#if UNITY_EDITOR 

            if (Application.isPlaying) {

                if (skinMatControllerReference != null) {
                    skinMatControllerReference.SetMatValue (
                        GetSelectedMatIndexFromSkinMatController (),
                        "_MaskStrength",
                        t_MaskStrength,
                        true);
                } else {
                    renderer.material.SetFloat ("_MaskStrength", t_MaskStrength);
                }

            } else {

                renderer.sharedMaterial.SetFloat ("_MaskStrength", t_MaskStrength);
            }

#else

            if (skinMatControllerReference != null) {
                skinMatControllerReference.SetMatValue (
                    GetSelectedMatIndexFromSkinMatController (),
                    "_MaskStrength",
                    t_MaskStrength);
            } else {
                renderer.material.SetFloat ("_MaskStrength", t_MaskStrength);
            }

#endif

        }

        #if UNITY_EDITOR

        colorOfMaskOverlay.a = t_MaskStrength;

        #endif

        
    }

    public void ShowColorMaskTransation (
        Texture t_TextureForColorMask,
        Color t_ColorOfColorMask,
        AnimationCurve t_TransitionCurve,
        float t_Duration = 1f
    ) {

        if (!m_IsColorMaskTransationRunning) {

            m_IsColorMaskTransationRunning = true;
            StartCoroutine (ControllerForColorMaskTransation (
                t_TextureForColorMask,
                t_ColorOfColorMask,
                t_TransitionCurve,
                t_Duration
            ));
        }
    }

    #endregion
}