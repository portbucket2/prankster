﻿using UnityEngine;

public abstract class IAnimationBaseClass : MonoBehaviour
{

#region Public Varaibles

    public ControllerForPranksInLevel controllerForPrankInLevels;

#endregion

#region Public Callback

    public void GoToNextSequence(){

        controllerForPrankInLevels.GoToNextSequence();
    }

    

#endregion

    
}
