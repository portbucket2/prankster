﻿using UnityEngine;
using System.Collections.Generic;
using com.faithstudio.Camera;

public class ObjectFollowerWithConstraint : MonoBehaviour
{   
    #region Custom Variables

    [System.Serializable]
    public struct Cordinate
    {
        public bool x;
        public bool y;
        public bool z;
    }

    #endregion

    #region Public Variables

    [Tooltip("If set to null, will take the following transform as instance")]
    public Transform        followerReference;
    public List<Transform>  targetReferences;
    public Vector3          targetOffSet;
    [Range(0f,1f)]
    public float forwardVelocity;

    [Header("ConstaintPosition")]
    public Cordinate constraint;

    [Header("Paramteters    :   Optional")]
    [Tooltip("If no origin is set, the current position of the camera will be the origin")]
    public Transform cameraOrigin;
    public bool defaultValueIfStayInCameraFocus = true;
    [Tooltip("For negetive value, will follow untill the 'Unfollow' function is called")]
    [Range(-1f,10)]
    public float defaultValueForDurationOfFollow = -1;

    #endregion

    #region Private Variables

    private GameObject m_DummyCameraOrigin;
    private Vector3 m_InitialPosition;
    private bool m_IsStayInCameraFocus;
    private float m_DurationOfFollow = -1;
    private float m_EndTimeToUnfollow;

    #endregion

    #region Mono Behaviour

    private void OnEnable(){
        
        if(followerReference == null)
            followerReference = transform;
    }

    private void Awake(){

        if(followerReference == null)
            followerReference = transform;
        
        m_InitialPosition = followerReference.position;

        enabled = false;
    }

    private void Update(){

        Bounds t_TargetBounds = new Bounds(targetReferences[0].position, Vector3.zero);
        foreach(Transform t_Target in targetReferences){
            t_TargetBounds.Encapsulate(t_Target.position);
        }

        Vector3 t_ModifiedPosition = Vector3.Lerp(
            followerReference.position,
            t_TargetBounds.center,
            forwardVelocity
        );
        t_ModifiedPosition = new Vector3(
            constraint.x ? 0 : t_ModifiedPosition.x,
            constraint.y ? 0 : t_ModifiedPosition.y,
            constraint.z ? 0 : t_ModifiedPosition.z
        );
        t_ModifiedPosition += targetOffSet;
        followerReference.position = t_ModifiedPosition;

        if(m_DurationOfFollow > 0 && (m_EndTimeToUnfollow - Time.time) < 0){
            if(m_IsStayInCameraFocus)
                Unfollow();
        }
    }

    #endregion

    #region Public Callback

    public void Follow(bool t_ResetToInitialPosition = true){

        Follow(defaultValueIfStayInCameraFocus, defaultValueForDurationOfFollow, t_ResetToInitialPosition);
    }

    public void Follow(bool t_IsStayInCameraFocus, bool t_ResetToInitialPosition = true){

        Follow(t_IsStayInCameraFocus, defaultValueForDurationOfFollow, t_ResetToInitialPosition);
    }

    public void Follow(float t_DurationOfFollow, bool t_ResetToInitialPosition = true){

        Follow(defaultValueIfStayInCameraFocus, t_DurationOfFollow,t_ResetToInitialPosition);
    }

    public void Follow(bool t_IsStayInCameraFocus, float t_DurationOfFollow, bool t_ResetToInitialPosition = true){

        m_IsStayInCameraFocus   = t_IsStayInCameraFocus;
        m_DurationOfFollow      = t_DurationOfFollow;

        if(m_IsStayInCameraFocus){

            if(cameraOrigin == null){

                m_DummyCameraOrigin = new GameObject();
                m_DummyCameraOrigin.transform.position = CameraMovementController.Instance.transform.position;
                cameraOrigin = m_DummyCameraOrigin.transform;
            }

            CameraMovementController.Instance.FollowCamera(
                cameraOrigin,
                new List<Transform>(){followerReference},
                true,
                targetOffSet
            );
            
        }

        if(m_DurationOfFollow > 0){

            m_EndTimeToUnfollow = Time.time + t_DurationOfFollow;
        }

        if(t_ResetToInitialPosition){

            ResetPosition();
        }

        enabled = true;
    }

    public void ResetPosition(){

        followerReference.position = m_InitialPosition;
    }

    public void Unfollow(){

        if(m_IsStayInCameraFocus){

            CameraMovementController.Instance.UnfollowCamera();
        }

        if(m_DummyCameraOrigin != null)
            Destroy(m_DummyCameraOrigin);

        enabled = false;
    }

    #endregion
}
