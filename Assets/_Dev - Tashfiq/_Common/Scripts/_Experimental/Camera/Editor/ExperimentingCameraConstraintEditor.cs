﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ExperimentingCameraConstraint))]
public class ExperimentingCameraConstraintEditor : Editor
{
    private ExperimentingCameraConstraint Reference;

    private void OnEnable(){

        Reference = (ExperimentingCameraConstraint) target;
    }

    public override void OnInspectorGUI(){

        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraConstraint"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraFocusOffset"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraOrigin"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("cameraFocus"));
        if(EditorGUI.EndChangeCheck() && EditorApplication.isPlaying){

            Reference.FollowCameraWithFocusConstraint();
        }
        

        serializedObject.ApplyModifiedProperties();
    }
}
