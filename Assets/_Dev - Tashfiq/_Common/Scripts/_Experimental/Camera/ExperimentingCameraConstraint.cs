﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;

public class ExperimentingCameraConstraint : MonoBehaviour
{
    public Constraint       cameraConstraint;
    public Vector3          cameraFocusOffset;
    public Transform        cameraOrigin;
    public List<Transform>  cameraFocus;

    private void Start(){

        FollowCameraWithFocusConstraint();
    }

    public void FollowCameraWithFocusConstraint(){

        Debug.Log("called");
        CameraMovementController.Instance.UnfollowCamera();
        CameraMovementController.Instance.FollowCameraWithFocusConstraint(
            cameraOrigin,
            cameraFocus,
            cameraConstraint,
            false,
            cameraFocusOffset
        );
    }
}
