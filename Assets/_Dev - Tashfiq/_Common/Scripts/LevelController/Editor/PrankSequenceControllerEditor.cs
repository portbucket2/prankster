﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor (typeof (PrankSequencerController))]
public class PrankSequenceControllerEditor : Editor {
    private PrankSequencerController Reference;
    private ReorderableList m_ReorderableListOfPrankSequence;
    private SerializedProperty SP_ListOfPrankSequence;

    private void OnEnable () {

        if (target == null)
            return;

        Reference = (PrankSequencerController) target;
        m_ReorderableListOfPrankSequence = new ReorderableList (
            serializedObject,
            serializedObject.FindProperty ("listOfPrankSequence"),
            true,
            true,
            true,
            true);

        m_ReorderableListOfPrankSequence.drawHeaderCallback = (Rect rect) => {

        };
        m_ReorderableListOfPrankSequence.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
            
            float t_HeightSeperator = 0;
            if(index > 0){
                t_HeightSeperator  = EditorGUIUtility.singleLineHeight;
                DrawHorizontalLineOnGUI(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight));
            }

            EditorGUI.LabelField(
                new Rect(rect.x, rect.y + t_HeightSeperator, rect.width, EditorGUIUtility.singleLineHeight),
                "Sequence(" + index + ")",
                EditorStyles.boldLabel);

            SerializedProperty t_PropertyIterator = m_ReorderableListOfPrankSequence.serializedProperty.GetArrayElementAtIndex(index);

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + EditorGUIUtility.singleLineHeight, rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("sequenceID")
                
            );

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 2), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("animationKey")
                
            );

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 3), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("durationForPrankSequence")
                
            );
            
            DrawHorizontalLineOnGUI(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 4) ,rect.width, EditorGUIUtility.singleLineHeight));

            
            EditorGUI.LabelField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 5), rect.width, EditorGUIUtility.singleLineHeight),
                "Camera",
                EditorStyles.boldLabel);


            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 6), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("cameraOrigin")
            );

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 7), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("cameraFocusOffset")
            );

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * 8), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("listOfCameraFocus"),
                true
            );

            float t_NumberOfCameraFocus = Reference.listOfPrankSequence[index].listOfCameraFocus.Count;
            EditorGUI.LabelField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (11 + t_NumberOfCameraFocus)), rect.width, EditorGUIUtility.singleLineHeight),
                "Object Interaction",
                EditorStyles.boldLabel);

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (12 + t_NumberOfCameraFocus)), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("listOfActivatedGameObject"),
                true
            );

            float t_NumberOfActivatedObject = Reference.listOfPrankSequence[index].listOfActivatedGameObject.Count;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (15 + t_NumberOfCameraFocus + t_NumberOfActivatedObject)), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("listOfMovementTransation"),
                true
            );

            float t_NumberOfMovemenetTransation = Reference.listOfPrankSequence[index].listOfMovementTransation.Count;

             EditorGUI.LabelField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (18 + t_NumberOfCameraFocus + t_NumberOfActivatedObject + t_NumberOfMovemenetTransation)), rect.width, EditorGUIUtility.singleLineHeight),
                "VFX",
                EditorStyles.boldLabel);

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (19 + t_NumberOfCameraFocus + t_NumberOfActivatedObject + t_NumberOfMovemenetTransation)), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("listOfParticles"),
                true
            );

            
            float t_NumberOfParticles = Reference.listOfPrankSequence[index].listOfParticles.Count;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y + t_HeightSeperator + (EditorGUIUtility.singleLineHeight * (22 + t_NumberOfCameraFocus + t_NumberOfActivatedObject + t_NumberOfMovemenetTransation + t_NumberOfParticles)), rect.width, EditorGUIUtility.singleLineHeight),
                t_PropertyIterator.FindPropertyRelative("listOfAnimationOnSequence"),
                true
            );
        
        };
        m_ReorderableListOfPrankSequence.elementHeightCallback = index => {

            float t_NumberOfCameraFocus = Reference.listOfPrankSequence[index].listOfCameraFocus.Count;
            float t_NumberOfActivatedObject = Reference.listOfPrankSequence[index].listOfActivatedGameObject.Count;
            float t_NumberOfMovemenetTransation = Reference.listOfPrankSequence[index].listOfMovementTransation.Count;
            float t_NumberOfScriptedParticle = Reference.listOfPrankSequence[index].listOfParticles.Count;
            float t_NumberOfAnimationSequence = Reference.listOfPrankSequence[index].listOfAnimationOnSequence.Count;

            return (27 + t_NumberOfCameraFocus + t_NumberOfActivatedObject+ t_NumberOfMovemenetTransation + t_NumberOfScriptedParticle + t_NumberOfAnimationSequence) * EditorGUIUtility.singleLineHeight;
        };
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        Reference.showDefaultInspector = EditorGUILayout.Toggle (
            "Show Default Inspector",
            Reference.showDefaultInspector
        );

        if (Reference.showDefaultInspector) {

            DrawDefaultInspector ();
        } else {

            DrawHorizontalLine();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("playerAnimatorReference"));
            DrawHorizontalLine();
            m_ReorderableListOfPrankSequence.DoLayoutList ();
        }

        serializedObject.ApplyModifiedProperties ();
    }

    #region Editor Module

    private void DrawHorizontalLine(){
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }

    private void DrawHorizontalLineOnGUI(Rect rect){
        EditorGUI.LabelField(rect,"", GUI.skin.horizontalSlider);
    }

    #endregion
}