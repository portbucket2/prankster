﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using com.faithstudio.Math;
using com.faithstudio.ScriptedParticle;
using UnityEngine;
using UnityEngine.Events;

public class PrankSequencerController : MonoBehaviour {
    #region Custom Variables

    [System.Serializable]
    public struct AnimationOnSequence {
        public string animationKey;
        public Animator animatorReference;
    }

    [System.Serializable]
    public class PrankSequqnce {

        
        public string sequenceID;
        public string animationKey;

        [Space(5.0f)]
        [Range (0.01f, 100)]
        public float durationForPrankSequence;
        [Range(0f,10f)]
        public float durationOnPostDelayAfterCallback;
        public bool isSequenceEndByCallback = false;
        

        [Space(5.0f)]
        public UnityEvent OnPrankStart;
        public UnityEvent OnPrankEnd;

        [Space(5.0f)]
        public Transform cameraOrigin;
        public Vector3 cameraFocusOffset;
        public List<Transform> listOfCameraFocus;

        [Space(5.0f)]
        public List<GameObject> listOfActivatedGameObject;
        public List<GameObject> listOfDeactivatedGameObject;
        public List<LerpWaypointController> listOfMovementTransation;
        public List<ParticleController> listOfParticles;
        public List<AnimationOnSequence> listOfAnimationOnSequence;

    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR

    [HideInInspector]
    public bool showDefaultInspector;

#endif

    public Animator playerAnimatorReference;
    public UnityEvent OnPrankReset;

    public List<PrankSequqnce> listOfPrankSequence;

    #endregion

    #region Private Variables

    private int m_CurrentPrankSequence;
    private bool m_IsPrankSequenceRunning;
    private bool m_IsWaitingForCallbackToEndSequence;

    private UnityAction OnPrankSequenceComplete;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    public int GetNumberOfPrankSubSequence () {

        return listOfPrankSequence.Count;
    }

    private bool IsPlayingAPrankSequenceAllowed () {

        if (m_IsPrankSequenceRunning)
            return false;

        return true;
    }

    private int IsValidPrankSequenceID (string t_SequenceID) {

        int t_NumberOfSequence = listOfPrankSequence.Count;
        for (int i = 0; i < t_NumberOfSequence; i++) {

            if (listOfPrankSequence[i].sequenceID == t_SequenceID)
                return i;
        }
        Debug.LogWarning ("Invalid Sequnece Id : " + t_SequenceID);
        return -1;
    }

    private IEnumerator ControllerForRunningPrankSubSequence (
        UnityAction<int> OnPrankSubSequenceComplete = null,
        bool t_StartCameraTransationWithSequence = true,
        bool t_DisableActivatedGameObjectOnEndSequence = false,
        bool t_StopParticleOnEndSequence = false) {
        
        listOfPrankSequence[m_CurrentPrankSequence].OnPrankStart?.Invoke();

        float t_CycleLength = 0.033f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds (t_CycleLength);

        if (playerAnimatorReference != null && listOfPrankSequence[m_CurrentPrankSequence].animationKey != null)
            playerAnimatorReference.SetTrigger (listOfPrankSequence[m_CurrentPrankSequence].animationKey);

        int t_NumberOfActivatedGameObject = listOfPrankSequence[m_CurrentPrankSequence].listOfActivatedGameObject.Count;
        for (int i = 0; i < t_NumberOfActivatedGameObject; i++) {

            //Debug.Log("(Enable) -> Sequence (" + listOfPrankSequence[m_CurrentPrankSequence].sequenceID + ") : " + listOfPrankSequence[m_CurrentPrankSequence].listOfActivatedGameObject[i].name);
            listOfPrankSequence[m_CurrentPrankSequence].listOfActivatedGameObject[i].SetActive (true);
        }

        int t_NumberOfDeactivatedGameObject = listOfPrankSequence[m_CurrentPrankSequence].listOfDeactivatedGameObject.Count;
        for (int i = 0; i < t_NumberOfDeactivatedGameObject; i++) {

             //Debug.Log("(Disable) -> Sequence (" + listOfPrankSequence[m_CurrentPrankSequence].sequenceID + ") : " + listOfPrankSequence[m_CurrentPrankSequence].listOfDeactivatedGameObject[i].name);
            listOfPrankSequence[m_CurrentPrankSequence].listOfDeactivatedGameObject[i].SetActive (false);
        }

        if (t_StartCameraTransationWithSequence) {
            
            CameraMovementController.Instance.FollowCamera (
                listOfPrankSequence[m_CurrentPrankSequence].cameraOrigin,
                listOfPrankSequence[m_CurrentPrankSequence].listOfCameraFocus,
                true,
                listOfPrankSequence[m_CurrentPrankSequence].cameraFocusOffset
            );
        } else {

            bool t_IsCameraTransationEnd = false;

            CameraMovementController.Instance.FocusCameraWithOrigin (
                listOfPrankSequence[m_CurrentPrankSequence].cameraOrigin,
                listOfPrankSequence[m_CurrentPrankSequence].listOfCameraFocus,
                true,
                listOfPrankSequence[m_CurrentPrankSequence].cameraFocusOffset,
                Vector3.zero,
                0,
                0,
                0,
                null,
                delegate {
                    t_IsCameraTransationEnd = true;
                }
            );

            //Debug.Log("Camera Transation");

            WaitUntil t_WaitForCameraTransationToBeEnd = new WaitUntil (() => {

                if (t_IsCameraTransationEnd)
                    return true;

                return false;
            });
            yield return t_WaitForCameraTransationToBeEnd;
        }

        int t_NumberOfParticles = listOfPrankSequence[m_CurrentPrankSequence].listOfParticles.Count;
        for (int i = 0; i < t_NumberOfParticles; i++) {

            listOfPrankSequence[m_CurrentPrankSequence].listOfParticles[i].PlayParticle ();
        }

        int t_NumberOfAnimationSequence = listOfPrankSequence[m_CurrentPrankSequence].listOfAnimationOnSequence.Count;
        for (int i = 0; i < t_NumberOfAnimationSequence; i++) {

            listOfPrankSequence[m_CurrentPrankSequence].listOfAnimationOnSequence[i].animatorReference.SetTrigger (
                listOfPrankSequence[m_CurrentPrankSequence].listOfAnimationOnSequence[i].animationKey
            );

        }

        float t_DurationOfPrankSequence = listOfPrankSequence[m_CurrentPrankSequence].durationForPrankSequence;
        float t_RemainingTimeForPrankSequence = t_DurationOfPrankSequence;

        List<LerpWaypointController> t_ListOfMovementTransation = listOfPrankSequence[m_CurrentPrankSequence].listOfMovementTransation;
        int t_NumberOfMovementTransation = t_ListOfMovementTransation.Count;

        if(listOfPrankSequence[m_CurrentPrankSequence].isSequenceEndByCallback) m_IsWaitingForCallbackToEndSequence = true;

        while (t_RemainingTimeForPrankSequence > 0) {
            
            float t_Progress = 1f - (Mathf.Clamp(0,Mathf.Infinity,t_RemainingTimeForPrankSequence) / t_DurationOfPrankSequence);

            for (int i = 0; i < t_NumberOfMovementTransation; i++) {
                t_ListOfMovementTransation[i].LerpThroughPositions (t_Progress);
            }

            yield return t_CycleDelay;
            t_RemainingTimeForPrankSequence -= t_CycleLength;
        }
        
        while((listOfPrankSequence[m_CurrentPrankSequence].isSequenceEndByCallback && m_IsWaitingForCallbackToEndSequence)){
            yield return t_CycleDelay;
        }

        yield return new WaitForSeconds(listOfPrankSequence[m_CurrentPrankSequence].durationOnPostDelayAfterCallback);

        for (int i = 0; i < t_NumberOfMovementTransation; i++) {
            t_ListOfMovementTransation[i].LerpThroughPositions (1f);
        }

        if (t_StopParticleOnEndSequence) {

            for (int i = 0; i < t_NumberOfParticles; i++) {

                listOfPrankSequence[m_CurrentPrankSequence].listOfParticles[i].StopParticle (false);
            }
        }

        if (t_DisableActivatedGameObjectOnEndSequence) {

            for (int i = 0; i < t_NumberOfActivatedGameObject; i++) {

                listOfPrankSequence[m_CurrentPrankSequence].listOfActivatedGameObject[i].SetActive (false);
            }
        }

        for (int i = 0; i < t_NumberOfDeactivatedGameObject; i++) {

            listOfPrankSequence[m_CurrentPrankSequence].listOfDeactivatedGameObject[i].SetActive (true);
        }

        if (t_StartCameraTransationWithSequence) {
            CameraMovementController.Instance.UnfollowCamera ();
        }

        OnPrankSubSequenceComplete?.Invoke (m_CurrentPrankSequence);

        listOfPrankSequence[m_CurrentPrankSequence].OnPrankEnd?.Invoke();
        m_CurrentPrankSequence++;
        if (m_CurrentPrankSequence >= listOfPrankSequence.Count)
            OnPrankSequenceComplete?.Invoke ();
        

        StopCoroutine (ControllerForRunningPrankSubSequence ());

        m_IsPrankSequenceRunning = false;
    }

    #endregion

    #region Public Callback

    public void PreProcess (UnityAction OnPrankSequenceComplete = null) {

        m_CurrentPrankSequence = 0;

        this.OnPrankSequenceComplete = OnPrankSequenceComplete;

    }

    public void PostProcess () {

        int t_NumberOfPrankSequence = listOfPrankSequence.Count;
        for (int i = 0; i < t_NumberOfPrankSequence; i++) {

            int t_NumberOfLerpMovementTransation = listOfPrankSequence[i].listOfMovementTransation.Count;
            for (int j = 0; j < t_NumberOfLerpMovementTransation; j++) {

                listOfPrankSequence[i].listOfMovementTransation[j].LerpThroughPositions (0);
            }

            int t_NumberoScriptedParticle = listOfPrankSequence[i].listOfParticles.Count;
            for (int j = 0; j < t_NumberoScriptedParticle; j++) {

                listOfPrankSequence[i].listOfParticles[j].StopParticle (false);
            }

            int t_NumberOfActivatedGameObject = listOfPrankSequence[i].listOfActivatedGameObject.Count;
            for (int j = 0; j < t_NumberOfActivatedGameObject; j++) {
                
                listOfPrankSequence[i].listOfActivatedGameObject[j].SetActive (false);
            }
        }
    }

    public void PlayPrankSequence (
        int t_SequnceIndex,
        UnityAction<int> OnPrankSubSequenceComplete = null,
        bool t_StartCameraTransationWithSequence = true,
        bool t_DisableActivatedGameObjectOnEndSequence = false,
        bool t_StopParticleOnEndSequence = false) {

        m_CurrentPrankSequence = t_SequnceIndex;
        PlayCurrentPrankSubSequence (
            OnPrankSubSequenceComplete,
            t_StartCameraTransationWithSequence,
            t_DisableActivatedGameObjectOnEndSequence,
            t_StopParticleOnEndSequence);
    }

    public void PlayPrankSequence (
        string t_SequenceID,
        UnityAction<int> OnPrankSubSequenceComplete = null,
        bool t_StartCameraTransationWithSequence = true,
        bool t_DisableActivatedGameObjectOnEndSequence = false,
        bool t_StopParticleOnEndSequence = false) {

        int t_SequenceIndex = IsValidPrankSequenceID (t_SequenceID);
        if (t_SequenceIndex != -1) {
            m_CurrentPrankSequence = t_SequenceIndex;
            PlayCurrentPrankSubSequence (
                OnPrankSubSequenceComplete,
                t_StartCameraTransationWithSequence,
                t_DisableActivatedGameObjectOnEndSequence,
                t_StopParticleOnEndSequence);
        }
    }

    public void PlayCurrentPrankSubSequence (
        UnityAction<int> OnPrankSubSequenceComplete = null,
        bool t_StartCameraTransationWithSequence = true,
        bool t_DisableActivatedGameObjectOnEndSequence = false,
        bool t_StopParticleOnEndSequence = false) {

        if (IsPlayingAPrankSequenceAllowed ()) {

            StartCoroutine (ControllerForRunningPrankSubSequence (
                OnPrankSubSequenceComplete,
                t_StartCameraTransationWithSequence,
                t_DisableActivatedGameObjectOnEndSequence,
                t_StopParticleOnEndSequence
            ));
        } else {

            Debug.LogError ("Cannot run the PrankSequence as one is currently running");
        }
    }

    public void GoToNextSequence(){

        m_IsWaitingForCallbackToEndSequence = false;
    }

    #endregion
}