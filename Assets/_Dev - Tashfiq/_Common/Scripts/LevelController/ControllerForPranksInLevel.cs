﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ControllerForPranksInLevel : PrankLevelBehaviour {
    
    #region Custom Variables

    [System.Serializable]
    public struct PrankSequencer{

        public bool reduceInitialDelay;
        [Range(0f,10f)]
        public float initalDelayOnPrankSequence;
        public UnityEvent OnBeforePrankSequenceEvent;
        public PrankSequencerController prankSequencerController;
    }

    #endregion

    #region Public Variables

    [Header ("Reference  :   External")]
    public UnityEvent OnPrankLevelLoaded;
    public List<PrankSequencer> listOfPrankSequence;

    #endregion

    #region Private Variables

    private string SQ_InitialFocus          = "InitialFocus";
    private string SQ_DoSuccessPrankAnimation               = "DoSuccessPrankAnimation";
    private string SQ_DoFailedPrankAnimation               = "DoFailedPrankAnimation";
    private string SQ_FocusOnRightAnswer    = "FocusOnRightAnswer";
    private string SQ_FocusOnWrongAnswer    = "FocusOnWrongAnswer";


    private bool m_IsQuestionIsAnswered;
    private bool m_HasGivenRightAnswer;
    private bool m_IsWaitingForUserRespondAfterFail;
    private int m_CurrentPrankIndex;
    private float m_LevelStartingTime;
    private UnityAction OnGivingRightAnswer;
    private UnityAction OnGivingWrongAnswer;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        m_LevelStartingTime = Time.time;
        OnPrankLevelLoaded.Invoke();
    }


    #endregion

    #region Configuretion

    private IEnumerator ControllerForPrank () {

        yield return new WaitForSeconds(0.1f);

        m_IsQuestionIsAnswered = false;
        WaitUntil t_WaitUntilQuestionAnswered = new WaitUntil (() => {
            if (m_IsQuestionIsAnswered)
                return true;

            return false;
        });

        bool t_IsPrankSequenceEnd = false;
        WaitUntil t_WaitUntilPrankSequenceEnd = new WaitUntil (() => {

            if (t_IsPrankSequenceEnd)
                return true;

            return false;
        });

        int t_NumberOfPrankSequence = listOfPrankSequence.Count;
        for (int i = 0; i < t_NumberOfPrankSequence; i++) {

            m_CurrentPrankIndex = i;
            listOfPrankSequence[i].OnBeforePrankSequenceEvent.Invoke();
            
            float t_AbsoluteDelay = listOfPrankSequence[i].reduceInitialDelay ? Mathf.Clamp(listOfPrankSequence[i].initalDelayOnPrankSequence - Time.time + m_LevelStartingTime, 0, Mathf.Infinity) : listOfPrankSequence[i].initalDelayOnPrankSequence;
            yield return new WaitForSeconds(t_AbsoluteDelay);

            //Sate  :   Do Initial Focus and give choice
            listOfPrankSequence[i].prankSequencerController.PlayPrankSequence (
                SQ_InitialFocus,
                delegate {

                    t_IsPrankSequenceEnd = false;
                    UIManager.Instance.RequestQuestionPanel (stepIndex: i);

                    OnGivingRightAnswer = delegate {

                        m_HasGivenRightAnswer = true;
                        m_IsQuestionIsAnswered = true;
                    };

                    OnGivingWrongAnswer = delegate {

                        m_HasGivenRightAnswer = false;
                        m_IsQuestionIsAnswered = true;
                    };
                },
                false);

            m_IsQuestionIsAnswered = false;
            yield return t_WaitUntilQuestionAnswered;

            //State :   Choose Prank
            t_IsPrankSequenceEnd = false;
            if (m_HasGivenRightAnswer) {

                listOfPrankSequence[i].prankSequencerController.PlayPrankSequence (
                    SQ_FocusOnRightAnswer,
                    delegate {
                        t_IsPrankSequenceEnd = true;
                    },
                    false
                );
            } else {

                listOfPrankSequence[i].prankSequencerController.PlayPrankSequence (
                    SQ_FocusOnWrongAnswer,
                    delegate {
                        t_IsPrankSequenceEnd = true;
                    },
                    false
                );
            }
            yield return t_WaitUntilPrankSequenceEnd;
            listOfPrankSequence[i].prankSequencerController.PostProcess ();

            //State :   ShowPrankAnimation
            t_IsPrankSequenceEnd = false;
            listOfPrankSequence[i].prankSequencerController.PlayPrankSequence(
                m_HasGivenRightAnswer ? SQ_DoSuccessPrankAnimation : SQ_DoFailedPrankAnimation,
                delegate{
                    t_IsPrankSequenceEnd = true;
                },
                false
            );
            yield return t_WaitUntilPrankSequenceEnd;

            ReportPhaseCompletion(i + 1, i == (t_NumberOfPrankSequence - 1) ? true : false);

            if (!m_HasGivenRightAnswer) {
                
                m_IsWaitingForUserRespondAfterFail = true;
                WaitUntil t_WaitUntilUserRespondOnFail = new WaitUntil (() => {
                    if (m_IsWaitingForUserRespondAfterFail)
                        return false;

                    return true;
                });
                yield return t_WaitUntilUserRespondOnFail;
                listOfPrankSequence[i].prankSequencerController.OnPrankReset.Invoke();
                i--;
            }
        }
    }

    private void PreProcess () {
        
        StartCoroutine (ControllerForPrank ());
    }

    #endregion

    #region Public Callback

    public void GoToNextSequence(){
        listOfPrankSequence[m_CurrentPrankIndex].prankSequencerController.GoToNextSequence();
    }

    #endregion

    #region Implemented Interface

    protected override void StartLevel () {
        //throw new System.NotImplementedException ();
        PreProcess ();
    }

    protected override void RightAnswerSelected () {
        //throw new System.NotImplementedException ();
        //Debug.Log("###RightAnswer");
        OnGivingRightAnswer?.Invoke ();
    }

    protected override void WrongAnswerSelected () {
        //throw new System.NotImplementedException ();
        //Debug.Log("###WrongAnswer");
        OnGivingWrongAnswer?.Invoke ();
    }

    // public override void ResetPhase () {
    //     throw new System.NotImplementedException ();
    // }

    //protected override void PhaseStart (int phaseIndex) {
    //    //throw new System.NotImplementedException ();
    //    Debug.Log("###Phase Start");
    //}

    //protected override void PhaseEnd (int phaseIndex) {
    //    //throw new System.NotImplementedException ();
    //    Debug.Log("###Phase End");
    //}

    protected override void ResetPhase (int phaseIndex) {
        //throw new System.NotImplementedException ();
        if(phaseIndex >= 0){

            //Debug.Log("###Phase Reset");
            m_IsWaitingForUserRespondAfterFail = false;
        }
        
    }

    #endregion
}