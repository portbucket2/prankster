﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnvironmentLightController : MonoBehaviour {

    #region Public Variables

    #if UNITY_EDITOR

    [Header("Editor Only")]
    public bool showDefaultEditor;
    public Vector3 lightHolderPosition;
    #endif

    public List<Material> listOfManuallyAddedSharedMaterial;

    public Transform lightHolder;
    public Color litColor;
    public Color lightColor;
    [Range(1f,1000f)]
    public float lightRange;
    [Range(0f,5f)]
    public float lightIntensity;

    [Space(5.0f)]
    public AnimationCurve transationCurveOfLightIntensity;
    [Range(0.1f,5f)]
    public float durationOfTransationOfLightIntensity;
    public bool isTransationOfLightIntensityLoop;

    #endregion  

    #region Private Variables

    private const string LIT_COLOR = "_LitColor";
    private const string LIGHT_WORLD_POSITION = "LIGHT_WORLD_POSITION";
    private const string LIGHT_COLOR = "_LightColor";
    private const string LIGHT_RANGE = "_LightRange";
    private const string LIGHT_INTENSITY = "_LightIntensity";
    
    public List<Renderer> rendererReference;

    private Transform m_CurrentLightHolder;

    private bool m_IsLightIntensityTransationControllerRunning;
    private AnimationCurve m_IntensityCurveDuringTransation = null; 
    private bool m_IsLightIntensityTransationLoop = false;
    private float m_RemainingTimeForTransationOfLightIntensity = 0;
    private float m_DurationForTransationOfLightIntensity = 0; 
    private float m_LightIntensityDuringTransation = 0; 
    
    
    #endregion

    #region Mono Behaviour

    private void Awake () {

#if UNITY_EDITOR
        if (Application.isPlaying) {

        } else {

        }
#else

#endif

        enabled = false;
    }

    private void Update(){

        ChangeLightPosition(m_CurrentLightHolder.position);
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForTransationOfLightIntensity(){

        bool t_ForwardDirection = true;
        float t_Progress;
        float t_CycleLength = 0.033f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);
        
        while(m_IsLightIntensityTransationControllerRunning){

            t_Progress = m_RemainingTimeForTransationOfLightIntensity / m_DurationForTransationOfLightIntensity;
            ChangeLightIntensity(m_LightIntensityDuringTransation * m_IntensityCurveDuringTransation.Evaluate(t_ForwardDirection ? 1f - t_Progress : t_Progress));

            yield return t_CycleDelay;
            m_RemainingTimeForTransationOfLightIntensity -= t_CycleLength;

            if(m_RemainingTimeForTransationOfLightIntensity <= 0){
                
                if(m_IsLightIntensityTransationLoop){
                    m_RemainingTimeForTransationOfLightIntensity = m_DurationForTransationOfLightIntensity;
                    t_ForwardDirection = !t_ForwardDirection;
                }else{
                    break;
                }
            }
        }

        StopCoroutine(ControllerForTransationOfLightIntensity());
        m_IsLightIntensityTransationControllerRunning = false;
    }

    #endregion

    #region Public Callback

#if UNITY_EDITOR
    public void FetchRenderer () {

        rendererReference = gameObject.GetComponentsInChildren<Renderer> ().ToList();
        
    }
#endif

    public void ChangeLightPosition(Vector3 t_LightPosition){

        foreach (Renderer t_Renderer in rendererReference) {

#if UNITY_EDITOR
            if (Application.isPlaying) {

                t_Renderer.material.SetVector(LIGHT_WORLD_POSITION, t_LightPosition);
            } else {
                t_Renderer.sharedMaterial.SetVector(LIGHT_WORLD_POSITION, t_LightPosition);
            }
#else
    t_Renderer.material.SetVector(LIGHT_WORLD_POSITION, t_LightPosition);
#endif
        }

        foreach(Material t_SharedMaterial in listOfManuallyAddedSharedMaterial){

            t_SharedMaterial.SetVector(LIGHT_WORLD_POSITION, t_LightPosition);
        }
    }

    public void ChangeRoomColor (Color t_NewRoomColor) {

        foreach (Renderer t_Renderer in rendererReference) {

#if UNITY_EDITOR
            if (Application.isPlaying) {

                t_Renderer.material.SetColor(LIT_COLOR, t_NewRoomColor);
            } else {
                t_Renderer.sharedMaterial.SetColor(LIT_COLOR, t_NewRoomColor);
            }
#else
    t_Renderer.material.SetColor(LIT_COLOR, t_NewRoomColor);
#endif
        }

        foreach(Material t_SharedMaterial in listOfManuallyAddedSharedMaterial){

            t_SharedMaterial.SetColor(LIT_COLOR, t_NewRoomColor);
        }
    }

    public void ChangeLightColor (Color t_NewRoomColor) {

        foreach (Renderer t_Renderer in rendererReference) {

#if UNITY_EDITOR
            if (Application.isPlaying) {

                t_Renderer.material.SetColor(LIGHT_COLOR, t_NewRoomColor);
            } else {
                t_Renderer.sharedMaterial.SetColor(LIGHT_COLOR, t_NewRoomColor);
            }
#else
    t_Renderer.material.SetColor(LIGHT_COLOR, t_NewRoomColor);
#endif
        }

        foreach(Material t_SharedMaterial in listOfManuallyAddedSharedMaterial){

            t_SharedMaterial.SetColor(LIGHT_COLOR, t_NewRoomColor);
        }
    }

    public void ChangeLightRange (float t_LightRange) {

        foreach (Renderer t_Renderer in rendererReference) {

#if UNITY_EDITOR
            if (Application.isPlaying) {

                t_Renderer.material.SetFloat(LIGHT_RANGE, t_LightRange);
            } else {
                t_Renderer.sharedMaterial.SetFloat(LIGHT_RANGE, t_LightRange);
            }
#else
    t_Renderer.material.SetFloat(LIGHT_RANGE, t_LightRange);
#endif
        }

        foreach(Material t_SharedMaterial in listOfManuallyAddedSharedMaterial){

            t_SharedMaterial.SetFloat(LIGHT_RANGE, t_LightRange);
        }
    }

    public void ChangeLightIntensity (float t_LightIntensity) {

        foreach (Renderer t_Renderer in rendererReference) {

#if UNITY_EDITOR
            if (Application.isPlaying) {

                t_Renderer.material.SetFloat(LIGHT_INTENSITY, t_LightIntensity);
            } else {
                t_Renderer.sharedMaterial.SetFloat(LIGHT_INTENSITY, t_LightIntensity);
            }
#else
    t_Renderer.material.SetFloat(LIGHT_INTENSITY, t_LightIntensity);
#endif
        }

        foreach(Material t_SharedMaterial in listOfManuallyAddedSharedMaterial){

            t_SharedMaterial.SetFloat(LIGHT_INTENSITY, t_LightIntensity);
        }
    }     

    public bool IsTransationOfLightIntensityPlaying(){
        return m_IsLightIntensityTransationControllerRunning;
    }

    public void ShowTransationForLightIntensity(float t_Duration = 0, bool t_IsLoop = false, float t_Intensity = 0, AnimationCurve t_IntensityCurve = null){

        if(t_Intensity == 0)
            m_LightIntensityDuringTransation = lightIntensity;
        else
            m_LightIntensityDuringTransation = t_Intensity;

        if(t_IntensityCurve == null)
            m_IntensityCurveDuringTransation = transationCurveOfLightIntensity;
        else
            m_IntensityCurveDuringTransation = t_IntensityCurve;

        if(t_IsLoop == false)
            m_IsLightIntensityTransationLoop = isTransationOfLightIntensityLoop;
        else
            m_IsLightIntensityTransationLoop = t_IsLoop;

        if(t_Duration == 0)
            m_DurationForTransationOfLightIntensity = durationOfTransationOfLightIntensity;
        else
            m_DurationForTransationOfLightIntensity = t_Duration;
        
        m_RemainingTimeForTransationOfLightIntensity = m_DurationForTransationOfLightIntensity;

        if(!m_IsLightIntensityTransationControllerRunning){

            m_IsLightIntensityTransationControllerRunning = true;
            StartCoroutine(ControllerForTransationOfLightIntensity());
        }    
    }

    public void StopTransationOfLightIntensity(bool t_HardReset = false){

        m_IsLightIntensityTransationLoop = false;
        m_RemainingTimeForTransationOfLightIntensity = 0f;

        if(t_HardReset)
            ChangeLightIntensity(m_LightIntensityDuringTransation * m_IntensityCurveDuringTransation.Evaluate(0));
    }

    public void EnableDynamicLighting(Transform t_LightHolder = null){

        if(t_LightHolder == null){

            if(lightHolder != null){

                m_CurrentLightHolder = lightHolder;
                enabled = true;
            }else{

                Debug.LogError("No Default LightHolder assigned from the editor");
            }
        }else{
            m_CurrentLightHolder = t_LightHolder;
        }
    }

    public void DisableDynamicLighting(){

        enabled = false;
    }

    #endregion

}