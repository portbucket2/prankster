﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (EnvironmentLightController))]
public class EnvironmentLightControllerEditor : Editor {
    private EnvironmentLightController Reference;

    private SerializedProperty SP_ListOfManuallyAddedSharedMaterial;
    private SerializedProperty SP_LitColor;
    private SerializedProperty SP_LightHolder;
    private SerializedProperty SP_LightHolderPosition;
    private SerializedProperty SP_LightColor;
    private SerializedProperty SP_LightRange;
    private SerializedProperty SP_LightIntensity;
    private SerializedProperty SP_TransationCruveForLightIntensity;
    private SerializedProperty SP_DurationOfTransationOfLightIntensity;
    private SerializedProperty SP_IsTransationOfLightIntensityLooped;

    private void OnEnable () {

        Reference = (EnvironmentLightController) target;

        SP_ListOfManuallyAddedSharedMaterial = serializedObject.FindProperty("listOfManuallyAddedSharedMaterial");

        SP_LitColor = serializedObject.FindProperty ("litColor");

        SP_LightHolder = serializedObject.FindProperty ("lightHolder");
        SP_LightHolderPosition = serializedObject.FindProperty ("lightHolderPosition");
        SP_LightColor = serializedObject.FindProperty ("lightColor");
        SP_LightRange = serializedObject.FindProperty ("lightRange");
        SP_LightIntensity = serializedObject.FindProperty ("lightIntensity");

        SP_TransationCruveForLightIntensity = serializedObject.FindProperty ("transationCurveOfLightIntensity");
        SP_DurationOfTransationOfLightIntensity = serializedObject.FindProperty ("durationOfTransationOfLightIntensity");
        SP_IsTransationOfLightIntensityLooped = serializedObject.FindProperty ("isTransationOfLightIntensityLoop");

        if (Reference != null) {

            Reference.FetchRenderer ();
        }
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        if (Reference.showDefaultEditor) {

            DrawDefaultInspector ();
        } else {

            Reference.showDefaultEditor = EditorGUILayout.Toggle (
                "ShowDefaultInspector",
                Reference.showDefaultEditor
            );

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(SP_ListOfManuallyAddedSharedMaterial);

            EditorGUILayout.Space ();
            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.PropertyField (SP_LitColor);
            if (EditorGUI.EndChangeCheck ()) {

                Color t_ModifiedColor = SP_LitColor.colorValue;
                Reference.ChangeRoomColor (t_ModifiedColor);
            }

            EditorGUILayout.Space ();
            EditorGUILayout.PropertyField (SP_LightHolder);
            if (Reference.lightHolder == null) {

                EditorGUI.BeginChangeCheck ();
                EditorGUILayout.PropertyField (SP_LightHolderPosition);
                if (EditorGUI.EndChangeCheck ()) {

                    Vector3 t_ModifiedVector = SP_LightHolderPosition.vector3Value;
                    Reference.ChangeLightPosition (t_ModifiedVector);
                }
            } else {

                if (Reference.lightHolder != null) {

                    if (Reference.lightHolderPosition != Reference.lightHolder.position) {

                        Reference.lightHolderPosition = Reference.lightHolder.position;
                        Reference.ChangeLightPosition (Reference.lightHolderPosition);
                    }
                }

            }

            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.PropertyField (SP_LightColor);
            if (EditorGUI.EndChangeCheck ()) {

                Color t_ModifiedColor = SP_LightColor.colorValue;
                Reference.ChangeLightColor (t_ModifiedColor);
            }

            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.PropertyField (SP_LightRange);
            if (EditorGUI.EndChangeCheck ()) {

                float t_ModifiedRange = SP_LightRange.floatValue;
                Reference.ChangeLightRange (t_ModifiedRange);
            }

            EditorGUI.BeginChangeCheck ();
            EditorGUILayout.PropertyField (SP_LightIntensity);
            if (EditorGUI.EndChangeCheck ()) {

                float t_ModifiedIntensity = SP_LightIntensity.floatValue;
                Reference.ChangeLightIntensity (t_ModifiedIntensity);
            }

            EditorGUILayout.Space ();
            EditorGUILayout.LabelField ("Configuretion   :   Transation");
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField (SP_IsTransationOfLightIntensityLooped);
                if(EditorApplication.isPlaying){

                    if(Reference.enabled){

                        if(GUILayout.Button("Disable DynamicLighting")){

                            Reference.DisableDynamicLighting();
                        }
                    }else{

                        if(GUILayout.Button("Enable DynamicLighting")){

                            Reference.EnableDynamicLighting();
                        }
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.PropertyField (SP_DurationOfTransationOfLightIntensity);
            EditorGUILayout.PropertyField (SP_TransationCruveForLightIntensity);

            if (EditorApplication.isPlaying) {

                if (Reference.IsTransationOfLightIntensityPlaying ()) {

                    if (GUILayout.Button ("Stop Transation")) {

                        Reference.StopTransationOfLightIntensity();
                    }
                } else {

                    if (GUILayout.Button ("Play Transation")) {

                        Reference.ShowTransationForLightIntensity();
                    }
                }

            }
        }

        serializedObject.ApplyModifiedProperties ();
    }
}