﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;

public class Level_32A : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();
    public SkinMatController skinMatController;
    public CameraShake camShake;
    public Material dirtyBody;
    public Material originalBody;
    public Transform characterMeshTransform;
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITNO = new WaitForSeconds(0f);
    WaitForEndOfFrame WAITEND = new WaitForEndOfFrame();

    public List<string> animationKeyList;


    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }

    IEnumerator ChangeMaterial(Material mat, WaitForSeconds WAIT)
    {
        yield return WAIT;
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in characterMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            skinnedMeshRenderer.sharedMaterial = mat;
        }
    }

    public void PlayAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
            // propsAnimatorReference[0].SetTrigger(animationHashedDictionary[t_Key]);
        }
    }



    public void PlayPropAnimation(string p_Key, int num)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[num].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }




    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return WAITEND;




        //CameraMovementController.Instance.FollowCamera(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList);


        /*CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
           });*/
        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {

               UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
           });


    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
            case 2:
                StartCoroutine(RightPhaseThreeRoutine());
                break;
            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return WAITEND;
        PlayPropAnimation(animationKeyList[1], 2);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {

               PlayAnimation(animationKeyList[1]);
               PlayPropAnimation(animationKeyList[1], 0);
           });
        yield return new WaitForSeconds(4.5f);

        PlayPropAnimation(animationKeyList[7], 2);

        yield return new WaitForSeconds(2.5f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        prankSequenceList[0].propsList[1].propsParticle[1].Play();
        skinMatController.SetMat(1);
        //yield return new WaitForSeconds(.5f);
        currentPhaseIndex++;
        //yield return new WaitForSeconds(2.5f); //6.5f

        yield return new WaitForSeconds(.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
             delegate
             {


             });
        propsOnHand[0].SetActive(false);
        yield return new WaitForSeconds(4.5f);
        ReportPhaseCompletion(1,  false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
             });

    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        yield return WAITEND;
        PlayPropAnimation(animationKeyList[8], 3);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {

             });
        yield return new WaitForSeconds(2f);
        PlayAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(3f);
        PlayAnimation(animationKeyList[4]);
        PlayPropAnimation(animationKeyList[4], 1);
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[0].propsParticle[1].Play();
        yield return new WaitForSeconds(.5f);
        propsOnHand[1].SetActive(false);
        propsOnHand[2].SetActive(false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
             delegate
             {

             });
        //yield return new WaitForSeconds(0.5f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(4f);
        currentPhaseIndex++;
        ReportPhaseCompletion(2,  false);
        PlayAnimation(animationKeyList[12]);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[2].camPosList[1].cameraOriginTrans, prankSequenceList[2].camPosList[1].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 2);
             });
        PlayAnimation(animationKeyList[10]);

        //UIManager.Instance.ShowLevelComplete();

    }

    IEnumerator RightPhaseThreeRoutine()
    {
        yield return WAITEND;
        PlayPropAnimation(animationKeyList[14], 5);
        yield return WAITHALF;
        prankSequenceList[2].propsList[0].propsParticle[0].Play();
        prankSequenceList[2].propsList[0].propsParticle[1].Play();
        PlayPropAnimation(animationKeyList[15], 6);
        yield return new WaitForSeconds(1.5f);

        prankSequenceList[2].propsList[0].propsParticle[0].Stop();
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FollowCamera(prankSequenceList[2].camPosList[0].cameraOriginTrans, prankSequenceList[2].camPosList[0].cameraTransList);
        PlayAnimation(animationKeyList[11]);
        yield return new WaitForSeconds(1f);
        camShake.ShowCameraShake(.5f, 2f, false);
        prankSequenceList[2].propsList[0].propsParticle[2].Play();
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(3,  true);
        //Debug.Log("Phase " + currentPhaseIndex);


    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;
            case 2:
                StartCoroutine(WrongPhaseThreeRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return WAITEND;
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(ActivateThumstacks(true));

        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {

               PlayAnimation(animationKeyList[2]);
           });

        yield return new WaitForSeconds(8f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return WAITEND;

        PlayPropAnimation(animationKeyList[5], 4);
        yield return new WaitForSeconds(.5f);
        while (prankSequenceList[1].propsList[1].propObject.GetComponent<GlueVisual>().manualglue(Time.deltaTime * 2f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(2f);
        PlayAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(3f);
        PlayAnimation(animationKeyList[5]);

        yield return new WaitForSeconds(6f);


        ReportPhaseCompletion(2,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseThreeRoutine()
    {
        yield return WAITEND;
        PlayPropAnimation(animationKeyList[16], 7);
        yield return WAITHALF;
        prankSequenceList[2].propsList[1].propsParticle[0].Play();

        yield return new WaitForSeconds(2.5f);
        PlayAnimation(animationKeyList[13]);
        CameraMovementController.Instance.FollowCamera(prankSequenceList[2].camPosList[1].cameraOriginTrans, prankSequenceList[2].camPosList[0].cameraTransList);
        yield return new WaitForSeconds(1.3f);
        CameraMovementController.Instance.FollowCamera(prankSequenceList[2].camPosList[2].cameraOriginTrans, prankSequenceList[2].camPosList[2].cameraTransList);
        yield return new WaitForSeconds(3f);
        ReportPhaseCompletion(3,  true);




    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;
            case 2:
                StartCoroutine(ResetPhaseThreeRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return WAITEND;




        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return WAITEND;

            prankSequenceList[1].propsList[1].propObject.GetComponent<GlueVisual>().ResetGlue();
            PlayAnimation(animationKeyList[9]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
             });
            propsOnHand[1].SetActive(true);
            propsOnHand[2].SetActive(true);



            //after video

        }
    }

    IEnumerator ResetPhaseThreeRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            yield return WAITEND;
            prankSequenceList[2].propsList[1].propsParticle[0].Stop();
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[2].camPosList[1].cameraOriginTrans, prankSequenceList[2].camPosList[1].cameraTransList,
             delegate
             {
                 
             });
            PlayAnimation(animationKeyList[10]);
        }
        else
        {
            UIManager.Instance.RequestQuestionPanel(stepIndex: 2);
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    private IEnumerator ActivateThumstacks(bool t_IsActive)
    {
        for (int i = 0; i < prankSequenceList[0].propsList[0].propObject.transform.childCount; i++)
        {
            prankSequenceList[0].propsList[0].propObject.transform.GetChild(i).gameObject.SetActive(t_IsActive);
            yield return new WaitForEndOfFrame();
        }
    }
}
