﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;

public class Level_17_GameManager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public Animator propsAnimatorReference;
    public Animator powderCanAnimatorReference;
    public Animator ThermoAnimatorReference;
    public SkinMatController skinController;
    public Transform characterMeshTransform;
    public Material wetBody;
    public Material originalBody;

    //public GameObject makeItVanish;

    public GameObject fakeRemote;
    public List<string> animationKeyList;
    
    
    public Dictionary<string, int> animationHashedDictionary;
    
    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    public void PlayPropsAnimation(string p_Key)
    {
        if(animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference.SetTrigger(animationHashedDictionary[p_Key]);
        }
    }
    public void PlayThermoAnimation(string m_Key)
    {
        if(animationHashedDictionary.ContainsKey(m_Key))
        {
            ThermoAnimatorReference.SetTrigger(animationHashedDictionary[m_Key]);
        }
    }

    public void PlayPowderCanAnimation(string c_Key)
    {
        if(animationHashedDictionary.ContainsKey(c_Key))
        {
            powderCanAnimatorReference.SetTrigger(animationHashedDictionary[c_Key]);
        }
    }

    IEnumerator ChangeMaterial()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in characterMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            skinnedMeshRenderer.sharedMaterial = wetBody;
        }
    }

    protected override void StartLevel()
    {
        
        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {

        PlayCharacterAnimation(animationKeyList[0]);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            });

    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[4].finalPos, prankSequenceList[0].propsList[4].propObject, delegate {
        }));
        //yield return new WaitForSeconds(.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos, prankSequenceList[0].propsList[2].propObject, delegate { 
        }));

        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {

                    PlayCharacterAnimation(animationKeyList[1]);
                    propsOnHand[0].SetActive(true);
                    PlayPropsAnimation(animationKeyList[6]);
                });
        yield return new WaitForSeconds(3f);
        prankSequenceList[0].propsList[2].propObject.SetActive(false);
        yield return new WaitForSeconds(2.7f);
        
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        ReportPhaseCompletion(1,  false);
        yield return new WaitForSeconds(2f);
        
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[3]);
                UIManager.Instance.RequestQuestionPanel(1);
            });

    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        propsOnHand[0].SetActive(false);
        propsOnHand[1].SetActive(false);

        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList);
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].finalPos, prankSequenceList[1].propsList[2].propObject, delegate {
            
            
        }));
        yield return new WaitForSeconds(.5f);
        NumberUp.Instance.AddToNumber(50);
        PlayThermoAnimation(animationKeyList[10]);
        yield return new WaitForSeconds(3f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].initialPos, prankSequenceList[1].propsList[2].propObject, delegate{}));
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans, prankSequenceList[1].camPosList[2].cameraTransList);
        PlayCharacterAnimation(animationKeyList[4]);
        propsOnHand[2].SetActive(true);
        PlayPropsAnimation(animationKeyList[8]);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[3].cameraOriginTrans, prankSequenceList[1].camPosList[3].cameraTransList);
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[2].propsParticle[0].gameObject.SetActive(true);
        prankSequenceList[1].propsList[2].propsParticle[0].Play();
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[4].cameraOriginTrans, prankSequenceList[1].camPosList[4].cameraTransList);
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[6].cameraOriginTrans, prankSequenceList[1].camPosList[6].cameraTransList);
        
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {

        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[4].finalPos, prankSequenceList[0].propsList[4].propObject, delegate {
        }));
        //yield return new WaitForSeconds(.5f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[3].finalPos, prankSequenceList[0].propsList[3].propObject, delegate {
        }));

        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
                delegate
                {
                    PlayCharacterAnimation(animationKeyList[2]);
                    propsOnHand[1].SetActive(true);
                    PlayPropsAnimation(animationKeyList[7]);
                });

        yield return new WaitForSeconds(3f);
        prankSequenceList[0].propsList[3].propObject.SetActive(false);
        yield return new WaitForSeconds(2.4f);
        //prankSequenceList[0].propsList[1].propObject.SetActive(false);
        //propsOnHand[1].SetActive(true);
        //yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(.5f);
        skinController.SetMat(1);
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        propsOnHand[0].SetActive(false);
        propsOnHand[1].SetActive(false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList);
        yield return new WaitForSeconds(2f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[3].finalPos, prankSequenceList[1].propsList[3].propObject, delegate {
            prankSequenceList[1].propsList[3].propsParticle[0].gameObject.SetActive(true);
            PlayPowderCanAnimation(animationKeyList[11]);
        }));
        //PlayPowderCanAnimation(animationKeyList[11]);
        /*yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos, prankSequenceList[1].propsList[1].propObject, delegate {
            propsOnHand[0].SetActive(false);
        }));
        */
        yield return new WaitForSeconds(4f);
        PlayPowderCanAnimation(animationKeyList[12]);
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[3].initialPos, prankSequenceList[1].propsList[3].propObject, delegate {}));
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[5].cameraOriginTrans, prankSequenceList[1].camPosList[5].cameraTransList,
                delegate
                {
                    
                });
        PlayCharacterAnimation(animationKeyList[5]);
        propsOnHand[3].SetActive(true);
        PlayPropsAnimation(animationKeyList[9]);

        yield return new WaitForSeconds(3f);
        prankSequenceList[1].propsList[3].propsParticle[1].gameObject.SetActive(true);
        prankSequenceList[1].propsList[3].propsParticle[1].Play();
        //prankSequenceList[1].propsList[1].propObject.SetActive(false);
        //propsOnHand[3].SetActive(true);
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {
       
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList, delegate
            {

            });
            skinController.SetMat(0);
            PlayCharacterAnimation(animationKeyList[0]);
            propsOnHand[1].SetActive(false);
            prankSequenceList[0].propsList[3].propObject.SetActive(true);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[3].initialPos, prankSequenceList[0].propsList[3].propObject, delegate {
            }));
            yield return new WaitForSeconds(0f);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[4].initialPos, prankSequenceList[0].propsList[4].propObject, delegate {
            }));
            
            //StartCoroutine(StartLevelRoutine());






        }
        else
        {
            yield return new WaitForSeconds(2f);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList, delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
            });
            //after video
            

        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            //reset things
            yield return new WaitForEndOfFrame();
            prankSequenceList[1].propsList[3].propsParticle[1].Stop();
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList);
            PlayCharacterAnimation(animationKeyList[3]);
        }
        else
        {
            //after video
            UIManager.Instance.RequestQuestionPanel(1);
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    
}
