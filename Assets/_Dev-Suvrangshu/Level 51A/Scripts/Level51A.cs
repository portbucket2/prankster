﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;
using UnityEngine.Events;

public class Level51A : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimatorIdle;
    public Animator propAnimator;
    public Animator propNewAnimator;
    public Animator pinAnimator;
    public Animator bowlingAnimator;

    public Animator bowlingBall;


    public bool isPhaseOne = true;
    public bool isPhaseTwo = false;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform phase1Success;
    public Transform phase1SuccessFocus;
    public Transform phase1Fail;
    public Transform phase1FailFocus;
    public Transform phase3StartPos;
    public Transform phase3StartFocus;
    public Transform zoom3Pos;
    public Transform zoom3Focus;
    public Transform phase3FailPos;
    public Transform phase3FailFocus;
    public Transform zoom4Pos;
    public Transform zoom4Focus;
    public Transform cameraEndPosBonus;


    [Header("particles")]
    public ParticleSystem fallFlatParticle;
    public ParticleSystem fallFlatHugeParticle;
    public ParticleSystem ballKickParticle;
    public ParticleSystem explosionParticle;
    public ParticleSystem slipOnParticle;
    public ParticleSystem pumpLeakParticle;
    public ParticleSystem pinParticle;
    public ParticleSystem fallPhase3Particle;
    public ParticleSystem bowlHitLegParticle;
    public ParticleSystem portalOneParticle;
    public ParticleSystem portalTwoParticle;
    public ParticleSystem bowlHitHeadParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator propsEntry;

    public List<GameObject> propsOnHand = new List<GameObject>();

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string Default = "default";
    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Transition = "transition";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Spring = "spring";
    readonly string Stone = "stone";
    readonly string BallEntry = "ballEntry";
    readonly string BallExit = "ballExit";
    readonly string Bomb = "bomb";
    readonly string Explode = "explode";
    readonly string IDLE3 = "idle3";
    readonly string Success3 = "success3";
    readonly string Fail3 = "fail3";
    readonly string LEAK = "pin";
    readonly string HIDE = "hide";
    readonly string TRANS = "trans";
    readonly string Bonus = "bonus";
    readonly string Entry = "entry";
    readonly string Start = "start";

    bool isAnsRight = false;
    List<Transform> focuslist;

    public List<PrankSequence> prankSequenceList;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else if(isPhaseTwo)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseThreeRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            
            isAnsRight = true;
            inputComplete = true;
        }
        else if(isPhaseTwo)
        {
            propsEntry.SetTrigger(Stone);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            propsEntry.SetTrigger(Bomb);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            
            isAnsRight = false;
            inputComplete = true;
        }
        else if(isPhaseTwo)
        {
            propsEntry.SetTrigger(Spring);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            propsEntry.SetTrigger(BallEntry);
            isAnsRight = false;
            inputComplete = true;
        }
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }


            StartLevel();
        }

        if(isPhaseTwo)
        {
            inputComplete = false;

        }

    }

    protected override void RVAnswerSelected()
    {
        propsEntry.SetTrigger(Bonus);
        
        StartCoroutine(RVSelectedPhaseRoutine());
    }

    IEnumerator RVSelectedPhaseRoutine()
    {
        yield return ENDOFFRAME;
        yield return WAITONE;
        propsEntry.SetTrigger(Start);
        bowlingAnimator.gameObject.SetActive(true);
        yield return WAITHALF;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(cameraEndPosBonus, focuslist);
        rigAnimator.SetTrigger(Transition);
        rigAnimator.speed = 1;
        yield return WAITTWO;
        //
        rigAnimator.SetTrigger(Bonus);
        yield return WAITONE;
        
        yield return WAITONE;
        bowlHitLegParticle.Play();
        bowlingAnimator.SetTrigger(Entry);
        yield return WAITONE;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPosBonus, focuslist);
        portalOneParticle.Play();
        
        portalTwoParticle.Play();
        yield return new WaitForSeconds(.7f);
        bowlHitHeadParticle.Play();
        yield return WAITHALF;
        
        yield return WAITTWO;
        currentPhaseIndex++;
        //yield return WAITTWO;
        ReportPhaseCompletion(2,  true);
        inputComplete = false;


    }


    IEnumerator StartPhaseOneRoutine()
    {
        //focuslist = new List<Transform>();
        //focuslist.Add(preEndFocus);
        //CameraMovementController.Instance.FollowCamera(preEndPos, focuslist);
        /*phase one
        */

        // That's the new Phase one for testing
        rigAnimator.SetTrigger(IDLE3);
        propNewAnimator.SetTrigger(IDLE3);
        yield return ENDOFFRAME;
        //focuslist = new List<Transform>();
        //focuslist.Add(phase3StartFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(phase3StartPos, focuslist);

        yield return WAITTWO;
        yield return WAITHALF;
        propNewAnimator.speed = 0;

        focuslist = new List<Transform>();
        focuslist.Add(zoom3Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom3Pos, focuslist);
        yield return WAITONE;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        if (isAnsRight)
        {
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, delegate { }));
            //yield return WAITONE;
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            
            propNewAnimator.speed = 1;
            //yield return WAITONE;
            rigAnimator.SetTrigger(Success3);
            propNewAnimator.SetTrigger(Success3);
            focuslist = new List<Transform>();
            focuslist.Add(phase3FailFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase3FailPos, focuslist);
            //slipOnParticle.Play();
            yield return WAITTHREE;
            //fallFlatParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(phase3FailPos, focuslist, false, Vector3.zero, Vector3.zero, 0, .25f, .5f, null);
            yield return ENDOFFRAME;

            yield return WAITONE;
            yield return WAITHALF;
            fallPhase3Particle.Play();
            yield return WAITONE;
            //

            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            //propNewAnimator.speed = 0;
            rigAnimator.SetTrigger(TRANS);
            propNewAnimator.SetTrigger(TRANS);
            yield return WAITTWO;
            yield return new WaitForSeconds(.7f);

            propsOnHand[3].SetActive(false);
            propsOnHand[4].SetActive(true);

            rigAnimator.SetTrigger(IDLE1);
            propAnimatorIdle.SetTrigger(IDLE1);

            yield return WAITTWO;
            yield return WAITHALF;

            isPhaseOne = false;
            isPhaseTwo = true;
            inputComplete = false;
            
            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            propsOnHand[1].SetActive(false);
            propsOnHand[0].SetActive(true);
            focuslist = new List<Transform>();
            focuslist.Add(zoom4Focus);
            CameraMovementController.Instance.FocusCameraWithOrigin(zoom4Pos, focuslist);

            pinAnimator.SetTrigger(LEAK);
            yield return new WaitForSeconds(1.2f);
            pinParticle.Play();
            yield return WAITONE;
            
            focuslist = new List<Transform>();
            focuslist.Add(phase3FailFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase3FailPos, focuslist);
            propNewAnimator.speed = 1;
            yield return WAITONE;
            rigAnimator.SetTrigger(Fail3);
            propNewAnimator.SetTrigger(Fail3);


            yield return WAITTHREE;
            yield return new WaitForSeconds(.3f);
            pumpLeakParticle.Play();
            
            //yield return new WaitForSeconds(.05f);
            //pumpLeakParticle.Play();
            yield return WAITTHREE;
            //CameraMovementController.Instance.UnfollowCamera();

            yield return WAITTWO;
            //yield return WAITHALF;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            Debug.Log(isPhaseTwo);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;


    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        //rigScale.localScale = new Vector3(1f,1f,1f);
       
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        

        //yield return WAITHALF;

        propAnimatorIdle.gameObject.SetActive(false);
        propAnimator.gameObject.SetActive(true);
        
        rigAnimator.SetTrigger(Success1);
        propAnimatorIdle.SetTrigger(Success1);
        rigAnimator.speed = 0;
        propAnimatorIdle.speed = 0;
        propAnimator.SetTrigger(Success1);
        propAnimator.speed = 0;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTWO;
        // do here the prank job detail

        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 1;
        propAnimator.speed = 1;
        playerPosition.UnFollow();
        if (isAnsRight)
        {
            //focuslist = new List<Transform>();
            //focuslist.Add(cameraStartFocus);
            //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);

            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            slipOnParticle.Play();
            yield return WAITONE;
            fallFlatParticle.Play();
            focuslist = new List<Transform>();
            focuslist.Add(phase1SuccessFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase1Success, focuslist);
            yield return WAITTWO;

            isPhaseTwo = false;

            inputComplete = false;

            ReportPhaseCompletion(2,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            yield return WAITONE;
            focuslist = new List<Transform>();
            focuslist.Add(phase1FailFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(phase1Fail, focuslist);

            yield return WAITTWO;
            yield return WAITTHREE;
            //CameraMovementController.Instance.UnfollowCamera();

            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(2,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;

    }

    IEnumerator StartPhaseThreeRoutine()
    {
        propsEntry.SetTrigger(Default);
        playerPosition.Follow();
        propAnimatorIdle.gameObject.SetActive(false);

        rigAnimator.SetTrigger(Transition);
        rigAnimator.speed = 0;


        CameraMovementController.Instance.UnfollowCamera();
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 2);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        yield return WAITONE;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraEndFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

        yield return WAITTWO;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        CameraMovementController.Instance.FollowCamera(phase1Success, focuslist);
        rigAnimator.SetTrigger(Transition);
        rigAnimator.speed = 1;
        yield return WAITTWO;
        playerPosition.UnFollow();

        if (isAnsRight)
        {
            playerPosition.UnFollow();
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FollowCamera(cameraEndPos, focuslist);
            //yield return WAITONE;
            //rigAnimator.SetTrigger(Common2);
            //propAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            //propAnimator.SetTrigger(Success2);

            yield return WAITTWO;
            ballKickParticle.Play();
            propsEntry.SetTrigger(Explode);
            yield return WAITONE;
            yield return WAITHALF;
            explosionParticle.Play();
            yield return WAITTWO;
            fallFlatHugeParticle.Play();
            yield return WAITTWO;


            ReportPhaseCompletion(3, true);
            inputComplete = false;
        }
        else
        {

            CameraMovementController.Instance.UnfollowCamera();
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            rigAnimator.SetTrigger(Fail2);
            propAnimator.SetTrigger(Fail2);
            yield return WAITTWO;
            propsEntry.SetTrigger(BallExit);
            ballKickParticle.Play();
            yield return WAITTHREE;


            ReportPhaseCompletion(3, true);
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
        }
        inputComplete = false;
        playerPosition.UnFollow();
    }

    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }
}
