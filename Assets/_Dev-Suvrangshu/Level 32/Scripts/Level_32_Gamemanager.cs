﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;

public class Level_32_Gamemanager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();
    public SkinMatController skinMatController;
    public Material dirtyBody;
    public Material originalBody;
    public Transform characterMeshTransform;
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITNO = new WaitForSeconds(0f);

    public List<string> animationKeyList;


    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }

    IEnumerator ChangeMaterial(Material mat, WaitForSeconds WAIT)
    {
        yield return WAIT;
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in characterMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            skinnedMeshRenderer.sharedMaterial = mat;
        }
    }

    public void PlayAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
           // propsAnimatorReference[0].SetTrigger(animationHashedDictionary[t_Key]);
        }
    }



    public void PlayThirdPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[2].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PhaseOnePropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[0].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PhaseTwoPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[1].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }
    public void PlayFourthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[3].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayFifthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[4].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }




    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();



        
        //CameraMovementController.Instance.FollowCamera(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList);
        

        /*CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
           });*/
        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {

               UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
           });


    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return new WaitForEndOfFrame();
        PlayThirdPropAnimation(animationKeyList[1]);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
               PlayAnimation(animationKeyList[1]);
               PhaseOnePropAnimation(animationKeyList[1]);
           });
        yield return new WaitForSeconds(4.5f);

        PlayThirdPropAnimation(animationKeyList[7]);
        
        yield return new WaitForSeconds(2.5f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        prankSequenceList[0].propsList[1].propsParticle[1].Play();
        skinMatController.SetMat(1);
        //yield return new WaitForSeconds(.5f);
        currentPhaseIndex++;
        //yield return new WaitForSeconds(2.5f); //6.5f
        
        yield return new WaitForSeconds(.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
             delegate
             {
                 

             });
        propsOnHand[0].SetActive(false);
        yield return new WaitForSeconds(4.5f);
        ReportPhaseCompletion(1,  false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
             });

        

    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        PlayFourthAnimation(animationKeyList[8]);
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {
                 
             });
        yield return new WaitForSeconds(2f);
        PlayAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(3f);
        PlayAnimation(animationKeyList[4]);
        PhaseTwoPropAnimation(animationKeyList[4]);
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[0].propsParticle[1].Play();
        yield return new WaitForSeconds(.5f);
        propsOnHand[1].SetActive(false);
        propsOnHand[2].SetActive(false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
             delegate
             {
                 
             });
        //yield return new WaitForSeconds(0.5f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(7f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();

    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(ActivateThumstacks(true));

        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
               PlayAnimation(animationKeyList[2]);
           });
        
        yield return new WaitForSeconds(8f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        
        PlayFifthAnimation(animationKeyList[5]);
        yield return new WaitForSeconds(.5f);
        while (prankSequenceList[1].propsList[1].propObject.GetComponent<GlueVisual>().manualglue(Time.deltaTime * 2f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(2f);
        PlayAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(3f);
        PlayAnimation(animationKeyList[5]);

        yield return new WaitForSeconds(6f);


        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return new WaitForEndOfFrame();
            



        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return new WaitForEndOfFrame();

            prankSequenceList[1].propsList[1].propObject.GetComponent<GlueVisual>().ResetGlue();
            PlayAnimation(animationKeyList[9]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
             });
            propsOnHand[1].SetActive(true);
            propsOnHand[2].SetActive(true);
            
            

            //after video

            








        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    private IEnumerator ActivateThumstacks(bool t_IsActive)
    {
        for (int i = 0; i < prankSequenceList[0].propsList[0].propObject.transform.childCount; i++)
        {
            prankSequenceList[0].propsList[0].propObject.transform.GetChild(i).gameObject.SetActive(t_IsActive);
            yield return new WaitForEndOfFrame();
        }
    }
}
