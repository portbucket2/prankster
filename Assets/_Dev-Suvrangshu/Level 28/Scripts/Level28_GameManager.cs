﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;

public class Level28_GameManager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();

    public Material bathtubCementMat;
    public Material mentholFace;
    public Material cementFace;
    public MeshRenderer tubWater;
    public Material originalBody;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITNO = new WaitForSeconds(0f);

    public SkinMatController skinMatController;
    public Transform characterMeshTransform;
    public List<string> animationKeyList;
    readonly string LerpValue = "LerpValue";

    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    public void PlayFirstPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[0].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayThirdAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[2].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlaySecondPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[1].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayFourthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[3].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }
    public void PlayFifthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[4].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }


    IEnumerator ChangeMaterial(Material mat, WaitForSeconds WAIT)
    {
        yield return WAIT;
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in characterMeshTransform.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            skinnedMeshRenderer.sharedMaterial = mat;
        }
    }



    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();



        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
           });



    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, delegate { }));
        yield return new WaitForSeconds(.7f);
        prankSequenceList[0].propsList[3].propObject.SetActive(true);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(.5f);
        PlayCharacterAnimation(animationKeyList[1]);
        yield return new WaitForSeconds(1f);
        propsOnHand[1].SetActive(true);
        
        yield return new WaitForSeconds(2f);
        while (prankSequenceList[0].propsList[3].propObject.GetComponent<GlueVisual>().manualglue(Time.deltaTime * 2f))
        {
            yield return new WaitForEndOfFrame();
        }
        
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[3].propObject.GetComponent<GlueVisual>().ResetGlue();
        propsOnHand[1].SetActive(false);
        yield return new WaitForSeconds(2.5f);
        skinMatController.SetMat(1);
        yield return new WaitForSeconds(1f);
        prankSequenceList[0].propsList[3].propObject.SetActive(false);
        PlayCharacterAnimation(animationKeyList[2]);
        
        

        yield return new WaitForSeconds(1f);
        PlayCharacterAnimation(animationKeyList[8]);

        yield return new WaitForSeconds(3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[4].cameraOriginTrans, prankSequenceList[0].camPosList[4].cameraTransList,
            delegate
            {
                
            });
        yield return new WaitForSeconds(1f);
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                
                PlayCharacterAnimation(animationKeyList[5]);

            });
        ReportPhaseCompletion(1,  false);
        yield return new WaitForSeconds(2f);
        
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        
        propsOnHand[3].SetActive(true);
        PlayFirstPropAnimation(animationKeyList[9]);
        yield return new WaitForSeconds(.5f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(.5f);
        tubWater.material = bathtubCementMat;
        float temp = 5f;
        float Val = -5f;
        while (Val < temp)
        {
            Val += Time.deltaTime * 5;
            bathtubCementMat.SetFloat(LerpValue, Val);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1f);
        propsOnHand[3].SetActive(true);



        PlayCharacterAnimation(animationKeyList[6]);


        
        
        yield return new WaitForSeconds(1.5f);
        skinMatController.SetMat(2);
        yield return new WaitForSeconds(5f);
        //UIManager.Instance.ShowLevelComplete();
        ReportPhaseCompletion(2,  true);

    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos,
            prankSequenceList[0].propsList[2].propObject, delegate { }));
        yield return new WaitForSeconds(.7f);
        prankSequenceList[0].propsList[4].propObject.SetActive(true);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(.5f);
        
        
        PlayCharacterAnimation(animationKeyList[1]);
        yield return new WaitForSeconds(1f);
        propsOnHand[2].SetActive(true);
        

        yield return new WaitForSeconds(2f);
        while (prankSequenceList[0].propsList[4].propObject.GetComponent<GlueVisual>().manualglue(Time.deltaTime * 2f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1f);
        propsOnHand[2].SetActive(false);
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[4].propObject.GetComponent<GlueVisual>().ResetGlue();
        prankSequenceList[0].propsList[4].propObject.SetActive(false);
        yield return new WaitForSeconds(1.7f);
        PlayCharacterAnimation(animationKeyList[3]);

        yield return new WaitForSeconds(1.2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
            delegate
            {

            });

        

        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        propsOnHand[4].SetActive(true);
        PlayFirstPropAnimation(animationKeyList[9]);
        yield return new WaitForSeconds(.5f);
        //PlayFifthAnimation(animationKeyList[11]);

        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(1f);
        prankSequenceList[1].propsList[1].propsParticle[1].Play();


        yield return new WaitForSeconds(2.5f);
        
        PlayCharacterAnimation(animationKeyList[7]);


        yield return new WaitForSeconds(0.5f);
        skinMatController.SetMat(0);

        yield return new WaitForSeconds(2f);
        propsOnHand[4].SetActive(false);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {


            






        }
        else
        {
            yield return new WaitForEndOfFrame();


            yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].initialPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
            yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[1].initialPos,
            prankSequenceList[0].propsList[1].propObject, delegate { }));
            yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[2].initialPos,
            prankSequenceList[0].propsList[2].propObject, delegate { }));
            
            PlayCharacterAnimation(animationKeyList[0]);
            StartCoroutine(StartLevelRoutine());


        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            //after video
            
            yield return new WaitForEndOfFrame();
            skinMatController.SetMat(1);
            prankSequenceList[1].propsList[1].propsParticle[0].Stop();
            prankSequenceList[1].propsList[1].propsParticle[1].Stop();
            PlayCharacterAnimation(animationKeyList[5]);
            yield return new WaitForSeconds(2f);
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            
            
            yield return new WaitForSeconds(1f);
            




        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }
}
