﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;


public class Level_19_GameManager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();




    public List<string> animationKeyList;


    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    public void PlayFirstPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[0].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayThirdAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[2].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlaySecondPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[1].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayFourthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[3].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }
    public void PlayFifthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[4].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }




    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();
        
        
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                
            });
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
           });
        
        

    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[1].finalPos,
            prankSequenceList[0].propsList[1].propObject, delegate { }));
        yield return new WaitForSeconds(.7f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(.5f);
        PlayCharacterAnimation(animationKeyList[1]);
        propsOnHand[0].SetActive(true);
        PlayFirstPropAnimation(animationKeyList[7]);
        
        

        prankSequenceList[0].propsList[1].propObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        //Transform getParticle = propsOnHand[0].transform.GetChild(1);
        //getParticle.gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        PlayCharacterAnimation(animationKeyList[2]);
        ReportPhaseCompletion(1,  false);

        yield return new WaitForSeconds(2f);
        currentPhaseIndex++;
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
            });
        propsOnHand[0].SetActive(false);
        propsOnHand[2].SetActive(true);
        yield return new WaitForSeconds(2f);
       /* CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                PlayCharacterAnimation(animationKeyList[4]);
                
                PlayThirdAnimation(animationKeyList[8]);
                

            });*/
        //yield return new WaitForSeconds(6f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                
            });
        
        yield return new WaitForSeconds(1f);
        PlayFourthAnimation(animationKeyList[9]);

        
        yield return new WaitForSeconds(1f);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();

        yield return new WaitForSeconds(3f);

        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
                
            });
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[4]);
        PlayThirdAnimation(animationKeyList[8]);
        yield return new WaitForSeconds(5f);
        PlayCharacterAnimation(animationKeyList[5]);
        PlayThirdAnimation(animationKeyList[10]);

        prankSequenceList[1].propsList[2].propsParticle[0].Play();
        prankSequenceList[1].propsList[0].propsParticle[1].Play();
        yield return new WaitForSeconds(0f);

        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[4].cameraOriginTrans, prankSequenceList[1].camPosList[4].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();

    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos,
            prankSequenceList[0].propsList[0].propObject, delegate { }));
        yield return StartCoroutine(ZAxisMoveRoutine(prankSequenceList[0].propsList[2].finalPos,
            prankSequenceList[0].propsList[2].propObject, delegate { }));
        yield return new WaitForSeconds(.7f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(.5f);
        //yield return new WaitForSeconds(3f);
        PlayCharacterAnimation(animationKeyList[1]);
        propsOnHand[1].SetActive(true);
        PlaySecondPropAnimation(animationKeyList[7]);

        prankSequenceList[0].propsList[2].propObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[2].propsParticle[0].Play();
        yield return new WaitForSeconds(5f);
        PlayCharacterAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(2f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                
            });
        yield return new WaitForSeconds(2f);
        PlayFifthAnimation(animationKeyList[11]);
        
        prankSequenceList[1].propsList[1].propsParticle[0].Play();

        yield return new WaitForSeconds(3f);

        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {

            });
        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[4]);
        PlayThirdAnimation(animationKeyList[8]);
        yield return new WaitForSeconds(5f);
        PlayCharacterAnimation(animationKeyList[6]);
        PlayThirdAnimation(animationKeyList[12]);
        
        prankSequenceList[1].propsList[1].propsParticle[1].Play();
        yield return new WaitForSeconds(2f);


        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {
            

            //StartCoroutine(StartLevelRoutine());






        }
        else
        {
            yield return new WaitForEndOfFrame();
           
            
            propsOnHand[1].transform.position = posForResetOne[0].transform.position;
            propsOnHand[1].SetActive(false);
            propsOnHand[5].transform.position = posForResetOne[1].transform.position;
            PlayCharacterAnimation(animationKeyList[0]);
            StartCoroutine(StartLevelRoutine());


        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            
        }
        else
        {
            //after video
            yield return new WaitForEndOfFrame();
            PlayThirdAnimation(animationKeyList[13]);
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
            });
            yield return new WaitForSeconds(1f);
            PlayCharacterAnimation(animationKeyList[14]);
            
            
            
            
        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }
}
