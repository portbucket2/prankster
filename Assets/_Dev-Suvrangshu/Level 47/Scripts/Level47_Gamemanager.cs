﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;


public class Level47_Gamemanager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();
    public SkinMatController skinMatController;

    public CameraShake camShake;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITNO = new WaitForSeconds(0f);

    public List<string> animationKeyList;


    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }



    public void PlayAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
            propsAnimatorReference[0].SetTrigger(animationHashedDictionary[t_Key]);
        }
    }



    public void PlayThirdPropAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[2].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }


    public void PlayFourthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[3].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayFifthAnimation(string p_Key)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[4].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }




    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();




        //CameraMovementController.Instance.FollowCamera(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList);


        /*CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
           });*/
        yield return new WaitForSeconds(.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {

               
           });
        
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
               UIManager.Instance.RequestQuestionPanel(stepIndex:0);
           });


    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return new WaitForEndOfFrame();
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(1f);
        propsOnHand[0].SetActive(false);
        propsOnHand[1].SetActive(true);
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {


           });
        PlayAnimation(animationKeyList[1]);
        yield return new WaitForSeconds(3.5f);
        PlayAnimation(animationKeyList[2]);
        yield return new WaitForSeconds(.2f);
        while (prankSequenceList[0].propsList[0].propObject.GetComponent<PaintVisual>().manualglue(Time.deltaTime * 1.5f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1.8f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
           delegate
           {

               
               
           });


        
        yield return new WaitForSeconds(.7f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[5].cameraOriginTrans, prankSequenceList[0].camPosList[5].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
           delegate
           {

               //PlayAnimation(animationKeyList[4]);

           });
        yield return new WaitForSeconds(.2f);
        camShake.ShowCameraShake(.5f, 2f, false);
        yield return new WaitForSeconds(1.3f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[4].cameraOriginTrans, prankSequenceList[0].camPosList[4].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
           delegate
           {

               PlayAnimation(animationKeyList[4]);
               prankSequenceList[0].propsList[0].propsParticle[1].Play();

           });
        currentPhaseIndex++;
        yield return new WaitForSeconds(2.5f);
        propsOnHand[6].SetActive(false);
        propsOnHand[5].SetActive(true);
        ReportPhaseCompletion(1,  false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex:1);
             });



    }

    private IEnumerator RightPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        prankSequenceList[0].propsList[0].propsParticle[1].Stop();
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[4].finalPos, prankSequenceList[1].propsList[4].propObject, delegate {
        }));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[1].finalPos, prankSequenceList[1].propsList[1].propObject, delegate {
        }));
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {

             });
        
        yield return new WaitForSeconds(.5f);
        
        PlayAnimation(animationKeyList[5]);
        yield return new WaitForSeconds(.5f);
        propsOnHand[3].SetActive(true);
        yield return new WaitForSeconds(3.5f);
        while (prankSequenceList[1].propsList[0].propObject.GetComponent<PaintVisual>().manualglue(Time.deltaTime * 1.4f))
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1.5f);
        prankSequenceList[1].propsList[1].propObject.SetActive(false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null,
           delegate
           {

               

           });

        yield return new WaitForSeconds(3.4f);
        prankSequenceList[1].propsList[3].propsParticle[0].Play();
        yield return new WaitForSeconds(2.6f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();

    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return new WaitForEndOfFrame();
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(1f);
        propsOnHand[0].SetActive(false);
        propsOnHand[2].SetActive(true);
        yield return new WaitForSeconds(1f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {


           });
        
        PlayAnimation(animationKeyList[1]);
        yield return new WaitForSeconds(3.5f);
        
        PlayAnimation(animationKeyList[3]);
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[1].propsParticle[1].Play();
        yield return new WaitForSeconds(2.5f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        prankSequenceList[0].propsList[0].propsParticle[1].Stop();
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[4].finalPos, prankSequenceList[1].propsList[4].propObject, delegate {
        }));
        yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].finalPos, prankSequenceList[1].propsList[2].propObject, delegate {
        }));
        yield return new WaitForSeconds(2f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {

             });
        
        yield return new WaitForSeconds(.5f);
        
        PlayAnimation(animationKeyList[6]);
        yield return new WaitForSeconds(.5f);
        propsOnHand[4].SetActive(true);
        yield return new WaitForSeconds(2f);
        prankSequenceList[0].propsList[0].propObject.GetComponent<PaintVisual>().StartDisabling();
        yield return new WaitForSeconds(2f);
        prankSequenceList[1].propsList[2].propObject.SetActive(false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {


           });
        yield return new WaitForSeconds(3.5f);

        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return new WaitForEndOfFrame();




        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            yield return new WaitForEndOfFrame();
            prankSequenceList[0].propsList[0].propsParticle[1].Play();

            propsOnHand[4].SetActive(false);
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[4].initialPos, prankSequenceList[1].propsList[4].propObject, delegate {
            }));
            yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[1].propsList[2].initialPos, prankSequenceList[1].propsList[2].propObject, delegate {
            }));
            PlayAnimation(animationKeyList[4]);

            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[3].cameraOriginTrans, prankSequenceList[0].camPosList[3].cameraTransList,
             delegate
             {



             });
            while (prankSequenceList[0].propsList[0].propObject.GetComponent<PaintVisual>().manualglue(Time.deltaTime * 1.5f))
            {
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(0.5f);
            prankSequenceList[0].propsList[0].propsParticle[1].Play();
        }
        else
        {
            
            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }


}
