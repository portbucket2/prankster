﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Camera;
public class Level_48A : PrankLevelBehaviour
{
    [Header("Character Animators")]
    public Animator rigAnimator;
    public Animator propAnimator;
    public Animator mopAnimator;
    public Animator fanAnimator;

    public GameObject fan;
    public GameObject bonusFan;

    public bool isPhaseOne = true;
    public bool isPhaseTwo = false;
    public bool inputComplete = false;

    [Header("Camera Follow transforms")]
    public Transform characterPos;
    public Transform cameraStartPos;
    public Transform cameraStartFocus;
    public Transform zoom1Pos;
    public Transform zoom1Focus;
    public Transform zoom2Pos;
    public Transform zoom2Focus;
    public Transform cameraEndPos;
    public Transform cameraEndFocus;
    public Transform phase3CommonPos;
    public Transform phase3CommonFocus;
    public Transform zoom3Pos;
    public Transform zoom3Focus;
    public Transform playerConPos;
    public Transform playerConFocus;
    public Transform fanFollower;
    public Transform cameraEndPosBonus;


    [Header("particles")]
    public ParticleSystem paintOnGroundParticle;
    public ParticleSystem kickBucketParticle;
    public ParticleSystem flowerSmellParticle;
    public ParticleSystem badSmellParticleParticle;
    public ParticleSystem vomittingParticleParticle;
    public ParticleSystem screwParticle;
    public ParticleSystem bucketParticle;
    public ParticleSystem tornedoParticle;
    //public ParticleSystem waterFromGlassParticle;
    //public ParticleSystem fallFlatParticle;
    //public ParticleSystem thoughtBubbleParticle;

    [Header("objects and other animators")]
    public SkinMatController skinMatController;
    public Animator cementHolder;
    public Animator colorBucket;
    public Animator sprayHolder;
    public Animator ScrewHolder;
    public Animator wrenchHolder;
    public MeshRenderer bucketWaterMat;
    public Color cementColor;
    public Color paintColor;
    public SkinnedMeshRenderer mop;
    public Material mopColored;
    //public Animator waterCoolerAnim;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);


    readonly string IDLE1 = "idle1";
    readonly string IDLE2 = "idle2";
    readonly string Common1 = "common1";
    readonly string Common2 = "common2";
    readonly string Success1 = "success1";
    readonly string Fail1 = "fail1";
    readonly string Success2 = "success2";
    readonly string Fail2 = "fail2";
    readonly string Move = "move";
    readonly string Default = "default";
    readonly string Flower = "flower";
    readonly string Fart = "fart";
    readonly string COLOR = "_Color";
    readonly string IDLE3 = "idle3";
    readonly string Common3 = "common3";
    readonly string Success3 = "success3";
    readonly string Fail3 = "fail3";
    readonly string Screw = "screw";
    readonly string Wrench = "wrench";
    readonly string Bonus = "bonus";
    readonly string Bonus_L = "bonus_l";

    bool isAnsRight = false;
    List<Transform> focuslist;
    // prank level behavior start

    private PlayerFocusForCamera playerPosition;

    protected override void StartLevel()
    {
        playerPosition = characterPos.GetComponent<PlayerFocusForCamera>();
        if (isPhaseOne)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseOneRoutine());
        }
        else if(isPhaseTwo)
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseTwoRoutine());
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(StartPhaseThreeRoutine());
        }
    }

    protected override void RightAnswerSelected()
    {
        if (isPhaseOne)
        {
            colorBucket.SetTrigger(Move);
            isAnsRight = true;
            inputComplete = true;
        }
        else if(isPhaseTwo)
        {
            sprayHolder.SetTrigger(Fart);
            isAnsRight = true;
            inputComplete = true;
        }
        else
        {
            ScrewHolder.SetTrigger(Screw);
            isAnsRight = true;
            inputComplete = true;
        }
    }

    protected override void WrongAnswerSelected()
    {
        if (isPhaseOne)
        {
            cementHolder.SetTrigger(Move);
            isAnsRight = false;
            inputComplete = true;
        }
        else if(isPhaseTwo)
        {
            sprayHolder.SetTrigger(Flower);
            isAnsRight = false;
            inputComplete = true;
        }
        else
        {
            wrenchHolder.SetTrigger(Wrench);
            isAnsRight = false;
            inputComplete = true;
        }
    }

    protected override void RVAnswerSelected()
    {
        fan.SetActive(false);
        bonusFan.SetActive(true);
        fanAnimator.gameObject.SetActive(true);
        StartCoroutine(RVSelectedPhaseRoutine());
    }


    protected override void ResetPhase(int phaseIndex)
    {
        if (phaseIndex >= 0)
        {
            currentPhaseIndex--;
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IResetOnAdCallback ireset = item as IResetOnAdCallback;
                if (ireset != null)
                {
                    ireset.ResetToPhase(currentPhaseIndex);
                }
            }

            StartLevel();
        }

    }

    IEnumerator RVSelectedPhaseRoutine()
    {
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(characterPos);
        focuslist.Add(fanFollower);
        CameraMovementController.Instance.FollowCamera(cameraEndPosBonus, focuslist, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null);
        rigAnimator.SetTrigger(Bonus);
        fanAnimator.SetTrigger(Bonus);
        yield return WAITFOUR;
        yield return WAITFOUR;
        
        yield return WAITONE;
        focuslist.Add(characterPos);
        focuslist.Add(fanFollower);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPosBonus, focuslist, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null);
        rigAnimator.SetTrigger(Bonus_L);
        fanAnimator.SetTrigger(Bonus_L);
        yield return WAITTWO;
        currentPhaseIndex++;
        ReportPhaseCompletion(3,  true);

    }
    IEnumerator StartPhaseOneRoutine()
    {
        /*phase one
        */

        rigAnimator.SetTrigger(IDLE1);
        propAnimator.SetTrigger(IDLE1);
        mopAnimator.SetTrigger(IDLE1);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(cameraStartFocus);
        CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;

        yield return WAITFOUR;

        focuslist = new List<Transform>();
        focuslist.Add(zoom1Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom1Pos, focuslist);
        yield return WAITONE;
        rigAnimator.speed = 0;
        propAnimator.speed = 0;
        mopAnimator.speed = 0;
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        //yield return WAITTWO;
        //// do here the prank job detail

        //yield return ENDOFFRAME;
        //focuslist = new List<Transform>();
        //focuslist.Add(cameraStartFocus);
        //CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
        //yield return WAITONE;
        //rigAnimator.speed = 1;
        //propAnimator.speed = 1;

        if (isAnsRight)
        {
            float temp = 5f;
            float Val = -5f;
            Color tempColor = bucketWaterMat.material.GetColor(COLOR);
            while (Val < temp)
            {
                Val += Time.deltaTime * 3;
                bucketWaterMat.material.SetColor(COLOR, Color.Lerp(tempColor, paintColor, Val));
                yield return ENDOFFRAME;
            }

            yield return ENDOFFRAME;

            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            rigAnimator.speed = 1;
            propAnimator.speed = 1;
            mopAnimator.speed = 1;



            //---------------
            rigAnimator.SetTrigger(Success1);
            propAnimator.SetTrigger(Success1);
            mopAnimator.SetTrigger(Success1);
            yield return WAITONE;
            mop.material = mopColored;
            yield return WAITTWO;
            paintOnGroundParticle.Play();
            yield return WAITFOUR;
            yield return WAITONE;

            isPhaseOne = false;
            isPhaseTwo = true;

            inputComplete = false;

            ReportPhaseCompletion(1,  false);
            StartLevel();
            // start second stage after delay
        }
        else
        {
            float temp = 5f;
            float Val = -5f;
            Color tempColor = bucketWaterMat.material.GetColor(COLOR);
            while (Val < temp)
            {
                Val += Time.deltaTime * 3;
                bucketWaterMat.material.SetColor(COLOR, Color.Lerp(tempColor, cementColor, Val));
                yield return ENDOFFRAME;
            }

            yield return ENDOFFRAME;

            focuslist = new List<Transform>();
            focuslist.Add(cameraStartFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraStartPos, focuslist);
            yield return WAITONE;
            rigAnimator.speed = 1;
            propAnimator.speed = 1;
            mopAnimator.speed = 1;

            //-----------
            rigAnimator.SetTrigger(Fail1);
            propAnimator.SetTrigger(Fail1);
            mopAnimator.SetTrigger(Fail1);
            yield return WAITFOUR;
            yield return WAITTWO;
            kickBucketParticle.Play();
            yield return WAITTWO;
            //CameraMovementController.Instance.UnfollowCamera();
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FollowCamera(characterPos, focuslist);
            //yield return WAITFOUR;

            inputComplete = false;
            ReportPhaseCompletion(1,  false);
            //UIManager.Instance.ShowLevelFailedUpdated();
        }

        inputComplete = false;
    }

    //---------------------Phase two------------------------------------------------------------------

    IEnumerator StartPhaseTwoRoutine()
    {
        /*phase two
        zoom to shoes
        show choice
        tie shoe success or spill mobile fail
        */
        paintOnGroundParticle.Pause();

        rigAnimator.SetTrigger(IDLE2);
        propAnimator.SetTrigger(IDLE2);
        mopAnimator.SetTrigger(IDLE2);
        yield return ENDOFFRAME;
        focuslist = new List<Transform>();
        focuslist.Add(zoom2Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom2Pos, focuslist);
        yield return WAITONE;

        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }

        yield return WAITTHREE;
        // do here the prank job detail, waiting prank to end.
        yield return ENDOFFRAME;

        if (isAnsRight)
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITONE;

            rigAnimator.SetTrigger(Common2);
            propAnimator.SetTrigger(Common2);
            mopAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Success2);
            //propAnimator.SetTrigger(Success2);
            mopAnimator.SetTrigger(Success2);

            yield return WAITTHREE;
            badSmellParticleParticle.Play();
            yield return WAITFOUR;
            vomittingParticleParticle.Play();
            //yield return WAITTWO;
            yield return WAITTWO;
            isPhaseTwo = false;

            ReportPhaseCompletion(2,  false);
            inputComplete = false;
            StartLevel();
        }
        else
        {
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITONE;

            rigAnimator.SetTrigger(Common2);
            propAnimator.SetTrigger(Common2);
            mopAnimator.SetTrigger(Common2);
            rigAnimator.SetTrigger(Fail2);
            //propAnimator.SetTrigger(Fail2);
            mopAnimator.SetTrigger(Fail2);

            yield return WAITFOUR;
            flowerSmellParticle.Play();
            yield return WAITTWO;
            yield return WAITFOUR;


            ReportPhaseCompletion(2,  false);
            
            //UIManager.Instance.ShowLevelFailedUpdated();
            inputComplete = false;
            
        }
        inputComplete = false;
    }

   
    IEnumerator StartPhaseThreeRoutine()
    {
        badSmellParticleParticle.Stop();
        yield return ENDOFFRAME;
        rigAnimator.SetTrigger(IDLE3);
        propAnimator.SetTrigger(IDLE3);
        focuslist = new List<Transform>();
        focuslist.Add(zoom3Focus);
        CameraMovementController.Instance.FocusCameraWithOrigin(zoom3Pos, focuslist);

        yield return WAITONE;

        
        UIManager.Instance.RequestQuestionPanel(stepIndex: 2);
        
        while (!inputComplete)
        {
            yield return ENDOFFRAME;
        }
        yield return WAITHALF;
        screwParticle.Play();
        yield return WAITTWO;
        screwParticle.Stop();
        yield return ENDOFFRAME;




        if (isAnsRight)
        {
            
            rigAnimator.SetTrigger(Common3);
            propAnimator.SetTrigger(Common3);
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            //yield return WAITONE;

            rigAnimator.SetTrigger(Success3);
            propAnimator.SetTrigger(Success3);
            yield return WAITFOUR;
            yield return WAITONE;
            yield return WAITHALF;
            bucketParticle.Play();
            bucketWaterMat.gameObject.SetActive(false);
            yield return WAITTWO;
            //yield return WAITONE;
            //yield return WAITHALF;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist, false, Vector3.zero, Vector3.zero, 0, .75f, .75f, null);
            yield return WAITTWO;

            ReportPhaseCompletion(3,  true);
            inputComplete = false;

        }
        else
        {
            rigAnimator.SetTrigger(Common3);
            propAnimator.SetTrigger(Common3);
            focuslist = new List<Transform>();
            focuslist.Add(cameraEndFocus);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);

            rigAnimator.SetTrigger(Fail3);
            propAnimator.SetTrigger(Fail3);

            yield return WAITFOUR;
            tornedoParticle.Play();
            yield return WAITFOUR;
            focuslist = new List<Transform>();
            focuslist.Add(characterPos);
            CameraMovementController.Instance.FocusCameraWithOrigin(cameraEndPos, focuslist);
            yield return WAITFOUR;
            //focuslist = new List<Transform>();
            //focuslist.Add(characterPos);
            //CameraMovementController.Instance.FocusCameraWithOrigin(playerConPos, focuslist);
            yield return WAITTHREE;
            ReportPhaseCompletion(3, true);
            inputComplete = false;
        }
    }
}
