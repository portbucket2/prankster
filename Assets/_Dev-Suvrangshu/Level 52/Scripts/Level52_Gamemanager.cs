﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faithstudio.Camera;

public class Level52_Gamemanager : PrankLevelBehaviour
{
    public Animator playerAnimatorReference;
    public List<Animator> propsAnimatorReference = new List<Animator>();
    public List<GameObject> posForResetOne = new List<GameObject>();
    public SkinMatController skinMatController;

    public CameraShake camShake;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITNO = new WaitForSeconds(0f);

    public List<string> animationKeyList;


    public Dictionary<string, int> animationHashedDictionary;

    public List<GameObject> propsOnHand = new List<GameObject>();

    public List<PrankSequence> prankSequenceList;
    public void PreProcess()
    {
        animationHashedDictionary = new Dictionary<string, int>();
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        /*for (int i = 0; i < prankSequenceList.Count; i++)
        {
            for (int j = 0; j < prankSequenceList[i].propsList.Count; j++)
            {
                prankSequenceList[i].propsList[j].initialPos = prankSequenceList[i].propsList[j].propObject.transform.position;
                prankSequenceList[i].propsList[j].initialRotation = prankSequenceList[i].propsList[j].propObject.transform.rotation.eulerAngles;
            }
        }*/
    }



    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }



    public void PlayPropsAnimation(string p_Key, int num)
    {
        if (animationHashedDictionary.ContainsKey(p_Key))
        {
            propsAnimatorReference[num].SetTrigger(animationHashedDictionary[p_Key]);
        }
    }

    public void PlayChipsAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            propsAnimatorReference[5].SetTrigger(animationHashedDictionary[t_Key]);
        }
    }
    public void PlayWaterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            propsAnimatorReference[6].SetTrigger(animationHashedDictionary[t_Key]);
        }
    }



    protected override void StartLevel()
    {

        PreProcess();
        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();




        //CameraMovementController.Instance.FollowCamera(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList);


        /*CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {
               
           });*/
        yield return new WaitForSeconds(.5f);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
           delegate
           {


           });

        yield return new WaitForSeconds(2f);
        PlayCharacterAnimation(animationKeyList[5]);
        PlayPropsAnimation(animationKeyList[5], 0);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
           delegate
           {

               UIManager.Instance.RequestQuestionPanel(stepIndex: 0);
           });


    }


    protected override void RightAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    private IEnumerator RightPhaseOneRoutine()
    {

        yield return new WaitForEndOfFrame();
        propsOnHand[5].SetActive(true);
        PlayChipsAnimation(animationKeyList[7]);
        yield return new WaitForSeconds(2f);
        
        PlayCharacterAnimation(animationKeyList[6]);
        PlayPropsAnimation(animationKeyList[6], 0);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
             delegate
             {
                 propsOnHand[5].SetActive(false);
                 propsOnHand[0].SetActive(false);
                 propsOnHand[1].SetActive(true);
                 PlayCharacterAnimation(animationKeyList[1]);
                 PlayPropsAnimation(animationKeyList[1], 1);

                 //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
             });
        yield return new WaitForSeconds(2f);
        
        //camShake.ShowCameraShake(.5f, 2f, false);
        currentPhaseIndex++;
        
        yield return new WaitForSeconds(3.5f);
        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        yield return new WaitForSeconds(1f);
        ReportPhaseCompletion(1,  false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
             delegate
             {
                 UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
             });




    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
                delegate
                {
                    
                    
                    
                });
        yield return new WaitForSeconds(1f);
        PlayPropsAnimation(animationKeyList[10], 7);

        yield return new WaitForSeconds(2f);
        propsOnHand[3].SetActive(true);
        propsOnHand[7].SetActive(false);
        
        PlayCharacterAnimation(animationKeyList[3]);
        PlayPropsAnimation(animationKeyList[3], 3);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {
                 
             });
        yield return new WaitForSeconds(2.5f);
        prankSequenceList[1].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(2.5f);
        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelComplete();

    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {


        yield return new WaitForEndOfFrame();
        propsOnHand[6].SetActive(true);
        PlayWaterAnimation(animationKeyList[8]);
        yield return new WaitForSeconds(.5f);
        prankSequenceList[0].propsList[1].propsParticle[0].Play();
        yield return new WaitForSeconds(1.5f);
        propsOnHand[6].SetActive(false);
        PlayCharacterAnimation(animationKeyList[6]);
        PlayPropsAnimation(animationKeyList[6], 0);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
             delegate
             {
                 propsOnHand[0].SetActive(false);
                 propsOnHand[2].SetActive(true);
                 PlayCharacterAnimation(animationKeyList[2]);
                 PlayPropsAnimation(animationKeyList[2], 2);
                 //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
             });
        //yield return new WaitForSeconds(2f);
        

        yield return new WaitForSeconds(6f);
        ReportPhaseCompletion(1,  false);
        //UIManager.Instance.ShowLevelFailedUpdated();


    }

    IEnumerator WrongPhaseTwoRoutine()
    {

        yield return new WaitForEndOfFrame();
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
                delegate
                {

                    

                });
        yield return new WaitForSeconds(1f);
        PlayPropsAnimation(animationKeyList[9], 7);
        
        propsOnHand[8].SetActive(true);
        prankSequenceList[1].propsList[0].propsParticle[0].Play();
        PlayPropsAnimation(animationKeyList[11], 8);
        yield return new WaitForSeconds(1f);
        prankSequenceList[1].propsList[0].propsParticle[1].Play();
        yield return new WaitForSeconds(2.5f);
        propsOnHand[4].SetActive(true);
        propsOnHand[7].SetActive(false);
        propsOnHand[8].SetActive(false);
        propsOnHand[3].SetActive(false);
        PlayCharacterAnimation(animationKeyList[4]);
        PlayPropsAnimation(animationKeyList[4], 4);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
             delegate
             {

             });
        yield return new WaitForSeconds(3f);

        ReportPhaseCompletion(2,  true);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    // public override void ResetPhase()
    // {
    //     switch (currentPhaseIndex)
    //     {
    //         case 0:
    //             StartCoroutine(ResetPhaseOneRoutine());
    //             break;
    //         case 1:
    //             StartCoroutine(ResetPhaseTwoRoutine());
    //             break;
    //         
    //         default:
    //             break;
    //     }
    // }
    protected override void ResetPhase(int phaseIndex)
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(ResetPhaseOneRoutine(phaseIndex));
                break;
            case 1:
                StartCoroutine(ResetPhaseTwoRoutine(phaseIndex));
                break;

            default:
                break;
        }
    }
    IEnumerator ResetPhaseOneRoutine(int t_PhaseIndex)
    {

        if (t_PhaseIndex < 0)
        {

        }
        else
        {
            yield return new WaitForEndOfFrame();




        }
    }

    IEnumerator ResetPhaseTwoRoutine(int t_PhaseIndex)
    {
        if (t_PhaseIndex < 0)
        {
            yield return new WaitForEndOfFrame();
            CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
             delegate
             {
                 prankSequenceList[1].propsList[0].propsParticle[1].Stop();
                 PlayCharacterAnimation(animationKeyList[12]);
             });
            propsOnHand[4].SetActive(false);
            propsOnHand[7].SetActive(true);

        }
        else
        {

            UIManager.Instance.RequestQuestionPanel(stepIndex: 1);

        }
    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public IEnumerator ZAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .2f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(t_TargetTrans.transform.position.x,
                t_TargetTrans.transform.position.y, Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }
}
