﻿using System.Collections.Generic;
using System.IO;
using com.adjust.sdk.purchase;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (AdjustPurchaseSDKManager))]
public class AdjustPurchaseSDKManagerEditor : Editor {
    #region Private Variables

    private AdjustPurchaseSDKManager m_Reference;
    private Editor m_AdjustEditor;
    private string m_PathForDynamicEnum = "Assets/_AdjustPurchaseSDK/DynamicEnum/";

    #endregion

    #region Editor

    private void OnEnable () {

        m_Reference = (AdjustPurchaseSDKManager) target;
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        EditorGUILayout.PropertyField (serializedObject.FindProperty ("appTokenForAndroid"));
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("appTokenForIOS"));

        DrawLineSeperator ();

        EditorGUI.BeginChangeCheck ();
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("isProductionBuild"));
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("startManually"));
        if (EditorGUI.EndChangeCheck ()) {
            SerializedProperty t_MarkAsChange = serializedObject.FindProperty ("updateAdjustConfiguretion");
            t_MarkAsChange.boolValue = true;
        }

        if (m_Reference.updateAdjustConfiguretion) {

            if (m_Reference.isProductionBuild) {

                m_Reference.logLevel = ADJPLogLevel.Info;
                m_Reference.environment = ADJPEnvironment.Production;
            } else {

                m_Reference.logLevel = ADJPLogLevel.Verbose;
                m_Reference.environment = ADJPEnvironment.Sandbox;
            }

            m_Reference.updateAdjustConfiguretion = false;
        }

        EditorGUI.indentLevel += 1;

        EditorGUILayout.BeginHorizontal (); {
            m_Reference.showAdjustPurchaseConfiguretion = EditorGUILayout.Foldout (
                m_Reference.showAdjustPurchaseConfiguretion,
                "Configuretion",
                true);
            if (GUILayout.Button (m_Reference.showAdjustPurchaseConfiguretionHelpBox ? "HideHelpBox" : "ShowHelpBox")) {

                m_Reference.showAdjustPurchaseConfiguretionHelpBox = !m_Reference.showAdjustPurchaseConfiguretionHelpBox;
            }

        }
        EditorGUILayout.EndHorizontal ();

        if (m_Reference.showAdjustPurchaseConfiguretionHelpBox) {
            EditorGUILayout.HelpBox (
                "#All the configuretion for 'Adjust' EventTracking (Including Revenue) are below" +
                "\n\n#if 'StartManually' is set to 'True', you can assign some delay when initializing 'AdjustSDK'" +
                "\n\n#It is highly recommand to initialize the 'AdjustSDK' manually (Source -> Rumman Bhai)",
                MessageType.Info
            );
        }

        if (m_Reference.showAdjustPurchaseConfiguretion) {

            EditorGUI.indentLevel += 1;

            DrawLineSeperator ();

            m_Reference.showAdvanceAdjustPurchaseConfiguretion = EditorGUILayout.Foldout (
                m_Reference.showAdvanceAdjustPurchaseConfiguretion,
                "Advance Settings",
                true
            );

            if (m_Reference.showAdvanceAdjustPurchaseConfiguretion) {

                EditorGUI.indentLevel += 1;

                EditorGUILayout.PropertyField (serializedObject.FindProperty ("logLevel"));
                EditorGUILayout.PropertyField (serializedObject.FindProperty ("environment"));

                EditorGUI.indentLevel -= 1;
            }

            EditorGUI.indentLevel -= 2;
        }

        EditorGUI.indentLevel -= 1;

        ShowAdjustPurchaseEditor ();

        serializedObject.ApplyModifiedProperties ();
    }

    private void ShowAdjustPurchaseEditor () {

        EditorGUI.indentLevel += 1;

        DrawLineSeperator ();

        EditorGUILayout.BeginHorizontal (); {
            m_Reference.showAdjustPreConfigRevenueEventTracker = EditorGUILayout.Foldout (m_Reference.showAdjustPreConfigRevenueEventTracker, "PreConfiguredTracker    :   RevenueEvent", true);

            if (m_Reference.showAdjustPreConfigRevenueEventTracker) {

                if (m_Reference.updateEnumListForPreConfigRevenueEventTracker) {

                    if (GUILayout.Button ("Update (Required)")) {

                        List<string> t_EnumParameter = new List<string> ();
                        int t_NumberOfAssignedRevenueEventTracker = m_Reference.listOfAdjustRevenueTracker.Count;

                        for (int j = 0; j < t_NumberOfAssignedRevenueEventTracker; j++) {
                            t_EnumParameter.Add (m_Reference.listOfAdjustRevenueTracker[j].uniqueIdentifier);
                        }
                        GenerateEnum ("AdjustRevenueEventTrack", t_EnumParameter, m_PathForDynamicEnum);

                        m_Reference.updateEnumListForPreConfigRevenueEventTracker = false;
                    }
                } else {
                    if (GUILayout.Button ("Add (RevenueEventTracker)")) {
                        m_Reference.listOfAdjustRevenueTracker.Add (new AdjustPurchaseSDKManager.AdjustRevenueTracker ());
                    }
                }
            }

            if (GUILayout.Button (m_Reference.showAdjustPreConfigRevenueEventTrackerHelpBox ? "HideHelpBox" : "ShowHelpBox")) {

                m_Reference.showAdjustPreConfigRevenueEventTrackerHelpBox = !m_Reference.showAdjustPreConfigRevenueEventTrackerHelpBox;
            }
        }
        EditorGUILayout.EndHorizontal ();

        if (m_Reference.showAdjustPreConfigRevenueEventTrackerHelpBox) {

            EditorGUILayout.HelpBox (
                "(1) Tap on the 'PreConfiguredTracker : RevenueEvent' to expand its functionality." +
                "\n(2) Once it is expand, you can simply add new 'RevenueEvent' by pressing the 'Add (RevenueEventTracker)'." +
                "\n(3) By taping the 'RevenueEventTracker (n) -> UniqueIdentifier', you can configure the following event." +
                "\n(4) 'UniqueIdentifier' (Not Part of AdjustSDK), is a unique key reference to call the following 'Event'." +
                "\n(5) Make sure you fill the platform specefic 'EventToken' on Android/iOS." +
                "\n\nIn Order to call the event, there are 3-ways you can achieve it." +
                "\n(1) Using -> InvokePreConfigRevenueEvent (string t_RevenueEventUniqueIdentifier) : By passing 'UniqueIdentifier." +
                "\n(2) Using -> InvokePreConfigRevenueEvent (int t_RevenueEventIndex) : By passing its 'Index." +
                "\n(3) Using -> InvokePreConfigRevenueEvent (int t_RevenueEventIndex,double t_PurchaseValue,string t_Currency,string t_TransactionId) by overriding the value from editor" +
                "\n\n#Note : If you want call 'AdjustRevenueEventTracking' by passing 'EventToken', use" +
                "\n -> TrackRevenue (string t_EventToken, double t_Amount, string t_Currency = 'USA') or" +
                "\n -> TrackRevenueWithDeDuplication (string t_EventToken, double t_Amount, string t_TransactionID, string t_Currency = 'USA')",
                MessageType.Info);
        }

        if (m_Reference.showAdjustPreConfigRevenueEventTracker) {

            EditorGUILayout.Space ();
            EditorGUI.indentLevel += 1;

            if (m_Reference.listOfAdjustRevenueTracker == null)
                m_Reference.listOfAdjustRevenueTracker = new List<AdjustPurchaseSDKManager.AdjustRevenueTracker> ();

            int t_NumberOfAssignedRevenueTracker = m_Reference.listOfAdjustRevenueTracker.Count;
            for (int i = 0; i < t_NumberOfAssignedRevenueTracker; i++) {

                SerializedProperty t_IsTrackerShowOnEditor = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("showOnEditor");
                SerializedProperty t_UniqueIdentifier = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("uniqueIdentifier");

                EditorGUILayout.BeginHorizontal (); {
                    t_IsTrackerShowOnEditor.boolValue = EditorGUILayout.Foldout (t_IsTrackerShowOnEditor.boolValue, "RevenueEventTracker (" + i + ") -> " + t_UniqueIdentifier.stringValue, true);
                    if (GUILayout.Button ("Remove")) {
                        m_Reference.listOfAdjustRevenueTracker.RemoveAt (i);
                        break;
                    }
                }
                EditorGUILayout.EndHorizontal ();

                if (t_IsTrackerShowOnEditor.boolValue) {

                    EditorGUI.indentLevel += 1;

                    EditorGUI.BeginChangeCheck ();
                    t_UniqueIdentifier.stringValue = EditorGUILayout.TextField ("UniqueIdentifier", t_UniqueIdentifier.stringValue);
                    if (EditorGUI.EndChangeCheck ()) {

                        SerializedProperty t_MarkChange = serializedObject.FindProperty ("updateEnumListForPreConfigRevenueEventTracker");
                        t_MarkChange.boolValue = true;
                    }

                    EditorGUILayout.Space ();

                    SerializedProperty t_EventTokenForAndroid = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("eventTokenForAndroid");
                    t_EventTokenForAndroid.stringValue = EditorGUILayout.TextField ("Android", t_EventTokenForAndroid.stringValue);

                    SerializedProperty t_EventTokenForIOS = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("eventTokenForIOS");
                    t_EventTokenForIOS.stringValue = EditorGUILayout.TextField ("iOS", t_EventTokenForIOS.stringValue);

                    EditorGUILayout.Space ();

                    SerializedProperty t_Currency = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("currency");
                    t_Currency.stringValue = EditorGUILayout.TextField ("Currency", t_Currency.stringValue);

                    SerializedProperty t_PurchaseValue = serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("purchaseValue");
                    t_PurchaseValue.doubleValue = EditorGUILayout.DoubleField ("PurchaseValue", t_PurchaseValue.doubleValue);

                    EditorGUILayout.Space ();

                    EditorGUILayout.PropertyField (serializedObject.FindProperty ("listOfAdjustRevenueTracker").GetArrayElementAtIndex (i).FindPropertyRelative ("GetTransactionId"));

                    EditorGUI.indentLevel -= 1;
                }
            }
            EditorGUI.indentLevel -= 1;
        }

        EditorGUI.indentLevel -= 1;
    }

    #endregion

    #region Configuretion

    private void DrawLineSeperator (float t_Height = 1, Color t_LineColor = new Color ()) {

        if (t_LineColor.a == 0)
            t_LineColor.a = 1f;

        GUIStyle t_GUIStyleForHorizontalLine;
        t_GUIStyleForHorizontalLine = new GUIStyle ();
        t_GUIStyleForHorizontalLine.normal.background = EditorGUIUtility.whiteTexture;
        t_GUIStyleForHorizontalLine.margin = new RectOffset (0, 0, 4, 4);
        t_GUIStyleForHorizontalLine.fixedHeight = t_Height;

        Color t_GUIColor = GUI.color;

        GUI.color = t_LineColor;
        GUILayout.Box (GUIContent.none, t_GUIStyleForHorizontalLine);
        GUI.color = t_GUIColor;
    }

    public void GenerateEnum (string t_EnumName, List<string> t_EnumParameters, string t_Path = "Assets/") {

        string[] t_FoldersName = t_Path.Split ('/');
        string t_PreviousPath = t_FoldersName[0];
        string t_ProgressivePath = t_FoldersName[0];
        int t_NumberOfFolder = t_FoldersName.Length;

        for (int j = 0; j < t_NumberOfFolder - 1; j++) {

            if (!AssetDatabase.IsValidFolder (t_ProgressivePath)) {

                AssetDatabase.CreateFolder (t_PreviousPath, t_FoldersName[j]);
            }

            t_PreviousPath = t_ProgressivePath;
            t_ProgressivePath += "/" + t_FoldersName[j + 1];
        }

        string t_FilePathAndName = t_Path + t_EnumName + ".cs";
        using (StreamWriter streamWriter = new StreamWriter (t_FilePathAndName)) {

            streamWriter.WriteLine ("public enum " + t_EnumName);

            streamWriter.WriteLine ("{");

            int t_NumberOfEnumParameters = t_EnumParameters.Count;
            for (int i = 0; i < t_NumberOfEnumParameters; i++) {

                streamWriter.WriteLine ("\t" + t_EnumParameters[i] + ((i + 1) < t_NumberOfEnumParameters ? "," : ""));
            }

            streamWriter.WriteLine ("}");
        }
        AssetDatabase.Refresh ();
    }

    #endregion

}