﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
public class BannarController : MonoBehaviour
{
    static BannarController instance;

    public GameObject rootObj;
    public bool waitForLoad = false;
    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            instance = this;
            this.transform.parent = null;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    IEnumerator Start()
    {
        rootObj.SetActive(false);
        if (waitForLoad)
        {
            //AdController.HideBanner();
            while (!UILoadSceneHandler.loadComplete)
            {
                yield return null;
            }

            yield return null;
            yield return null;
            yield return null;
            yield return null;
        }

        if (!NoAdManager.isBlockingAds)
        {
            rootObj.SetActive(true);
            AdController.ShowBanner();
        }
    }

    public static void DisableBanner()
    {
        AdController.HideBanner();
        if (instance) instance.rootObj.SetActive(false);
    }
}
