﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class level59Gameplay : PrankLevelBehaviour
{
    public bool TapToPlay;
    public Animator playerAnimatorReference;
    public Animator[] envAnimators;
    public Level59AnimationEvents levelAnimEvents;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    public ParticleSystem ParticleElectric;
    public ParticleSystem ParticleFire;
    public ParticleSystem ParticleSmoke;

    public ToolAppearanceShawon chiliFlakeShaker;
    public ToolAppearanceShawon saltFlakeShaker;
    public ToolAppearanceShawon coffeeCup;
    public ToolAppearanceShawon mentos;

    public GameObject[] toolAnimsObj;
    Animator[] toolAnims;

    public CameraCotrollerShawon camControlerShawon;
    public void PreProcess()
    {
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
        
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    protected override void StartLevel()
    {
        

        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        GetComponent<EnergyDrinkCanDisappear>().AppearCan();
        camControlerShawon.CameraTransition(0, 0, .01f);
        toolsAnimatorGrab();
        AnimatorsTriggerCall("AnimStart");
        yield return new WaitForSeconds(0.1f);
        



    }
    private IEnumerator KickStartLevelRoutineSPECIAL()
    {
        camControlerShawon.CameraTransition(1, 1, 0.07f);

        yield return new WaitForSeconds(2f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

    }


    protected override void RightAnswerSelected()
    {
        
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
        
            default:
                break;
        }
        
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        //if (chiliFlakeShaker)
        //{
        //    chiliFlakeShaker.GoToPosition(1);
        //}
        //
        //
        currentPhaseIndex += 1;

        toolAnims[1].SetTrigger("plateAppear");
        yield return new WaitForSeconds(2f);

        camControlerShawon.CameraTransition(0, 0, 0.1f);
        yield return new WaitForSeconds(1.0f);
        //if (chiliFlakeShaker)
        //{
        //    chiliFlakeShaker.GoToPosition(2);
        //}

        //playerAnimatorReference.SetTrigger("ChoiceOne");
        //playerAnimatorReference.SetBool("sucOne", true);
        AnimatorsTriggerCall("ChoiceOne");
        AnimatorsTriggerCall("sucOne", true);

        
        //yield return new WaitForSeconds(7.5f);
        //playerAnimatorReference.SetTrigger("doneFirst");

        yield return new WaitForSeconds(5.5f);
        camControlerShawon.CameraTransition(0, 4, 0.1f);
        yield return new WaitForSeconds(7f);

        levelAnimEvents.EndOfPhaseOneSuc();

        camControlerShawon.CameraTransition(3, 2, 0.1f);
        yield return new WaitForSeconds(0.5f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);



    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        //if (mentos)
        //{
        //    mentos.GoToPosition(1);
        //}


        //if (mentos)
        //{
        //    mentos.GoToPosition(2);
        //}
        /// To do this phase
        /// 
        //playerAnimatorReference.SetTrigger("ChoiceTwo");
        //playerAnimatorReference.SetBool("sucTwo", true);
        //yield return new WaitForSeconds(0.5f);
        GetComponent<EnergyDrinkCanDisappear>().DisAppearCan();
        toolAnims[3].SetTrigger("catAppear");
        yield return new WaitForSeconds(2.5f);
        camControlerShawon.CameraTransition(0,5, 0.2f);
        yield return new WaitForSeconds(0.5f);
        AnimatorsTriggerCall("ChoiceTwo");
        AnimatorsTriggerCall("sucTwo", true);
        yield return new WaitForSeconds(6f);
        camControlerShawon.CameraTransition(0, 7, 0.1f);
        yield return new WaitForSeconds(5f);



        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        //if (saltFlakeShaker)
        //{
        //    saltFlakeShaker.GoToPosition(1);
        //}




        //if (saltFlakeShaker)
        //{
        //    saltFlakeShaker.GoToPosition(2);
        //}
        //Debug.Log(currentPhaseIndex);
        //playerAnimatorReference.SetTrigger("ChoiceOne");
        //playerAnimatorReference.SetBool("sucOne", false);
        toolAnims[0].SetTrigger("plateAppear");
        yield return new WaitForSeconds(2f);
        camControlerShawon.CameraTransition(0, 0, 0.1f);
        yield return new WaitForSeconds(1f);

        AnimatorsTriggerCall("ChoiceOne");
        AnimatorsTriggerCall("sucOne", false);

        yield return new WaitForSeconds(3.3f);
        camControlerShawon.CameraTransition(4, 6, 0.1f);
        yield return new WaitForSeconds(1.7f);
        camControlerShawon.CameraTransition(0,6, 0.05f);
        yield return new WaitForSeconds(2.0f);

        PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false);

        //camControlerShawon.CameraTransition(0, 0, 0.1f);

    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        //if (coffeeCup)
        //{
        //    coffeeCup.GoToPosition(1);
        //}
        //

        //if (coffeeCup)
        //{
        //    coffeeCup.GoToPosition(2);
        //}
        //playerAnimatorReference.SetTrigger("ChoiceTwo");
        //playerAnimatorReference.SetBool("sucTwo", false);

        //camControlerShawon.CameraTransition(2, 2, 0.1f);
        //yield return new WaitForSeconds(0.5f);
        GetComponent<EnergyDrinkCanDisappear>().DisAppearCan();
        toolAnims[2].SetTrigger("bottleAppear");
        yield return new WaitForSeconds(3f);
        camControlerShawon.CameraTransition(0, 3, 0.1f);
        yield return new WaitForSeconds(0.5f);
        AnimatorsTriggerCall("ChoiceTwo");
        AnimatorsTriggerCall("sucTwo", false);
        yield return new WaitForSeconds(8f);

        PrankLevelBehaviour.instance.ReportPhaseCompletion(2,  true);

        //camControlerShawon.CameraTransition(0, 0, 0.1f);

    }

    IEnumerator ResetPhaseOneRoutine()
    {
        throw new System.NotImplementedException();
        
    }

    IEnumerator ResetPhaseTwoRoutine()
    {
        throw new System.NotImplementedException();
    }


    protected override void ResetPhase(int phaseIndex)
    {
        
        if(phaseIndex >= 0)
        {
            switch (currentPhaseIndex)
            {
                case 0:
                    ResetOne();
                    break;
                case 1:
                    ResetTwo();
                    break;

                default:
                    break;
            }
        }
        
        



    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public void ResetOne()
    {
        //playerAnimatorReference.SetTrigger("ResetOne");
        StartCoroutine(StartLevelRoutine());
        AnimatorsTriggerCall("ResetOne");
        //toolsAnim.SetTrigger("ResetOne");
    }

    public void ResetTwo()
    {
        //playerAnimatorReference.SetTrigger("ResetTwo");
        //levelAnimEvents.ToolAnimReset();
        StartCoroutine( ResetTwoRoutine());
        AnimatorsTriggerCall("ResetTwo");
        //toolsAnim.SetTrigger("bottleLoop");
        //levelAnimEvents.MentosParticleStop();


    }
    IEnumerator ResetTwoRoutine()
    {
        GetComponent<EnergyDrinkCanDisappear>().AppearCan();

        camControlerShawon.CameraTransition(0, 4, 0.1f);
        yield return new WaitForSeconds(5f);
        
        camControlerShawon.CameraTransition(3, 2, 0.1f);
        yield return new WaitForSeconds(1f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        
    }
    IEnumerator ParticleElectricCoroutine()
    {
        
        
        ParticleElectric.Play();
        yield return new WaitForSeconds(1.8f);
        ParticleElectric.Stop();
    }
    IEnumerator ParticleFireCoroutine()
    {

        yield return new WaitForSeconds(0.7f);
        ParticleFire.Play();
        yield return new WaitForSeconds(0.5f);
        ParticleSmoke.Play();
        yield return new WaitForSeconds(2.2f);
        ParticleFire.Stop();
        yield return new WaitForSeconds(5f);
        ParticleSmoke.Stop();
    }
    public void ParticleShock()
    {
        StartCoroutine(ParticleElectricCoroutine());
    }

    public void AnimatorsTriggerCall(string t)
    {
        playerAnimatorReference.SetTrigger(t);
        if(envAnimators[0])
        {
            for (int i = 0; i < envAnimators.Length; i++)
            {
                envAnimators[i].SetTrigger(t);
            }
        }    
    }
    public void AnimatorsTriggerCall(string t , bool b)
    {
        playerAnimatorReference.SetBool(t , b);
        if (envAnimators[0])
        {
            for (int i = 0; i < envAnimators.Length; i++)
            {
                envAnimators[i].SetBool(t, b);
            }
        }       
    }

    public void toolsAnimatorGrab()
    {
        toolAnims = new Animator[toolAnimsObj.Length];
        for (int i = 0; i < toolAnimsObj.Length; i++)
        {
            toolAnims[i] = toolAnimsObj[i].GetComponentInChildren<Animator>();
        }
    }

    public void KickStartPranking()
    {
        StartCoroutine(KickStartLevelRoutineSPECIAL());
    }



}
