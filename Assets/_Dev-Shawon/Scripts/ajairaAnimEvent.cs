﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ajairaAnimEvent : MonoBehaviour
{
    public ParticleSystem toolParticle;
    
    public void ToolparticlePlay()
    {
        toolParticle.Play();
    }
    public void ToolparticlePause()
    {
        toolParticle.Pause();
    }
}
