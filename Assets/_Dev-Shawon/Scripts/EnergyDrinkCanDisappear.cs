﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDrinkCanDisappear : MonoBehaviour
{
    public Vector3 rootPos;
    public Vector3 sidePos;
    public GameObject CanObj;
    public Vector3 targetPos;
    public bool reached;
    // Start is called before the first frame update
    void Start()
    {
        rootPos = CanObj.transform.position;
        sidePos = rootPos + new Vector3(0, 0, 5);
        reached = true;
        //DisAppearCan();
    }

    // Update is called once per frame
    void Update()
    {
        if (!reached)
        {
            CanObj.transform.position = Vector3.Lerp(CanObj.transform.position, targetPos, Time.deltaTime * 1);
        }

        if(Vector3.Distance(CanObj.transform.position, targetPos) < 0.1f)
        {
            CanObj.transform.position = targetPos;
            reached = true;
        }
        
    }

    public void DisAppearCan()
    {
        targetPos = sidePos;
        reached = false;

    }
    public void AppearCan()
    {
        CanObj.transform.position= targetPos = rootPos;
        reached = true;
    }
}
