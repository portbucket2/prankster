﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolAnimEvent : MonoBehaviour
{
    public ParticleSystem[] particlesToPlay;
    public Animator animRef;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ParticlePlayIndex(int i)
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Play();
    }
    public void ParticlePauseIndex(int i)
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Pause();
    }
    public void ParticleStopIndex(int i)
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Stop();
    }

    public void CatAnimTrigger()
    {
        animRef.SetBool("walk", false);
    }
}
