﻿using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Camera;
using UnityEngine;
using UnityEngine.Events;

public class level12Gameplay : PrankLevelBehaviour
{
    public bool TapToPlay;
    public Animator playerAnimatorReference;
    public Animator toolsAnim;
    public Level12AnimationEvents levelAnimEvents;
    public List<string> animationKeyList;
    public Dictionary<string, int> animationHashedDictionary;

    public List<PrankSequence> prankSequenceList;

    public ParticleSystem ParticleElectric;
    public ParticleSystem ParticleFire;
    public ParticleSystem ParticleSmoke;

    public ToolAppearanceShawon chiliFlakeShaker;
    public ToolAppearanceShawon saltFlakeShaker;
    public ToolAppearanceShawon coffeeCup;
    public ToolAppearanceShawon mentos;
    public void PreProcess()
    {
        for (int i = 0; i < animationKeyList.Count; i++)
        {
            animationHashedDictionary.Add(animationKeyList[i], Animator.StringToHash(animationKeyList[i]));
        }
    }

    public void PlayCharacterAnimation(string t_Key)
    {
        if (animationHashedDictionary.ContainsKey(t_Key))
        {
            playerAnimatorReference.SetTrigger(animationHashedDictionary[t_Key]);
        }
    }

    protected override void StartLevel()
    {
        //Debug.LogError("THis IS ERROER");

        StartCoroutine(StartLevelRoutine());
    }
    private IEnumerator StartLevelRoutine()
    {
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[0].cameraOriginTrans, prankSequenceList[0].camPosList[0].cameraTransList,
            delegate
            {
                
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[0]);
            });
        yield return new WaitForSeconds(2f);
        
        yield return new WaitForEndOfFrame();
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[1].cameraOriginTrans, prankSequenceList[0].camPosList[1].cameraTransList,
            delegate
            {
                
                //playerAnimatorReference.SetTrigger("AnimStart");
            });
        yield return new WaitForSeconds(1f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 0);

    }


    protected override void RightAnswerSelected()
    {
        
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(RightPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(RightPhaseTwoRoutine());
                break;
        
            default:
                break;
        }
        //Debug.Log(currentPhaseIndex);
        //Debug.Log("Shawon Right option");
    }
    private IEnumerator RightPhaseOneRoutine()
    {
        if (chiliFlakeShaker)
        {
            chiliFlakeShaker.GoToPosition(1);
        }
        
        //Debug.Log("Right One Done");
        currentPhaseIndex += 1;
        //levelAnimEvents.ToolAnimWiring();
        yield return new WaitForSeconds(3f);
        if (chiliFlakeShaker)
        {
            chiliFlakeShaker.GoToPosition(2);
        }
        //Debug.Log(currentPhaseIndex);
        playerAnimatorReference.SetTrigger("ChoiceOne");
        playerAnimatorReference.SetBool("sucOne", true);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
                
            });

        /// To do this phase
        ///
        ///
        //yield return StartCoroutine(XAxisMoveRoutine(prankSequenceList[0].propsList[0].finalPos, prankSequenceList[0].propsList[0].propObject,
        //    delegate
        //    {
        //        Debug.Log("Cube has been Reached");
        //        prankSequenceList[0].propsList[0].propsParticle[0].Play();
        //    }));

        //StartCoroutine(ParticleElectricCoroutine());

        yield return new WaitForSeconds(7.5f);
        playerAnimatorReference.SetTrigger("doneFirst");
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
            });
        yield return new WaitForSeconds(2f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {
                
            });
        // currentPhaseIndex++;
        //
        // CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans,prankSequenceList[1].camPosList[0].cameraTransList,
        //     delegate
        //     {
        //         UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
        //     });

    }

    private IEnumerator RightPhaseTwoRoutine()
    {
        if (mentos)
        {
            mentos.GoToPosition(1);
        }
        //Debug.Log("Right Two Done");
        //levelAnimEvents.ToolAnimLighter();
        //StartCoroutine(ParticleFireCoroutine());
        yield return new WaitForSeconds(2.5f);
        if (mentos)
        {
            mentos.GoToPosition(2);
        }
        /// To do this phase
        /// 
        playerAnimatorReference.SetTrigger("ChoiceTwo");
        playerAnimatorReference.SetBool("sucTwo", true);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans, prankSequenceList[1].camPosList[2].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
                
            });

        yield return new WaitForSeconds(5f);

        //UIManager.Instance.ShowLevelComplete();

        PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true);
    }

    protected override void WrongAnswerSelected()
    {
        switch (currentPhaseIndex)
        {
            case 0:
                StartCoroutine(WrongPhaseOneRoutine());
                break;
            case 1:
                StartCoroutine(WrongPhaseTwoRoutine());
                break;

            default:
                break;
        }
    }
    IEnumerator WrongPhaseOneRoutine()
    {
        if (saltFlakeShaker)
        {
            saltFlakeShaker.GoToPosition(1);
        }


        //currentPhaseIndex += 1;
        //levelAnimEvents.ToolAnimTaping();
        yield return new WaitForSeconds(3.5f);
        if (saltFlakeShaker)
        {
            saltFlakeShaker.GoToPosition(2);
        }
        //Debug.Log(currentPhaseIndex);
        playerAnimatorReference.SetTrigger("ChoiceOne");
        playerAnimatorReference.SetBool("sucOne", false);
        
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[0].camPosList[2].cameraOriginTrans, prankSequenceList[0].camPosList[2].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
                //playerAnimatorReference.SetTrigger("ChoiceOne");
                //playerAnimatorReference.SetBool("sucOne", false);
            });
        yield return new WaitForSeconds(8f);
        //Debug.LogError("Wrong phase ONE");
        PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false);
        //UIManager.Instance.ShowLevelFailedUpdated();
    }

    IEnumerator WrongPhaseTwoRoutine()
    {
        if (coffeeCup)
        {
            coffeeCup.GoToPosition(1);
        }
        //levelAnimEvents.ToolAnimGum();
        yield return new WaitForSeconds(3.5f);
        if (coffeeCup)
        {
            coffeeCup.GoToPosition(2);
        }
        playerAnimatorReference.SetTrigger("ChoiceTwo");
        playerAnimatorReference.SetBool("sucTwo", false);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[2].cameraOriginTrans, prankSequenceList[1].camPosList[2].cameraTransList,
            delegate
            {
                

            });
        yield return new WaitForSeconds(5f);
        //UIManager.Instance.ShowLevelFailedUpdated();
        PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true);
        //throw new System.NotImplementedException();
    }

    IEnumerator ResetPhaseOneRoutine()
    {
        throw new System.NotImplementedException();
        //Debug.Log("RESETTTT");
    }

    IEnumerator ResetPhaseTwoRoutine()
    {
        throw new System.NotImplementedException();
    }

    //protected override void PhaseStart(int phaseIndex)
    //{
    //    throw new System.NotImplementedException();
    //}

    //protected override void PhaseEnd(int phaseIndex)
    //{
    //    throw new System.NotImplementedException();
    //}

    protected override void ResetPhase(int phaseIndex)
    {
        //Debug.Log("ResetHosseNaki?");
        if(phaseIndex >= 0)
        {
            switch (currentPhaseIndex)
            {
                case 0:
                    ResetOne();
                    break;
                case 1:
                    ResetTwo();
                    break;

                default:
                    break;
            }
        }
        
        //throw new System.NotImplementedException();



    }
    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action = null)
    {
        t_TargetTrans.SetActive(true);
        float t_Progression = 0f;
        float t_Duration = .5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTrans.transform.position;
        Vector3 t_DestValue = t_Dest;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTrans.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                t_TargetTrans.transform.position.y, t_TargetTrans.transform.position.z);

            if (t_Progression >= 1f)
            {
                t_Action.Invoke();
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

    public void ResetOne()
    {
        playerAnimatorReference.SetTrigger("ResetOne");
        StartCoroutine(StartLevelRoutine());
        toolsAnim.SetTrigger("ResetOne");
    }

    public void ResetTwo()
    {
        playerAnimatorReference.SetTrigger("ResetTwo");
        levelAnimEvents.ToolAnimReset();
        StartCoroutine( ResetTwoRoutine());
        toolsAnim.SetTrigger("bottleLoop");
        levelAnimEvents.MentosParticleStop();


    }
    IEnumerator ResetTwoRoutine()
    {
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[0].cameraOriginTrans, prankSequenceList[1].camPosList[0].cameraTransList,
            delegate
            {
                //UIManager.Instance.RequestQuestionPanel(objectives.objectiveList[1]);
            });
        yield return new WaitForSeconds(2f);
        UIManager.Instance.RequestQuestionPanel(stepIndex: 1);
        CameraMovementController.Instance.FocusCameraWithOrigin(prankSequenceList[1].camPosList[1].cameraOriginTrans, prankSequenceList[1].camPosList[1].cameraTransList,
            delegate
            {

            });
    }
    IEnumerator ParticleElectricCoroutine()
    {
        
        //yield return new WaitForSeconds(4.3f);
        ParticleElectric.Play();
        yield return new WaitForSeconds(1.8f);
        ParticleElectric.Stop();
    }
    IEnumerator ParticleFireCoroutine()
    {

        yield return new WaitForSeconds(0.7f);
        ParticleFire.Play();
        yield return new WaitForSeconds(0.5f);
        ParticleSmoke.Play();
        yield return new WaitForSeconds(2.2f);
        ParticleFire.Stop();
        yield return new WaitForSeconds(5f);
        ParticleSmoke.Stop();
    }
    public void ParticleShock()
    {
        StartCoroutine(ParticleElectricCoroutine());
    }



}
