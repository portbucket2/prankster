﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level8AnimationEventss : MonoBehaviour
{
    public Animator playerAnimatorReference;
    public Animator toolsAnim;
    public level8Gameplay levelGameplay;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ToolAnimFailOne(float val)
    {
        toolsAnim.SetTrigger("toolFailOne");

    }

    public void ToolAnimFailTwo(float val)
    {
        toolsAnim.SetTrigger("toolFailTwo");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimTaping()
    {
        toolsAnim.SetTrigger("toolTaping");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimWiring()
    {
        toolsAnim.SetTrigger("toolWiring");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimLighter()
    {
        toolsAnim.SetTrigger("toolLighter");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimGum()
    {
        toolsAnim.SetTrigger("toolGum");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimReset()
    {
        toolsAnim.SetTrigger("toolReset");
        //Debug.Log("FuckAnimEvent");
    }

    public void ToolShockAnim()
    {
        levelGameplay.ParticleShock();
        //Debug.Log("FuckAnimEvent");
    }

    public void EndOfPhaseOneSuc()
    {
        PrankLevelBehaviour.instance.ReportPhaseCompletion(1,  false);
        //Debug.Log("End 1 1 0");
    }
    public void EndOfPhaseOneFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false, false);
        //Debug.Log("End 1 0 0");
    }
    public void EndOfPhaseTwoSuc()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true, true);
        //Debug.Log("End 2 1 1");
    }
    public void EndOfPhaseTwoFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(2, false, true);
        //Debug.Log("End 2 0 1");
    }
}
