﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolAppearanceShawon : MonoBehaviour
{
    public GameObject tool;
    public Transform[] positions;
    public int curPosIndex;
    public bool moving;
    public Animator toolAnim;
    public ParticleSystem[] particlesToPause;

    // Start is called before the first frame update
    void Start()
    {
        toolAnim = tool.GetComponentInChildren<Animator>();
        tool.transform.localScale = Vector3.zero;
        curPosIndex = 0;
        tool.transform.position = positions[curPosIndex].position;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            tool.transform.position = Vector3.Lerp(tool.transform.position, positions[curPosIndex].position, Time.deltaTime * 8);
            if(Vector3.Distance(tool.transform.position, positions[curPosIndex].position) < 0.001f)
            {
                tool.transform.position = positions[curPosIndex].position;
                moving = false;

                if(curPosIndex == 1)
                {
                    tool.transform.localScale = Vector3.one;
                    toolAnim.SetTrigger("action");
                }
                else if(curPosIndex == 2)
                {
                    tool.transform.localScale = Vector3.zero;
                    curPosIndex = 0;
                    tool.transform.position = positions[curPosIndex].position;

                    if (particlesToPause.Length > 0)
                    {
                        foreach (var item in particlesToPause)
                        {
                            item.Pause();
                        }

                        StartCoroutine(particleClearRoutine());
                        
                    }
                }
                else 
                {
                    tool.transform.localScale = Vector3.zero;
                }
            }
        }

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    //GoToPosition(2);
        //}
    }

    public void GoToPosition(int i)
    {
        curPosIndex = i;
        moving = true;
        if (curPosIndex == 1)
        {
            tool.transform.localScale = Vector3.one;
        }
        else if(curPosIndex == 2)
        {
            toolAnim.SetTrigger("stopAction");
        }
    }

    private IEnumerator particleClearRoutine()
    {
        yield return new WaitForSeconds(7f);
        if (particlesToPause[0])
        {
            foreach (var item in particlesToPause)
            {
                item.Stop();
                item.Clear();
            }

        }
    }



}
