﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level59AnimationEvents : MonoBehaviour
{

    public Animator playerAnimatorReference;
    public level59Gameplay gameplayScript;
    //public Animator props12Anim;
    //public level12Gameplay levelGameplay;

    

    public ParticleSystem[] particlesToPlay;
    
    
    public void ParticlePlayIndex(int i )
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Play();
       

    }
    public void ParticlePauseIndex(int i)
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Pause();
    }
    public void ParticleStopIndex(int i)
    {
        //Debug.Log(i +" played really");
        particlesToPlay[i].Stop();
    }


    public void EndOfPhaseOneSuc()
    {
        PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false);
        //Debug.Log("End 1 1 0");
    }
    public void EndOfPhaseOneFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false, false);
        //Debug.Log("End 1 0 0");
    }
    public void EndOfPhaseTwoSuc()
    {
       // PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true, true);
        //Debug.Log("End 2 1 1");
    }
    public void EndOfPhaseTwoFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(2, false,true);
        //Debug.Log("End 2 0 1");
    }

    public void KickStart()
    {
        gameplayScript.KickStartPranking();
    }
    public void FocusCamOnPlayer()
    {
        //gameplayScript.camControlerShawon.CameraTransition(0, 0, 0f);
    }
}
