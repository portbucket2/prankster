﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level12AnimationEvents : MonoBehaviour
{
    public Animator playerAnimatorReference;
    public Animator props12Anim;
    public level12Gameplay levelGameplay;

    public ParticleSystem flameThrownParticle;
    public ParticleSystem flameBurstParticle;
    public ParticleSystem mentosParticle;

    
    


    public void TransToOne()
    {
        props12Anim.SetTrigger("TransToOne");
        //Debug.Log("FuckAnimEvent");
    }
    public void ToolAnimSucOne()
    {
        props12Anim.SetTrigger("SucOne");

    }
    public void ToolAnimFailOne()
    {
        props12Anim.SetTrigger("FailOne");

    }
    public void ToolAnimBottleLoop()
    {
        props12Anim.SetTrigger("bottleLoop");

    }
    public void ToolAnimSucTwo()
    {
        props12Anim.SetTrigger("SucTwo");

    }

    

    public void ToolAnimFailTwo()
    {
        props12Anim.SetTrigger("FailTwo");
        
    }
    
    
    
    public void ToolAnimReset()
    {
        //props12Anim.SetTrigger("toolReset");
        //Debug.Log("FuckAnimEvent");
    }
    public void FlameThrownParticle()
    {
        flameThrownParticle.Play();
    }
    public void FlameBurstParticle()
    {
        flameBurstParticle.Play();
    }
    public void MentosParticlePlay()
    {
        mentosParticle.Play();
    }
    public void MentosParticleStop()
    {
        mentosParticle.Stop();
    }



    public void EndOfPhaseOneSuc()
    {
        PrankLevelBehaviour.instance.ReportPhaseCompletion(1,false);
        //Debug.Log("End 1 1 0");
    }
    public void EndOfPhaseOneFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(1, false, false);
        //Debug.Log("End 1 0 0");
    }
    public void EndOfPhaseTwoSuc()
    {
       // PrankLevelBehaviour.instance.ReportPhaseCompletion(2, true, true);
        //Debug.Log("End 2 1 1");
    }
    public void EndOfPhaseTwoFail()
    {
        //PrankLevelBehaviour.instance.ReportPhaseCompletion(2, false,true);
        //Debug.Log("End 2 0 1");
    }
}
