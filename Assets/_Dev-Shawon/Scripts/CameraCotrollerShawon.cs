﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCotrollerShawon : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] targets;
    public GameObject[] stations;
    GameObject target;
    GameObject station;
    public GameObject dummyTar;
    public float snapSpeed;
    public AnimationCurve animCurve;
    public float Progress;
    public float upOffset;
    public int initialTargetIndex;
    
    Vector3 oLdPos;

    void Start()
    {
        CameraTransition(initialTargetIndex, 0, 0.1f);
        dummyTar.transform.position = target.transform.position;

        Quaternion targetRot = Quaternion.LookRotation(dummyTar.transform.position - transform.position, Vector3.up);
        //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRot, Time.deltaTime * snapSpeed * Time.deltaTime * 60);
        oLdPos = station.transform.position;
        //this.transform.rotation = targetRot;
    }

    // Update is called once per frame
    void Update()
    {
        

        if(Progress < 1)
        {
            Progress += Time.deltaTime* 0.5f;
        }
        else
        {
            Progress = 1;
            oLdPos = station.transform.position;
        }

        transform.position = Vector3.Lerp(oLdPos, station.transform.position, animCurve.Evaluate(Progress));

        //this.transform.LookAt(target.transform, Vector3.up);

        dummyTar.transform.position = Vector3.Lerp(dummyTar.transform.position,( target.transform.position+ new Vector3(0, upOffset,0)), Time.deltaTime * 60 * snapSpeed);
        Quaternion targetRot = Quaternion.LookRotation(dummyTar.transform.position - transform.position, Vector3.up);
        //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRot, Time.deltaTime * snapSpeed * Time.deltaTime * 60);
        this.transform.rotation = targetRot;
    }

    public void Transition()
    {
        oLdPos = transform.position;
        Progress = 0;

    }

    public void CameraTransition(int targetIndex, int stationIndex, float snap)
    {
        if (targets[targetIndex])
        {
            target = targets[targetIndex];
        }
        if (stations[stationIndex])
        {
            station = stations[stationIndex];
        }
        snapSpeed = snap;

        Transition();


    }
    public void CameraTransition(int targetIndex)
    {
        if (targets[targetIndex])
        {
            target = targets[targetIndex];
        }

        Transition();
    }
    public void CameraTransition( int stationIndex, float snap)
    {
        
        if (stations[stationIndex])
        {
            station = stations[stationIndex];
        }
        snapSpeed = snap;
        Transition();
    }
}
