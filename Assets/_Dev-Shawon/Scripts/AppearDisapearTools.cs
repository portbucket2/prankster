﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearDisapearTools : MonoBehaviour
{
    public GameObject[] toolsEnv;
    public GameObject[] toolsCustom;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisAppearAllTools()
    {
        for (int i = 0; i < toolsEnv.Length; i++)
        {

            toolsEnv[i].SetActive(false);
            toolsCustom[i].SetActive(false);
            if (toolsCustom[i].GetComponentInParent<Animator>())
            {
                toolsCustom[i].GetComponentInParent<Animator>().SetTrigger("resetToolAnim");
            }
            
        }
    }
    public void AppearToolEnv(int i)
    {
        DisAppearAllTools();
        toolsEnv[i].SetActive(true);
    }
    public void AppearToolCustom(int i)
    {
        DisAppearAllTools();
        toolsCustom[i].SetActive(true);
    }
    public void AppearToolsCustom()
    {
        DisAppearAllTools();
        for (int i = 0; i < toolsCustom.Length; i++)
        {
            toolsCustom[i].SetActive(true);
        }
        
    }
}
