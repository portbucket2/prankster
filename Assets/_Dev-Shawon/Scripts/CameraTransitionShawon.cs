﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransitionShawon : MonoBehaviour
{
    public GameObject followerCam;
    public GameObject target;
    public Vector3 initPos;
    public Quaternion initRot;
    public float progression;
    public bool moving;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (moving )
        {
            progression += Time.deltaTime * speed;
            if(progression >= 1)
            {
                progression = 1;
                moving = false;
            }

            followerCam.transform.position = Vector3.Lerp(initPos, target.transform.position, progression);
            followerCam.transform.rotation = Quaternion.Lerp(initRot, target.transform.rotation, progression);
        }
    }

    public void GetTargetInfo()
    {
        initPos = followerCam.transform.position;
        initRot = followerCam.transform.rotation;

    }
    public void GoCamTransition(GameObject tar)
    {
        target = tar;
        progression = 0;
        GetTargetInfo();
        moving = true;


    }
}
