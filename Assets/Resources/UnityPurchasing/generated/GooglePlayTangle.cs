#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("98tev1Sb5k6TM2w18eLzI4jCGml/ih5/X704f2DtBFgpdR08JJRVujUGhv29DIWSkzB4EOehk7x4C7YQysgz5v3BHCasmMZghxZG0HLFDwj3LLpajQRVB+UXXCvx4hXRLe3vOdmQ1toU4ulQFWQ64BBP+rIjYdV6QMRMAF9JqMtXSwMxv4oAD1m+GZpye/7E+2acRn0Q2JtrR8aDCd9TvtxiRSd15LEyElhacSUAT4hoGWpMYeLs49Nh4unhYeLi42J3qgWShOTTYeLB0+7l6sllq2UU7uLi4ubj4MJRFSDVZOzw3qDuTUtTEDKavZ8KZjv5IYqipZNSzecxeOKGo3KbON+CDsY2sEek1B6ohrAmQb499VpCg63u6rxsg1zQ0uHg4uPi");
        private static int[] order = new int[] { 9,11,8,5,7,8,7,12,12,11,11,13,12,13,14 };
        private static int key = 227;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
