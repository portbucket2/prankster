﻿//
// Based on https://gist.github.com/auycro/c671c5ae830588e89f70
// And https://gist.github.com/eppz/1ebbc1cf6a77741f56d63d3803e57ba3
// And Unity API
#if UNITY_IPHONE || UNITY_IOS
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

using UnityEditor.iOS.Xcode;

using System.Collections.Generic;

namespace Portbliss.Analytics
{
    public class BuildPostProcessor
    {
        static List<string> frameWorks;
        static readonly bool willUseFrameWorks = true;

        [PostProcessBuildAttribute(1)]
        public static void OnPostProcessBuild(BuildTarget target, string path)
        {
            if (target == BuildTarget.iOS)
            {
                AddFrameWorksAndFlags(path);
            }
        }

        static void AddFrameWorksAndFlags(string path)
        {
            // Read.
            string projectPath = PBXProject.GetPBXProjectPath(path);
            PBXProject project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projectPath));
            string targetName = project.GetUnityFrameworkTargetGuid(); //PBXProject.GetUnityTargetName(); // note, not "project." ...
            string targetGUID = project.GetUnityFrameworkTargetGuid(); //project.TargetGuidByName(targetName);

            frameWorks = new List<string>();
            frameWorks.Add("AdSupport.framework");
            frameWorks.Add("iAd.framework");
            // Frameworks
            if (willUseFrameWorks)
            {
                foreach (var f in frameWorks)
                {
                    Debug.Log("will ad framework on xcode project: " + f + " and target GUID: " + targetGUID);
                    project.AddFrameworkToProject(targetGUID, f, false);
                }
            }

            // Add `-ObjC` to "Other Linker Flags".
            project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
            //project.SetBuildProperty(targetGUID, "ENABLE_BITCODE", "NO");//normally we do not wish to disable bitcode
            // Write.
            File.WriteAllText(projectPath, project.WriteToString());
        }
    }
}
#endif