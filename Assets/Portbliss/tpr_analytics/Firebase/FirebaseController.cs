﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Analytics;

namespace Portbliss.Analytics
{
    public class FirebaseController : MonoBehaviour
    {
        [SerializeField] bool testMode = false;
        // Start is called before the first frame update

        
        void Start()
        {
            /*
            Debug.Log("<color='green'>will now start firebase.</color>");
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith((task) =>
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                Debug.Log("<color='green'>firebase has been started.</color>");
                if (testMode)
                {
                    FirebaseAnalytics.LogEvent("test event", "test parameter!", 1);
                }
            });
            */
            
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    var app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.

                    FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                    Debug.Log("<color='green'>firebase has been started.</color>");
                    if (testMode)
                    {
                        FirebaseAnalytics.LogEvent("test event", "test parameter!", 1);
                        Debug.Log("<color='green'>firebase test event has been fired.</color>");
                    }
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
            
        }
        
    }
}