using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss;

namespace Portbliss.Analytics
{
    public class AnalyticsController : MonoBehaviour
    {
        static AnalyticsController instance;
        [SerializeField] bool willUseLog = false;
        static bool WillUseLog { get { return instance == null ? false : instance.willUseLog; } }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                InitAnalytics();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                    return;
                }
            }
        }

        void InitAnalytics()
        {
            LogUtil.Blue("facebook initialization method!", WillUseLog);
            if (!FB.IsInitialized)
            {
                LogUtil.Blue("will try to initialize facebook!", WillUseLog);
                FB.Init(() => {
                    LogUtil.Blue("facebook initialized!", WillUseLog);
                    FB.ActivateApp();
                });
            }
            else
            {
                LogUtil.Blue("already facebook initialized!", WillUseLog);
                FB.ActivateApp();
            }
        }

        #region GM EVENTS
        
        public static void LogLevelStarted(int levelNum)
        {
            LogUtil.Blue("LogLevelStarted() call", WillUseLog);
            
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                FB.LogAppEvent("Level_Start", 0, param);
                LogUtil.Blue("LogLevelStarted() data sent waited", WillUseLog);
            });
        }
        
        public static void LogStepStart(int levelNum, int step)
        {
            LogUtil.Blue("LogStepStart() call", WillUseLog);
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["step"] = step;
                FB.LogAppEvent("step_start", 0, param);
                LogUtil.Blue("LogStepStart() data sent waited", WillUseLog);
            });
        }
        public static void LogStepSuccess(int levelNum, int step)
        {
            LogUtil.Blue("LogStepSuccess() call", WillUseLog);
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["step"] = step;
                FB.LogAppEvent("step_completed", 0, param);
                LogUtil.Blue("LogStepSuccess() data sent waited", WillUseLog);
            });
        }
        public static void LogStepFailed(int levelNum, int step)
        {
            LogUtil.Blue("LogStepFailed() call", WillUseLog);
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["step"] = step;
                FB.LogAppEvent("step_failed", 0, param);
                LogUtil.Blue("LogStepFailed() data sent waited", WillUseLog);
            });
        }
        public static void LogLevelCompleted(int levelNum)
        {
            LogUtil.Blue("LogLevelCompleted() call", WillUseLog);
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                FB.LogAppEvent("Level_Achieved", 0, param);
                LogUtil.Blue("LogLevelCompleted() data sent waited", WillUseLog);
            });
        }
        public static void LogLifeTimeEvent(string tag)
        {
            //LogUtil.Blue("LogLevelCompleted() call", WillUseLog);
            WaitForInit(() =>
            {
                var param = new Dictionary<string, object>();
                //param[AppEventParameterName.Level] = levelNum;
                FB.LogAppEvent(tag, 0, param);
                //LogUtil.Blue("LogLevelCompleted() data sent waited", WillUseLog);
            });
        }
        #endregion

        public static void LogABTesting(string abType, string abValue)
        {
            LogUtil.Blue("LogABTesting() call", WillUseLog);
            WaitForInit(() => {
                var Params = new Dictionary<string, object>();
                Params["AB value"] = "" + abValue;

                FB.LogAppEvent(abType, null, Params);
                LogUtil.Blue("AB value choice made type:" + abType + " and ab value: " + abValue + "", WillUseLog);
                LogUtil.Blue("LogABTesting() data sent waited", WillUseLog);
            });
        }

        public static void LogRewardedVideos(string rv_event, int levelnNumber, string placement)
        {
            WaitForInit(() => {
                var Params = new Dictionary<string, object>();
                Params["level"] = levelnNumber.ToString();
                Params["placement"] = placement;

                FB.LogAppEvent(rv_event, null, Params);
            });
        }

        static void WaitForInit(Action OnComplete)
        {
            instance.StartCoroutine(instance.WaitForInitCOR(OnComplete));
        }

        IEnumerator WaitForInitCOR(Action OnComplete)
        {
            yield return null;
            while (FB.IsInitialized == false)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }
    }
}
