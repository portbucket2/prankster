﻿using System.Collections;
using System.Collections.Generic;
using Portbliss;
using FRIA;
using UnityEngine;

public class GameConfig : MonoBehaviour
{
    static GameConfig instance;
    public static HardData<bool> hasIAP_NoAdPurchasedHD { get; private set; }
    public static HardData<int> adWatchCountHD { get; private set; }
    public static HardData<bool> IAP_PurchasedOrNot_HasBeenCheckedHD { get; private set; }
    public static HardData<bool> hasDoneConsentRelatedTasksHD { get; private set; }
    public static HardData<bool> hasIAP_Restored_iOS { get; private set; }
    public static OneTimeEventConfig levelCompletionAppsflyerOneTimeHD { get; private set; }
    public static OneTimeEventConfig adWatchNTimesCompletedAppsflyerHD { get; private set; }
    public static Dictionary<int, string> levelCompletedAdjustEvents { get; private set; }
    public static Dictionary<int, string> adWatchAdjustEvents { get; private set; }

    public static bool hasRewardedVideoAdBeenShown { get; set; }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            InitSystem();
            DontDestroyOnLoad(this);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }
        //Debug.Log(;
        //todo interstitial
        //TimedAd.AdIteration((success) =>
        //{

        //});
    }
    
    void InitSystem()
    {
        hasIAP_NoAdPurchasedHD = new HardData<bool>("_NO_AD_IAP_PURCHASED_BUILD2", false);
        adWatchCountHD = new HardData<int>("INTERSTITIAL_AD_SHOW_COUNT", 0);
        IAP_PurchasedOrNot_HasBeenCheckedHD = new HardData<bool>("_IS_NO_AD_PURCHASED_CHECKED_BUILD2", false);
        hasDoneConsentRelatedTasksHD = new HardData<bool>("CONSENT_TASK_DONE_FLAG", false);
        hasIAP_Restored_iOS = new HardData<bool>("_APP_IAP_RESTORE_PURCHASE_", false);
        levelCompletionAppsflyerOneTimeHD = new OneTimeEventConfig(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
            "LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_");
        adWatchNTimesCompletedAppsflyerHD = new OneTimeEventConfig(new List<int> { 10, 20 }, "ads_watched_HD_");

#if UNITY_ANDROID
        adWatchAdjustEvents = new Dictionary<int, string> { [10] = "99j3mh", [20] = "mgiv4p" };
#elif UNITY_IOS
        adWatchAdjustEvents = new Dictionary<int, string> { [10] = "ld994d", [20] = "kjbuzy" };
#endif


#if UNITY_ANDROID
        levelCompletedAdjustEvents = new Dictionary<int, string>
        {
            [1] = "snbnvj",
            [2] = "l8z4s4",
            [3] = "en52ef",
            [4] = "gvnb0o",
            [5] = "wq0eiu",
            [6] = "22ijqs",
            [7] = "fypss2",
            [8] = "nsquj8",
            [9] = "1pfjn9",
            [10] = "mkgyw0",
            [20] = "savg4d",
            [30] = "3rtx64",
            [40] = "lchvmd",
            [50] = "o0uv8h",
            [60] = "wjo0lk",
            [70] = "8batqu",
            [80] = "urowki",
            [90] = "76rxr1",
            [100] = "911crk"
        };
#elif UNITY_IOS
        levelCompletedAdjustEvents = new Dictionary<int, string>
        {
            [1] = "fusima",
            [2] = "dwzdyv",
            [3] = "7kuxwz",
            [4] = "bz9qil",
            [5] = "wmvyf5",
            [6] = "tqhqvs",
            [7] = "b9qibc",
            [8] = "tg5mke",
            [9] = "enpt27",
            [10] = "w1qv8q",
            [20] = "2w9atv",
            [30] = "ifvqz1",
            [40] = "wmyoa3",
            [50] = "ycyd4k",
            [60] = "kfkxqa",
            [70] = "a8sksd",
            [80] = "7l9b8u",
            [90] = "ng12ez",
            [100] = "911tja"
        };
#endif

        hasRewardedVideoAdBeenShown = false;
    }
}
