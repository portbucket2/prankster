﻿using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.IAP
{
    public class IAP_No_AdButtonBinder : MonoBehaviour
    {
        [SerializeField] Button[] allBtnOfNoAd;
        [SerializeField] GameObject[] whiteBannerBGs;

        void Start ()
        {
            foreach (var b in allBtnOfNoAd)
            {
                if (b == null) { continue; }
                b.gameObject.SetActive(true);
                if (GameConfig.hasIAP_NoAdPurchasedHD.value == true)
                {
                    b.gameObject.SetActive(false);
                    if (whiteBannerBGs != null)
                    {
                        foreach (var g in whiteBannerBGs)
                        {
                            if (g != null)
                            {
                                g.SetActive(false);
                            }
                        }
                    }
                }
                else
                {
                    StartCoroutine(CheckForNoAdIAP());
                }
            }

            
        }

        IEnumerator CheckForNoAdIAP()
        {
            while (IAP_Controller.hasAdPurchased == false)
            {
                yield return null;
            }

            foreach (var b in allBtnOfNoAd)
            {
                if (b == null) { continue; }
                b.gameObject.SetActive(false);
                if (whiteBannerBGs != null)
                {
                    foreach (var g in whiteBannerBGs)
                    {
                        if (g != null)
                        {
                            g.SetActive(false);
                        }
                    }
                }
            }
            
        }
    }

}
