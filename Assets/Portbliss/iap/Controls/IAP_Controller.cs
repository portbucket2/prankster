using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UniBliss;
using Portbliss.IAP.Internal;
using System.Collections;
using Portbliss.Ad;
//using UnityEngine.UI;
using com.adjust.sdk.purchase;
using com.adjust.sdk;
using UnityEngine.Purchasing.Security;


// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace Portbliss.IAP
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class IAP_Controller : MonoBehaviour
    {
        public static bool hasAdPurchased { get; set; }
        [SerializeField] bool IsDebugMessageOn = false;
        [SerializeField] bool useConsumable = false, useNonConsumable = false, useSubscription = false;
        [SerializeField] List<string> consumableProdIDs_Android = new List<string>(), nonConsumableProdIDs_Android = new List<string>();
        [SerializeField] List<SubscriptionDesc> subscriptionDescriptions_Android = new List<SubscriptionDesc>();

        [SerializeField] List<string> consumableProdIDs_iOS = new List<string>(), nonConsumableProdIDs_iOS = new List<string>();
        [SerializeField] List<SubscriptionDesc> subscriptionDescriptions_iOS = new List<SubscriptionDesc>();

        public static IAP_Controller instance;
        
        public static bool IsDebugEnabled { get { return instance == null ? false : instance.IsDebugMessageOn; } }

        IStoreController con;
        IExtensionProvider prov;
        internal IStoreController StoreController { get { return con; } }
        internal IExtensionProvider StoreExtensionProvider { get { return prov; } }
        internal void SetStoreData(IStoreController conArg, IExtensionProvider provArg) { this.con = conArg; this.prov = provArg; }
        internal bool IsConsumableValid(string prodID)
        {
            List<string> consumableProdIDs = consumableProdIDs_Android;
#if UNITY_ANDROID
            consumableProdIDs = consumableProdIDs_Android;
#else
            consumableProdIDs = consumableProdIDs_iOS;
#endif
            return consumableProdIDs.Contains(prodID);
        }

        internal bool IsNonConsumableValid(string prodID)
        {
            List<string> nonConsumableProdIDs = nonConsumableProdIDs_Android;
#if UNITY_ANDROID
            nonConsumableProdIDs = nonConsumableProdIDs_Android;
#else
            nonConsumableProdIDs = nonConsumableProdIDs_iOS;
#endif
            return nonConsumableProdIDs.Contains(prodID);
        }

        internal bool IsSubscriptionValid(string prodID)
        {
            List<SubscriptionDesc> subscriptionDescriptions = subscriptionDescriptions_Android;
#if UNITY_ANDROID
            subscriptionDescriptions = subscriptionDescriptions_Android;
#else
            subscriptionDescriptions = subscriptionDescriptions_iOS;
#endif

            var obj = subscriptionDescriptions.Find((desc) => { return desc.SubscriptionProdID == prodID; });
            return !(obj == null);
        }
        internal string CurrentlyBeingPurchasedProductID { get; private set; }

        Action<bool> OnComplete;
        [SerializeField] GameObject loadingEffect, topMostScreenBlocker, waitingObj;
        bool IsInitialized { get { return instance == null ? false : 
                    (instance.StoreController != null && instance.StoreExtensionProvider != null); } }
        IAP_Consumable iap_consumable;
        IAP_NonConsumable iap_nonConsumable;
        IAP_Subscription iap_subscription;
        IAP_Callback callbackScript;
        [SerializeField] string noAdNonConsumableID_IfAny_Android, noAdNonConsumableID_IfAny_iOS;
        string noAdNonConsumableID_IfAny;
        [SerializeField] bool testModeDelayNoAdPurchaseOn = true;
        //[SerializeField] Button buyBtn, cancelBtn;
        [Space(10)]
        [SerializeField] bool useValidationForNonconsumableAndSubscription;
        [SerializeField] string noAD_IAP_AdjustValidationToken_iOS, noAD_IAP_AdjustValidationToken_Android;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartIAP_System();
                DontDestroyOnLoad(this);
            }
            else
            {
                DestroyImmediate(this);
            }
        }

        public bool IsThisNoAdProduct(string prod)
        {
#if UNITY_ANDROID
            return prod == noAdNonConsumableID_IfAny_Android;
#else
            return prod == noAdNonConsumableID_IfAny_iOS;
#endif
        }

        void StartIAP_System()
        {
#if UNITY_ANDROID
            useValidationForNonconsumableAndSubscription = false;
#endif

            loadingEffect.SetActive(false);
            waitingObj.SetActive(false);
            hasAdPurchased = false;
            isNoAdDoing = false;
            topMostScreenBlocker.SetActive(false);
            List<string> nonConsumableProdIDs = nonConsumableProdIDs_Android;
#if UNITY_ANDROID
            nonConsumableProdIDs = nonConsumableProdIDs_Android;
            noAdNonConsumableID_IfAny = noAdNonConsumableID_IfAny_Android;
#else
            nonConsumableProdIDs = nonConsumableProdIDs_iOS;
            noAdNonConsumableID_IfAny = noAdNonConsumableID_IfAny_iOS;
#endif

            if (nonConsumableProdIDs.Contains(noAdNonConsumableID_IfAny) == false)
            {
                nonConsumableProdIDs.Add(noAdNonConsumableID_IfAny);
            }

            if (StoreController == null)
            {
                GameUtil.LogGreen("Now we will try to initialize IAP.", IsDebugEnabled);
                if (IsInitialized)
                {
                    return;
                }
                GameUtil.LogYellow("IAP not initialized, so we will really init it.", IsDebugEnabled);
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
                if (useConsumable)
                {
                    List<string> consumableProdIDs = consumableProdIDs_Android;
#if UNITY_ANDROID
                    consumableProdIDs = consumableProdIDs_Android;
#else
                    consumableProdIDs = consumableProdIDs_iOS;
#endif

                    foreach (var p in consumableProdIDs)
                    {
                        GameUtil.LogGreen("adding consumable prod ID:" + p + "", IsDebugEnabled);
                        builder.AddProduct(p, ProductType.Consumable);
                    }
                }

                if (useNonConsumable)
                {
#if UNITY_ANDROID
                    nonConsumableProdIDs = nonConsumableProdIDs_Android;
#else
                    nonConsumableProdIDs = nonConsumableProdIDs_iOS;
#endif
                    foreach (var p in nonConsumableProdIDs)
                    {
                        GameUtil.LogGreen("adding non-consumable prod ID:" + p + "", IsDebugEnabled);
                        builder.AddProduct(p, ProductType.NonConsumable);
                    }
                }

                if (useSubscription)
                {
                    List<SubscriptionDesc> subscriptionDescriptions = subscriptionDescriptions_Android;
#if UNITY_ANDROID
                    subscriptionDescriptions = subscriptionDescriptions_Android;
#else
                    subscriptionDescriptions = subscriptionDescriptions_iOS;
#endif

                    foreach (var d in subscriptionDescriptions)
                    {
                        GameUtil.LogGreen("adding subscription prod ID:" + d.SubscriptionProdID + "", IsDebugEnabled);
                        builder.AddProduct(d.SubscriptionProdID, ProductType.Subscription, new IDs(){
                { d.AppleSubscriptionName, AppleAppStore.Name },
                { d.GoogleSubscriptionName, GooglePlay.Name },
            });
                    }
                }

                GameUtil.LogGreen("calling IAP API init...", IsDebugEnabled);
                iap_consumable = new IAP_Consumable(this);
                iap_nonConsumable = new IAP_NonConsumable(this);
                iap_subscription = new IAP_Subscription(this);
                callbackScript = GetComponent<IAP_Callback>();
                if (callbackScript == null)
                {
                    callbackScript = gameObject.AddComponent<IAP_Callback>();
                }
                callbackScript.InjectDep(this);
                IAP_Core.InjectDep(this);
                IAP_Restore.InjectDep(this);
                UnityPurchasing.Initialize(callbackScript, builder);
            }
            if (callbackScript != null)
            {
                callbackScript.OnFireAnyCallbackV2 += CallbackScript_OnFireAnyCallback;
            }
            
        }

        void OnDisable()
        {
            if (callbackScript != null)
            {
                callbackScript.OnFireAnyCallbackV2 -= CallbackScript_OnFireAnyCallback;
            }
            
        }

        void CallbackScript_OnFireAnyCallback(string productID, bool isItPurchaseEvent, bool purchaseSuccess)
        {
            if (string.IsNullOrEmpty(productID) == false
                && isItPurchaseEvent && CurrentlyBeingPurchasedProductID == productID)
            {
                if (purchaseSuccess)
                {
                    GameUtil.LogGreen("from callback, ExitIAP_WithSuccess() for id: " + productID, IsDebugEnabled);
                    ExitIAP_WithSuccess(productID);
                }
                else
                {
                    GameUtil.LogRed("from callback, ExitIAP_Withfail() for id: " + productID, IsDebugEnabled);
                    ExitIAP_Withfail();
                }
            }
        }

        internal void BuyIAP_Product<T>(string productID, Action<bool> OnComplete)
            where T : IIAP_Buy
        {
            instance.OnComplete = OnComplete; instance.CurrentlyBeingPurchasedProductID = productID;
            GameUtil.LogGreen("let us start the full IAP process for id: " + productID, IsDebugEnabled);
            if (IsInitialized == false)
            {
                GameUtil.LogRed("BuyProductID FAIL. Not initialized.", IsDebugEnabled);
                ExitIAP_Withfail();
            }
            else
            {
                GetProperIAP_Object<T>().BuyProduct(productID);
            }
        }

        void Buy_NoAdCore(Action<bool> OnComplete)
        {
            BuyIAP_Product<IAP_NonConsumable>(noAdNonConsumableID_IfAny, (success) =>
            {
                if (success)
                {
                    GameConfig.hasIAP_NoAdPurchasedHD.value = true;
                    //.SetShowIAP(false);
                    GameUtil.LogYellow("Buy_NoAdCore e 'GameConfig.hasIAP_NoAdPurchasedHD.value = true' set hoise", IsDebugEnabled);
                    AdController.HideBanner();
                    //Portbliss.Ad.AdController.HideBannerForceAPI();
                }
                OnComplete?.Invoke(success);
            });
        }

        string GetAdjustToken()
        {
            string adjustToken = "";
#if UNITY_ANDROID
            adjustToken = noAD_IAP_AdjustValidationToken_Android;
#elif UNITY_IOS
            adjustToken = noAD_IAP_AdjustValidationToken_iOS;
#endif
            return adjustToken;
        }


        bool isNoAdDoing = false;
        public void Buy_NoAd(Action<bool> OnComplete)
        {
            if (isNoAdDoing) { return; }
            isNoAdDoing = true;
            loadingEffect.SetActive(true);
            topMostScreenBlocker.SetActive(false);
            //buyBtn.interactable = true;
            //cancelBtn.interactable = true;
            waitingObj.SetActive(false);

            //code start
            //buyBtn.interactable = false;
            //cancelBtn.interactable = false;
            topMostScreenBlocker.SetActive(true);
            waitingObj.SetActive(true);
            if (testModeDelayNoAdPurchaseOn)
            {
                WaitXSeconds(4, () =>
                {
                    Buy_NoAdCore((success) =>
                    {
                        //buyBtn.interactable = true;
                        //cancelBtn.interactable = true;
                        loadingEffect.SetActive(false);
                        topMostScreenBlocker.SetActive(false);
                        waitingObj.SetActive(false);
                        OnComplete?.Invoke(success);

                        if (success && useValidationForNonconsumableAndSubscription == false)
                        {
                            Product product = StoreController.products.WithID(noAdNonConsumableID_IfAny);
                            //AnalyticsControllerOriginal.LogPurchase(product.metadata.isoCurrencyCode, product.metadata.localizedPrice + "", product.transactionID, GetAdjustToken());
                        }
                        isNoAdDoing = false;
                    });
                });
            }
            else
            {
                waitingObj.SetActive(true);
                Buy_NoAdCore((success) =>
                {
                    //buyBtn.interactable = true;
                    //cancelBtn.interactable = true;
                    loadingEffect.SetActive(false);
                    topMostScreenBlocker.SetActive(false);
                    waitingObj.SetActive(false);
                    OnComplete?.Invoke(success);

                    if (success && useValidationForNonconsumableAndSubscription == false)
                    {
                        Product product = StoreController.products.WithID(noAdNonConsumableID_IfAny);
                        //AnalyticsControllerOriginal.LogPurchase(product.metadata.isoCurrencyCode, product.metadata.localizedPrice + "", product.transactionID, GetAdjustToken());
                    }
                    isNoAdDoing = false;
                });
            }

            //code end
            /*
            buyBtn.onClick.RemoveAllListeners();
            buyBtn.onClick.AddListener(() =>
            {
                //paste uporer code if UI is needed
            });

            cancelBtn.onClick.RemoveAllListeners();
            cancelBtn.onClick.AddListener(() =>
            {
                loadingEffect.SetActive(false);
            });
            */
        }

        IIAP_Buy GetProperIAP_Object<T>()
        {
            if (typeof(T) == typeof(IAP_Consumable)) { return iap_consumable; }
            else if (typeof(T) == typeof(IAP_NonConsumable)) { return iap_nonConsumable; }
            else { return iap_subscription; }
        }

        public void RestoreIAPForApple(Action<bool> OnRestore = null)
        {
            // If Purchasing has not yet been set up ...
            bool wrongPlatform = !(Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer);
            if (!instance.IsInitialized || wrongPlatform)
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                GameUtil.LogRed("RestorePurchases FAIL. Not initialized or wrong platform.", IsDebugEnabled);
                if (wrongPlatform) { OnRestore?.Invoke(true); }
                else { OnRestore?.Invoke(false); }
                return;
            }
            else
            {
                GameUtil.LogGreen("lets us initiate IAP restore process for apple platform", IsDebugEnabled);
                IAP_Restore.RestorePurchases(OnRestore);
            }
        }
        
        internal void ExitIAP_Withfail()
        {
            GameUtil.LogRed("ExitIAP_Withfail()", IsDebugEnabled);
            OnComplete?.Invoke(false);
            OnComplete = null;
            loadingEffect.SetActive(false);
            CurrentlyBeingPurchasedProductID = "";
        }

        internal void ExitIAP_WithSuccess(string productID)
        {
            Dictionary<string, string> additionalParametes = new Dictionary<string, string>();
            Product product = StoreController.products.WithID(productID);
            string price = "";
            string currency = "";
            if (product != null && product.metadata != null)
            {
                price = product.metadata.localizedPrice + "";
                currency = product.metadata.isoCurrencyCode;
            }

            bool willDoValidation = (product.definition.type == ProductType.NonConsumable || product.definition.type == ProductType.Subscription)
                    && useValidationForNonconsumableAndSubscription;
            bool allDataOk = false;

#if UNITY_ANDROID

            IAP_RecieptObject_Android recieptObject = null;
            IAP_PayloadObject_Android payloadObj = null;
            
            if (product != null)
            {
                try
                {
                    recieptObject = JsonUtility.FromJson<IAP_RecieptObject_Android>(product.receipt);
                }
                catch (Exception ex1)
                {
                    GameUtil.LogRed("'IAP_RecieptObject_Android' object json perse error: " + ex1.Message, IsDebugEnabled);
                }

                if (recieptObject != null)
                {
                    try
                    {
                        payloadObj = JsonUtility.FromJson<IAP_PayloadObject_Android>(recieptObject.Payload);
                    }
                    catch (Exception ex2)
                    {
                        GameUtil.LogRed("'IAP_PayloadObject_Android' object json perse error: " + ex2.Message, IsDebugEnabled);
                    }
                }
            }

            allDataOk = product != null && recieptObject != null && payloadObj != null && product.hasReceipt
                && string.IsNullOrEmpty(product.receipt) == false && productID == CurrentlyBeingPurchasedProductID
                && string.IsNullOrEmpty(payloadObj.json) == false && string.IsNullOrEmpty(payloadObj.signature) == false
                && string.IsNullOrEmpty(product.metadata.localizedPrice + "") == false
                && string.IsNullOrEmpty(product.metadata.isoCurrencyCode) == false;
#elif UNITY_IOS

            allDataOk = product != null && product.hasReceipt
                && string.IsNullOrEmpty(product.receipt) == false && productID == CurrentlyBeingPurchasedProductID
                && string.IsNullOrEmpty(product.metadata.localizedPrice + "") == false
                && string.IsNullOrEmpty(product.metadata.isoCurrencyCode) == false;
#endif
            

            if (willDoValidation)
            {
                if (allDataOk)
                {
                    var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                        AppleTangle.Data(), Application.identifier);


#if UNITY_ANDROID
                    GameUtil.LogGreen("now we will try to validate IAP with adjust sdk! platform: android", IsDebugEnabled);
                    // Purchase verification request on Android.
                    GooglePlayReceipt gReceipt = null;
                    var g_result = validator.Validate(product.receipt);
                    foreach (IPurchaseReceipt r in g_result)
                    {
                        if (r.productID != productID) { continue; }
                        gReceipt = r as GooglePlayReceipt;
                        GameUtil.LogRed("we have got the reciept data!", true);
                        break;
                    }

                    if (gReceipt == null)
                    {
                        GameUtil.LogRed("google reciept object is null!! ERROR!!!!!!!!!", IsDebugEnabled);
                        OnComplete?.Invoke(true);
                        OnComplete = null;
                        loadingEffect.SetActive(false);
                        CurrentlyBeingPurchasedProductID = "";
                    }
                    else
                    {
                        Dictionary<string, object> receiptDict_v2 = null;
                        try
                        {
                            receiptDict_v2 = (Dictionary<string, object>)MiniJson.JsonDecode(product.receipt);
                        }
                        catch (Exception ex)
                        {
                            GameUtil.LogRed("receiptDict_v2----fails!", IsDebugEnabled);
                            return;
                        }
                        
                        string payload_v2 = "";
                        if (receiptDict_v2 != null && receiptDict_v2.ContainsKey("Payload"))
                        {
                            payload_v2 = (string)receiptDict_v2["Payload"];
                        }
                        else
                        {
                            GameUtil.LogRed("is receiptDict_v2 null? " + (receiptDict_v2 == null) +
                                " and receiptDict_v2 contains key Payload? " +
                                (receiptDict_v2.ContainsKey("Payload")), IsDebugEnabled);
                        }

                        Dictionary<string, object> gpDetailsDict = null;
                        try
                        {
                            gpDetailsDict = (Dictionary<string, object>)MiniJson.JsonDecode(payload_v2);
                        }
                        catch (Exception ex)
                        {
                            GameUtil.LogRed(" purchaseToken------fail", IsDebugEnabled);
                            return;
                        }

                        GameUtil.LogGreen("now we will debug into the 'gpDetailsDict' dictionary to see what is inside", IsDebugEnabled);
                        foreach (var d in gpDetailsDict)
                        {
                            GameUtil.LogBlue("element key: "+d.Key+" and value: "+d.Value);
                        }
                        GameUtil.LogMagenta("we have looked inside of gpDetailsDict", IsDebugEnabled);
                        /*
                        string purchaseToken = "";

                        if (gpDetailsDict != null && gpDetailsDict.ContainsKey("purchaseToken"))
                        {
                            purchaseToken = (string)gpDetailsDict["purchaseToken"];
                        }
                        else
                        {
                            GameUtil.LogRed("gpDetailsDict is null? " + (gpDetailsDict == null)
                                + " gpDetailsDict contains key purchaseToken? "
                                + (gpDetailsDict.ContainsKey("purchaseToken")), IsDebugEnabled);
                        }
                        */
                        string jsonDict = "";
                        if (gpDetailsDict != null && gpDetailsDict.ContainsKey("json"))
                        {
                            jsonDict = (string)gpDetailsDict["json"];
                        }
                        else
                        {
                            GameUtil.LogRed("is gpDetailsDict null? " + (gpDetailsDict == null) +
                                " and gpDetailsDict contains key json? " +
                                (gpDetailsDict.ContainsKey("json")), IsDebugEnabled);
                        }

                        //start.....
                        Dictionary<string, object> jsonDictProcessed = null;
                        try
                        {
                            jsonDictProcessed = (Dictionary<string, object>)MiniJson.JsonDecode(jsonDict);
                        }
                        catch (Exception ex)
                        {
                            GameUtil.LogRed(" jsonDictProcessed------fail", IsDebugEnabled);
                            return;
                        }

                        GameUtil.LogGreen("now we will debug into the 'jsonDictProcessed' dictionary to see what is inside", IsDebugEnabled);
                        foreach (var d in jsonDictProcessed)
                        {
                            GameUtil.LogBlue("element key: " + d.Key + " and value: " + d.Value);
                        }
                        GameUtil.LogMagenta("we have looked inside of jsonDictProcessed", IsDebugEnabled);


                        string purchaseToken = "";

                        if (jsonDictProcessed != null && jsonDictProcessed.ContainsKey("purchaseToken"))
                        {
                            purchaseToken = (string)jsonDictProcessed["purchaseToken"];
                        }
                        else
                        {
                            GameUtil.LogRed("jsonDictProcessed is null? " + (jsonDictProcessed == null)
                                + " jsonDictProcessed contains key purchaseToken? "
                                + (jsonDictProcessed.ContainsKey("purchaseToken")), IsDebugEnabled);
                        }

                        //gReceipt.purchaseToken, recieptObject.Payload
                        AdjustPurchase.VerifyPurchaseAndroid(productID, gReceipt.purchaseToken, recieptObject.Payload, (verificationInfo) => { //purchaseToken, payload_v2

                            GameUtil.LogGreen("so we varified this purchase on android and lets see the result from verification process - message: " + verificationInfo.Message
                                + " and status code: " + verificationInfo.StatusCode + " and verification state: " + verificationInfo.VerificationState, IsDebugEnabled);

                            if (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed)
                            {
                                //AnalyticsControllerOriginal.LogPurchase(currency, price, recieptObject.TransactionID, GetAdjustToken());
                                GameUtil.LogGreen("We have told the adjust sdk to report revenue after IAP verification. platform: android", IsDebugEnabled);
                            }

                            GameUtil.LogGreen("purchase validated for id: " + productID, IsDebugEnabled);
                            GameUtil.LogGreen("ExitIAP_WithSuccess() for id: " + productID + " after validation, success? "
                                + (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed), IsDebugEnabled);
                            OnComplete?.Invoke(verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed);
                            OnComplete = null;
                            loadingEffect.SetActive(false);
                            CurrentlyBeingPurchasedProductID = "";
                        });
                    }


#elif UNITY_IOS
                    GameUtil.LogGreen("getting v2 data", IsDebugEnabled);
                    //var productID = purchaseEventArgs.purchasedProduct.definition.id;
                    var receiptDict_v2 = (Dictionary<string, object>)MiniJson.JsonDecode(product.receipt);
                    var payload_v2 = (null == receiptDict_v2) ? "" : (string)receiptDict_v2["Payload"];


                    GameUtil.LogGreen("now we will try to validate IAP with adjust sdk! platform: iOS", IsDebugEnabled);
                    // Purchase verification request on iOS.
                    //product.receipt
                    AdjustPurchase.VerifyPurchaseiOS(payload_v2, product.transactionID, productID, (verificationInfo) =>
                    {
                        GameUtil.LogGreen("so we varified this purchase on iOS and lets see the result from verification process - message: " + verificationInfo.Message
                            + " and status code: " + verificationInfo.StatusCode + " and verification state: " + verificationInfo.VerificationState, IsDebugEnabled);

                        if (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed)
                        {
                            //AnalyticsControllerOriginal.LogPurchase(currency, price, product.transactionID, GetAdjustToken());
                            GameUtil.LogGreen("We have told the adjust sdk to report revenue after IAP verification. platform: iOS", IsDebugEnabled);
                        }

                        GameUtil.LogGreen("purchase validated for id: " + productID, IsDebugMessageOn);
                        GameUtil.LogGreen("ExitIAP_WithSuccess() for id: " + productID + " after validation, success? "
                            + (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed), IsDebugEnabled);
                        OnComplete?.Invoke(verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed);
                        OnComplete = null;
                        loadingEffect.SetActive(false);
                        CurrentlyBeingPurchasedProductID = "";
                    });
#endif
                }
                else
                {
                    ValidationfailCall(productID);
                }
            }
            else
            {
                GameUtil.LogGreen("ExitIAP_WithSuccess() for id: " + productID + ". No validation required!", IsDebugEnabled);
                OnComplete?.Invoke(true);
                OnComplete = null;
                loadingEffect.SetActive(false);
                CurrentlyBeingPurchasedProductID = "";
            }
        }

        void ValidationfailCall(string productID)
        {
            GameUtil.LogRed("ExitIAP_WithSuccess() for id: " + productID+" before validation fail.", IsDebugEnabled);
            OnComplete?.Invoke(false);
            OnComplete = null;
            loadingEffect.SetActive(false);
            CurrentlyBeingPurchasedProductID = "";
        }

        internal void WaitForRestoreIfAny(Action OnComplete)
        {
            StartCoroutine(WaitForRestore(OnComplete));
        }

        IEnumerator WaitForRestore(Action OnComplete)
        {
            while (IAP_Restore.IsRestoring)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }

        internal static bool HasRestoredIAP()
        {
            return GameConfig.hasIAP_Restored_iOS.value;
        }

        IEnumerator WaitForInitializationCOR(Action OnComplete)
        {
            while (IsInitialized == false)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }

        void WaitForInit(Action OnComplete)
        {
            StartCoroutine(WaitForInitializationCOR(OnComplete));
        }

        void WaitXSeconds(float delay, Action OnComplete)
        {
            StartCoroutine(WaitXSecondsCOR(delay, OnComplete));
        }

        IEnumerator WaitXSecondsCOR(float delay, Action OnComplete)
        {
            yield return new WaitForSeconds(delay);
            OnComplete?.Invoke();
        }

        public void IsProductPurchased(string productID, Action<bool> OnGetPurchaseInfo)
        {
            WaitForInit(() =>
            {
                Product p = instance.StoreController.products.WithID(productID);
                bool purchased = p.hasReceipt == true && string.IsNullOrEmpty(p.transactionID) == false;
                OnGetPurchaseInfo?.Invoke(purchased);
            });
        }

        public void IsNoAdPurchased(Action<bool> OnGetPurchaseInfo)
        {
            IsProductPurchased(noAdNonConsumableID_IfAny, OnGetPurchaseInfo);
        }

        internal bool IsProductPurchased(string productID)
        {
            Product p = instance.StoreController.products.WithID(productID);
            return p.hasReceipt == true && string.IsNullOrEmpty(p.transactionID) == false;
        }
    }
}
