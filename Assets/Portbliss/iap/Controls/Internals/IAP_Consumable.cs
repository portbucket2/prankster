﻿using Portbliss.IAP.Internal;

namespace Portbliss.IAP
{
    public class IAP_Consumable : IIAP_Buy
    {
        IAP_Controller IAPController;
        internal IAP_Consumable(IAP_Controller iAPController)
        {
            IAPController = iAPController;
        }

        public void BuyProduct(string productID)
        {
            IAPController.WaitForRestoreIfAny(() =>
            {
                if (IAPController.IsConsumableValid(productID))
                {
                    GameUtil.LogGreen("lets try to buy consumable product: " + productID, IAP_Controller.IsDebugEnabled);
                    IAP_Core.BuyProductID(productID);
                }
                else
                {
                    GameUtil.LogRed("you are trying to buy consumable product ID: " + productID +
                          "which are not defined in the controller script. Won't be successful!", IAP_Controller.IsDebugEnabled);
                }
            });
        }
    }
}