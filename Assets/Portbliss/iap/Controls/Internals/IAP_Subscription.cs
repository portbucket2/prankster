﻿using UnityEngine.Purchasing;
using Portbliss.IAP.Internal;

namespace Portbliss.IAP
{
    public class IAP_Subscription : IIAP_Buy
    {
        IAP_Controller IAPController;
        internal IAP_Subscription(IAP_Controller iAPController)
        {
            IAPController = iAPController;
        }

        void BuyCore(string productID)
        {
            if (IAPController.IsSubscriptionValid(productID) == false)
            {
                GameUtil.LogRed("you are trying to buy Subscription product: " + productID +
                    " which are not defined in the controller script. Won't be successful!", IAP_Controller.IsDebugEnabled);
                return;
            }
            GameUtil.LogGreen("lets try to buy Subscription product: " + productID, IAP_Controller.IsDebugEnabled);
            IAP_Core.BuyProductID(productID);
        }

        public void BuyProduct(string productID)
        {
            GameUtil.LogGreen("We will try to buy Subscription product: " + productID + " But first, lets wait " +
                "for any restore purchase operation that is running currently.", IAP_Controller.IsDebugEnabled);
            IAPController.WaitForRestoreIfAny(() =>
            {
                Product product = IAPController.StoreController.products.WithID(productID);
                
                if (product.hasReceipt == true && string.IsNullOrEmpty(product.transactionID) == false)
                {
                    GameUtil.LogGreen("we have the reciept of Subscription product"
                        + productID + ". So lets exit with success!", IAP_Controller.IsDebugEnabled);
                    IAPController.ExitIAP_WithSuccess(productID);
                }
                else
                {
                    GameUtil.LogGreen("lets try to buy Subscription product: " + productID
                       + " But let us first check if we restored purchases yet or not.", IAP_Controller.IsDebugEnabled);
                    if (IAP_Controller.HasRestoredIAP())
                    {
                        GameUtil.LogGreen("we already restored purchases. " +
                            "So we can gladly initiate purchase process " +
                            "through 'BuyCore()' for Subscription productID: " + productID, IAP_Controller.IsDebugEnabled);
                        BuyCore(productID);
                    }
                    else
                    {
                        GameUtil.LogGreen("we can not buy Subscription product: " + productID
                        + " , before we restored the purchase. we are doing to now.", IAP_Controller.IsDebugEnabled);
                        IAP_Restore.RestorePurchases((restoreSuccess) =>
                        {
                            GameUtil.LogGreen("purchase restored, harddata flag saved. We now will check the product reciept again," +
                                "Since we restored the IAP. " +
                                "If we do not have reciept, only then we send buy request to apple." +
                                "For Subscription product ID:" + productID, IAP_Controller.IsDebugEnabled);
                            if (IAPController.IsProductPurchased(productID))
                            {
                                GameUtil.LogGreen("we have the reciept of Subscription product"
                        + productID + ". So lets exit with success!", IAP_Controller.IsDebugEnabled);
                                IAPController.ExitIAP_WithSuccess(productID);
                            }
                            else
                            {
                                GameUtil.LogGreen("finally let us buy Subscription product: "
                        + productID + "", IAP_Controller.IsDebugEnabled);
                                BuyCore(productID);
                            }
                        });
                    }
                }
            });
        }
    }
}