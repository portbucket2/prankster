﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Portbliss.IAP.Internal
{
    public class IAP_Callback : MonoBehaviour, IStoreListener
    {
        internal Action<string, bool, bool> OnFireAnyCallbackV2;
        //internal event Func2 OnFireAnyCallback;
        static IAP_Controller IAPController;
        internal void InjectDep(IAP_Controller iap_Controller)
        {
            IAPController = iap_Controller;
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            GameUtil.LogGreen("OnInitialized: PASS", IAP_Controller.IsDebugEnabled);
            IAPController.SetStoreData(controller, extensions);
            OnFireAnyCallbackV2?.Invoke("", false, false);
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            GameUtil.LogRed("OnInitializeFailed InitializationFailureReason:" + error, IAP_Controller.IsDebugEnabled);
            OnFireAnyCallbackV2?.Invoke("", false, false);
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs args)
        {
            GameUtil.LogGreen("ProcessPurchase for id: " + args.purchasedProduct.definition.id, IAP_Controller.IsDebugEnabled);
            OnFireAnyCallbackV2?.Invoke(args.purchasedProduct.definition.id, true, true);
            if (IAPController.IsThisNoAdProduct(args.purchasedProduct.definition.id))
            {
                IAP_Controller.hasAdPurchased = true;
                GameConfig.hasIAP_NoAdPurchasedHD.value = true;
                //GameConfig.SetShowIAP(false);
                //Portbliss.Ad.AdController.HideBannerForceAPI();
            }
            GameUtil.LogGreen("reciept is: "+ args.purchasedProduct.receipt, IAP_Controller.IsDebugEnabled);

            //string fPath = Path.Combine(Application.persistentDataPath, "recieptdata.txt");
            //File.WriteAllText(fPath, args.purchasedProduct.receipt);
            return PurchaseProcessingResult.Complete;
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            string msg = string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason);
            GameUtil.LogRed(msg, IAP_Controller.IsDebugEnabled);
            OnFireAnyCallbackV2?.Invoke(product.definition.storeSpecificId, true, false);
        }
    }
}