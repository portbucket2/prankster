﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Portbliss.IAP.Internal
{
    internal static class IAP_Core
    {
        static IAP_Controller IAPController;
        internal static void InjectDep(IAP_Controller iAPController)
        {
            IAPController = iAPController;
        }

        internal static void BuyProductID(string productId)
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = IAPController.StoreController.products.WithID(productId);
            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                string v = string.Format("Purchasing ready product asychronously: '{0}'", product.definition.id);
                GameUtil.LogGreen(v, IAP_Controller.IsDebugEnabled);
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                IAPController.StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                GameUtil.LogRed("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase", IAP_Controller.IsDebugEnabled);
                IAPController.ExitIAP_Withfail();
            }
        }
    }
}