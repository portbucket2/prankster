﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.IAP;
using Portbliss.IAP.Internal;
using System;

namespace Portbliss.IAP.Test
{
    public class IAP_TestControl : MonoBehaviour
    {
        [SerializeField]
        Button buyCon1, buyCon2, buyCon3, buyNonCon1, buyNonCon2, buyNonCon3,
            buySubs1, buySubs2, buySubs3, backBtn, bringUI, restoreBtn;
        [SerializeField] GameObject panel, iapBringObject;
        [SerializeField] string conStr1, conStr2, conStr3, nonConStr1, nonConStr2, nonConStr3,
            subsStr1, subsStr2, subsStr3;
        // Start is called before the first frame update
        void Start()
        {
            panel.SetActive(false);
            iapBringObject.SetActive(true);
            BindIAPButton<Button, IAP_Consumable>(conStr1, buyCon1);
            BindIAPButton<Button, IAP_Consumable>(conStr2, buyCon2);
            BindIAPButton<Button, IAP_Consumable>(conStr3, buyCon3);
            BindIAPButton<Button, IAP_NonConsumable>(nonConStr1, buyNonCon1);
            BindIAPButton<Button, IAP_NonConsumable>(nonConStr2, buyNonCon2);
            BindIAPButton<Button, IAP_NonConsumable>(nonConStr3, buyNonCon3);
            BindIAPButton<Button, IAP_Subscription>(subsStr1, buySubs1);
            BindIAPButton<Button, IAP_Subscription>(subsStr2, buySubs2);
            BindIAPButton<Button, IAP_Subscription>(subsStr3, buySubs3);

            bringUI.onClick.RemoveAllListeners();
            bringUI.onClick.AddListener(() =>
            {
                panel.SetActive(true);
                iapBringObject.SetActive(false);
            });

            backBtn.onClick.RemoveAllListeners();
            backBtn.onClick.AddListener(() =>
            {
                panel.SetActive(false);
                iapBringObject.SetActive(true);
            });

            restoreBtn.onClick.RemoveAllListeners();
            restoreBtn.onClick.AddListener(() =>
            {
                restoreBtn.interactable = false;
                IAP_Controller.instance.RestoreIAPForApple((success) =>
                {
                    restoreBtn.interactable = true;
                    restoreBtn.image.color = success ? Color.green : Color.red;
                });
            });
        }

        void BindIAPButton<T1, T2>(string prodID, T1 btn)
        {
            Button button =(Button)Convert.ChangeType(btn, typeof(Button));
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                SetActiveAllButtons(false);

                if (typeof(T2) == typeof(IAP_Consumable))
                {
                    IAP_Controller.instance.BuyIAP_Product<IAP_Consumable>(prodID, (success) =>
                    {
                        SetActiveAllButtons(true);
                        button.image.color = success ? Color.green : Color.red;
                    });
                }
                else if (typeof(T2) == typeof(IAP_NonConsumable))
                {
                    IAP_Controller.instance.BuyIAP_Product<IAP_NonConsumable>(prodID, (success) =>
                    {
                        SetActiveAllButtons(true);
                        button.image.color = success ? Color.green : Color.red;
                    });
                }
                else
                {
                    IAP_Controller.instance.BuyIAP_Product<IAP_Subscription>(prodID, (success) =>
                    {
                        SetActiveAllButtons(true);
                        button.image.color = success ? Color.green : Color.red;
                    });
                }
            });
        }

        void SetActiveAllButtons(bool isEnable )
        {
            buyCon1.interactable = isEnable; buyCon2.interactable = isEnable; buyCon3.interactable = isEnable;
            buyNonCon1.interactable = isEnable; buyNonCon2.interactable = isEnable; buyNonCon3.interactable = isEnable;
            buySubs1.interactable = isEnable; buySubs2.interactable = isEnable; buySubs3.interactable = isEnable;
        }



    }
}