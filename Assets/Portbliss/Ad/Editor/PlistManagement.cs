﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IPHONE || UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using System.Collections.Generic;

namespace Portbliss.Ad
{
    public class PlistManagement : MonoBehaviour
    {

#if UNITY_IOS

        [PostProcessBuildAttribute(2)]
        //[PostProcessBuild]
        static void OnPostprocessBuild(BuildTarget buildTarget, string path)
        {
            // Read plist
            var plistPath = Path.Combine(path, "Info.plist");
            var plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            // Update value
            PlistElementDict rootDict = plist.root;
            rootDict.SetString("NSCalendarsUsageDescription", "Store calendar events from ads");
            //rootDict.SetString("NSPhotoLibraryUsageDescription", "For gif sharing");
            //rootDict.SetString("NSLocationWhenInUseUsageDescription", "Used to deliver better advertising experience");
            PlistElementDict NSAppTransportSecurity = rootDict.CreateDict("NSAppTransportSecurity");
            NSAppTransportSecurity.SetBoolean("NSAllowsArbitraryLoads", true);
            //NSAppTransportSecurity.values.Keys.Remove("NSAllowsArbitraryLoadsInWebContent");

            string webContentKey = "NSAllowsArbitraryLoadsInWebContent";
            if (NSAppTransportSecurity.values.ContainsKey(webContentKey))
            {
                NSAppTransportSecurity.values.Remove(webContentKey);
            }


            // remove exit on suspend if it exists.
            string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
            if (rootDict.values.ContainsKey(exitsOnSuspendKey))
            {
                rootDict.values.Remove(exitsOnSuspendKey);
            }

            // Write plist
            File.WriteAllText(plistPath, plist.WriteToString());
        }
#endif
    }
}