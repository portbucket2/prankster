﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI;
using UniBliss;
using com.adjust.sdk;
using com.adjust;

namespace Portbliss.Ad
{
    public delegate void OnCompleteAdFuc(bool success);
    public enum AdIntensityType { Normal, Medium, Aggresive, Invalid }
    public partial class AdController : MonoBehaviour
    {
        const bool logEnabled = false;
        public static AdController instance;
        [Multiline]
        [SerializeField] string sdkKey = "Lzi5VR_J50y55PM5ctwAwALT5d9g1CKMhT1TF0naOa4fSUn98Vd6rXsvAp4I3A-5LaPvNk4RSvKe5fesxKhRzh";
        static bool isSDKready = false;
        public static bool IsSDK_Ready { get { return isSDKready; } }
        public static bool AllowSkipFromMax = true;
#if UNITY_ANDROID
        const string interstitialAdUnitId = "68fdd2d5120a5c4f";
#elif UNITY_IOS
        const string interstitialAdUnitId = "93224a0a406f6cd6";
#else
        const string interstitialAdUnitId = "";
#endif

#if UNITY_ANDROID
        const string rewardedAdUnitId = "6228943f1e354a55";
#elif UNITY_IOS
        const string rewardedAdUnitId = "029dcebce7de6358";
#else
        const string rewardedAdUnitId = "";
#endif

#if UNITY_ANDROID
        const string bannerAdUnitId = "260e9dd32db5829e"; // Retrieve the id from your account
#elif UNITY_IOS
        const string bannerAdUnitId = "0e4d9b21e2409804"; // Retrieve the id from your account
#else
        const string bannerAdUnitId = ""; // Retrieve the id from your account
#endif
        [SerializeField] Color bannerColor = Color.white;
        [SerializeField] bool useInterstitial = true, useRewardedVideoAd = false, useBanner = true;
        [SerializeField] MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
        [SerializeField] bool testModeShowMediationDebugger = false;
        [SerializeField] bool ForceLocationEU_Test = false;
        [SerializeField] GDPRController GDPR_Script;
        OnCompleteAdFuc intersitialDel, rewardedDel;
        static bool hasShownVideoAd = false, hasDismissedVideoAd = false;
        static bool hasShownInterstitial = false, hasDismissedIntersitial = false;
        static bool isShowingBanner = false;
        public static bool IsBanerAdShowing { get { return isShowingBanner; } }
        public static bool gdpr_done
        {
            get; private set;
        }
        private static HardData<bool> _isUserEU;
        public static bool isUserEU
        {
            get
            {
                if (_isUserEU == null) _isUserEU = new HardData<bool>("IS_USER_EU", false);

                Debug.LogFormat("<color='magenta'>returned value is EU {0}</color>", _isUserEU.value);
                return _isUserEU.value;
            }
            set
            {
                Debug.LogFormat("<color='magenta'>user set to isEU {0}</color>", value);
                if (_isUserEU == null) _isUserEU = new HardData<bool>("IS_USER_EU", false);
                _isUserEU.value = value;
            }
        }


        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartAdSystem();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != null)
                {
                    DestroyImmediate(gameObject);
                }
                else
                {
                    DestroyImmediate(this);
                }
            }
        }
        public static void CompleteConsentTask()
        {

#if !UNITY_EDITOR
                MaxSdk.SetHasUserConsent(GDPRController.instance.HasAdPermission);
#endif
            if (GDPRController.instance.HasAnalyticsPermission == false)
            {
                if (GDPRController.instance.AdjustForgetUserHD.value == false)
                {
#if !UNITY_EDITOR
                        Adjust.gdprForgetMe();
#endif
                    Debug.Log("USER FORGOT!!!!!!!!!!!!!!!!!!!!!");
                }
                GDPRController.instance.ForgetUserPermanently_Adjust();
            }

            Debug.Log("<color='red'>permission window done!</color>");

            GameConfig.hasDoneConsentRelatedTasksHD.value = GDPRController.instance.HasCompleteConsent;
            gdpr_done = true;
        }

        private void OnDisable()
        {
            GDPR_Script.OnGDPR_UI_Completion -= CompleteConsentTask;
        }

        //public static bool non_ios_14_5plus = true;
        void StartAdSystem()
        {
#if UNITY_IOS || UNITY_ANDROID
            gdpr_done = false;
            GDPR_Script.OnGDPR_UI_Completion += CompleteConsentTask;
            isShowingBanner = false;
            isSDKready = false;
            MaxSdk.SetVerboseLogging(true);
            MaxSdk.SetSdkKey(sdkKey);
            MaxSdk.InitializeSdk();
            
            //
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                AdjustSDKManager.Instance.StartAdjustAfterMaxSDKInitialized();

                bool isATEEnabled = false;

#if UNITY_IOS || UNITY_IPHONE || UNITY_EDITOR
                if (MaxSdkUtils.CompareVersions(UnityEngine.iOS.Device.systemVersion, "14.5") != MaxSdkUtils.VersionComparisonResult.Lesser)
                {
                    //non_ios_14_5plus = false;

                    //Previous CodeBase
                    //AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);

                    //New CodeBase
                    isATEEnabled = sdkConfiguration.AppTrackingStatus == MaxSdkBase.AppTrackingStatus.Authorized;
                    AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(isATEEnabled);
                    FB.Mobile.SetAdvertiserTrackingEnabled(isATEEnabled);

                    Debug.LogFormat("<color='magenta'>ios 14.5+ detected!! Initiate WTF protocol</color>");
                }
                else
                {
                    Debug.LogFormat("<color='magenta'>normal mode running_ not ios 14.5+</color>");
                }
#endif


                InitializeAndPreloadSelectedAdTypes();
                // AppLovin SDK is initialized, start loading ads
                isSDKready = true;
                LogUtil.Green("ad sdk has been initialized!", true);
                Debug.LogFormat("<color='magenta'> On Max Init</color>");

                if (GameConfig.hasDoneConsentRelatedTasksHD.value == false )
                {
                    if (!isATEEnabled && (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies || ForceLocationEU_Test))
                    {
                        if (GDPR_Script == null)
                        {
                            GameConfig.hasDoneConsentRelatedTasksHD.value = true;
                            gdpr_done = true;

                            Debug.LogFormat("<color='magenta'>A1</color>");
                        }
                        else
                        {
                            // Show user consent dialog
                            GDPR_Script.StartGDPRFromWelcome();
                            Debug.LogFormat("<color='magenta'>A2</color>");
                        }
                        isUserEU = true;
                    }
                    else if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply )
                    {
                        //consentIsRequired = false;
                        // No need to show consent dialog, proceed with initialization
                        GameConfig.hasDoneConsentRelatedTasksHD.value = true;
                        gdpr_done = true;
                        Debug.LogFormat("<color='magenta'>B</color>");
                    }
                    else
                    {
                        // Consent dialog state is unknown. Proceed with initialization, but check if the consent
                        // dialog should be shown on the next application initialization
                        GameConfig.hasDoneConsentRelatedTasksHD.value = false;
                        gdpr_done = true;
                        Debug.LogFormat("<color='magenta'>C</color>");
                    }
                }
                else
                {
                    gdpr_done = true;
                    Debug.LogFormat("<color='magenta'>D</color>");
                }
                
                foreach (var s in ABManager.allSettings)
                {
                    string varName = s.Value.GetID();
                    string varValue = "";
                    varValue = MaxSdk.VariableService.GetString(varName);

                    Debug.Log("<color='magenta'>variable name: " + varName + " and variable value: " + varValue + "</color>");
                    s.Value.Assign_IfUnassigned(varValue);
                }
                ABManager.SetFetchComplete();

                if (testModeShowMediationDebugger)
                {
                    MaxSdk.ShowMediationDebugger();
                }
            };
#endif
        }
        void InitializeAndPreloadSelectedAdTypes()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (useInterstitial)
            {
                InitializeInterstitialAds();
            }

            if (useRewardedVideoAd)
            {
                InitializeRewardedAds();
            }

            if (useBanner)
            {
                InitializeBannerAds();
            }
#endif
        }

        public static void ShowInterstitialAd(OnCompleteAdFuc OnComplete)
        {
            if (instance.intersitialDel != null)
            {
                Debug.Log("<color='red'>previous interstitial callback has not been fired. can not call the method again.</color>");
                return;
            }
#if UNITY_IOS || UNITY_ANDROID
            hasShownInterstitial = hasDismissedIntersitial = false;
            Debug.Log("<color='magenta'>so interstitial ad id: " + interstitialAdUnitId + " and is sdk ready? "
            + isSDKready + " is that interstitial ad ready? " + MaxSdk.IsInterstitialReady(interstitialAdUnitId) + "</color>");
            if (MaxSdk.IsInterstitialReady(interstitialAdUnitId) && isSDKready)
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
                instance.intersitialDel = OnComplete;
            }
            else
            {
                instance.intersitialDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif

        }

        public static bool ShowRewardedVideoAd(OnCompleteAdFuc OnComplete)
        {
            if (instance.rewardedDel != null)
            {
                Debug.Log("<color='red'>previous rewarded ad callback has not been fired. can not call the method again.</color>");
                return false;
            }
#if UNITY_IOS || UNITY_ANDROID
            hasShownVideoAd = hasDismissedVideoAd = false;
            Debug.Log("<color='magenta'>so rewarded ad id: " + rewardedAdUnitId + " and is sdk ready? "
            + isSDKready + " is that rewarded ad ready? " + MaxSdk.IsRewardedAdReady(rewardedAdUnitId) + "</color>");
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId) && isSDKready)
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
                instance.rewardedDel = OnComplete;
                return true;
            }
            else
            {
                instance.rewardedDel = null;
                OnComplete?.Invoke(false);
                return false;
            }
#else
            OnComplete?.Invoke(true);
            return false;
#endif
        }

        public static void ShowBanner()
        {
            if (NoAdManager.isBlockingAds) return;
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == false)
            {
                isShowingBanner = true;
                MaxSdk.ShowBanner(bannerAdUnitId);
            }
#endif
        }

        public static void HideBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == true)
            {
                isShowingBanner = false;
                MaxSdk.HideBanner(bannerAdUnitId);
            }
#endif
        }
       
    }
}