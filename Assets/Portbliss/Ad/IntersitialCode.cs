﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Ad
{
    public partial class AdController : MonoBehaviour
    {
#if UNITY_IOS || UNITY_ANDROID
        void InitializeInterstitialAds()
        {
            // Attach callback
            MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
            MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
            MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
            MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;
            MaxSdkCallbacks.OnInterstitialDisplayedEvent += MaxSdkCallbacks_OnInterstitialDisplayedEvent;
            // Load the first interstitial
            LoadInterstitial();
        }

        void LoadInterstitial()
        {
            if(IsInvoking("LoadInterstitial"))
            {
                CancelInvoke("LoadInterstitial");
            }

            MaxSdk.LoadInterstitial(interstitialAdUnitId);
        }

        void OnInterstitialLoadedEvent(string adUnitId)
        {
            // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
            LogUtil.Green("so interstitial ad loaded.", true);
        }

        void OnInterstitialFailedEvent(string adUnitId, int errorCode)
        {
            // Interstitial ad failed to load. We recommend re-trying in 3 seconds.
            LogUtil.Red("interstitial ad failed to load. We recommend re-trying in 3 seconds", true);
            Invoke("LoadInterstitial", 3);
            //Debug.Log("OnInterstitialFailedEvent at: " + Time.time);
        }

        void MaxSdkCallbacks_OnInterstitialDisplayedEvent(string obj)
        {
            hasShownInterstitial = true;
            if(hasDismissedIntersitial == true)
            {
                intersitialDel?.Invoke(true);
                intersitialDel = null;
            }

            Debug.Log("<color='green'>MaxSdkCallbacks_OnInterstitialDisplayedEvent at: " + Time.time + "</color>");
        }

        void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            // Interstitial ad failed to display. We recommend loading the next ad
            LoadInterstitial();
            intersitialDel?.Invoke(false);
            intersitialDel = null;
            Debug.Log("<color='green'>InterstitialFailedToDisplayEvent at: " + Time.time+"</color>");
        }

        void OnInterstitialDismissedEvent(string adUnitId)
        {
            // Interstitial ad is hidden. Pre-load the next ad
            LoadInterstitial();
            hasDismissedIntersitial = true;
            intersitialDel?.Invoke(hasShownInterstitial);
            intersitialDel = null;
            Debug.Log("<color='green'>OnInterstitialDismissedEvent at: "+Time.time+"</color>");
        }
#endif
    }
}