﻿using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Ad
{
    public class GDPR_SettingsButtonControl : MonoBehaviour
    {
        [SerializeField] Button gdprButtonInSettings;
        void Start()
        {
            gdprButtonInSettings.gameObject.SetActive(false);
           
            StartCoroutine(CheckGDPR());
        }

        IEnumerator CheckGDPR()
        {
            Debug.LogFormat("<color='magenta'>THIS CODE IS NOT SUPPOSED TO RUN!</color>");
            yield return null;
            while (AdController.gdpr_done == false)
            {
                yield return null;
            }
            if (AdController.instance != null)
            {
                HardData<bool> isUserEU = new HardData<bool>("IS_USER_EU", false);
                if (isUserEU.value)
                {
                    gdprButtonInSettings.gameObject.SetActive(true);
                    gdprButtonInSettings.onClick.AddListener(() =>
                    {
                        GDPRController.instance.StartGDPRFromUserRequest();
                    });
                }

                /*
                if (GDPRController.instance.ShowAnnoyance.value == 0 || GDPRController.instance.ShowAnnoyance.value == 1)
                {
                    gdprButtonInSettings.gameObject.SetActive(true);
                    gdprButtonInSettings.onClick.AddListener(() =>
                    {
                        GDPRController.instance.StartGDPRFromSettings();
                    });
                }
                */
            }
        }
    }
}