﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;
using System;

namespace Portbliss.Ad
{
    public class GDPRController : MonoBehaviour
    {
        public static GDPRController instance;
        [SerializeField] string NameOfTheGame;

        public static bool isGDPRFlowOnRequestActive 
        {
            get 
            {
                if (!instance) return false;
                else
                    return instance.isOnRequestGDPRFlowRunning;
            }
        }
        bool isOnRequestGDPRFlowRunning;
        public bool HasCompleteConsent { get { return HasAnalyticsPermission && HasAdPermission; } }
        public bool HasAnalyticsPermission { get { return ShowAnal.value; } }
        public bool HasAdPermission { get { return ShowAd.value; } }
        HardData<bool> ShowAnal;
        HardData<bool> ShowAd;
        public HardData<int> ShowAnnoyance;
        HardData<bool> adjustForgetUserHD;
        public HardData<bool> AdjustForgetUserHD { get { return adjustForgetUserHD; } }

        [SerializeField] Button TopButton;
        [SerializeField] Button BottomButton;
        [SerializeField] Text TopButtonText;
        [SerializeField] Text BottomButtonText;
        [SerializeField] Toggle AnalToggle;
        [SerializeField] Toggle AdToggle;

        [SerializeField] GameObject WelcomePanel;
        [SerializeField] GameObject InfoPanel;
        [SerializeField] GameObject SettingsPanel;
        [SerializeField] GameObject WarningPanel;

        [SerializeField] GameObject GDPRPanel;
        [SerializeField] Button Annoyingbutton;

        public enum PanelState
        {
            WELCOME, INFO, SETTINGS, WARNING
        }

        PanelState currentPanel;
        internal Action OnGDPR_UI_Completion;
        //bool analyticsConsentGiven = false, advertisingConsentGiven = false;
        void Awake ()
        {
            if (instance = this)
            {
                instance = this;
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                }
                
                return;
            }

            adjustForgetUserHD = new HardData<bool>("GDPR_FORGET_ME_HD", false);


            ShowAnal = new HardData<bool> ( "ANALYTICS_ENABLED", true );
            ShowAd = new HardData<bool> ( "AD_ENABLED", true );
            ShowAnnoyance = new HardData<int> ( "CONSENT_STATUS", -1 );


            TopButton.onClick.AddListener ( GoToGame );
            BottomButton.onClick.AddListener ( Matbori );
            AdToggle.onValueChanged.AddListener ( AdToggleCallback );
            AnalToggle.onValueChanged.AddListener ( AnalToggleCallback );
            Annoyingbutton.onClick.AddListener ( StartGDPRFromUserRequest );

            //WelcomeOpen ();

            //if ( ShowAnnoyance.value == 0 )
            //{
            //    ToggleGDPRWindow ( false );
            //    ToggleAnnoyingWindow ( true );
            //}

            if (GameConfig.hasDoneConsentRelatedTasksHD.value == false)
            {
                Annoyingbutton.gameObject.SetActive(false);
            }
        }

        void GoToGame ()
        {
            //if ( currentPanel == PanelState.WARNING )
            //{
            //    SettingsOpen ();
            //}
            //else
            //{
            //    if ( ShowAnal.value == true && ShowAd.value == true )
            //    {
            //        GoToGameAnyway ( true );
            //    }
            //    else
            //    {
            //        WarningOpen ();
            //    }
            //}

            switch ( currentPanel )
            {
                case PanelState.WELCOME:
                    GoToGameAnyway ( true );
                    break;
                case PanelState.INFO:
                    GoToGameAnyway ( true );
                    break;
                case PanelState.SETTINGS:
                    if ( ShowAnal.value == true && ShowAd.value == true )
                    {
                        GoToGameAnyway ( true );
                    }
                    else
                    {
                        //WarningOpen ();
                        GoToGameAnyway(false);
                    }
                    break;
                case PanelState.WARNING:
                    SettingsOpen ();
                    break;
            }
        }

        void Matbori ()
        {
            switch ( currentPanel )
            {
                case PanelState.WELCOME:
                    InfoOpen ();
                    break;
                case PanelState.INFO:
                    SettingsOpen ();
                    break;
                case PanelState.SETTINGS:
                    InfoOpen ();
                    break;
                case PanelState.WARNING:
                    GoToGameAnyway ( false );
                    break;
            }
        }

        public void ForgetUserPermanently_Adjust()
        {
            adjustForgetUserHD.value = true;
        }

        void GoToGameAnyway ( bool consented )
        {
            if (consented) 
            {
                ShowAd.value = true;
                ShowAnal.value = true;
            }
            ToggleGDPRWindow ( false );
            OnGDPR_UI_Completion?.Invoke ();
            ShowAnnoyance.value = consented ? 1 : 0;

            //if ( !consented )
            //{
            //    ToggleAnnoyingWindow ( true );
            //}
        }

        void AdToggleCallback ( bool on )
        {
            ShowAd.value = on;
        }

        void AnalToggleCallback ( bool on )
        {
            ShowAnal.value = on;
        }

        void WelcomeOpen ()
        {
            currentPanel = PanelState.WELCOME;
            TopButtonText.text = "Awesome! I support that!";
            BottomButtonText.text = "Manage Data Settings";
            CloseAllPanels ();
            WelcomePanel.SetActive ( true );
        }

        void InfoOpen ()
        {
            currentPanel = PanelState.INFO;
            TopButtonText.text = "Awesome! I support that!";
            BottomButtonText.text = "Next";
            CloseAllPanels ();
            InfoPanel.SetActive ( true );
        }

        void SettingsOpen ()
        {
            currentPanel = PanelState.SETTINGS;
            TopButtonText.text = "Accept";
            BottomButtonText.text = "Back";
            AnalToggle.isOn = ShowAnal.value;
            
            AdToggle.isOn = ShowAd.value;
            CloseAllPanels ();
            SettingsPanel.SetActive ( true );
        }

        void WarningOpen ()
        {
            currentPanel = PanelState.WARNING;
            TopButtonText.text = "Let me fix my settings";
            BottomButtonText.text = "I understand";
            CloseAllPanels ();
            WarningPanel.SetActive ( true );
        }

        void CloseAllPanels ()
        {
            WelcomePanel.SetActive ( false );
            InfoPanel.SetActive ( false );
            SettingsPanel.SetActive ( false );
            WarningPanel.SetActive ( false );
        }

        public void ToggleGDPRWindow ( bool on )
        {
            GDPRPanel.SetActive ( on );
        }

        public void ToggleAnnoyingWindow ( bool on )
        {
           Annoyingbutton.gameObject.SetActive ( on );
        }

        public void StartGDPRFromWelcome ()
        {
            ToggleGDPRWindow ( true );
            ToggleAnnoyingWindow ( false );
            WelcomeOpen ();
        }

        public void StartGDPRFromUserRequest ()
        {
            
            ToggleGDPRWindow ( true );
            ToggleAnnoyingWindow ( false );
            SettingsOpen ();
            StartCoroutine(StartGDPR_FlowByRequest());
        }

        IEnumerator StartGDPR_FlowByRequest()
        {
            isOnRequestGDPRFlowRunning = true;
            AdController.HideBanner();
            while (GDPRPanel.activeInHierarchy)
            {
                yield return null;
            }
            yield return null;

            AdController.ShowBanner();
            isOnRequestGDPRFlowRunning = false;
        }
    }
}