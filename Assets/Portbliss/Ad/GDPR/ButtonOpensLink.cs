﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Ad
{
    public class ButtonOpensLink : MonoBehaviour
    {

        [SerializeField] string URLtoOpen;
        // Start is called before the first frame update
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(() =>
            {
                Application.OpenURL(URLtoOpen);
                Debug.Log("<color='green'>url:"+URLtoOpen+" has been opened in browser!</color>");
            });
        }
    }
}