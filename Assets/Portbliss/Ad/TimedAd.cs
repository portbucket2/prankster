﻿using  UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using Portbliss;
/// <summary>
/// @rumman vai, vai 
/// This code was not meant to be initialized by your ad controller.
/// I wrote this class with a specific purpose of making ad call logic seperate from gameplay code. 
/// which was dependendant on my way of initialization and broken through your initializer.
/// </summary>
public class TimedAd
{
    //public const bool IGNORE_OTHER_CONDITIONS_TO_ENABLE = true;
    public static TimedAd instance;
    public float lastTime = 0;
    //public const float interval = 10;
    //private static void InitSys()
    //{
    //    if (instance == null)
    //    {
    //        instance = new TimedAd();
    //        instance.lastTime = 0;
    //    }
    //}

    static void AdCore(Action<bool> OnComplete)
    {
        //if (Application.isEditor) Debug.Log("<color=#AA00FF>=================InterStitial===================</color>");
        instance.lastTime = Time.realtimeSinceStartup;
        LifetimeEvents.Report_InterstialShown();
        AdController.ShowInterstitialAd((success) =>
        {
            //if(Application.isEditor) Debug.Log("<color=#AA00FF>=================InterStitial End===================</color>");
            instance.lastTime = Time.realtimeSinceStartup;
            GameConfig.adWatchCountHD.value = GameConfig.adWatchCountHD.value + 1;
            //AnalyticsAssistant.LogAdWatchOneTimeAppsFlyerIfApplicable(GameConfig.adWatchCountHD.value);
            OnComplete?.Invoke(success);
        });
    }

    public static void AdIteration(Action<bool> OnComplete, bool ignoreFirstIteration = false)
    {
#if UNITY_IOS
        if (ABManager.GetValueInt(ABtype.interstitial_disable) == 1) return;
#endif

        float adDelay = 15f;
//#if UNITY_IOS
        //switch (ABManager.GetValueFloat(ABtype.ad_frequency))
        //{
        //    default:
        //        adDelay = ABManager.GetValueFloat(ABtype.ad_frequency);
        //        break;
        //    case 0://remains 15
        //        break;
        //}
//#endif


        //Debug.LogError(adDelay);
        if (instance == null)
        {
            if (Application.isEditor)
            {
                Debug.LogFormat("<color=#AA00FF>first ad iteration {0}</color>", (ignoreFirstIteration ? "ignore" : "shouldPlay"));
            }
            instance = new TimedAd();
            instance.lastTime = Time.realtimeSinceStartup;
            if (ignoreFirstIteration) return;
            else
            {
                instance.lastTime = Time.realtimeSinceStartup - 100000;
            }
        }

        bool willPlayAd = false;
        /*
        //int lvNumAfter = ABManager.GetValue(ABtype.First_Ad);
        int firstBootTime = 0;
        //int adDelay = ABManager.GetValue(ABtype.Ad_Frequency);
        if (Time.realtimeSinceStartup > instance.lastTime + adDelay 
            //&& LevelLoader.instance.GetLastLevelCumulativeNumber() > lvNumAfter
            //&& LevelPrefabManager.hasLevelProgressOccured == true 
            //&& Mathf.Abs(MainGameManager.firstBootTime - Time.realtimeSinceStartup) > firstBootTime 
            //&& GameConfig.hasRewardedVideoAdBeenShown == false
            )
        {
            willPlayAd = true;
        }
        */

        //Portbliss.ABManager.GetValueFloat(Portbliss.ABtype.delay);

        //if (Application.isEditor)
        //{
        //    Debug.LogFormat("<color=#00AAFF>ad timing last: {0}, now {1}</color>", instance.lastTime,Time.realtimeSinceStartup);
        //}
        if (Time.realtimeSinceStartup > instance.lastTime + adDelay
           // && GameConfig.hasRewardedVideoAdBeenShown == false
           )
        {
            willPlayAd = true;
        }

        if (willPlayAd)
        {

            AdCore(OnComplete);
        }
        else
        {
            Debug.Log("so we wont play ad at this time since condition is not favorable");
            OnComplete?.Invoke(false);
        }
    }
    public static void AB_ControlledAd(Action<bool> OnComplete, IntAdPlacement placement)
    {
        if (NoAdManager.isBlockingAds) return;
        bool log = false;
        switch (placement)
        {
            case IntAdPlacement.Success_LevelDeparture:
                return;
                //if (ABManager.GetValueInt(ABtype.inter_point) == 1) 
                //{
                //    if(log)Debug.LogFormat("<color='red'>{0}</color> AdCalled",placement.ToString());
                //    break; 
                //}
                //else return;
            case IntAdPlacement.Success_PreCompletionUI:
                break;
                //if (ABManager.GetValueInt(ABtype.inter_point) == 0)
                //{
                //    if (log) Debug.LogFormat("<color='red'>{0}</color> AdCalled", placement.ToString());
                //    break;
                //}
                //else return;
            case IntAdPlacement.Fail_LevelRestart:
                break;
                //if (ABManager.GetValueInt(ABtype.fail_ad_pos) == 0)
                //{
                //    if (log) Debug.LogFormat("<color='red'>{0}</color> AdCalled", placement.ToString());
                //    break;
                //}
                //else return;
            case IntAdPlacement.Fail_PreFailUI:
                return;
                //if (ABManager.GetValueInt(ABtype.fail_ad_pos) == 1)
                //{
                //    if (log) Debug.LogFormat("<color='red'>{0}</color> AdCalled", placement.ToString());
                //    break;
                //}
                //else return;
            default:
                return;
        }

        AdIteration(OnComplete);

    }


}
public enum IntAdPlacement
{
    Success_LevelDeparture = 0,
    Success_PreCompletionUI = 1,
    Fail_LevelRestart =2, 
    Fail_PreFailUI =3,
}