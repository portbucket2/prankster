﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Ad
{
    public partial class AdController : MonoBehaviour
    {
#if UNITY_IOS || UNITY_ANDROID
        void InitializeRewardedAds()
        {
            // Attach callback
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

            // Load the first RewardedAd
            LoadRewardedAd();
        }

        void LoadRewardedAd()
        {
            if(IsInvoking("LoadRewardedAd"))
            {
                CancelInvoke("LoadRewardedAd");
            }

            MaxSdk.LoadRewardedAd(rewardedAdUnitId);
        }

        void OnRewardedAdLoadedEvent(string adUnitId)
        {
            LogUtil.Green("so rewarded ad loaded.", true);
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
        }

        void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
            LogUtil.Red("Rewarded ad failed to load. We recommend re-trying in 3 seconds", true);
            Invoke("LoadRewardedAd", 3);
        }

        void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            // Rewarded ad failed to display. We recommend loading the next ad
            LogUtil.Red("Rewarded ad failed to display. We recommend loading the next ad", true);
            LoadRewardedAd();
            rewardedDel?.Invoke(false);
            rewardedDel = null;
        }

        void OnRewardedAdDismissedEvent(string adUnitId)
        {
            // Rewarded ad is hidden. Pre-load the next ad
            LogUtil.Green("Rewarded ad is hidden. Pre-load the next ad", true);
            LoadRewardedAd();
            hasDismissedVideoAd = true;
            rewardedDel?.Invoke(hasShownVideoAd);
            rewardedDel = null;
        }

        void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        {
            LogUtil.Green("Rewarded ad rewarded!", true);
            hasShownVideoAd = true;
            if (hasDismissedVideoAd == true)
            {
                rewardedDel?.Invoke(true);
                rewardedDel = null;
            }
        }
#endif
    }
}