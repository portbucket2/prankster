﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Ad
{
    public partial class AdController : MonoBehaviour
    {
#if UNITY_IOS || UNITY_ANDROID
        void InitializeBannerAds()
        {
            // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
            // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
            MaxSdk.CreateBanner(bannerAdUnitId, instance.bannerPosition);
            // Set background or background color for banners to be fully functional
            MaxSdk.SetBannerBackgroundColor(bannerAdUnitId, instance.bannerColor);
        }
#endif
    }
}