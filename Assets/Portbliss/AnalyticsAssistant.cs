﻿using com.adjust.sdk;
using Portbliss.Analytics;
using System.Collections.Generic;
using UnityEngine;
public static class AnalyticsAssistant 
{

#if UNITY_EDITOR
    static bool logToConsole = true;
#else
    static bool logToConsole = false;
#endif

    //public static void LevelStarted(int levelNo, string gameplay, int layerCount)
    //{
    //    if (logToConsole) Debug.LogFormat("Started Level {0}, gameplay: {1}, height {2}", levelNo, gameplay,layerCount);
    //    AnalyticsController.LogLevelStarted(levelNo, gameplay, layerCount);
    //}
    //public static void LevelCompleted(int levelNo, int stars, string gameplay, int layerCount)
    //{
    //    if (logToConsole) Debug.LogFormat("Completed Level {0} with stars {3}, gameplay: {1}, height {2}", levelNo, gameplay, layerCount, stars);
    //    AnalyticsController.LogLevelCompleted(levelNo,stars,gameplay,layerCount);
    //}
    //public static void LevelRestarted(int levelNo, string gameplay, int layerCount)
    //{
    //    if (logToConsole) Debug.LogFormat("Restartd Level {0}, gameplay: {1}, height {2}", levelNo, gameplay, layerCount);
    //    AnalyticsController.LogLevelReStarted(levelNo, gameplay, layerCount);
        
    //}
    
    public static void LogLevelStarted(int level)
    {
        LocalLog( string.Format("{0}", level));
        AnalyticsController.LogLevelStarted(level);
    }
    
    public static void LogStepStart(int level, int step)
    {
        LocalLog(string.Format("{0}_{1}", level,step));
        AnalyticsController.LogStepStart(level,step);
    }

    public static void LogStepSuccess(int level, int step)
    {

        LocalLog(string.Format("WIN {0}_{1}", level, step));
        AnalyticsController.LogStepSuccess(level,step);
    }
    public static void LogStepFailed(int level, int step)
    {
        LocalLog(string.Format("FAIL {0}_{1}", level, step));
        AnalyticsController.LogStepFailed(level,step);
    }


    public static void LogRv(RVState rvState, RVPlacement placement, int level = -1)
    {
        switch (rvState)
        {
            case RVState.rv_click:
                LifetimeEvents.Report_RVClicked();
                break;
            case RVState.rv_start:
                break;
            case RVState.rv_complete:
                LifetimeEvents.Report_RVWatched();
                break;
            default:
                break;
        }
        if(level <0) level = LevelManager.SelectedLevelNumber;
        LocalLog(string.Format("RVLog {0} lvl:{1}, placement: {2}", rvState.ToString(),level, placement.ToString()));
        AnalyticsController.LogRewardedVideos(rvState.ToString(), level, placement.ToString());
    }
    static Dictionary<string, string> adjust_EventToken_Dictionary;
    public static void LogLifeTimeEvent(string lifeTimeEventName)
    {
        if (adjust_EventToken_Dictionary == null)//initialize token dictionary
        {
            adjust_EventToken_Dictionary = new Dictionary<string, string>();
#if UNITY_ANDROID
            adjust_EventToken_Dictionary.Add("d3_retained", "75vx40");
            adjust_EventToken_Dictionary.Add("d7_retained", "laedbj");
            adjust_EventToken_Dictionary.Add("watch_inter_10", "xl27un");
            adjust_EventToken_Dictionary.Add("watch_inter_25", "fa7vmb");
            adjust_EventToken_Dictionary.Add("watch_rewarded_5", "df4m4b");
            adjust_EventToken_Dictionary.Add("watch_rewarded_15", "7wb697");
            adjust_EventToken_Dictionary.Add("click_reward_5", "ie7sf1");
            adjust_EventToken_Dictionary.Add("click_reward_15", "sjq95f");
            adjust_EventToken_Dictionary.Add("level_achieved_10", "he9eam");
            adjust_EventToken_Dictionary.Add("level_achieved_20", "g47ti1");
            adjust_EventToken_Dictionary.Add("completed_all_levels", "gml507");
#elif UNITY_IOS
            adjust_EventToken_Dictionary.Add("d3_retained", "ux5wg3");
            adjust_EventToken_Dictionary.Add("d7_retained", "m1whk7");
            adjust_EventToken_Dictionary.Add("watch_inter_10", "jzd0g0");
            adjust_EventToken_Dictionary.Add("watch_inter_25", "rx7fyc");
            adjust_EventToken_Dictionary.Add("watch_rewarded_5", "g5brho");
            adjust_EventToken_Dictionary.Add("watch_rewarded_15", "4lh4bx");
            adjust_EventToken_Dictionary.Add("click_reward_5", "5e3ten");
            adjust_EventToken_Dictionary.Add("click_reward_15", "w9pgy7");
            adjust_EventToken_Dictionary.Add("level_achieved_10", "g4wq0j");
            adjust_EventToken_Dictionary.Add("level_achieved_20", "mnz64a");
            adjust_EventToken_Dictionary.Add("completed_all_levels", "yyz6rd");
#endif
        }

        AdjustEvent adjustEvent = null;
        if (adjust_EventToken_Dictionary.ContainsKey(lifeTimeEventName))
        {
            string token = adjust_EventToken_Dictionary[lifeTimeEventName];
            adjustEvent = new AdjustEvent(token);
        }
        else
        {
            Debug.LogError("Adjust event key missing");
        }
        Adjust.trackEvent(adjustEvent);

        AnalyticsController.LogLifeTimeEvent(lifeTimeEventName);
        Debug.LogFormat("<color='red'>{0}</color>", lifeTimeEventName);
    }
    public static void LogLevelCompleted(int level)
    {

        LocalLog(string.Format("Completed======================= {0}", level));
        AnalyticsController.LogLevelCompleted(level);
    }
    static void LocalLog(string log)
    {
        if (logToConsole)
            Debug.LogFormat("<color=#FFFF00>{0}</color>",log);
    }

    //public static void SwitchedEarlyToSpatula(int levelNo)
    //{
    //    if (logToConsole) Debug.LogFormat("Switched To Spatula manually at level no: {0}", levelNo);
    //    AnalyticsController.LogToolSwitchedTo(levelNo,"Spatula");
    //}

    //public static void SpeedChanged()
    //{
    //    if (logToConsole) Debug.LogFormat("Speed was changed");
    //    AnalyticsController.LogSpeedChanged();
    //}

    //public static void LogABTesting(string abType, string abValue)
    //{
    //    Debug.Log("<color='green'>AB value choice made type:" + abType + " and ab value: " + abValue+" in Analytics assistant</color>");
    //    AnalyticsController.LogABTesting(abType, abValue);
    //}

    //public static void LevelCompletedAppsFlyerIfItIsProperLevel(int lvNow)
    //{
    //    List<int> lvList = GameConfig.levelCompletionAppsflyerOneTimeHD.AllNumbers;
    //    if (lvNow >= 1)
    //    {
    //        foreach (var v in lvList)
    //        {
    //            HardData<bool> hd = GameConfig.levelCompletionAppsflyerOneTimeHD.GetConfigForLevel(v); //new HardData<bool>("LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_" + v, false);
    //            if (hd.value == false && lvNow >= v)
    //            {
    //                Debug.Log("<color='magenta'>level achieved num: " + v + "</color>");

    //                string evToken_levelCompleted = GameConfig.levelCompletedAdjustEvents[v];
    //                AdjustEvent adjustEvent = new AdjustEvent(evToken_levelCompleted);
    //                Adjust.trackEvent(adjustEvent);
    //                hd.value = true;
    //            }
    //        }
    //    }
    //}


//    public static void LogRewardedVideoAdStart(int levelnumber)
//    {
//        Debug.Log("<color=magenta>'LogRewardedVideoAdStart' call</color>");
//        AnalyticsController.LogRewardedVideoAdStart(levelnumber);
//    }

//    public static void LogRewardedVideoAdComplete(int levelnumber)
//    {
//        Debug.Log("<color=magenta>'LogRewardedVideoAdComplete' call</color>");
//        AnalyticsController.LogRewardedVideoAdComplete(levelnumber);
//    }

//    public static void LogAdWatchOneTimeAppsFlyerIfApplicable(int lvNow)
//    {
//        List<int> lvList = GameConfig.adWatchNTimesCompletedAppsflyerHD.AllNumbers;
//        if (lvNow >= 1)
//        {
//            foreach (var v in lvList)
//            {
//                HardData<bool> hasAdWatched = GameConfig.adWatchNTimesCompletedAppsflyerHD.GetConfigForLevel(v);// new HardData<bool>("ads_watched_HD_" + v, false);
//                if (hasAdWatched.value == false && lvNow >= v)
//                {
//                    string adWatchEventToken = "";
//                    try
//                    {
//                        adWatchEventToken = GameConfig.adWatchAdjustEvents[v];
//                    }
//                    catch (System.Exception ex)
//                    {
//                        Debug.Log("token retrieve error, please check the dictionary in 'GameConfig' script.");
//                    }
//                    AdjustEvent adjustEvent = new AdjustEvent(adWatchEventToken);
//                    Adjust.trackEvent(adjustEvent);
//                    Debug.Log("<color='magenta'>ad watch num: " + v + "</color>");
//                    hasAdWatched.value = true;
//                }
//            }
//        }
//    }


//    public static void LogPurchase(string currencyCode, string amount, string transactionID)
//    {
        
//        string purchaseEventToken = "mzap7z";
//#if UNITY_ANDROID
//        purchaseEventToken = "mzap7z";
//#elif UNITY_IOS
//        purchaseEventToken = "9m1js7";
//#endif

//        AdjustEvent adjustEvent = new AdjustEvent(purchaseEventToken);
//        double revAmount = 0;
//        double.TryParse(amount, out revAmount);
//        adjustEvent.setRevenue(revAmount, currencyCode);
//        adjustEvent.setTransactionId(transactionID);
//        Adjust.trackEvent(adjustEvent);

//        Debug.Log("<color='magenta'>a_f purchase analytics method</color>");

//    }
}

public enum RVState
{
    rv_click,
    rv_start,
    rv_complete,
}
public enum RVPlacement
{
    currency_multiplier,
    skin_shop_unlock,
    room_shop_unlock,
    revive,
    collectible_item_unlock,
    rewarded_level_unlock,
    rewarded_item_unlock,
    rv_option_selected,
}