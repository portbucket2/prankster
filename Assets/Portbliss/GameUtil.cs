﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameUtil
{
    public static void LogGreen(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='green'>" + msg + "</color>");
        }
    }

    public static void LogRed(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='red'>" + msg + "</color>");
        }
    }

    public static void LogBlue(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='blue'>" + msg + "</color>");
        }
    }

    public static void LogMagenta(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='magenta'>" + msg + "</color>");
        }
    }

    public static void LogYellow(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='yellow'>" + msg + "</color>");
        }
    }

    public static void LogBlack(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='black'>" + msg + "</color>");
        }
    }

    public static void LogWhite(string msg, bool willShow = true)
    {
        if (willShow)
        {
            Debug.Log("<color='white'>" + msg + "</color>");
        }
    }
}
