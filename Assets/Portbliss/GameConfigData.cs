﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;

public class OneTimeEventConfig
{
    public class ConfigLvPair
    {
        public HardData<bool> config;
        public int lvNum;

        public ConfigLvPair(string keyName, int num)
        {
            config = new HardData<bool>(keyName, false);
            lvNum = num;
        }
    }

    List<ConfigLvPair> allConfigs;
    List<int> allNumbers;
    public List<int> AllNumbers { get { return allNumbers; } }
    public OneTimeEventConfig(List<int> lvList, string keyStartingName)
    {
        allConfigs = new List<ConfigLvPair>();
        allNumbers = new List<int>();
        foreach (var v in lvList)
        {
            ConfigLvPair p = new ConfigLvPair(keyStartingName + v, v);
            allConfigs.Add(p);
            allNumbers.Add(v);
        }
    }

    public HardData<bool> GetConfigForLevel(int lv)
    {
        HardData<bool> sd = allConfigs == null || allConfigs.Count == 0 ? null : allConfigs[0].config;
        foreach (var v in allConfigs)
        {
            if (v.lvNum == lv)
            {
                sd = v.config;
                break;
            }
        }
        return sd;
    }
}