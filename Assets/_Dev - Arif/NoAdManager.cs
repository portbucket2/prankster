﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using Portbliss.IAP;
using Portbliss.Ad;
public class NoAdManager 
{
    public static bool isBlockingAds 
    { 
        get 
        {
            return IAPManager.isADBlocked;
        }
    }

    public static bool ShowPurchaseButton
    {
        get
        {
#if UNITY_IOS
            return false;
#elif UNITY_ANDROID
            if (isBlockingAds) return false;
            return true;
#endif
        }
    }

    // Update is called once per frame
    public static void RequestNoAdPurchase(System.Action<bool> onSuccess)
    {
        if (IAPManager.instance)
        {
            IAPManager.instance.BuyRemoveAds((success) =>
            {
                if (success)
                {
                    BannarController.DisableBanner();
                    CrossPromoController.Active(false);
                }
                onSuccess?.Invoke(success);

            });
        }
        else
        {   
            BannarController.DisableBanner();
            onSuccess?.Invoke(true);
        }

    }

}
