﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleDisplayController : MonoBehaviour
{
    public List<Transform> roots;
    public List<CollectibleTies> ties;


    private void Awake()
    {
        foreach (var item in ties)
        {
            item.profile = PrankCollectibleManager.GetProfile_ByID(item.id);
            item.go.SetActive(item.profile.unlocked.value && PrankCollectibleManager.enableCollectibles);
        }
        RootManagement();
        if (!PrankCollectibleManager.enableCollectibles) return;
        PrankCollectibleManager.onPrankCollectibleUnlocked += OnCollectibleUnlocked;
    }
    private void OnDestroy()
    {
        PrankCollectibleManager.onPrankCollectibleUnlocked -= OnCollectibleUnlocked;
    }
    private void RootManagement()
    {
        foreach (Transform root in roots)
        {
            root.gameObject.SetActive(false);
            foreach (Transform item in root)
            {
                if (item.gameObject.activeSelf)
                {
                    root.gameObject.SetActive(true);
                    break;
                }
            }
        }
    }

    private void OnCollectibleUnlocked(CollectibleType id)
    {
        foreach (var item in ties)
        {
            if (item.id == id)
            {
                item.go.SetActive(true);
                RootManagement();
                return;
            }
        }
    }

    [System.Serializable]
    public class CollectibleTies
    {
        public GameObject go;
        public CollectibleType id;
        internal PrankCollectibleProfile profile;
    }
}
