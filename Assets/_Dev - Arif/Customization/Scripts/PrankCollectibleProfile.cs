﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class PrankCollectibleProfile
{
    public CollectibleType id;
    public Sprite displaySprite;
    internal HardData<bool> unlocked;
    public string levels;
    [System.NonSerialized] internal List<string> levelIDs = new List<string>();

    public PrankCollectibleProfile()
    { 
    }
    public PrankCollectibleProfile(CollectibleType id)
    {
        this.id = id;
    }
}
public enum CollectibleType
{
    Pogostick = 1,
    BoomBox = 2,
    Dynamite =3,
    Rocket = 4,
    Iron =5,
    BowlingBall =6,
    Bucket =7,
    Boomerang = 8,
    Casper =9,
    Flytrap = 10,
    RatTrap = 11,
    SkateBoard = 12,
}
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(PrankCollectibleProfile))]
public class PrankCollectibleProfileEditor : PropertyDrawer
{
    const float ROW_COUNT = 3.5f;
    const float GAP = 5; 
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUIUtility.labelWidth = 0;
        label.text = string.Format("");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float elementHeight = (position.height - GAP) / ROW_COUNT;
        float imageOffset = position.height;

        float labelWidth = 60;

        var label_idRect = new Rect(position.x + imageOffset, position.y, labelWidth, elementHeight);
        var label_integerRect = new Rect(position.x + imageOffset, position.y + elementHeight + GAP, labelWidth, elementHeight);

        var idRect = new Rect(position.x + labelWidth + imageOffset, position.y, position.width - imageOffset - labelWidth, elementHeight);
        var levelsRect = new Rect(position.x + labelWidth + imageOffset, position.y + elementHeight + GAP, position.width - imageOffset - labelWidth, elementHeight);
        
        var imageRect = new Rect(position.x, position.y, imageOffset, position.height);

        EditorGUI.LabelField(label_idRect, "ID");
        EditorGUI.LabelField(label_integerRect, "Levels");

        //EditorGUI.LabelField(labelRect, "isRight?");
        EditorGUI.PropertyField(idRect, property.FindPropertyRelative("id"), GUIContent.none);
        EditorGUI.PropertyField(levelsRect, property.FindPropertyRelative("levels"), GUIContent.none);
        //EditorGUI.PropertyField(imageRect, property.FindPropertyRelative("itemSprite"), GUIContent.none);
        property.FindPropertyRelative("displaySprite").objectReferenceValue = EditorGUI.ObjectField(imageRect, property.FindPropertyRelative("displaySprite").objectReferenceValue, typeof(Sprite), false);


        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * ROW_COUNT + GAP;
    }
}
#endif