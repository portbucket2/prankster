﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "ShopGroupManager", menuName = "Customization/ShopGroupManager", order = 1)]
public class ShopGroupManager : ScriptableObject
{

    public ShopType shopType;
    public List<CategoryTab> customizationGroups = new List<CategoryTab>();
    public List<ISlidable> tablist
    {
        get
        {
            EnsureInit();
            List<ISlidable> islist = new List<ISlidable>();
            islist.AddRange(customizationGroups);
            return islist;
        }
    }



    [System.NonSerialized]
    private bool initialized = false;
    private Dictionary<int, HardData<int>> choices = new Dictionary<int, HardData<int>>();

    public event System.Action<int,int> onChoiceChanged;
    public void SetChoice(int type, int choice)
    {
        if (!choices.ContainsKey(type))
        {
            choices.Add(type, new HardData<int>(string.Format("shop{0}_category{1}_choice",shopType, type), 0));
        }
        choices[type].value = choice;
        onChoiceChanged?.Invoke(type,choice);
    }
    public int GetChoice(int type)
    {
        if (!choices.ContainsKey(type))
        {
            choices.Add(type, new HardData<int>(string.Format("shop{0}_category{1}_choice", shopType, type), 0));
        }

        return choices[type];
    }

    
    private void EnsureInit()
    {
        if (initialized) return;
        initialized = true;
        for (int cg = 0; cg < customizationGroups.Count; cg++)
        {
            List<ISlidable> list = customizationGroups[cg].slideItemList;
            for (int i = 0; i < list.Count; i++)
            {
                BasicItemProfile bip = list[i] as BasicItemProfile;
                bip.unlocked = new FRIA.HardData<bool>(string.Format("shop_{0}_category{1}_item{2}", shopType, customizationGroups[cg].title, bip.id), SpecialBuildManager.allPurchaseUnlocked);
                int choiceIndex = i + 1;
                int cat = cg;
                bip.onSelectionRequest += () =>
                {
                    SetChoice(cat, choiceIndex);
                };
            }
        }
    }

    public BasicItemProfile GetItemProfile(int type)
    {
        EnsureInit();
        CategoryTab ct  = customizationGroups[type];
        int index = GetChoice(type);
        if (index < 1 || index > ct.slideItemList.Count) return null;

        return  ct.slideItemList[index - 1] as BasicItemProfile;
    }

    
}

public enum ShopType
{
    SKIN = 0,
    ROOM = 1,
}

#if UNITY_EDITOR
[CustomEditor(typeof(ShopGroupManager))]
public class ShopGroupMan_Editor : Editor
{
    private const string BASE_SPRITE_PATH = "Assets/UI/LionUI/Categories";
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ShopGroupManager sgm = (ShopGroupManager)target;
        if (GUILayout.Button("Load Sprites From Path"))
        {
            foreach (CategoryTab ct in sgm.customizationGroups)
            {
                string fullPath1 = string.Format("{0}/{1}/regular/{2}.png",BASE_SPRITE_PATH,sgm.shopType.ToString(),ct.name);
                string fullPath2 = string.Format("{0}/{1}/selected/{2}.png", BASE_SPRITE_PATH, sgm.shopType.ToString(), ct.name);
                Sprite sprite1 = AssetDatabase.LoadAssetAtPath(fullPath1, typeof(Sprite)) as Sprite;
                Sprite sprite2 = AssetDatabase.LoadAssetAtPath(fullPath2, typeof(Sprite)) as Sprite;
                Debug.Log(sprite1);
                Debug.Log(sprite2);
                ct.displaySprite = sprite1;
                ct.displaySprite_seleceted = sprite2;
                EditorFix.SetObjectDirty(ct);
                AssetDatabase.SaveAssets();
            }
        }
    }
} 

#endif