﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "SkinProfileManager", menuName = "Customization/SkinProfileManager", order = 1)]
public class SkinProfileManager : ScriptableObject
{
    public static List<Action> onSkinChangeActions = new List<Action>();
    
    public static void AddListnerToOnSkinChange(System.Action act)
    {
        EnsureInit();
        onSkinChangeActions.Add(act);
    }
    public static void RemoveListnerFromOnSkinChange(System.Action act)
    {
        EnsureInit();
        onSkinChangeActions.Remove(act);
    }

    public static void SelectSkin(SkinType type, int choice)
    {
        EnsureInit();
        resourceRef.skinShopGroup.SetChoice((int)type, choice);
    }
    public static int GetSkinChoice(SkinType type)
    {
        EnsureInit();
        return resourceRef.skinShopGroup.GetChoice((int) type);
    }
    private static List<TextureItemProfile> GetList(SkinType type)
    {
        EnsureInit();
        return (resourceRef.skinShopGroup.customizationGroups[(int)type] as CategoryTab_Texture).texturetabContents;
    }

    public Texture2D mainTexture;
    public Texture2D mappingTexture;
    public ShopGroupManager skinShopGroup;
    //public CustomizationType type;




    public static SkinProfileManager resourceRef;
    public static void EnsureInit()
    {
        if (resourceRef) return;
        resourceRef =  Resources.Load<SkinProfileManager>("SkinProfileManager");
        resourceRef.skinShopGroup.onChoiceChanged += (int c, int i) =>
        {
            foreach (var item in onSkinChangeActions)
            {
                item?.Invoke();
            }
        };
    }

    public static Texture2D GetMainTexture()
    {
        EnsureInit();
        return resourceRef.mainTexture;
    }
    public static Texture2D GetMapTexture()
    {
        EnsureInit();
        return resourceRef.mappingTexture;
    }

    public static TextureItemProfile GetItemProfile(SkinType type, int index)
    {
        EnsureInit();
        List<TextureItemProfile> list = GetList(type);
        if (index < 1 || index > list.Count) return null;
        return list[index-1];
    }


    public static Texture2D GetSkin(SkinType type)
    {
        EnsureInit();
        int choiceIndex = GetSkinChoice(type);
        TextureItemProfile profile = GetItemProfile(type, choiceIndex);

        return profile != null ? profile.texture : null;
    }
}


public enum SkinType
{
    hair = 0,
    shirt = 1,
    pant = 2,
    flesh = 3,
}
