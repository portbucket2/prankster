﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManagementBridge : MonoBehaviour
{
    public static LevelManagementBridge instance;

    public static int latestReachedIndesx
    {
        get
        {
            return LevelManager.GetLatestReachedLevelNumber()-1;
        }
    }
    public static int currentIndex
    {
        get
        {
            return LevelManager.SelectedLevelNumber - 1;
        }
    }



    public Text levelText;
    private void Awake()
    {
        instance = this;

        levelText.text = string.Format("Level {0}",currentIndex+1);
    }
}

