﻿using Portbliss.Ad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LevelUIItemLoader : MonoBehaviour
{
    public GameObject mainObject;
    public Button mainButton;

    public Image bgImage;

    LevelData levelData;

    public Sprite sprite_Unlocked;
    public Sprite sprite_Locked;
    public Sprite sprite_Current;


    public GameObject onLockContent;
    public GameObject onUnLockContent;
    public GameObject onRewardedContent;
    public Text levelName;
    public Text bonusName;
    public void LoadNormal(int levelIndex)
    {
        //LevelArea levelArea = LevelDataKeeper.instance.levelAreas[areaIndex];
        levelData = BuildSceneInfo.Instance.levels[levelIndex];

        mainObject.SetActive(true);

        bool isProgressUnlocked = SpecialBuildManager.allLevelUnlocked || LevelManagementBridge.latestReachedIndesx>=levelIndex;// LevelDataKeeper.instance.allLevelsUnlocked || LevelDataKeeper.instance.levelAreas[areaIndex].lastUnlockedLevelIndex.value >= levelIndex;
        bool isNowPlaying = levelIndex == LevelManagementBridge.currentIndex;// (PrimaryLoader.currentLevelInstance!=null && PrimaryLoader.currentLevelInstance.levelDefinition == levelDef && PrimaryLoader.currentLevelInstance.loadType == LoadType.NORMAL);

        mainButton.interactable = isProgressUnlocked;


        onLockContent.SetActive(!isProgressUnlocked);
        onUnLockContent.SetActive(isProgressUnlocked && !levelData.isBonusLocked);
        onRewardedContent.SetActive(isProgressUnlocked && levelData.isBonusLocked);

        levelName.gameObject.SetActive(isProgressUnlocked && !levelData.isBonus);
        bonusName.gameObject.SetActive(isProgressUnlocked && levelData.isBonus && !levelData.isBonusLocked);

        bgImage.sprite = isNowPlaying ? sprite_Current : (isProgressUnlocked?sprite_Unlocked:sprite_Locked);

        levelName.text = string.Format("{0}", levelIndex + 1);

        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(()=> {
            if (levelData.isBonusLocked)
            {
                mainButton.interactable = false;
                AnalyticsAssistant.LogRv(RVState.rv_click, RVPlacement.rewarded_level_unlock);
                bool willShow = AdController.ShowRewardedVideoAd((bool success) =>
                {
                    if (success)
                    {
                        levelData.UnlockBonusLevel();
                        LevelManager.LoadLevelDirect(false, levelIndex + 1);
                        AnalyticsAssistant.LogRv(RVState.rv_complete, RVPlacement.rewarded_level_unlock);
                    }
                    else
                    {
                        mainButton.interactable = true;
                    }
                });
                if(willShow) AnalyticsAssistant.LogRv(RVState.rv_start, RVPlacement.rewarded_level_unlock);
            }
            else
            {

                LevelManager.LoadLevelDirect(true, levelIndex + 1);
            }
        });
    }
    public void LoadAsDummy()
    {
        mainObject.SetActive(false);
    }

   
}