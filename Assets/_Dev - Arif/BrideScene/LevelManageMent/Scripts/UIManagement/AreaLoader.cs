﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using FRIA;

public class AreaLoader : MonoBehaviour
{
    public Text areaTitle;
    public GameObject levelUIPrefab;
    public RectTransform contentPanel;

    public event System.Action onRefresh;

    public bool centreObjectIfNecessaryFor3ItemColumn = true;

    private List<LevelUIItemLoader> selectionItems = new List<LevelUIItemLoader>();

    public LevelUIItemLoader GetClosestUIItem(int levelIndex)
    {
        if (levelIndex >= selectionItems.Count) return selectionItems[selectionItems.Count - 1];
        else return selectionItems[levelIndex];
    }
    public int ItemCount { get { return selectionItems.Count; } }
    public void Load()
    {
        //LevelArea area = LevelDataKeeper.instance.levelAreas[areaIndex];


        for (int i = selectionItems.Count-1; i >=0 ; i--)
        {
            Pool.Destroy(selectionItems[i].gameObject);
            selectionItems.RemoveAt(i);
        }
        int N = BuildSceneInfo.Instance.levelCount;
        int m = N % 3;
        bool addOne = centreObjectIfNecessaryFor3ItemColumn && m == 1;
        //Debug.Log(addOne);

        for (int i = 0; i < BuildSceneInfo.Instance.levelCount + (addOne?1:0); i++)
        {
            Transform tr = Pool.Instantiate(levelUIPrefab).transform;
            tr.SetParent(contentPanel);
            tr.localScale = Vector3.one;
            tr.rotation = Quaternion.identity;

            selectionItems.Add(tr.GetComponent<LevelUIItemLoader>());
        }
        Refresh(addOne);
    }

    public void Refresh(bool hasExtra)
    {
        onRefresh?.Invoke();





        if (hasExtra)

        {
            //Debug.Log("B");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                if (i == selectionItems.Count - 2)
                {
                    selectionItems[i].LoadAsDummy();
                }
                else if (i == selectionItems.Count - 1)
                {
                    selectionItems[i].LoadNormal(i-1);
                }
                else
                {
                    selectionItems[i].LoadNormal(i);
                }
            }
        }
        else
        {
            //Debug.Log("A");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                selectionItems[i].LoadNormal(i);
            }
        }
    }
}