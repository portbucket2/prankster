﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class StarGazer : MonoBehaviour
{


    private static HardData<int> _starsAvailable;
    public static int StarsAvailable
    {
        set
        {
            if (_starsAvailable == null)
            {
                _starsAvailable = new HardData<int>("TIMING_STARS", 0);
            }
            _starsAvailable.value = value;

            onStarCountChanged?.Invoke(value);
        }

        get
        {
            if (_starsAvailable == null)
            {
                _starsAvailable = new HardData<int>("TIMING_STARS", 0);
            }
            return _starsAvailable;
        }
    }

    private const float timing_ratio_for_3star = 1.00f;
    private const float timing_ratio_for_2star = 1.50f;


    public static System.Action<int> onStarCountChanged;
    //public static int StarEarnableForTiming(LevelDefinition definition, float secondsToComplete)
    //{
    //    float expectedT = definition.standardTiming;
    //    if (secondsToComplete < timing_ratio_for_3star * expectedT)
    //    {
    //        return 3;
    //    }
    //    else if (secondsToComplete < timing_ratio_for_2star * expectedT)
    //    {
    //        return 2;
    //    }
    //    else
    //    {
    //        return 1;
    //    }
    //}


    public static void AddStarsForLevelCompletion(int count)
    {
        StarsAvailable += count;
    }

    public static void ConsumeStars(int stars)
    {
        StarsAvailable -= stars;
    }

    // mono -----------------------------------------


    public Text starCountText;
    private void Start()
    {
        ChangeStarText(StarsAvailable);
        onStarCountChanged += ChangeStarText;
    }
    private void OnDestroy()
    {
        onStarCountChanged -= ChangeStarText;
    }
    private void ChangeStarText(int count)
    {
        starCountText.text = count.ToString();
    }



}
