﻿using FRIA;
using Portbliss.Ad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BridgeSceneManager : MonoBehaviour
{
    public static BridgeSceneManager instance;

    public Transform selectableModels;

    public BridgeState state { get { return switcheroo.currentState; } }
    public bool allowSwipe
    {
        get
        {
            return state == BridgeState.IDLE;
        }
    }

    public GameObject noAdGameObject;
    [Header("Buttons")]
    public Button noAdButton;
    public Button levelsButton;
    public Button skinShopButton;
    public Button roomShopButton;

    [Header("References")]
    public List<GameObject> idleList;
    public List<GameObject> loaderList;
    public List<GameObject> shopList;
    public List<GameObject> settingsList;


    StateSwitchController<BridgeState> switcheroo;

    private void Start()
    {
        int index = Random.Range(0, selectableModels.childCount);
        int i = 0;
        foreach (Transform item in selectableModels)
        {
            item.gameObject.SetActive(i==index);
            i++;
        }

        instance = this;

        switcheroo = new StateSwitchController<BridgeState>();
        switcheroo.AddState(BridgeState.Leave, new List<GameObject>());
        switcheroo.AddState(BridgeState.IDLE, idleList);
        switcheroo.AddState(BridgeState.IDLE_NO_INPUT, idleList);
        switcheroo.AddState(BridgeState.LEVEL_LOAD, loaderList);
        switcheroo.AddState(BridgeState.SHOP, shopList);
        switcheroo.AddState(BridgeState.SETTINGS, settingsList);
        SwtichState(BridgeState.IDLE);

        levelsButton.onClick.AddListener(OnLevelListShowButton);
        skinShopButton.onClick.AddListener(OnSkinShopButton);
        roomShopButton.onClick.AddListener(OnRoomShopButton);
        noAdButton.onClick.AddListener(OnNoAdButton);

        noAdGameObject.SetActive(NoAdManager.ShowPurchaseButton);
    }

    public void OnTapDetected(Vector3 mousePos)
    {
        if (GDPRController.isGDPRFlowOnRequestActive) return;
        if (SpecialBuildManager.enableUIToggle )
        {
            if (mousePos.y > (0.75f * (float)Screen.height)) return;
        }

        Centralizer.Add_DelayedAct(() =>
        {
        if (state == BridgeState.IDLE) 
            {
                if (LevelProgressionController.Proceed_IsBonus(fromHome: true))
                {
                    SwtichState(BridgeState.Leave);
                }
                //LevelManager.LoadLevelDirect(autoTapEnable: true);
            }
        }, 0);

    }

    void SwtichState(BridgeState state)
    {
        switch (state)
        {
            case BridgeState.IDLE:
                CrossPromoController.Active(true);
                break;
            default:
                CrossPromoController.Active(false);
                break;
        }
        switcheroo.SwtichState(state);
    }


    public void OnNoAdButton()
    {
        SwtichState(BridgeState.IDLE_NO_INPUT);
        BackToBase();
        NoAdManager.RequestNoAdPurchase((bool success) =>
        {
            if (success)
            {
                noAdButton.gameObject.SetActive(false);
            }
        });
    }
    public void OnSkinShopButton()
    {
        SwtichState(BridgeState.SHOP);
        ShopLoadManager.instance.LoadShop(ShopType.SKIN);
    }
    public void OnRoomShopButton()
    {

        SwtichState(BridgeState.SHOP);
        ShopLoadManager.instance.LoadShop(ShopType.ROOM);
    }
    public void OnLevelListShowButton()
    {
        SwtichState(BridgeState.LEVEL_LOAD);
    }

    public void OnSettingsShowButton()
    {
        SwtichState(BridgeState.SETTINGS);
    }
    public void BackToBase(float delay = 0.1f)
    {
        Centralizer.Add_DelayedAct(() =>
        {
            SwtichState(BridgeState.IDLE);
        }, delay);

    }

}


public enum BridgeState
{
    IDLE_NO_INPUT = -2,
    Leave = -1,
    IDLE = 0,
    LEVEL_LOAD = 1,
    SHOP = 2,
    SETTINGS = 3,
}

