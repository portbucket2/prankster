﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCamBridgeScene : MonoBehaviour
{
    public DragReporter dr;
    public BridgeSceneManager sceneMan;

    public Transform camTrans;
    public Transform right;
    public Transform left;
    public Transform centre;
    void Start()
    {
        normalizationWidth = Screen.width * normalizationRatio;
        dr.onTap += sceneMan.OnTapDetected;
    }


    public float normalizationRatio = 0.25f;
    float normalizationWidth;
    // Update is called once per frame
    void Update()
    {

        if (dr.currentDrag != null && BridgeSceneManager.instance.allowSwipe)
        {
            float x = (dr.currentDrag.travelVec.x) / normalizationWidth;
            //Debug.Log(dr.currentDrag.travelVec);
            if (x > 0)
            {
                x = Mathf.Clamp01(x);
                camTrans.position = Vector3.Lerp(centre.position, right.position, x);
                camTrans.rotation = Quaternion.Lerp(centre.rotation, right.rotation, x);
            }
            else if (x < 0)
            {
                x = Mathf.Clamp01(-x);
                camTrans.position = Vector3.Lerp(centre.position, left.position, x);
                camTrans.rotation = Quaternion.Lerp(centre.rotation, left.rotation, x);
            }
            else
            {

                camTrans.position = Vector3.Lerp(camTrans.position, centre.position, 3* Time.deltaTime);
                camTrans.rotation = Quaternion.Lerp(camTrans.rotation, centre.rotation, 3*Time.deltaTime);
            }

        }
        else
        {


            camTrans.position = Vector3.Lerp(camTrans.position, centre.position, 3 * Time.deltaTime);
            camTrans.rotation = Quaternion.Lerp(camTrans.rotation, centre.rotation, 3 * Time.deltaTime);
        }
    }
}
