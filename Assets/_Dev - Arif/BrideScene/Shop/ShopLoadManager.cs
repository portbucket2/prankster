﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.Ad;

public class ShopLoadManager : MonoBehaviour
{

    public Button closeButton;
    public GameObject rootObject;
    public RectTransform rootRect;

    public static ShopLoadManager instance;
    public SlidePanelController tabPanel;
    public SlidePanelController itemPanel;


    bool adBZ;

    float rootBottomOffset_ad;
    float rootBottomOffset_noad;
    private void Awake()
    {
        rootBottomOffset_ad = rootRect.offsetMin.y;
        rootBottomOffset_noad = 20;
        instance = this;
        closeButton.onClick.AddListener(OnClose);
        rootObject.SetActive(false);
    }

    void OnClose()
    {
        rootObject.SetActive(false);
        BridgeSceneManager.instance.BackToBase();
    }

    public ShopGroupManager skinGroup;
    public ShopGroupManager roomGroup;

    private ShopGroupManager currentShopGroup;

    public void LoadShop(ShopType type)
    {
        rootRect.offsetMin = new Vector2(rootRect.offsetMin.x, NoAdManager.isBlockingAds? rootBottomOffset_noad : rootBottomOffset_ad);
        switch (type)
        {
            case ShopType.SKIN:
                currentShopGroup = skinGroup;
                break;
            case ShopType.ROOM:
                currentShopGroup = roomGroup;
                break;
        }
        rootObject.SetActive(true);
        OnShopCategorySelection(0);
    }

    void OnShopCategorySelection(int tabIndex)
    {
        LoadCategory(tabIndex);
        tabPanel.Load(currentShopGroup.tablist, tabIndex, OnShopCategorySelection, false) ;
    }
    private void LoadCategory(int tabIndex)
    {
        CategoryTab catTab = (currentShopGroup.tablist[tabIndex] as CategoryTab);
        itemPanel.Load(catTab.slideItemList, currentShopGroup.GetChoice(tabIndex) ,(int i) => 
        {
            ItemClick(tabIndex, i);
        }, catTab.defaultIsEmpty, catTab.defaultItem);
    }
    void ItemClick(int tab, int selection)
    {
        CategoryTab catTab = (currentShopGroup.tablist[tab] as CategoryTab);
        SkinType sType = (SkinType)tab;

        if (selection != 0)
        {
            IPurchasable sip = catTab.slideItemList[selection - 1] as IPurchasable;
            if (!sip.unlocked)
            {
                bool adType = sip.cost < 0;
                if (adType)
                {
                    OnAd(() => {
                        sip.unlocked = true;
                        ApplySelection(tab, selection);
                    });
                }
                else
                {
                    if (BasicCoinTracker.CoinBalance >= sip.cost)
                    {
                        sip.unlocked = true;
                        BasicCoinTracker.ChangeCoin(-sip.cost);
                        ApplySelection(tab,selection);
                    }
                }
            }
            else
            {
                ApplySelection(tab, selection);
            }
        }
        else
        {
            ApplySelection(tab, selection);
        }
    }
    void ApplySelection(int tab, int selection)
    {
        currentShopGroup.SetChoice( tab, selection);
        LoadCategory(tab);
    }

    void OnAd(Action onSuccess, Action onFail=null)
    {
        if (adBZ) return;
        else adBZ = true;
        RVPlacement rvPlacement;
        switch (currentShopGroup.shopType)
        {
            default:
            case ShopType.SKIN:
                rvPlacement = RVPlacement.skin_shop_unlock;
                break;
            case ShopType.ROOM:
                rvPlacement = RVPlacement.room_shop_unlock;
                break;
        }
        AnalyticsAssistant.LogRv(RVState.rv_click, rvPlacement);
        bool didShowRV = AdController.ShowRewardedVideoAd((success) =>
        {
            adBZ = false;
            if (success)
            {
                 onSuccess?.Invoke(); 
                AnalyticsAssistant.LogRv(RVState.rv_complete, rvPlacement);
            }
            else
            {
                onFail?.Invoke();
            }
        });
        if(didShowRV) AnalyticsAssistant.LogRv(RVState.rv_start, rvPlacement);

    }
  
}
