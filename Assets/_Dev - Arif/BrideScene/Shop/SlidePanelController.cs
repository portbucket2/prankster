﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using com.faithstudio.Gameplay;
using UnityEngine.UI;

public class SlidePanelController : MonoBehaviour
{

    public GameObject prefab;

    public RectTransform contentDropPoint;

    public List<GameObject> itemUIs;

    public Scrollbar scrollbar;


    public void Load(List<ISlidable> slidables, int selected_index, System.Action<int> onItemClick, bool defaultNoBG, Sprite defaultSprite = null)
    {
        for (int i = itemUIs.Count -1 ; i >=0; i--)
        {
            GameObject go = itemUIs[i];
            DestroyImmediate(go);
        }
        itemUIs.Clear();


        for (int i = 0; i <= slidables.Count; i++)
        {
            GameObject go = Instantiate(prefab);
            go.transform.SetParent(contentDropPoint);
            go.transform.localScale = Vector3.one;
            itemUIs.Add(go);
            int index = i;
            bool selected = selected_index == index;
            if (defaultSprite)
            {
                if (i == 0)
                {
                    go.GetComponent<SlideItemUI>().Load(selected, index, defaultSprite,defaultNoBG, onItemClick);
                }
                else
                {
                    go.GetComponent<SlideItemUI>().Load(selected, index, slidables[index-1],false, onItemClick);
                }
            }
            else
            {
                go.GetComponent<SlideItemUI>().Load(selected, index, slidables[index],false, onItemClick);
                if (i == slidables.Count - 1) break;
            }


        }

        Centralizer.Add_DelayedAct( () => {
            scrollbar.value = Mathf.Clamp01( selected_index/(itemUIs.Count-1 + 0.0001f));
        },0);
    }

}
