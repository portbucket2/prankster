﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public interface ISlidable
{
    string title { get; }
    Sprite GetDisplaySprite(bool selected);
}
public interface IPurchasable : ISlidable
{
    int cost { get; }
    bool unlocked { get;set; }

    void RequestToBeSelected();
}
//[System.Serializable]
//public class PurchaseItemProfile : ISlidable
//{

//    string ISlidable.title { get { return title; } }

//    Sprite ISlidable.displaySprite { get { return displaySprite; } }
//}