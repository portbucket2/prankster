﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FRIA/VertexColorControl/AlphaBasedLerpedColor" {
   Properties {
      _CoreColor1 ("InnerCoreColor", Color) = (1,1,1,1) 
	  _CoreColorPower("CorePower",float) = 20
      //_CoreColor2 ("CentreCoreColor", Color) = (1,1,1,1) 
      _SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
      _Shininess ("Shininess", Float) = 10
	  _MainTex ("Texture Image", 2D) = "white" {} 
	  //_NormalFraction("Normal modifier", Range(-1,1)) = 0.01 
	  //_TestColor("Test Color", color)= (1,1,1,0.5)
   }
   SubShader {
      Pass {	
         Tags { "Queue"="Transparent"  "RenderType"="Transparent" "LightMode" = "ForwardBase" } 
            // pass for ambient light and first light source
			   ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha
		 Cull Off
         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag 
 
         #include "UnityCG.cginc"
         uniform float4 _LightColor0; 
            // color of light source (from "Lighting.cginc")
 
         // User-specified properties
         uniform float4 _CoreColor1;
		 uniform float _CoreColorPower;
         //uniform float4 _CoreColor2;
		 uniform float4 _SpecColor; 
         uniform float _Shininess;
		 uniform sampler2D _MainTex;
		// uniform float _NormalFraction;
		 //uniform float4 _TestColor;
		 

         struct vertexInput {
			float4 color : COLOR;
            float4 vertex : POSITION;
            float3 normal : NORMAL;
			float2 uv : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 posWorld : TEXCOORD0;
            float3 normalDir : TEXCOORD1;
			float2 uv : TEXCOORD2;
			float3 viewDirWorld : TEXCOORD3; 
			float3 normalWorld : TEXCOORD4;
			float4 vertColor : TEXCOORD5;
         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;
 
            float4x4 modelMatrix = unity_ObjectToWorld;
            float4x4 modelMatrixInverse = unity_WorldToObject; 
 
            output.posWorld = mul(modelMatrix, input.vertex);
            output.normalDir = normalize(
               mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
            output.pos = UnityObjectToClipPos(input.vertex);
			output.uv = input.uv;

			float4 posWorld = mul(unity_ObjectToWorld, input.vertex);
			output.viewDirWorld = normalize(_WorldSpaceCameraPos - output.posWorld.xyz);
			output.normalWorld = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
			output.vertColor = input.color;
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR
         {
			
			//if( input.vertColor.a < _TestColor.a){discard;}
			//float dotValue = dot(input.viewDirWorld, input.normalWorld);
			//if(dotValue < _NormalFraction){discard;}
			
            float3 normalDirection = normalize(input.normalDir);
 
            float3 viewDirection = normalize(
               _WorldSpaceCameraPos - input.posWorld.xyz);
            float3 lightDirection;
            float attenuation;
 
            if (0.0 == _WorldSpaceLightPos0.w) // directional light?
            {
               attenuation = 1.0; // no attenuation
               lightDirection = normalize(_WorldSpaceLightPos0.xyz);
            } 
            else // point or spot light
            {
               float3 vertexToLightSource = 
                  _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
               float distance = length(vertexToLightSource);
               attenuation = 1.0 / distance; // linear attenuation 
               lightDirection = normalize(vertexToLightSource);
            }
 
            float3 ambientLighting = 
               UNITY_LIGHTMODEL_AMBIENT.rgb ;//* _Color.rgb;
 
            float3 diffuseReflection = 
               attenuation * _LightColor0.rgb //* _Color.rgb
               * max(0.0, dot(normalDirection, lightDirection));
 
            float3 specularReflection;
            if (dot(normalDirection, lightDirection) < 0.0) 
               // light source on the wrong side?
            {
               specularReflection = float3(0.0, 0.0, 0.0); 
                  // no specular reflection
            }
            else // light source on the right side
            {
               specularReflection = attenuation * _LightColor0.rgb 
                  * _SpecColor.rgb * pow(max(0.0, dot(
                  reflect(-lightDirection, normalDirection), 
                  viewDirection)), _Shininess);
            }
			float4 texCol = tex2D(_MainTex, input.uv);	
			texCol.a =1;

			float4 finalCol = float4(ambientLighting + diffuseReflection 
               //+ specularReflection
			   , 1.0)* texCol;
			finalCol.a = input.vertColor.a;
			finalCol = lerp(_CoreColor1,finalCol, pow(input.vertColor.a,_CoreColorPower));

            return finalCol;
         }
 
         ENDCG
      }
   }
   Fallback "Specular"
}