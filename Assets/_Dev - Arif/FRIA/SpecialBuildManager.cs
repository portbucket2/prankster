﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialBuildManager : MonoBehaviour
{
    public BuildType buildType;

    #region settings 
    public static bool enableUIToggle
    {
        get
        {
            if (!instance) return false;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return false;
                    case BuildType.LION_MARKETING:
                        return true;
                }
            }
        }
    }
    public static bool disableCoinReduction
    {
        get
        {
            if (!instance) return false;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return false;
                    case BuildType.LION_MARKETING:
                    case BuildType.LION_QA:
                        return true;
                }
            }
        }
    }
    public static bool allLevelUnlocked
    {
        get
        {
            if (!instance) return false;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return false;
                    case BuildType.LION_MARKETING:
                    case BuildType.LION_QA:
                        return true;
                }
            }
        }
    }
    public static bool allPurchaseUnlocked
    {
        get
        {
            if (!instance) return false;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return false;
                    case BuildType.LION_MARKETING:
                    //case BuildType.LION_QA:
                        return true;
                }
            }
        }
    }
    public static int initialCoinBonus
    {
        get
        {
            if (!instance) return 0;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return 0;
                    case BuildType.LION_MARKETING:
                    case BuildType.LION_QA:
                        return 5000;
                }
            }
        }
    }
    public static bool enableABTestUI
    {
        get
        {
            if (!instance) return false;
            else
            {
                switch (instance.buildType)
                {
                    default:
                        return false;
                    case BuildType.LION_QA:
                        return true;
                }
            }
        }
    }



    #endregion
    public static SpecialBuildManager instance;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            instance.txt1.text = "";
            DontDestroyOnLoad(this.gameObject);
            Log2("initialized");

            if (enableUIToggle)
            {
                uiToggleButton.onClick.AddListener(OnUIToggleButton);
            }
            else
            {
                uiToggleButton.gameObject.SetActive(false);
            }
        }
    }
    public Text txt1;
    public Text txt2;



    public static void Log1(string s)
    {
        if (!instance) return;
        instance.txt1.text += s; 
    }
    public static void Log2(string s)
    {
        if (!instance) return;
        instance.txt2.text = s;
    }


    //public static event Action onUIToggle;
    public static Dictionary<Camera, int> camdic = new Dictionary<Camera, int>();
    public Button uiToggleButton;

    public static bool uiDisabled { get; private set; }
    public void OnUIToggleButton()
    {
        //onUIToggle?.Invoke();
        uiDisabled = !uiDisabled;
    }

    public LayerMask uilayer;
    private void Update()
    {
        if (!enableUIToggle) return;
        Camera[] cams = FindObjectsOfType<Camera>();
        foreach (Camera cam in cams)
        {
            if (!camdic.ContainsKey(cam))
            {
                camdic.Add(cam, cam.cullingMask);
            }
        }

        foreach (var item in camdic)
        {
            if (item.Key == null) camdic.Remove(item.Key);
        }
        foreach (var item in camdic)
        {
            if (!uiDisabled)
            {
                item.Key.cullingMask = item.Value ;
            }
            else
            {
                item.Key.cullingMask = item.Value - (int)uilayer;
            }
        }
    }
    public enum BuildType
    {
        PRODUCTION = 0,
        LION_MARKETING = 10,
        LION_QA = 11,
    }
}

