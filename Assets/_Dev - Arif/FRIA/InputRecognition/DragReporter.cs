﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragReporter : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Vector2> onDrag;
    public event System.Action<Vector3> onTap;
    float minimalPixelCount;

    public static DragReporter instance;
    public Drag currentDrag = null;
    private void Start()
    {
        instance = this;
        minimalPixelCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (currentDrag != null && currentDrag.didntDragFarEnough)
            {
                onTap?.Invoke(Input.mousePosition);
            }
            currentDrag = null;
        }

        if (currentDrag != null)
        {
            Vector2 v = currentDrag.GetDragAmount(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            onDrag?.Invoke(v);
        }
    }
}

public class Drag
{
    Vector2 startPos;
    Vector2 lastPos;
    public Vector2 travelVec { get { return lastPos - startPos; } }
    float startTime;
    float minPixel = 100;

    public bool didntDragFarEnough
    {
        get
        {
            return travelVec.magnitude < minPixel;
        }
    }
    public Drag(Vector2 startPosition, float minPix)
    {
        this.minPixel = minPix;
        startTime = Time.time;
        startPos = startPosition;
        lastPos = startPosition;
    }
    public Vector2 GetDragAmount(Vector2 currentPosition)
    {
        lastPos = currentPosition;
        //Debug.Log(travelVec);

        if (didntDragFarEnough)
        {
            return travelVec;
        }
        else return Vector2.zero;
    }
}
