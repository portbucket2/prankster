﻿using Portbliss;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestABDataChangeUI : MonoBehaviour
{
    public Text typeTitleText;
    public Text valueText;
    public Button nextValueButton;
    public Button prevValueButton;

    int selectionIndex = -1;

    ABManager.HardAB ab;
    string[] values;
    Func<int, string> converter;

    public bool loaded = false;
    public void Load(ABtype type, string[] values,Func<int, string> converter)
    {
        loaded = true;
        this.values = values;
        this.converter = converter;
        ab = ABManager.GetABAccess(type);
        
        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] == ab.GetValue()) selectionIndex =i;
        }


        typeTitleText.text = ab.GetID();
        if (selectionIndex >= 0)
        {
            valueText.text = converter(selectionIndex);
        }
        else
        {
            valueText.text = "server was silent";
        }

        nextValueButton.onClick.RemoveAllListeners();
        nextValueButton.onClick.AddListener(OnNext);

        prevValueButton.onClick.RemoveAllListeners();
        prevValueButton.onClick.AddListener(OnPrev);
    }
    
    void OnNext()
    {
        if (selectionIndex == -1) selectionIndex = 0;
        {
            selectionIndex++;
            if (selectionIndex >= values.Length) selectionIndex = 0;
        }
        valueText.text = converter(selectionIndex);
    }
    void OnPrev()
    {
        if (selectionIndex == -1) selectionIndex = 0;
        else
        {
            selectionIndex--;
            if (selectionIndex < 0) selectionIndex = values.Length - 1;
        }

        valueText.text = converter(selectionIndex);
    }

    public void OnApply()
    {
        if (selectionIndex >= 0)
        {
            ab.ForceSetValue(values[selectionIndex]);
        }
    }
}
