﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRIA
{

    public class QuickSettingsCreator
    {
        [UnityEditor.MenuItem("Assets/Create/QuickSettings")]
        public static void Create()
        {
            QuickSettings so = ScriptableObject.CreateInstance<QuickSettings>();
            UnityEditor.AssetDatabase.CreateAsset(so, "Assets/QuickSettings.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.EditorUtility.FocusProjectWindow();
            UnityEditor.Selection.activeObject = so;
        }
    }
}