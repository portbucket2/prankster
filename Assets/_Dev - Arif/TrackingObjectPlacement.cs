﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DEV_ARIF;
public class TrackingObjectPlacement : MonoBehaviour, IPauseCompatible
{ 
    public Transform trackingObj;


    public Transform rootTrackPoints;
    public float speed;

    public bool pause;
    void IPauseCompatible.SetPause(bool enabled)
    {
        pause = enabled;
    }

    private void Awake()
    {
        int i = 0;
        Transform lastItem=null;
        foreach (Transform item in rootTrackPoints)
        {
            if (i != 0)
            {
                Track trck = new Track();
                trck.startPos = lastItem;
                trck.endPos = item;
                trck.time = (lastItem.position-item.position).magnitude/speed;
                tracks.Add(trck);
            }
            lastItem = item;
            i++;
            if (i < rootTrackPoints.childCount)
            {
            }
        }
    }
    public List<Track> tracks;

    public void ForceToStartOfTrack(int index)
    {
        trackingObj.position = tracks[index].startPos.position;
    }
    public void TrackNow(int index,System.Action onComplete)
    {
        StopAllCoroutines();
        StartCoroutine(StartTrack(tracks[index],onComplete));
    }
    //public void ForcePos()
   IEnumerator StartTrack(Track tr, System.Action onComplete)
    {
        float stime = Time.time;
        trackingObj.position = tr.startPos.position;
        while (Time.time < stime + tr.time)
        {
            if (pause)
            {
                stime += Time.deltaTime;

            }
            else
            {
                float prog = (Time.time - stime) / tr.time;
                trackingObj.position = Vector3.Lerp(tr.startPos.position, tr.endPos.position, prog);
            }
            yield return null;
        }
        trackingObj.position = tr.endPos.position;
        onComplete?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}

[System.Serializable]
public class Track
{
    public Transform startPos;
    public Transform endPos;
    public float time;
}