﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(fileName = "CollectibleManager", menuName = "Collectible", order = 10)]
public class PrankCollectibleManager : ScriptableObject
{
    private static PrankCollectibleManager _instance;
    public static PrankCollectibleManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Resources.Load<PrankCollectibleManager>("CollectibleManager");
                foreach (var item in _instance.profiles)
                {
                    item.unlocked = new FRIA.HardData<bool>(string.Format("CollectibleUnlocked_{0}",item.id),false);
                    item.levelIDs.AddRange( item.levels.Split(','));
//#if UNITY_EDITOR
//                    string s = item.displaySprite.name;
//                    foreach (string lid in item.levelIDs)
//                    {
//                        s = string.Format("{0}_{1}",s,lid);
//                    }
//                    Debug.LogError(s);
//#endif
                }
            }
            return _instance;
        }
    }
    public static bool enableCollectibles
    {
        get
        {
            return true;
//#if UNITY_ANDROID
//            return ABManager.GetValueInt(ABtype.enable_collectibles) == 1;
//#else
//            return false;
//#endif
        }
    }

    
    public List<PrankCollectibleProfile> profiles;


    public static event System.Action<CollectibleType> onPrankCollectibleUnlocked;

    public static PrankCollectibleProfile GetProfile_ByID(CollectibleType id)
    {
        foreach (var item in Instance.profiles)
        {
            if (item.id == id)
            {
                return item;
            }
        }
        return null;
    }
    public static PrankCollectibleProfile GetProfile_ByLevel(string levelID)
    {
        if (!enableCollectibles)
        {
            return null;
        }
        foreach (var item in Instance.profiles)
        {
            if (item.levelIDs.Contains(levelID) && !item.unlocked.value)
            {
                return item;
            }
        }
        return null;
    }
    public static void SetUnlocked(CollectibleType id)
    {
        foreach (var item in Instance.profiles)
        {
            if (item.id == id && !item.unlocked.value)
            {
                item.unlocked.value = true;
                onPrankCollectibleUnlocked?.Invoke(id);
                return;
            }
        }
    }



}

#if UNITY_EDITOR
[CustomEditor(typeof(PrankCollectibleManager))]
public class CollectibleManagerEditor : Editor
{
    public static PrankCollectibleManager currentSelection;
    public override void OnInspectorGUI()
    {
        currentSelection = (PrankCollectibleManager)base.target;
        serializedObject.Update();
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("title"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite_seleceted"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultIsEmpty"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultItem"));
        Show(serializedObject.FindProperty("profiles"), currentSelection.profiles);
        serializedObject.ApplyModifiedProperties();
    }

    public static void Show(SerializedProperty list, List<PrankCollectibleProfile> listRef)
    {
        //List<SkinItemProfile> listRef = sst.tabContents;
        EditorGUILayout.Space();
        EditorGUI.indentLevel += 1;


        //List<int> duplicateCheckList = new List<int>();
        for (int i = 0; i < list.arraySize; i++)
        {
            GUILayout.BeginVertical("Box");
            //SkinItemProfile sp = splu.listup[i];
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Width(30));
            float defWidth = 70;
            float defHeight = 20;
            if (GUILayout.Button("- Remove", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.RemoveAt(i);
            }
            if (GUILayout.Button("+ Add", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.Insert(i + 1, new PrankCollectibleProfile());
            }

            EditorGUILayout.LabelField("", GUILayout.MinWidth(0));
            //EditorGUILayout.LabelField("");
            if (GUILayout.Button("↑", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != 0)
                {
                    PrankCollectibleProfile ld = listRef[i];
                    listRef[i] = listRef[i - 1];
                    listRef[i - 1] = ld;

                }
            }

            if (GUILayout.Button("↓", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != list.arraySize - 1)
                {
                    PrankCollectibleProfile ld = listRef[i];
                    listRef[i] = listRef[i + 1];
                    listRef[i + 1] = ld;
                }
            }

            GUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        if (listRef.Count == 0)
            if (GUILayout.Button("(+)Add"))
            {
                listRef.Insert(listRef.Count, new PrankCollectibleProfile());
            }

        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space();


    }
}
#endif
