﻿using FRIA;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviewPromptManager : MonoBehaviour
{
    public static HardData<bool> reviewPromptPending;

    public const int LEVEL_START_NUMBER_TO_SHOW = 3;
    public float delay;
    // Start is called before the first frame update
    public GameObject root;
    public Button onNoSelected;
    public Button onYesSelected;
    IEnumerator Start()
    {
        root.SetActive(false);
        yield return new WaitForSeconds(delay);
        if(reviewPromptPending==null)  reviewPromptPending = new HardData<bool>("REVIEW_PROMPT_PENDING", true);
        if (reviewPromptPending && LevelManager.SelectedLevelNumber==LEVEL_START_NUMBER_TO_SHOW)
        {
#if UNITY_EDITOR || UNITY_ANDROID
            LoadReviewPrompt();
#elif UNITY_IOS
            UnityEngine.iOS.Device.RequestStoreReview();
#endif
            reviewPromptPending.value = false;
        }
    }

    void LoadReviewPrompt()
    {
        root.SetActive(true);
        onNoSelected.onClick.RemoveAllListeners();
        onNoSelected.onClick.AddListener(() => { root.SetActive(false); });
        onYesSelected.onClick.RemoveAllListeners();
        onYesSelected.onClick.AddListener(() => 
        {
#if UNITY_EDITOR
            Debug.Log("Requested review");
#elif UNITY_ANDROID
            Application.OpenURL("market://details?id=" + Application.identifier);
#endif
            root.SetActive(false); 
        });
    }
}
