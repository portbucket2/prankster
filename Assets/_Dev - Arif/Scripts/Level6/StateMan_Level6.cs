﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DEV_ARIF;
public class StateMan_Level6 : MonoBehaviour, IResetOnAdCallback, IPauseCompatible
{


    public static StateMan_Level6 instance;

    public Animator characterAnim;
    public TrackingObjectPlacement trackingMan;
    public GameObject manholeParticle;
    public GameObject trashBaloonParticle;
    [Range(0, 1)]
    public float snapAmount0;
    //[Range(0, 1)]
    public float fixAmount1;


    //PrankGameplayEventController sceneMan;
    //int endEventIndex;
    bool tapStartAccepted;
    bool waitingTimeRelease;


    private void Awake()
    {
        PrankGameplayEventController.onTapToStart += SetPermitStart;
        PrankGameplayEventController.onAnswerRecieved += OnPhaseAnswer;
    }
    private void OnDestroy()
    {
        PrankGameplayEventController.onTapToStart -= SetPermitStart;
        PrankGameplayEventController.onAnswerRecieved -= OnPhaseAnswer;
    }


    void IPauseCompatible.SetPause(bool enable)
    {
        characterAnim.speed = enable ? 0 : 1;
        trackingMan.pause = enable;
    }
    public void SetAngryCam()//called on anim a couple of times
    {
            switch (PrankGameplayEventController.PhaseNumber)
            {
                case 1:
                    CamSwapController.SetCamera(CamID.angry_1);
                    break;
                case 2:
                    CamSwapController.SetCamera(CamID.angry_2);
                    break;
            }
    }


    void IResetOnAdCallback.ResetToPhase(int phaseIndex)
    {
        tapStartAccepted = true;
        CamSwapController.SetCamera(CamID.main);
        trashBaloonParticle.SetActive(false);
        this.GetComponent<SkinMatController>().SetMat(0);
        switch (phaseIndex)
        {
            case 0:
                this.transform.localPosition -= new Vector3(snapAmount0, 0, 0);
                trackingMan.TrackNow(0, null);
                characterAnim.SetInteger("phase1", 0);
                characterAnim.SetInteger("phase2", 0);
                characterAnim.SetTrigger("reset0");
                break;
            case 1:
                //trackingMan.TrackNow(1, null);
                characterAnim.SetInteger("phase1", 1);
                characterAnim.SetInteger("phase2", 0);
                characterAnim.SetTrigger("reset1");
                this.transform.parent = trackingMan.trackingObj;
                this.transform.localPosition = new Vector3(-0.41925f,0,0);
                //this.transform.position -= new Vector3(fixAmount1, 0, 0);
                trackingMan.ForceToStartOfTrack(1);
                break;
        }

    }
    //bool postReset2 = false;

    private void Start()
    {
        characterAnim.ResetTrigger("reset1");
        characterAnim.ResetTrigger("reset2");
        characterAnim.SetTrigger("reset0");
        characterAnim.SetInteger("phase1", 0);
        characterAnim.SetInteger("phase2", 0);
        trackingMan.TrackNow(0, null);
        instance = this;
        manholeParticle.SetActive(false);
        trashBaloonParticle.SetActive(false);
    }
    public void SetPermitStart()
    {
        if (waitingTimeRelease)
        {
            PauseController.Unpause();
        }
        tapStartAccepted = true;

    }
    public void OnWalk_0_End()
    {
        if (!tapStartAccepted)
        {
            PauseController.Pause();

            waitingTimeRelease = true;
        }
    }


    public void OnWalk_DoubleEvent_Start()
    {
        switch (PrankGameplayEventController.PhaseNumber)
        {
            case 1:
                AskQuestion();
                break;
            case 2:
                break;
        }
    }
    public void OnWalk_DoubleEvent_End()
    {
        switch (PrankGameplayEventController.PhaseNumber)
        {
            case 1:
                this.transform.position += new Vector3(snapAmount0, 0, 0);
                break;
            case 2:
                this.transform.parent = null;
                break;
        }
    }


    public void OnWalk_3_StartMoving()
    {

        CamSwapController.SetCamera(CamID.main);
        trackingMan.TrackNow(1, null);

    }

    public void AskQuestion()// called on anim once
    {
        switch (PrankGameplayEventController.PhaseNumber)
        {
            case 1:
                PrankGameplayEventController.CallForQuestion(.75f,.75f);
                break;
            case 2:
                PrankGameplayEventController.CallForQuestion(.75f,.75f);
                break;
        }
    }
    public void OnPhaseAnswer(int phaseIndex, bool isCorrect)
    {
        characterAnim.SetInteger(string.Format("phase{0}", phaseIndex + 1), isCorrect ? 1 : -1);
        PropAnim_Level6.Entry(phaseIndex+1, isCorrect);
    }

    public void OnActionStart()
    {
        PropAnim_Level6.Action(PrankGameplayEventController.PhaseNumber);
    }



    public void OnEnableManholeParticle_body()
    {
        manholeParticle.SetActive(true);
    }
    public void OnEnableTrashBaloonParticle()
    {
        trashBaloonParticle.SetActive(true);
    }
    public void OnPhaseCompletion(float delay)
    {
        FRIA.Centralizer.Add_DelayedAct(() => 
        { 
            //Debug.Log("C REACHED");
            PrankGameplayEventController.OnPhaseEndReached();
        },delay);        
    }


}