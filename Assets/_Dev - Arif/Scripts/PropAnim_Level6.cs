﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropAnim_Level6 : MonoBehaviour,IResetOnAdCallback
{
    //public static PropAnim_Level6 instance;
    public Animator propAnim1;
    public Animator propAnim2;
    private static Animator anim1;
    private static Animator anim2;
    // Start is called before the first frame update
    private void Awake()
    {
        anim1 = propAnim1;
        anim2 = propAnim2;
    }
    void IResetOnAdCallback.ResetToPhase(int phaseIndex)
    {
        switch (phaseIndex)
        {
            case 0:
                anim1.ResetTrigger("entry_A");
                anim1.ResetTrigger("entry_B");
                anim1.ResetTrigger("action");
                anim1.SetTrigger("reset");
                break;
            case 1:
                anim2.ResetTrigger("entry_A");
                anim2.ResetTrigger("entry_B");
                anim2.ResetTrigger("action");
                anim2.SetTrigger("reset");
                break;
        }
    }

    public static void Entry(int phaseNumber,bool isCorrect)
    {
        switch (phaseNumber)
        {
            case 1:
                anim1.SetTrigger(string.Format("entry_{0}", isCorrect ? "A" : "B"));
                break;
            case 2:
                anim2.SetTrigger(string.Format("entry_{0}", isCorrect ? "A" : "B"));
                break;
        } 
    }
    public static void Action(int phaseNumber)
    {
        switch (phaseNumber)
        {
            case 1:

                anim1.SetTrigger("action");
                break;
            case 2:
                anim2.SetTrigger("action");
                break;
        }
    }



    //public void OnEntryComplete()
    //{
    //    stateMan.SetPause(false);
    //}
}
