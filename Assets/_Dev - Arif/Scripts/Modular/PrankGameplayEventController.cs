﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using FRIA;
namespace DEV_ARIF
{
    public class PrankGameplayEventController : PrankLevelBehaviour
    {

        public static Action onTapToStart;
        public static Action<int,bool> onAnswerRecieved;
        public static Action<int> onResetAccepted;


        public static int PhaseNumber
        {
            get
            {
                return instance.currentPhaseIndex +1; 
            }
        }

        private void Start()
        {
            currentPhaseIndex = 0;
        }



        #region OutGoing
        static float rightDelay;
        static float wrongDelay;
        public static void CallForQuestion(float rightOptionDelay, float wrongOptionDelay)
        {
            rightDelay = rightOptionDelay;
            wrongDelay = wrongOptionDelay;
            UIManager.Instance.RequestQuestionPanel(stepIndex: instance.currentPhaseIndex);
            switch (PhaseNumber)
            {
                case 1:
                    CamSwapController.SetCamera(CamID.prop_1);
                    break;
                case 2:
                    CamSwapController.SetCamera(CamID.prop_2);
                    break;
            }
            PauseController.Pause();
        }


        public static void OnPhaseEndReached()
        {
            bool isLastPhase = instance.currentPhaseIndex == 1;

            instance.ReportPhaseCompletion(instance.currentPhaseIndex +1,  isLastPhase);
            switch (instance.lastOptionCorrectness)
            {
                case Correctness.RIGHT:
                case Correctness.RV:
                    instance.currentPhaseIndex++;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Incoming

        protected override void StartLevel()
        {
            onTapToStart?.Invoke();
        }

        protected override void RightAnswerSelected()
        {
            onAnswerRecieved?.Invoke(currentPhaseIndex,true);

            Centralizer.Add_DelayedMonoAct(this,()=>PauseController.Unpause(),rightDelay);
        }

        protected override void WrongAnswerSelected()
        {
            onAnswerRecieved?.Invoke(currentPhaseIndex, false);
            Centralizer.Add_DelayedMonoAct(this, () => PauseController.Unpause(), wrongDelay);
        }

        protected override void ResetPhase(int phaseIndex)
        {
            //Debug.LogFormat("Phase Reset with Index {0}", phaseIndex);
            if (phaseIndex >= 0)
            {
                PauseController.Unpause();
                MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
                foreach (var item in monos)
                {
                    IResetOnAdCallback ireset = item as IResetOnAdCallback;
                    if (ireset != null)
                    {
                        ireset.ResetToPhase(currentPhaseIndex);
                    }
                }
            }

        }

        #endregion
    }
}

public interface IResetOnAdCallback
{
    void ResetToPhase(int phaseIndex);
}