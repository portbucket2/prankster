﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace DEV_ARIF
{
    public class CamSwapController : MonoBehaviour
    {

        private static CamSwapController instance;
        public bool willSetMainCamAtStart = true;
        public List<CamChoice> cams;
        
        private void Start()
        {
            instance = this;
            if(willSetMainCamAtStart) SetCam(CamID.main);
        }

        public void SetCam(CamID id)
        {
            bool found=false;
            for (int i = 0; i < cams.Count; i++)
            {
                if (cams[i].id == id)
                {
                    found = true;
                    cams[i].vCam.Priority =10;
                }
                else
                {
                    cams[i].vCam.Priority = 1;
                }
            }

            if (!found)
            {
                Debug.LogError("Cam not found!");
            }
        }

        public static void SetCamera(CamID id)
        {
            instance.SetCam(id);
        }
    }
    [System.Serializable]
    public class CamChoice
    {
        public Cinemachine.CinemachineVirtualCamera vCam;
        public CamID id;
    }
    public enum CamID
    {
        main = 0,
        prop_1 = 11,
        prop_2 = 12,
        angry_1 = 21,
        angry_2 = 22,
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(CamChoice))]
    public class CamChoiceEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUIUtility.labelWidth = 0;
            label.text = string.Format("");

            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            float elementHeight = position.height;
            var enabledRect = new Rect(position.x, position.y, 80, elementHeight);
            var numberRect = new Rect(position.x + 80, position.y, position.width - 80, elementHeight);

            EditorGUI.PropertyField(enabledRect, property.FindPropertyRelative("id"), GUIContent.none);
            EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("vCam"), GUIContent.none);

            EditorGUI.indentLevel = indent;


            EditorGUI.EndProperty();

        }

        //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        //{
        //    return base.GetPropertyHeight(property, label) * 2 + 10;
        //}

    }
#endif
}

