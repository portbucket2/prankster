﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DEV_ARIF
{

    public class PauseController : MonoBehaviour
    {
        public static void Pause()
        {
            //Debug.LogFormat("<color='magenta'>Pause</color>");
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IPauseCompatible ipause = item as IPauseCompatible;
                if (ipause != null)
                {
                    ipause.SetPause(true);
                }
            }
        }

        public static void Unpause()
        {
            //Debug.LogFormat("<color='magenta'>Unpause</color>");
            MonoBehaviour[] monos = FindObjectsOfType<MonoBehaviour>();
            foreach (var item in monos)
            {
                IPauseCompatible ipause = item as IPauseCompatible;
                if (ipause != null)
                {
                    ipause.SetPause(false);
                }
            }
        }
    }

    public interface IPauseCompatible
    {
        void SetPause(bool enabled);
    }
}