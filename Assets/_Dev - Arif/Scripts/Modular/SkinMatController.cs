﻿using FRIA;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class SkinMatController : MonoBehaviour
{

    Texture2D textureMain;
    Texture2D texureMapping;
    Texture2D hairTexture;
    Texture2D upperTexture;
    Texture2D lowerTexture;
    Texture2D fleshTexture;

    public bool disableTextureLoading = false;
    public List<Material> mats = new List<Material>();
    
    List<Material> matRefClones;
    List<Material> runningMatList
    {
        get
        {
            if (Application.isPlaying)
            {
                return matRefClones;
            }
            else
            {
                return mats;
            }
        }
    }
    List<SkinnedMeshRenderer> rends;

    private void Start()
    {
        SkinProfileManager.AddListnerToOnSkinChange(OnTextureChoiceChange);
        Init();
        SetMat(0);
    }
    private void Init()
    {
        matRefClones = new List<Material>();
        rends = new List<SkinnedMeshRenderer>();

        if (Application.isPlaying)
        {
            foreach (var item in mats)
            {
                matRefClones.Add(Instantiate(item));
            }
        }
        rends.AddRange(GetComponentsInChildren<SkinnedMeshRenderer>());
        foreach (var item in rends)
        {
            item.updateWhenOffscreen = true;
        }

        OnTextureChoiceChange();
    }

    private void OnDestroy()
    {
        SkinProfileManager.RemoveListnerFromOnSkinChange(OnTextureChoiceChange);
    }


    private void OnTextureChoiceChange()
    {
        textureMain = SkinProfileManager.GetMainTexture();
        texureMapping = SkinProfileManager.GetMapTexture();
        hairTexture = SkinProfileManager.GetSkin(SkinType.hair);
        upperTexture = SkinProfileManager.GetSkin(SkinType.shirt);
        lowerTexture = SkinProfileManager.GetSkin(SkinType.pant);
        fleshTexture = SkinProfileManager.GetSkin(SkinType.flesh);
        if (hairTexture == null) hairTexture = textureMain;
        if (upperTexture == null) upperTexture = textureMain;
        if (lowerTexture == null) lowerTexture = textureMain;
        if (fleshTexture == null) fleshTexture = textureMain;

        if (!disableTextureLoading)
        {
            if (Application.isPlaying)
            {
                foreach (var item in rends)
                {
                    item.material.SetTexture("_MainTexture", textureMain);
                    item.material.SetTexture("_TexMap", texureMapping);
                    item.material.SetTexture("_RTex", hairTexture);
                    item.material.SetTexture("_GTex", upperTexture);
                    item.material.SetTexture("_BTex", lowerTexture);
                    item.material.SetTexture("_ATex", fleshTexture);

                }
            }
            foreach (var item in runningMatList)
            {
                item.SetTexture("_MainTexture", textureMain);
                item.SetTexture("_TexMap", texureMapping);
                item.SetTexture("_RTex", hairTexture);
                item.SetTexture("_GTex", upperTexture);
                item.SetTexture("_BTex", lowerTexture);
                item.SetTexture("_ATex", fleshTexture);
            }
        }


    }

    static HardData<int> currentIndexHD;
    public int currentIndex
    {
        get
        {
            if (currentIndexHD == null) currentIndexHD = new HardData<int>("CURRENT_SELECTED_MATINDEX",0);
            if (currentIndexHD.value >= mats.Count) currentIndexHD.value = 0;
            return currentIndexHD;
        }
        set
        {
            if (currentIndexHD == null) currentIndexHD = new HardData<int>("CURRENT_SELECTED_MATINDEX", 0);
            if (currentIndexHD.value >= mats.Count) currentIndexHD.value = 0;
            currentIndexHD.value = value;
        }
    }
    public void SetMat(int index)
    {
        if (index < 0 || index >= runningMatList.Count)
        {
            Debug.LogError("index out of matlist bounds");
        }
        else
        {
            foreach (var item in rends)
            {
                currentIndex = index;
                item.material = runningMatList[index];
            }
            OnTextureChoiceChange();
        }
    }

    public void NextMat()
    {
        Init();
        if (currentIndex >= mats.Count - 1) SetMat(0);
        else SetMat(currentIndex+1);
    }
    public void PrevMat()
    {
        Init();
        if (currentIndex <= 0) SetMat(mats.Count-1);
        else SetMat(currentIndex -1);
    }

    public void SetMatValue(int index, string vec1name, float value, bool assignNow = false)
    {
        if(assignNow)SetMat(index);
        matRefClones[index].SetFloat(vec1name,value);
        if (currentIndex == index)
        {
            foreach (var item in rends)
            {
                item.material.SetFloat(vec1name,value);
            }
        }
    }

    public float GetMatCurrentValue(int index, string vec1name)
    {
       return matRefClones[index].GetFloat(vec1name);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SkinMatController))]
public class SkinMatControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SkinMatController smc = (SkinMatController)target;
        if (smc.mats.Count > 0)
        {
            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            int i = smc.currentIndex;
            GUILayout.Label(string.Format("CurrentlySelected {0}: {1}", i,  smc.mats[i]?smc.mats[smc.currentIndex].name:"null"));
            if (GUILayout.Button("<="))
            {
                smc.PrevMat();
            }
            if (GUILayout.Button("=>"))
            {
                smc.NextMat();
            }
            EditorGUILayout.EndHorizontal();
        }

    }
}
#endif