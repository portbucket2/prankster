﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueVisual : MonoBehaviour
{
    Vector3[] points;

    public float radius = 1;
    public float gluespeed = 1;

    public Color gluecolor = Color.white;
    public Material glueMAT;
    
    Transform[] spheres;
    Transform[] cylinders;
    Transform tempSphere1;
    Transform tempSphere2;
    Transform tempcylinder;

    public Transform[] gluepointtransforms;
    // Start is called before the first frame update

    private void Awake()
    {
        gluepointtransforms = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            gluepointtransforms[i] = transform.GetChild(i);
        }
    }

    void Start()
    {
        if (gluepointtransforms.Length > 1)
        {
            spheres = new Transform[gluepointtransforms.Length];
            cylinders = new Transform[gluepointtransforms.Length - 1];

            generateglue(gluepointtransforms);
        }
        else
        {
            generatepoints();

            spheres = new Transform[points.Length];
            cylinders = new Transform[points.Length - 1];

            Transform[] tnsfms = new Transform[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                tnsfms[i] = new GameObject().transform;
                tnsfms[i].position =transform.TransformPoint( points[i]);
            }
            generateglue(tnsfms);
        }

        

        tempcylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform;
        tempcylinder.GetComponent<MeshRenderer>().material = glueMAT;
        tempSphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
        tempSphere1.GetComponent<MeshRenderer>().material =glueMAT;
        tempSphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
        tempSphere2.GetComponent<MeshRenderer>().material = glueMAT;

        tempcylinder.gameObject.SetActive(false);
        tempSphere1.gameObject.SetActive(false);
        tempSphere2.gameObject.SetActive(false);
    }

    float animateddistance = 0;
    public bool selected;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)&&selected)
        {
            animateddistance += Time.deltaTime * gluespeed;

            if (!animateglue(animateddistance, gluepointtransforms.Length > 1))
            {
                tempcylinder.gameObject.SetActive(false);
                tempSphere1.gameObject.SetActive(false);
                tempSphere2.gameObject.SetActive(false);
            }
        }
        
    }

    public void glueprocess()
    {
        StartCoroutine(startgluing());
    }

    public IEnumerator startgluing()
    {
        animateddistance = 0;
        while (animateglue(animateddistance, gluepointtransforms.Length > 1))
        {
            animateddistance += Time.deltaTime * gluespeed;

            yield return null;
        }

        tempcylinder.gameObject.SetActive(false);
        tempSphere1.gameObject.SetActive(false);
        tempSphere2.gameObject.SetActive(false);
    }

    public bool manualglue(float deltime)
    {
        animateddistance += deltime * gluespeed;

        if (animateglue(animateddistance, gluepointtransforms.Length > 1))
        {
            return true;
        }
        else
        {
            tempcylinder.gameObject.SetActive(false);
            tempSphere1.gameObject.SetActive(false);
            tempSphere2.gameObject.SetActive(false);

            return false;
        }
    }

    void generateglue(Vector3[] allpoints)
    {
        for (int i = 0; i < allpoints.Length; i++)
        {
            spheres[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
            spheres[i].position = allpoints[i];
            spheres[i].localScale = Vector3.one * radius;
            spheres[i].gameObject.SetActive(false);
            spheres[i].SetParent(transform, true);

            if (i != 0)
            {
                cylinders[i-1] = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform;
                cylinders[i - 1].position = (allpoints[i - 1] + allpoints[i]) / 2;
                cylinders[i - 1].up = (allpoints[i] - allpoints[i - 1]).normalized;
                cylinders[i - 1].localScale = new Vector3(radius, (allpoints[i] - allpoints[i - 1]).magnitude / 2, radius);
                cylinders[i - 1].gameObject.SetActive(false);
                cylinders[i - 1].SetParent(transform, true);
            }

        }
    }

    void generateglue(Transform[] allpoints)
    {
        for (int i = 0; i < allpoints.Length; i++)
        {
            spheres[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
            spheres[i].GetComponent<MeshRenderer>().material = glueMAT;
            spheres[i].position = allpoints[i].position;
            spheres[i].localScale = Vector3.one * radius;
            spheres[i].gameObject.SetActive(false);
            spheres[i].SetParent(transform, true);

            if (i != 0)
            {
                cylinders[i - 1] = GameObject.CreatePrimitive(PrimitiveType.Cylinder).transform;
                cylinders[i - 1].GetComponent<MeshRenderer>().material = glueMAT;
                cylinders[i - 1].position = (allpoints[i - 1].position + allpoints[i].position) / 2;
                cylinders[i - 1].up = (allpoints[i].position - allpoints[i - 1].position).normalized;
                cylinders[i - 1].localScale = new Vector3(radius, (allpoints[i].position - allpoints[i - 1].position).magnitude / 2, radius);
                cylinders[i - 1].gameObject.SetActive(false);
                cylinders[i - 1].SetParent(transform, true);
            }

        }
    }

    bool animateglue(float distance, bool usingtransform)
    {
        if (usingtransform)
        {
            int index = gluepointtransforms.Length ;
            float surplus = 0;

            float totaldist = 0;

            for (int i = 0; i < gluepointtransforms.Length; i++)
            {
                if (i < (gluepointtransforms.Length - 1))
                {
                    float prevdist = totaldist;
                    totaldist += (gluepointtransforms[i + 1].position - gluepointtransforms[i].position).magnitude;
                    if (distance <= totaldist)
                    {
                        index = i + 1;
                        surplus = distance - prevdist;
                        break;
                    }
                }
            }

            for (int i = 0; i < gluepointtransforms.Length; i++)
            {
                spheres[i].gameObject.SetActive(i < index);

                if (i != 0)
                {
                    cylinders[i - 1].gameObject.SetActive(i < index);
                }
            }

            if (index < gluepointtransforms.Length)
            {
                Vector3 pos1 = gluepointtransforms[index - 1].position;
                Vector3 pos2 = gluepointtransforms[index - 1].position + (gluepointtransforms[index].position - gluepointtransforms[index - 1].position).normalized * surplus;
                tempSphere1.position = pos1;
                tempSphere1.localScale = Vector3.one * radius;
                tempSphere1.gameObject.SetActive(true);

                tempSphere2.position = pos2;
                tempSphere2.localScale = Vector3.one * radius;
                tempSphere2.gameObject.SetActive(true);

                tempcylinder.position = (pos1 + pos2) / 2;
                tempcylinder.up = (gluepointtransforms[index].position - gluepointtransforms[index - 1].position).normalized;
                tempcylinder.localScale = new Vector3(radius, surplus / 2, radius);
                tempcylinder.gameObject.SetActive(true);
            }

            return index < gluepointtransforms.Length;

        }
        else
        {
            int index =  points.Length;
            float surplus = 0;

            float totaldist = 0;

            for (int i = 0; i < points.Length; i++)
            {
                if (i < (points.Length - 1))
                {
                    float prevdist = totaldist;
                    totaldist += (points[i + 1] - points[i]).magnitude;
                    if (distance <= totaldist)
                    {
                        index = i + 1;
                        surplus = distance - prevdist;
                        break;
                    }
                }
            }

            for (int i = 0; i < points.Length; i++)
            {
                spheres[i].gameObject.SetActive(i < index);

                if (i != 0)
                {
                    cylinders[i - 1].gameObject.SetActive(i < index);
                }
            }

            if (index < points.Length)
            {
                Vector3 pos1 = points[index - 1];
                Vector3 pos2 = points[index - 1] + (points[index] - points[index - 1]).normalized * surplus;
                tempSphere1.position = pos1;
                tempSphere1.localScale = Vector3.one * radius;
                tempSphere1.gameObject.SetActive(true);

                tempSphere2.position = pos2;
                tempSphere2.localScale = Vector3.one * radius;
                tempSphere2.gameObject.SetActive(true);

                tempcylinder.position = (pos1 + pos2) / 2;
                tempcylinder.up = (points[index] - points[index - 1]).normalized;
                tempcylinder.localScale = new Vector3(radius, surplus / 2, radius);
                tempcylinder.gameObject.SetActive(true);
            }

            return index < points.Length;

        }

    }

    public float width = 2;
    public float height = 2;

    public float curvatureRadius = 1;

    void generatepoints()
    {
        if (curvatureRadius > width / 2)
        {
            curvatureRadius = width / 2;
        }

        if (curvatureRadius > height / 2)
        {
            curvatureRadius = height / 2;
        }

        points = new Vector3[41];

        for (int i = 0; i <= 9; i++)
        {
            Vector2 point = new Vector2(-((width / 2) - curvatureRadius), (height / 2) - curvatureRadius)
                + new Vector2(-Mathf.Cos(i * 10 * Mathf.Deg2Rad), Mathf.Sin(i * 10 * Mathf.Deg2Rad))* curvatureRadius;
            points[i] = new Vector3(point.x, point.y, 0);
        }

        for (int i = 0; i <= 9; i++)
        {
            Vector2 point = new Vector2((width / 2) - curvatureRadius, (height / 2) - curvatureRadius)
                + new Vector2(Mathf.Sin(i * 10 * Mathf.Deg2Rad), Mathf.Cos(i * 10 * Mathf.Deg2Rad))* curvatureRadius;
            points[i+10] = new Vector3(point.x, point.y, 0);
        }

        for (int i = 0; i <= 9; i++)
        {
            Vector2 point = new Vector2((width / 2) - curvatureRadius, -((height / 2) - curvatureRadius))
                + new Vector2(Mathf.Cos(i * 10 * Mathf.Deg2Rad), -Mathf.Sin(i * 10 * Mathf.Deg2Rad))* curvatureRadius;
            points[i+20] = new Vector3(point.x, point.y, 0);
        }

        

        for (int i = 0; i <= 9; i++)
        {
            Vector2 point = new Vector2(-((width / 2) - curvatureRadius), -((height / 2) - curvatureRadius))
                + new Vector2(-Mathf.Sin(i * 10 * Mathf.Deg2Rad), -Mathf.Cos(i * 10 * Mathf.Deg2Rad))*curvatureRadius;
            points[i+30] = new Vector3(point.x, point.y, 0);
        }

        points[40] = points[0];
    }

    public void ResetGlue()
    {
        animateddistance = 0;
        for (int i = 0; i < spheres.Length; i++)
        {
            spheres[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < cylinders.Length; i++)
        {
            cylinders[i].gameObject.SetActive(false);
        }
    }

}
