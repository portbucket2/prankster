﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using Portbliss.Ad;

public class SettingsUI : MonoBehaviour
{
    public GameObject root;
    [Header("Window Control")]
    public GameObject settingsWindowGameObject;
    public Button settingsButton;
    public Button closeButton;
    public Button closeButtonOuter;

    [Header("Policy Button")]
    public GameObject policyButtonObject;
    public Button policyButton;



    void Start()
    {
        policyButtonObject.SetActive(false);
        StartCoroutine(CheckGDPR());
        StartCoroutine(CheckUIToggle());
        settingsButton.onClick.AddListener(OnSettingsShowButton);
        closeButton.onClick.AddListener(OnSettingsCloseButton);
        closeButtonOuter.onClick.AddListener(OnSettingsCloseButton);
    }
    IEnumerator CheckUIToggle()
    {
        while (SpecialBuildManager.enableUIToggle)
        {
            root.SetActive(!SpecialBuildManager.uiDisabled);
            yield return null;
        }
    }

    IEnumerator CheckGDPR()
    {
        yield return null;
        while (AdController.gdpr_done == false)
        {
            yield return null;
        }
        if (AdController.instance != null && AdController.isUserEU)
        {
            policyButtonObject.SetActive(true);
            policyButton.onClick.AddListener(OnGDPR_FlowRequest);
        }
    }





    private void OnSettingsShowButton()
    {
        settingsWindowGameObject.SetActive(true);
        BridgeSceneManager.instance.OnSettingsShowButton();

    }
    private void OnSettingsCloseButton()
    {
        settingsWindowGameObject.SetActive(false);
        BridgeSceneManager.instance.BackToBase();
    }
    
    private void OnGDPR_FlowRequest()
    {
        if (GDPRController.isGDPRFlowOnRequestActive) return;
        GDPRController.instance.StartGDPRFromUserRequest();
        //OnSettingsCloseButton();
    }
}
